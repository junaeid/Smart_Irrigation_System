-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: sis
-- ------------------------------------------------------
-- Server version	5.6.35-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cer_area`
--

DROP TABLE IF EXISTS `cer_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_area` (
  `areaID` bigint(20) NOT NULL AUTO_INCREMENT,
  `areaCode` varchar(255) DEFAULT NULL,
  `areaName` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`areaID`),
  UNIQUE KEY `UK_9tdoxvm1jdlbosppwaijocfhb` (`areaName`,`areaCode`),
  KEY `FK_mh2wcmmerw2u1qohuqjfnob9g` (`operatorID`),
  CONSTRAINT `FK_mh2wcmmerw2u1qohuqjfnob9g` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_area`
--

LOCK TABLES `cer_area` WRITE;
/*!40000 ALTER TABLE `cer_area` DISABLE KEYS */;
INSERT INTO `cer_area` VALUES (1,'1','dhanmondi','2017-03-30 11:16:59','','0:0:0:0:0:0:0:1',NULL,'operator',1);
/*!40000 ALTER TABLE `cer_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_cer`
--

DROP TABLE IF EXISTS `cer_cer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_cer` (
  `cerID` bigint(20) NOT NULL AUTO_INCREMENT,
  `cerName` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `establishDate` datetime DEFAULT NULL,
  `imageName` varchar(255) DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL,
  `imageTitle` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `permanentAddress` varchar(255) DEFAULT NULL,
  `presentAddress` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `registrationNumber` varchar(255) DEFAULT NULL,
  `sizeOfImage` varchar(255) DEFAULT NULL,
  `tinCerNumber` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cerID`),
  UNIQUE KEY `UK_2h7iwnjdiswakkr9x4qy3bjmd` (`mobile`,`email`,`tinCerNumber`),
  UNIQUE KEY `UK_ic5ej6hvs5yolbd8n070qaboi` (`email`),
  UNIQUE KEY `UK_4sn9m55qwh92onaap8ngwqjvm` (`mobile`),
  UNIQUE KEY `UK_rpcw0349b9lqfeosinrxlc1ds` (`tinCerNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_cer`
--

LOCK TABLES `cer_cer` WRITE;
/*!40000 ALTER TABLE `cer_cer` DISABLE KEYS */;
INSERT INTO `cer_cer` VALUES (1,'Centre for Energy Research','2017-03-30 11:12:09','cer@gmail.com','','2017-03-09 00:00:00','','','','0:0:0:0:0:0:0:1','016923424','dhanmondi','dhanmondi','N/A','543898','','32590123','admin');
/*!40000 ALTER TABLE `cer_cer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_contactperson`
--

DROP TABLE IF EXISTS `cer_contactperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_contactperson` (
  `contactPersonID` bigint(20) NOT NULL AUTO_INCREMENT,
  `contactPersonName` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `userType` varchar(255) DEFAULT NULL,
  `cerID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `organizationID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`contactPersonID`),
  KEY `FK_lor5qnh7juj0a4ncbwfqynjxy` (`cerID`),
  KEY `FK_etsphhtvp2goyuxantmcawu2h` (`operatorID`),
  KEY `FK_h4sp7jb835fw08kfx9uw5lhmm` (`organizationID`),
  CONSTRAINT `FK_etsphhtvp2goyuxantmcawu2h` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_h4sp7jb835fw08kfx9uw5lhmm` FOREIGN KEY (`organizationID`) REFERENCES `cer_organization` (`organizationID`),
  CONSTRAINT `FK_lor5qnh7juj0a4ncbwfqynjxy` FOREIGN KEY (`cerID`) REFERENCES `cer_cer` (`cerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_contactperson`
--

LOCK TABLES `cer_contactperson` WRITE;
/*!40000 ALTER TABLE `cer_contactperson` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_contactperson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_customer`
--

DROP TABLE IF EXISTS `cer_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_customer` (
  `customerID` bigint(20) NOT NULL AUTO_INCREMENT,
  `birthCertificate` varchar(255) DEFAULT NULL,
  `bloodGroup` varchar(255) DEFAULT NULL,
  `customerName` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `dateOfBirth` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `maritalStatus` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `nationalID` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `vulveNumber` varchar(255) DEFAULT NULL,
  `areaID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`customerID`),
  UNIQUE KEY `UK_6v333iles0rpv1y4jn9w2087x` (`nationalID`,`areaID`),
  KEY `FK_lxydb2022cbif0vkvlfwistao` (`areaID`),
  KEY `FK_4l2yep94u3fpqhmo698k8wrqu` (`operatorID`),
  CONSTRAINT `FK_4l2yep94u3fpqhmo698k8wrqu` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_lxydb2022cbif0vkvlfwistao` FOREIGN KEY (`areaID`) REFERENCES `cer_area` (`areaID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_customer`
--

LOCK TABLES `cer_customer` WRITE;
/*!40000 ALTER TABLE `cer_customer` DISABLE KEYS */;
INSERT INTO `cer_customer` VALUES (1,'096456','O+','huda sir','2017-03-30 11:17:59','1971-01-13 00:00:00','','Male','0:0:0:0:0:0:0:1','Married','46546546','5876843242','bangladeshi','N/A','Islam','operator','45645645',1,1),(2,'34534646','B-','shariar sir','2017-04-01 13:52:35','2017-04-14 00:00:00','','Male','0:0:0:0:0:0:0:1','Married','345345','0045345','bangladeshi','N/A','Islam','operator','2',1,1);
/*!40000 ALTER TABLE `cer_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_field`
--

DROP TABLE IF EXISTS `cer_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_field` (
  `fieldID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldMeasurement` double DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `fieldTypeID` bigint(20) DEFAULT NULL,
  `ipcuID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `soilId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`fieldID`),
  UNIQUE KEY `UK_h05o7f66xvaywsqxij40bqx7x` (`customerID`),
  KEY `FK_b9snj6yj77b0vhaq4bn7u10g` (`fieldTypeID`),
  KEY `FK_2d98tp4bal31hj5q397hdiky8` (`ipcuID`),
  KEY `FK_c61e370itecu9glwlgd6cj7jq` (`operatorID`),
  KEY `FK_m7vg38gxipye1mctm9e5gj7xj` (`soilId`),
  CONSTRAINT `FK_2d98tp4bal31hj5q397hdiky8` FOREIGN KEY (`ipcuID`) REFERENCES `cer_ipcu` (`ipcuID`),
  CONSTRAINT `FK_b9snj6yj77b0vhaq4bn7u10g` FOREIGN KEY (`fieldTypeID`) REFERENCES `cer_fieldtype` (`fieldTypeID`),
  CONSTRAINT `FK_c61e370itecu9glwlgd6cj7jq` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_h05o7f66xvaywsqxij40bqx7x` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`),
  CONSTRAINT `FK_m7vg38gxipye1mctm9e5gj7xj` FOREIGN KEY (`soilId`) REFERENCES `cer_soil` (`soilId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_field`
--

LOCK TABLES `cer_field` WRITE;
/*!40000 ALTER TABLE `cer_field` DISABLE KEYS */;
INSERT INTO `cer_field` VALUES (1,'2017-03-30 11:19:34','',234,'0:0:0:0:0:0:0:1','N/A','operator',1,1,1,1,1),(2,'2017-04-01 13:52:53','',0,'0:0:0:0:0:0:0:1','N/A','operator',2,1,1,1,1);
/*!40000 ALTER TABLE `cer_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_fieldtype`
--

DROP TABLE IF EXISTS `cer_fieldtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_fieldtype` (
  `fieldTypeID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldTypeName` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`fieldTypeID`),
  UNIQUE KEY `UK_q7tsrfbmh88crdnodkf85uy49` (`fieldTypeName`),
  KEY `FK_5gq1wdjess7kugwfd1wa52u7q` (`operatorID`),
  CONSTRAINT `FK_5gq1wdjess7kugwfd1wa52u7q` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_fieldtype`
--

LOCK TABLES `cer_fieldtype` WRITE;
/*!40000 ALTER TABLE `cer_fieldtype` DISABLE KEYS */;
INSERT INTO `cer_fieldtype` VALUES (1,'2017-03-30 11:18:46','','high','0:0:0:0:0:0:0:1',NULL,'operator',NULL);
/*!40000 ALTER TABLE `cer_fieldtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_ipcu`
--

DROP TABLE IF EXISTS `cer_ipcu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_ipcu` (
  `ipcuID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `distanceFromCanel` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `ipcuDepth` double DEFAULT NULL,
  `ipcuWattCapacity` double DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `waterDynamicHead` varchar(255) DEFAULT NULL,
  `areaID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ipcuID`),
  KEY `FK_s9paffro5ucx7593d3e9dx44e` (`areaID`),
  KEY `FK_etr8s3ajkkyqbg162vodsw3le` (`operatorID`),
  CONSTRAINT `FK_etr8s3ajkkyqbg162vodsw3le` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_s9paffro5ucx7593d3e9dx44e` FOREIGN KEY (`areaID`) REFERENCES `cer_area` (`areaID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_ipcu`
--

LOCK TABLES `cer_ipcu` WRITE;
/*!40000 ALTER TABLE `cer_ipcu` DISABLE KEYS */;
INSERT INTO `cer_ipcu` VALUES (1,'2017-03-30 11:18:36','185','','0:0:0:0:0:0:0:1',140,120,'N/A','operator','54',1,1);
/*!40000 ALTER TABLE `cer_ipcu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_operator`
--

DROP TABLE IF EXISTS `cer_operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_operator` (
  `operatorID` bigint(20) NOT NULL AUTO_INCREMENT,
  `birthCerNumber` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `establishDate` datetime DEFAULT NULL,
  `imageName` varchar(255) DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL,
  `imageTitle` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `operatorName` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `permanentAddress` varchar(255) DEFAULT NULL,
  `presentAddress` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `registrationNumber` varchar(255) DEFAULT NULL,
  `sizeOfImage` varchar(255) DEFAULT NULL,
  `tinCerNumber` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `voterID` varchar(255) DEFAULT NULL,
  `organizationID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`operatorID`),
  UNIQUE KEY `UK_lf2fm2g7o15l42tql6vlnwi0f` (`mobile`,`email`,`tinCerNumber`),
  UNIQUE KEY `UK_m3ac5wywra26bggk4uff1usx5` (`email`),
  UNIQUE KEY `UK_amoh9wqvq36jvs7p24qaq5kx4` (`mobile`),
  UNIQUE KEY `UK_pnw3kbol08g5m8hy5t2nyyols` (`tinCerNumber`),
  KEY `FK_a5kwgr2t3u9rcehhkmb43ekp7` (`organizationID`),
  CONSTRAINT `FK_a5kwgr2t3u9rcehhkmb43ekp7` FOREIGN KEY (`organizationID`) REFERENCES `cer_organization` (`organizationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_operator`
--

LOCK TABLES `cer_operator` WRITE;
/*!40000 ALTER TABLE `cer_operator` DISABLE KEYS */;
INSERT INTO `cer_operator` VALUES (1,NULL,'2017-03-30 11:15:55','rahimafrooz@gmail.com','',NULL,NULL,NULL,NULL,'0:0:0:0:0:0:0:1','03539785345','rahimafrooz',NULL,'unknown',NULL,'N/A','457655675',NULL,NULL,'idcol',NULL,1);
/*!40000 ALTER TABLE `cer_operator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_organization`
--

DROP TABLE IF EXISTS `cer_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_organization` (
  `organizationID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `establishDate` datetime DEFAULT NULL,
  `imageName` varchar(255) DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL,
  `imageTitle` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `organizationName` varchar(255) DEFAULT NULL,
  `permanentAddress` varchar(255) DEFAULT NULL,
  `presentAddress` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `registrationNumber` varchar(255) DEFAULT NULL,
  `sizeOfImage` varchar(255) DEFAULT NULL,
  `tinCerNumber` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `cerID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`organizationID`),
  UNIQUE KEY `UK_c6rm75pid25e1j09sesk2s60b` (`mobile`,`email`),
  KEY `FK_kufvck1f7xd8oo6qdx40u7ke3` (`cerID`),
  CONSTRAINT `FK_kufvck1f7xd8oo6qdx40u7ke3` FOREIGN KEY (`cerID`) REFERENCES `cer_cer` (`cerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_organization`
--

LOCK TABLES `cer_organization` WRITE;
/*!40000 ALTER TABLE `cer_organization` DISABLE KEYS */;
INSERT INTO `cer_organization` VALUES (1,'2017-03-30 11:14:26','idcol@gmail.com','','2017-03-09 00:00:00','','','','0:0:0:0:0:0:0:1','0169234243','Infrastructure Development Company Limited ','bashundhara','bashundhara','N/A','567234','','75653453','cer',1);
/*!40000 ALTER TABLE `cer_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_paymentsummary`
--

DROP TABLE IF EXISTS `cer_paymentsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_paymentsummary` (
  `paymentSummaryId` bigint(20) NOT NULL AUTO_INCREMENT,
  `balanceAmount` double DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `totalDepositeAmount` double DEFAULT NULL,
  `totalExpenseAmount` double DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`paymentSummaryId`),
  KEY `FK_q7ydyximywkwsy92mgo8h5132` (`customerID`),
  KEY `FK_fp0315wtmsgolr2b9qrfbuxku` (`operatorID`),
  CONSTRAINT `FK_fp0315wtmsgolr2b9qrfbuxku` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_q7ydyximywkwsy92mgo8h5132` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_paymentsummary`
--

LOCK TABLES `cer_paymentsummary` WRITE;
/*!40000 ALTER TABLE `cer_paymentsummary` DISABLE KEYS */;
INSERT INTO `cer_paymentsummary` VALUES (1,0,'2017-03-30 11:19:59','','0:0:0:0:0:0:0:1','N/A',10120,10120,'operator',1,1),(2,0,'2017-04-01 13:53:32','','0:0:0:0:0:0:0:1','N/A',1500,1500,'operator',2,1);
/*!40000 ALTER TABLE `cer_paymentsummary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_paymenttype`
--

DROP TABLE IF EXISTS `cer_paymenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_paymenttype` (
  `paymentTypeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `paymentTypeName` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`paymentTypeId`),
  KEY `FK_2u6438j2ve0q2snq68ecxu86` (`customerID`),
  CONSTRAINT `FK_2u6438j2ve0q2snq68ecxu86` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_paymenttype`
--

LOCK TABLES `cer_paymenttype` WRITE;
/*!40000 ALTER TABLE `cer_paymenttype` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_paymenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_requestinformation`
--

DROP TABLE IF EXISTS `cer_requestinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_requestinformation` (
  `requestInformationID` bigint(20) NOT NULL AUTO_INCREMENT,
  `season` int(11) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `deviceStatus` int(11) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldID` int(11) DEFAULT NULL,
  `fieldValve` int(11) DEFAULT NULL,
  `flowRate` int(11) DEFAULT NULL,
  `humidity` int(11) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mainValve` int(11) DEFAULT NULL,
  `motoIPCU` int(11) DEFAULT NULL,
  `nodeID` int(11) DEFAULT NULL,
  `operatorID` int(11) DEFAULT NULL,
  `pumpID` int(11) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `soilMoisture` int(11) DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `waterCredit` int(11) DEFAULT NULL,
  `waterLevelSensor` int(11) DEFAULT NULL,
  PRIMARY KEY (`requestInformationID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_requestinformation`
--

LOCK TABLES `cer_requestinformation` WRITE;
/*!40000 ALTER TABLE `cer_requestinformation` DISABLE KEYS */;
INSERT INTO `cer_requestinformation` VALUES (1,1,'2017-04-04 19:00:12',11,'',2,1,0,43,'0:0:0:0:0:0:0:1',0,0,2,1,1,NULL,231,27,'operator',1,0,1),(2,0,'2017-04-04 19:00:37',11,'',1,1,0,43,'0:0:0:0:0:0:0:1',0,0,2,1,1,NULL,341,27,'operator',1,0,84),(3,1,'2017-04-04 19:00:44',1,'',2,1,0,43,'0:0:0:0:0:0:0:1',0,0,1,1,1,NULL,0,27,'operator',1,0,0),(4,0,'2017-04-04 19:00:57',1,'',1,0,0,43,'0:0:0:0:0:0:0:1',0,0,1,1,1,NULL,63,27,'operator',1,0,13);
/*!40000 ALTER TABLE `cer_requestinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_requestinformationlog`
--

DROP TABLE IF EXISTS `cer_requestinformationlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_requestinformationlog` (
  `requestInformationLogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `deviceStatus` int(11) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldValve` int(11) DEFAULT NULL,
  `flowRate` int(11) DEFAULT NULL,
  `humidity` int(11) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mainValve` int(11) DEFAULT NULL,
  `motoIPCU` int(11) DEFAULT NULL,
  `nodeID` int(11) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `season` int(11) DEFAULT NULL,
  `soilMoisture` int(11) DEFAULT NULL,
  `temperature` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `waterCredit` int(11) DEFAULT NULL,
  `waterLevelSensor` int(11) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `fieldID` bigint(20) DEFAULT NULL,
  `ipcuID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`requestInformationLogID`),
  KEY `FK_dmplw744olr0ebyyqb44o1tqn` (`customerID`),
  KEY `FK_t744ni5dxgvvl0ifyegs05ga7` (`fieldID`),
  KEY `FK_2ajufj0ba1oy9acf4d8x106xo` (`ipcuID`),
  KEY `FK_6lwe7qwk7wuw2gf70k48dmn89` (`operatorID`),
  CONSTRAINT `FK_2ajufj0ba1oy9acf4d8x106xo` FOREIGN KEY (`ipcuID`) REFERENCES `cer_ipcu` (`ipcuID`),
  CONSTRAINT `FK_6lwe7qwk7wuw2gf70k48dmn89` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_dmplw744olr0ebyyqb44o1tqn` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`),
  CONSTRAINT `FK_t744ni5dxgvvl0ifyegs05ga7` FOREIGN KEY (`fieldID`) REFERENCES `cer_field` (`fieldID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_requestinformationlog`
--

LOCK TABLES `cer_requestinformationlog` WRITE;
/*!40000 ALTER TABLE `cer_requestinformationlog` DISABLE KEYS */;
INSERT INTO `cer_requestinformationlog` VALUES (1,'2017-04-04 19:00:12',1,'',1,0,43,'0:0:0:0:0:0:0:1',0,0,1,'N/A',1,0,27,'operator',0,0,1,2,1,1),(2,'2017-04-04 19:00:37',1,'',0,0,43,'0:0:0:0:0:0:0:1',0,0,1,'N/A',0,63,27,'operator',0,13,1,1,1,1);
/*!40000 ALTER TABLE `cer_requestinformationlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_responseinformation`
--

DROP TABLE IF EXISTS `cer_responseinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_responseinformation` (
  `responseId` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance` double DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldID` int(11) DEFAULT NULL,
  `fieldVulve` int(11) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mainVulve` int(11) DEFAULT NULL,
  `nodeId` int(11) DEFAULT NULL,
  `operatorID` int(11) DEFAULT NULL,
  `paymentSummaryId` bigint(20) DEFAULT NULL,
  `pumpID` int(11) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `sensorStatus` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`responseId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_responseinformation`
--

LOCK TABLES `cer_responseinformation` WRITE;
/*!40000 ALTER TABLE `cer_responseinformation` DISABLE KEYS */;
INSERT INTO `cer_responseinformation` VALUES (1,500,'2017-04-04 19:00:12','',2,1,'0:0:0:0:0:0:0:1',0,2,1,1,1,NULL,11,'operator',1),(2,500,'2017-04-04 19:00:37','',1,0,'0:0:0:0:0:0:0:1',0,2,1,1,1,NULL,11,'operator',1),(3,500,'2017-04-04 19:00:44','',2,1,'0:0:0:0:0:0:0:1',0,1,1,1,1,NULL,1,'operator',1),(4,500,'2017-04-04 19:00:57','',1,1,'0:0:0:0:0:0:0:1',1,1,1,1,1,NULL,1,'operator',1);
/*!40000 ALTER TABLE `cer_responseinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_responseinformationlog`
--

DROP TABLE IF EXISTS `cer_responseinformationlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_responseinformationlog` (
  `responseId` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance` double DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldValve` int(11) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `mainValve` int(11) DEFAULT NULL,
  `nodeId` int(11) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `sensorStatus` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `fieldID` bigint(20) DEFAULT NULL,
  `ipcuID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`responseId`),
  KEY `FK_f50xcbw7uwa6mwcw0lgxjtn7u` (`customerID`),
  KEY `FK_4spy0eor696jmja4spk6rhleg` (`fieldID`),
  KEY `FK_8o1qvca2sdiutmaiqh473d552` (`ipcuID`),
  KEY `FK_2xnf071rdnd17h59iwmhschh6` (`operatorID`),
  CONSTRAINT `FK_2xnf071rdnd17h59iwmhschh6` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_4spy0eor696jmja4spk6rhleg` FOREIGN KEY (`fieldID`) REFERENCES `cer_field` (`fieldID`),
  CONSTRAINT `FK_8o1qvca2sdiutmaiqh473d552` FOREIGN KEY (`ipcuID`) REFERENCES `cer_ipcu` (`ipcuID`),
  CONSTRAINT `FK_f50xcbw7uwa6mwcw0lgxjtn7u` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_responseinformationlog`
--

LOCK TABLES `cer_responseinformationlog` WRITE;
/*!40000 ALTER TABLE `cer_responseinformationlog` DISABLE KEYS */;
INSERT INTO `cer_responseinformationlog` VALUES (1,500,'2017-04-04 19:00:12','',1,'0:0:0:0:0:0:0:1',0,2,'N/A',11,'operator',1,2,1,1),(2,500,'2017-04-04 19:00:37','',0,'0:0:0:0:0:0:0:1',0,2,'N/A',11,'operator',1,1,1,1);
/*!40000 ALTER TABLE `cer_responseinformationlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sensor`
--

DROP TABLE IF EXISTS `cer_sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sensor` (
  `sensorID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `sensorCode` varchar(255) DEFAULT NULL,
  `sensorName` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `ipcuID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`sensorID`),
  UNIQUE KEY `UK_98toy9peolspbeh0gdtxu74kc` (`sensorCode`,`ipcuID`),
  KEY `FK_rki46twypod9ea4b2x0eimjlq` (`ipcuID`),
  KEY `FK_mjqahwdb81prfbtrianaoowby` (`operatorID`),
  CONSTRAINT `FK_mjqahwdb81prfbtrianaoowby` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_rki46twypod9ea4b2x0eimjlq` FOREIGN KEY (`ipcuID`) REFERENCES `cer_ipcu` (`ipcuID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sensor`
--

LOCK TABLES `cer_sensor` WRITE;
/*!40000 ALTER TABLE `cer_sensor` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sms`
--

DROP TABLE IF EXISTS `cer_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sms` (
  `smsID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `smsBody` varchar(255) DEFAULT NULL,
  `smsLength` double DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`smsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sms`
--

LOCK TABLES `cer_sms` WRITE;
/*!40000 ALTER TABLE `cer_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sms_balance`
--

DROP TABLE IF EXISTS `cer_sms_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sms_balance` (
  `smsBalanceID` bigint(20) NOT NULL AUTO_INCREMENT,
  `activeStatus` int(11) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `dueAmount` double DEFAULT NULL,
  `expireDate` date DEFAULT NULL,
  `expireMonth` varchar(255) DEFAULT NULL,
  `expireYear` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `paidAmount` double DEFAULT NULL,
  `paybleAmount` double DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `smsBalance` bigint(20) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`smsBalanceID`),
  KEY `FK_44o8k3x1k4xaejwxdk3qi1y3v` (`operatorID`),
  CONSTRAINT `FK_44o8k3x1k4xaejwxdk3qi1y3v` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sms_balance`
--

LOCK TABLES `cer_sms_balance` WRITE;
/*!40000 ALTER TABLE `cer_sms_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sms_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sms_logdetails`
--

DROP TABLE IF EXISTS `cer_sms_logdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sms_logdetails` (
  `smsLogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `customerID` bigint(20) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `fromNumber` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `operatorName` varchar(255) DEFAULT NULL,
  `phnBkID` bigint(20) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `sendDate` datetime DEFAULT NULL,
  `sendMonth` varchar(255) DEFAULT NULL,
  `sendToUser` varchar(255) DEFAULT NULL,
  `sendYear` varchar(255) DEFAULT NULL,
  `smsBody` varchar(255) DEFAULT NULL,
  `smsBodySize` int(11) DEFAULT NULL,
  `smsCount` int(11) DEFAULT NULL,
  `smsType` varchar(255) DEFAULT NULL,
  `successfulDate` date DEFAULT NULL,
  `successfulMonth` varchar(255) DEFAULT NULL,
  `successfulStatus` int(11) DEFAULT NULL,
  `sucessfulYear` varchar(255) DEFAULT NULL,
  `toNumber` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `logSummaryID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`smsLogID`),
  KEY `FK_5v9h99h8mwq0c4hipqs3i8enl` (`logSummaryID`),
  CONSTRAINT `FK_5v9h99h8mwq0c4hipqs3i8enl` FOREIGN KEY (`logSummaryID`) REFERENCES `cer_sms_logsummary` (`logSummaryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sms_logdetails`
--

LOCK TABLES `cer_sms_logdetails` WRITE;
/*!40000 ALTER TABLE `cer_sms_logdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sms_logdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sms_logsummary`
--

DROP TABLE IF EXISTS `cer_sms_logsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sms_logsummary` (
  `logSummaryID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `numberOfSMS` int(11) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `sendDate` datetime DEFAULT NULL,
  `sendMonth` varchar(255) DEFAULT NULL,
  `sendYear` varchar(255) DEFAULT NULL,
  `smsBodySize` int(11) DEFAULT NULL,
  `smsCategory` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`logSummaryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sms_logsummary`
--

LOCK TABLES `cer_sms_logsummary` WRITE;
/*!40000 ALTER TABLE `cer_sms_logsummary` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sms_logsummary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sms_phonbk`
--

DROP TABLE IF EXISTS `cer_sms_phonbk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sms_phonbk` (
  `phnBkID` bigint(20) NOT NULL,
  `contactName` varchar(255) DEFAULT NULL,
  `contactNote` varchar(255) DEFAULT NULL,
  `contactNumber` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `phnBkCategoryID` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`phnBkID`),
  UNIQUE KEY `UK_5oblww0qnwp47enc2xh9wm2m3` (`contactName`,`phnBkCategoryID`,`operatorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sms_phonbk`
--

LOCK TABLES `cer_sms_phonbk` WRITE;
/*!40000 ALTER TABLE `cer_sms_phonbk` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sms_phonbk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_sms_phonbk_category`
--

DROP TABLE IF EXISTS `cer_sms_phonbk_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_sms_phonbk_category` (
  `phnBkCategoryID` varchar(255) NOT NULL,
  `categoryNote` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `phnBkCategoryName` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`phnBkCategoryID`),
  UNIQUE KEY `UK_l52tgtkpg40uyfueivtx1c0bx` (`phnBkCategoryName`,`operatorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_sms_phonbk_category`
--

LOCK TABLES `cer_sms_phonbk_category` WRITE;
/*!40000 ALTER TABLE `cer_sms_phonbk_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_sms_phonbk_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_smsfrom`
--

DROP TABLE IF EXISTS `cer_smsfrom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_smsfrom` (
  `smsFromID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `smsFromMobile` varchar(255) DEFAULT NULL,
  `smsFromName` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`smsFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_smsfrom`
--

LOCK TABLES `cer_smsfrom` WRITE;
/*!40000 ALTER TABLE `cer_smsfrom` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_smsfrom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_smstemplate`
--

DROP TABLE IF EXISTS `cer_smstemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_smstemplate` (
  `smsTemplateID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `smsTemplateType` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`smsTemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_smstemplate`
--

LOCK TABLES `cer_smstemplate` WRITE;
/*!40000 ALTER TABLE `cer_smstemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_smstemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_smsto`
--

DROP TABLE IF EXISTS `cer_smsto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_smsto` (
  `smsToID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `smsToMobile` varchar(255) DEFAULT NULL,
  `smsToName` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`smsToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_smsto`
--

LOCK TABLES `cer_smsto` WRITE;
/*!40000 ALTER TABLE `cer_smsto` DISABLE KEYS */;
/*!40000 ALTER TABLE `cer_smsto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_soil`
--

DROP TABLE IF EXISTS `cer_soil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_soil` (
  `soilId` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldDescription` varchar(255) DEFAULT NULL,
  `geographicalInformation` varchar(255) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `soilType` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`soilId`),
  KEY `FK_bo1u3k41667783ryhyro1xmq1` (`operatorID`),
  CONSTRAINT `FK_bo1u3k41667783ryhyro1xmq1` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_soil`
--

LOCK TABLES `cer_soil` WRITE;
/*!40000 ALTER TABLE `cer_soil` DISABLE KEYS */;
INSERT INTO `cer_soil` VALUES (1,'2017-03-30 11:19:14','','12432','345','0:0:0:0:0:0:0:1',NULL,'clay','operator',1);
/*!40000 ALTER TABLE `cer_soil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_transaction_log`
--

DROP TABLE IF EXISTS `cer_transaction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_transaction_log` (
  `transationLogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `creditAmount` double DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `debitAmount` double DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `paymentDate` datetime DEFAULT NULL,
  `paymentTypeName` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`transationLogID`),
  KEY `FK_2v44o3qdovq76ikpsx46yxptk` (`customerID`),
  KEY `FK_eg4o2stqwphxja171qeon8po6` (`operatorID`),
  CONSTRAINT `FK_2v44o3qdovq76ikpsx46yxptk` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`),
  CONSTRAINT `FK_eg4o2stqwphxja171qeon8po6` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_transaction_log`
--

LOCK TABLES `cer_transaction_log` WRITE;
/*!40000 ALTER TABLE `cer_transaction_log` DISABLE KEYS */;
INSERT INTO `cer_transaction_log` VALUES (1,0,'2017-03-30 11:19:59',120,'','0:0:0:0:0:0:0:1','March','2017-03-30 11:19:59','bKash','N/A','operator','2017',1,1),(2,0,'2017-04-01 13:34:24',10000,'','0:0:0:0:0:0:0:1','April','2017-04-01 13:34:24','bKash','N/A','operator','2017',1,1),(3,0,'2017-04-01 13:53:32',1500,'','0:0:0:0:0:0:0:1','April','2017-04-01 13:53:32','bKash','N/A','operator','2017',2,1);
/*!40000 ALTER TABLE `cer_transaction_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_watercreditlog`
--

DROP TABLE IF EXISTS `cer_watercreditlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_watercreditlog` (
  `waterCreditID` bigint(20) NOT NULL AUTO_INCREMENT,
  `availableWaterCredit` int(11) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `fieldID` bigint(20) NOT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `ipcuID` bigint(20) NOT NULL,
  `month` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `usedDate` datetime DEFAULT NULL,
  `usedWaterCredit` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`waterCreditID`),
  KEY `FK_39kegj3b356ao6bu4d0279rbj` (`customerID`),
  KEY `FK_mgwkihgxs2tnqswa6ymepf1k3` (`operatorID`),
  CONSTRAINT `FK_39kegj3b356ao6bu4d0279rbj` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`),
  CONSTRAINT `FK_mgwkihgxs2tnqswa6ymepf1k3` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_watercreditlog`
--

LOCK TABLES `cer_watercreditlog` WRITE;
/*!40000 ALTER TABLE `cer_watercreditlog` DISABLE KEYS */;
INSERT INTO `cer_watercreditlog` VALUES (1,12,'2017-03-30 11:19:59','',0,'0:0:0:0:0:0:0:1',0,'March','N/A','2017-03-30 11:19:59',0,'operator','2017',1,1),(2,1000,'2017-04-01 13:34:24','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:34:24',0,'operator','2017',1,1),(3,150,'2017-04-01 13:53:32','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:53:32',0,'operator','2017',2,1),(4,1012,'2017-04-01 13:55:04','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:55:04',0,'N/A','2017',1,1),(5,1012,'2017-04-01 13:55:20','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:55:20',0,'N/A','2017',1,1),(6,1012,'2017-04-01 13:55:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:55:28',0,'N/A','2017',1,1),(7,1012,'2017-04-01 13:56:13','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:56:13',0,'N/A','2017',1,1),(8,1012,'2017-04-01 13:56:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:56:28',0,'N/A','2017',1,1),(9,1012,'2017-04-01 13:56:38','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:56:38',0,'N/A','2017',1,1),(10,1012,'2017-04-01 13:56:45','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:56:45',0,'N/A','2017',1,1),(11,1012,'2017-04-01 13:57:43','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:57:43',0,'N/A','2017',1,1),(12,1012,'2017-04-01 13:57:52','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:57:52',0,'N/A','2017',1,1),(13,1012,'2017-04-01 13:58:25','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:58:25',0,'N/A','2017',1,1),(14,1012,'2017-04-01 13:58:37','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:58:37',0,'N/A','2017',1,1),(15,1012,'2017-04-01 13:58:45','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:58:45',0,'N/A','2017',1,1),(16,1012,'2017-04-01 13:58:53','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:58:53',0,'N/A','2017',1,1),(17,1012,'2017-04-01 13:59:15','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:59:15',0,'N/A','2017',1,1),(18,1012,'2017-04-01 13:59:47','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:59:47',0,'N/A','2017',1,1),(19,1012,'2017-04-01 13:59:59','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 13:59:59',0,'N/A','2017',1,1),(20,1012,'2017-04-01 14:00:07','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:00:07',0,'N/A','2017',1,1),(21,1012,'2017-04-01 14:00:25','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:00:25',0,'N/A','2017',1,1),(22,1012,'2017-04-01 14:00:54','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:00:54',0,'N/A','2017',1,1),(23,1012,'2017-04-01 14:01:23','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:01:23',0,'N/A','2017',1,1),(24,1012,'2017-04-01 14:01:34','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:01:34',0,'N/A','2017',1,1),(25,1012,'2017-04-01 14:02:03','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:02:03',0,'N/A','2017',1,1),(26,1012,'2017-04-01 14:02:10','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:02:10',0,'N/A','2017',1,1),(27,1012,'2017-04-01 14:02:23','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:02:23',0,'N/A','2017',1,1),(28,1012,'2017-04-01 14:02:32','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:02:32',0,'N/A','2017',1,1),(29,1012,'2017-04-01 14:03:41','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:03:41',0,'N/A','2017',1,1),(30,1012,'2017-04-01 14:03:46','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:03:46',0,'N/A','2017',1,1),(31,1012,'2017-04-01 14:03:54','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:03:54',0,'N/A','2017',1,1),(32,1012,'2017-04-01 14:04:58','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:04:58',0,'N/A','2017',1,1),(33,1012,'2017-04-01 14:05:25','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:05:25',0,'N/A','2017',1,1),(34,1012,'2017-04-01 14:05:34','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:05:34',0,'N/A','2017',1,1),(35,1012,'2017-04-01 14:05:41','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:05:41',0,'N/A','2017',1,1),(36,1012,'2017-04-01 14:05:55','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:05:55',0,'N/A','2017',1,1),(37,1012,'2017-04-01 14:06:26','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:06:26',0,'N/A','2017',1,1),(38,1012,'2017-04-01 14:06:41','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:06:41',0,'N/A','2017',1,1),(39,1012,'2017-04-01 14:06:55','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:06:55',0,'N/A','2017',1,1),(40,1012,'2017-04-01 14:07:08','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:07:08',0,'N/A','2017',1,1),(41,1012,'2017-04-01 14:07:37','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:07:37',0,'N/A','2017',1,1),(42,1012,'2017-04-01 14:07:46','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:07:46',0,'N/A','2017',1,1),(43,1012,'2017-04-01 14:07:56','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:07:56',0,'N/A','2017',1,1),(44,1012,'2017-04-01 14:08:02','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:08:02',0,'N/A','2017',1,1),(45,1012,'2017-04-01 14:11:13','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:11:13',0,'N/A','2017',1,1),(46,1012,'2017-04-01 14:12:04','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:12:04',0,'N/A','2017',1,1),(47,1012,'2017-04-01 14:12:17','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:12:17',0,'N/A','2017',1,1),(48,1012,'2017-04-01 14:12:50','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:12:50',0,'N/A','2017',1,1),(49,1012,'2017-04-01 14:12:59','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:12:59',0,'N/A','2017',1,1),(50,1012,'2017-04-01 14:13:05','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:13:05',0,'N/A','2017',1,1),(51,1012,'2017-04-01 14:13:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:13:28',0,'N/A','2017',1,1),(52,1012,'2017-04-01 14:13:39','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:13:39',0,'N/A','2017',1,1),(53,1012,'2017-04-01 14:14:09','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:14:09',0,'N/A','2017',1,1),(54,1012,'2017-04-01 14:14:32','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:14:32',0,'N/A','2017',1,1),(55,1012,'2017-04-01 14:14:45','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:14:45',0,'N/A','2017',1,1),(56,1012,'2017-04-01 14:15:17','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:15:17',0,'N/A','2017',1,1),(57,1012,'2017-04-01 14:15:36','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:15:36',0,'N/A','2017',1,1),(58,1012,'2017-04-01 14:15:44','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:15:44',0,'N/A','2017',1,1),(59,1012,'2017-04-01 14:15:50','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:15:50',0,'N/A','2017',1,1),(60,1012,'2017-04-01 14:16:49','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:16:49',0,'N/A','2017',1,1),(61,1012,'2017-04-01 14:17:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:17:28',0,'N/A','2017',1,1),(62,1012,'2017-04-01 14:17:51','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:17:51',0,'N/A','2017',1,1),(63,1012,'2017-04-01 14:18:04','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:18:04',0,'N/A','2017',1,1),(64,1012,'2017-04-01 14:19:06','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:19:06',0,'N/A','2017',1,1),(65,1012,'2017-04-01 14:19:14','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:19:14',0,'N/A','2017',1,1),(66,1012,'2017-04-01 14:19:21','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:19:21',0,'N/A','2017',1,1),(67,1012,'2017-04-01 14:19:41','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:19:41',0,'N/A','2017',1,1),(68,1012,'2017-04-01 14:20:03','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:20:03',0,'N/A','2017',1,1),(69,1012,'2017-04-01 14:20:10','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:20:10',0,'N/A','2017',1,1),(70,1012,'2017-04-01 14:20:26','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:20:26',0,'N/A','2017',1,1),(71,1012,'2017-04-01 14:20:48','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:20:48',0,'N/A','2017',1,1),(72,1012,'2017-04-01 14:21:11','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:21:11',0,'N/A','2017',1,1),(73,1012,'2017-04-01 14:21:32','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:21:32',0,'N/A','2017',1,1),(74,1012,'2017-04-01 14:21:51','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:21:51',0,'N/A','2017',1,1),(75,1012,'2017-04-01 14:22:17','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:22:17',0,'N/A','2017',1,1),(76,1012,'2017-04-01 14:22:25','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:22:25',0,'N/A','2017',1,1),(77,1012,'2017-04-01 14:22:37','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:22:37',0,'N/A','2017',1,1),(78,1012,'2017-04-01 14:22:57','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:22:57',0,'N/A','2017',1,1),(79,1012,'2017-04-01 14:23:17','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:23:17',0,'N/A','2017',1,1),(80,1012,'2017-04-01 14:23:25','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:23:25',0,'N/A','2017',1,1),(81,1012,'2017-04-01 14:23:46','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:23:46',0,'N/A','2017',1,1),(82,1012,'2017-04-01 14:24:08','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:24:08',0,'N/A','2017',1,1),(83,1012,'2017-04-01 14:24:33','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:24:33',0,'N/A','2017',1,1),(84,1012,'2017-04-01 14:24:51','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:24:51',0,'N/A','2017',1,1),(85,1012,'2017-04-01 14:25:31','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:25:31',0,'N/A','2017',1,1),(86,1012,'2017-04-01 14:25:40','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:25:40',0,'N/A','2017',1,1),(87,1012,'2017-04-01 14:26:04','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:26:04',0,'N/A','2017',1,1),(88,1012,'2017-04-01 14:26:14','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:26:14',0,'N/A','2017',1,1),(89,1012,'2017-04-01 14:26:36','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:26:36',0,'N/A','2017',1,1),(90,1012,'2017-04-01 14:26:45','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:26:45',0,'N/A','2017',1,1),(91,1012,'2017-04-01 14:27:10','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:27:10',0,'N/A','2017',1,1),(92,1012,'2017-04-01 14:27:23','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:27:23',0,'N/A','2017',1,1),(93,1012,'2017-04-01 14:28:14','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:28:14',0,'N/A','2017',1,1),(94,1012,'2017-04-01 14:28:52','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:28:52',0,'N/A','2017',1,1),(95,1012,'2017-04-01 14:29:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:29:28',0,'N/A','2017',1,1),(96,1012,'2017-04-01 14:29:38','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:29:38',0,'N/A','2017',1,1),(97,1012,'2017-04-01 14:29:50','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:29:50',0,'N/A','2017',1,1),(98,1012,'2017-04-01 14:30:17','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:30:17',0,'N/A','2017',1,1),(99,1012,'2017-04-01 14:30:38','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:30:38',0,'N/A','2017',1,1),(100,1012,'2017-04-01 14:30:46','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:30:46',0,'N/A','2017',1,1),(101,1012,'2017-04-01 14:30:57','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:30:57',0,'N/A','2017',1,1),(102,1012,'2017-04-01 14:31:16','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:31:16',0,'N/A','2017',1,1),(103,1012,'2017-04-01 14:31:42','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:31:42',0,'N/A','2017',1,1),(104,1012,'2017-04-01 14:31:50','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:31:50',0,'N/A','2017',1,1),(105,1012,'2017-04-01 14:32:07','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:32:07',0,'N/A','2017',1,1),(106,1012,'2017-04-01 14:33:00','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:33:00',0,'N/A','2017',1,1),(107,1012,'2017-04-01 14:33:07','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:33:07',0,'N/A','2017',1,1),(108,1012,'2017-04-01 14:33:38','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:33:38',0,'N/A','2017',1,1),(109,1012,'2017-04-01 14:33:58','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:33:58',0,'N/A','2017',1,1),(110,1012,'2017-04-01 14:34:05','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:34:05',0,'N/A','2017',1,1),(111,1012,'2017-04-01 14:34:15','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:34:15',0,'N/A','2017',1,1),(112,1012,'2017-04-01 14:34:34','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:34:34',0,'N/A','2017',1,1),(113,1012,'2017-04-01 14:35:05','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:35:05',0,'N/A','2017',1,1),(114,1012,'2017-04-01 14:35:15','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:35:15',0,'N/A','2017',1,1),(115,1012,'2017-04-01 14:35:29','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:35:29',0,'N/A','2017',1,1),(116,1012,'2017-04-01 14:35:44','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:35:44',0,'N/A','2017',1,1),(117,1012,'2017-04-01 14:36:16','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:36:16',0,'N/A','2017',1,1),(118,1012,'2017-04-01 14:36:27','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:36:27',0,'N/A','2017',1,1),(119,1012,'2017-04-01 14:36:33','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:36:33',0,'N/A','2017',1,1),(120,1012,'2017-04-01 14:36:53','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:36:53',0,'N/A','2017',1,1),(121,1012,'2017-04-01 14:37:27','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-01 14:37:27',0,'N/A','2017',1,1),(122,1012,'2017-04-03 16:21:15','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:21:15',0,'N/A','2017',1,1),(123,1012,'2017-04-03 16:21:22','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:21:22',0,'N/A','2017',1,1),(124,1012,'2017-04-03 16:21:29','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:21:29',0,'N/A','2017',1,1),(125,1012,'2017-04-03 16:21:48','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:21:48',0,'N/A','2017',1,1),(126,1012,'2017-04-03 16:22:19','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:22:19',0,'N/A','2017',1,1),(127,1012,'2017-04-03 16:22:27','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:22:27',0,'N/A','2017',1,1),(128,1012,'2017-04-03 16:22:34','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:22:34',0,'N/A','2017',1,1),(129,1012,'2017-04-03 16:22:53','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:22:53',0,'N/A','2017',1,1),(130,1012,'2017-04-03 16:41:31','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:41:31',0,'N/A','2017',1,1),(131,1012,'2017-04-03 16:41:38','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:41:38',0,'N/A','2017',1,1),(132,1012,'2017-04-03 16:41:45','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:41:45',0,'N/A','2017',1,1),(133,1012,'2017-04-03 16:42:23','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:42:23',0,'N/A','2017',1,1),(134,1012,'2017-04-03 16:42:49','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:42:49',0,'N/A','2017',1,1),(135,1012,'2017-04-03 16:43:40','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:43:40',0,'N/A','2017',1,1),(136,1012,'2017-04-03 16:43:53','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:43:53',0,'N/A','2017',1,1),(137,1012,'2017-04-03 16:43:59','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:43:59',0,'N/A','2017',1,1),(138,1012,'2017-04-03 16:44:31','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:44:31',0,'N/A','2017',1,1),(139,1012,'2017-04-03 16:44:44','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:44:44',0,'N/A','2017',1,1),(140,1012,'2017-04-03 16:45:03','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:45:03',0,'N/A','2017',1,1),(141,1012,'2017-04-03 16:45:10','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:45:10',0,'N/A','2017',1,1),(142,1012,'2017-04-03 16:45:36','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:45:36',0,'N/A','2017',1,1),(143,1012,'2017-04-03 16:46:08','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:46:08',0,'N/A','2017',1,1),(144,1012,'2017-04-03 16:46:15','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:46:15',0,'N/A','2017',1,1),(145,1012,'2017-04-03 16:46:41','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:46:41',0,'N/A','2017',1,1),(146,1012,'2017-04-03 16:47:00','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:47:00',0,'N/A','2017',1,1),(147,1012,'2017-04-03 16:47:19','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:47:19',0,'N/A','2017',1,1),(148,1012,'2017-04-03 16:47:55','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:47:55',0,'N/A','2017',1,1),(149,1012,'2017-04-03 16:48:02','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:48:02',0,'N/A','2017',1,1),(150,1012,'2017-04-03 16:48:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:48:28',0,'N/A','2017',1,1),(151,1012,'2017-04-03 16:48:35','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:48:35',0,'N/A','2017',1,1),(152,1012,'2017-04-03 16:48:54','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:48:54',0,'N/A','2017',1,1),(153,1012,'2017-04-03 16:49:07','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:49:07',0,'N/A','2017',1,1),(154,1012,'2017-04-03 16:49:33','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:49:33',0,'N/A','2017',1,1),(155,1012,'2017-04-03 16:49:40','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:49:40',0,'N/A','2017',1,1),(156,1012,'2017-04-03 16:50:05','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:50:05',0,'N/A','2017',1,1),(157,1012,'2017-04-03 16:50:12','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:50:12',0,'N/A','2017',1,1),(158,1012,'2017-04-03 16:50:43','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:50:43',0,'N/A','2017',1,1),(159,1012,'2017-04-03 16:50:50','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:50:50',0,'N/A','2017',1,1),(160,1012,'2017-04-03 16:51:09','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:51:09',0,'N/A','2017',1,1),(161,1012,'2017-04-03 16:51:22','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:51:22',0,'N/A','2017',1,1),(162,1012,'2017-04-03 16:51:47','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:51:47',0,'N/A','2017',1,1),(163,1012,'2017-04-03 16:51:54','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:51:54',0,'N/A','2017',1,1),(164,1012,'2017-04-03 16:52:13','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:52:13',0,'N/A','2017',1,1),(165,1012,'2017-04-03 16:52:26','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:52:26',0,'N/A','2017',1,1),(166,1012,'2017-04-03 16:52:57','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:52:57',0,'N/A','2017',1,1),(167,1012,'2017-04-03 16:53:04','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:53:04',0,'N/A','2017',1,1),(168,1012,'2017-04-03 16:53:23','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:53:23',0,'N/A','2017',1,1),(169,1012,'2017-04-03 16:53:30','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:53:30',0,'N/A','2017',1,1),(170,1012,'2017-04-03 16:54:07','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:54:07',0,'N/A','2017',1,1),(171,1012,'2017-04-03 16:54:15','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:54:15',0,'N/A','2017',1,1),(172,1012,'2017-04-03 16:54:28','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:54:28',0,'N/A','2017',1,1),(173,1012,'2017-04-03 16:54:35','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:54:35',0,'N/A','2017',1,1),(174,1012,'2017-04-03 16:55:33','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:55:33',0,'N/A','2017',1,1),(175,1012,'2017-04-03 16:55:40','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:55:40',0,'N/A','2017',1,1),(176,1012,'2017-04-03 16:56:17','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:56:17',0,'N/A','2017',1,1),(177,1012,'2017-04-03 16:56:24','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:56:24',0,'N/A','2017',1,1),(178,1012,'2017-04-03 16:56:37','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:56:37',0,'N/A','2017',1,1),(179,1012,'2017-04-03 16:56:50','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-03 16:56:50',0,'N/A','2017',1,1),(180,1012,'2017-04-04 11:23:03','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 11:23:03',0,'N/A','2017',1,1),(181,1012,'2017-04-04 11:23:10','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 11:23:10',0,'N/A','2017',1,1),(182,1012,'2017-04-04 11:23:18','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 11:23:18',0,'N/A','2017',1,1),(183,1012,'2017-04-04 11:23:25','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 11:23:25',0,'N/A','2017',1,1),(184,1012,'2017-04-04 12:35:49','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 12:35:49',0,'N/A','2017',1,1),(185,1012,'2017-04-04 12:36:08','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 12:36:08',0,'N/A','2017',1,1),(186,1012,'2017-04-04 12:36:16','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 12:36:16',0,'N/A','2017',1,1),(187,1012,'2017-04-04 12:36:47','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 12:36:47',0,'N/A','2017',1,1),(188,1012,'2017-04-04 12:36:54','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 12:36:54',0,'N/A','2017',1,1),(189,1012,'2017-04-04 19:00:12','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 19:00:12',0,'N/A','2017',1,1),(190,1012,'2017-04-04 19:00:44','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 19:00:44',0,'N/A','2017',1,1),(191,1012,'2017-04-04 19:00:57','',0,'0:0:0:0:0:0:0:1',0,'April','N/A','2017-04-04 19:00:57',0,'N/A','2017',1,1);
/*!40000 ALTER TABLE `cer_watercreditlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cer_watercreditsummary`
--

DROP TABLE IF EXISTS `cer_watercreditsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cer_watercreditsummary` (
  `waterCreditSummaryID` bigint(20) NOT NULL AUTO_INCREMENT,
  `availableWaterCredit` int(11) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `usedWaterCredit` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `customerID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`waterCreditSummaryID`),
  KEY `FK_smf5kxdvj2tx2pp3pulojporn` (`customerID`),
  KEY `FK_cpqcxxroshnu5bs7owh139o0o` (`operatorID`),
  CONSTRAINT `FK_cpqcxxroshnu5bs7owh139o0o` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_smf5kxdvj2tx2pp3pulojporn` FOREIGN KEY (`customerID`) REFERENCES `cer_customer` (`customerID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cer_watercreditsummary`
--

LOCK TABLES `cer_watercreditsummary` WRITE;
/*!40000 ALTER TABLE `cer_watercreditsummary` DISABLE KEYS */;
INSERT INTO `cer_watercreditsummary` VALUES (1,1012,'2017-03-30 11:19:59','','0:0:0:0:0:0:0:1','N/A',0,'operator',1,1),(2,150,'2017-04-01 13:53:32','','0:0:0:0:0:0:0:1','N/A',0,'operator',2,1);
/*!40000 ALTER TABLE `cer_watercreditsummary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forget_password`
--

DROP TABLE IF EXISTS `forget_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forget_password` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `cerID` varchar(255) DEFAULT NULL,
  `dateExecuted` datetime DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `operatorID` varchar(255) DEFAULT NULL,
  `organizationID` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `resetCode` varchar(255) DEFAULT NULL,
  `resetDate` date DEFAULT NULL,
  `resetMonth` varchar(255) DEFAULT NULL,
  `resetYear` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forget_password`
--

LOCK TABLES `forget_password` WRITE;
/*!40000 ALTER TABLE `forget_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `forget_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mqtt_test_save`
--

DROP TABLE IF EXISTS `mqtt_test_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mqtt_test_save` (
  `test_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tempareture` int(11) DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mqtt_test_save`
--

LOCK TABLES `mqtt_test_save` WRITE;
/*!40000 ALTER TABLE `mqtt_test_save` DISABLE KEYS */;
/*!40000 ALTER TABLE `mqtt_test_save` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `userType` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `cerID` bigint(20) DEFAULT NULL,
  `operatorID` bigint(20) DEFAULT NULL,
  `organizationID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`),
  KEY `FK_hpgy4m4hjrj9qbu14se7rcy3o` (`cerID`),
  KEY `FK_g6lqljw9ar2o65ycqylcxpvi7` (`operatorID`),
  KEY `FK_i0pls5jj4vn9n9ntvbed5mcwt` (`organizationID`),
  CONSTRAINT `FK_g6lqljw9ar2o65ycqylcxpvi7` FOREIGN KEY (`operatorID`) REFERENCES `cer_operator` (`operatorID`),
  CONSTRAINT `FK_hpgy4m4hjrj9qbu14se7rcy3o` FOREIGN KEY (`cerID`) REFERENCES `cer_cer` (`cerID`),
  CONSTRAINT `FK_i0pls5jj4vn9n9ntvbed5mcwt` FOREIGN KEY (`organizationID`) REFERENCES `cer_organization` (`organizationID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'2017-03-30 11:13:04','','0:0:0:0:0:0:0:1','centre for energy research','123','n/a',1,'admin','cer','cer',1,NULL,NULL),(2,'2017-03-30 11:14:46','','0:0:0:0:0:0:0:1','Infrastructure Development Company Limited ','123','n/a',1,'cer','organization','idcol',NULL,NULL,1),(3,'2017-03-30 11:16:26','','0:0:0:0:0:0:0:1','rahimafrooz','123','n/a',1,'idcol','operator','operator',NULL,1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_role`
--

DROP TABLE IF EXISTS `users_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_role` (
  `userRoleId` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateExecuted` datetime DEFAULT NULL,
  `ipExecuted` varchar(255) DEFAULT NULL,
  `recordNote` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) DEFAULT NULL,
  `rolename` varchar(255) DEFAULT NULL,
  `userExecuted` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `userID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userRoleId`),
  KEY `FK_g3mgnmjtuoay28gchrvn325qt` (`userID`),
  CONSTRAINT `FK_g3mgnmjtuoay28gchrvn325qt` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_role`
--

LOCK TABLES `users_role` WRITE;
/*!40000 ALTER TABLE `users_role` DISABLE KEYS */;
INSERT INTO `users_role` VALUES (1,'2017-03-30 11:13:04','0:0:0:0:0:0:0:1','n/a',1,'ROLE_ADMIN','admin','cer',1),(2,'2017-03-30 11:14:46','0:0:0:0:0:0:0:1','n/a',1,'ROLE_ADMIN','cer','idcol',2),(3,'2017-03-30 11:16:26','0:0:0:0:0:0:0:1','n/a',1,'ROLE_ADMIN','idcol','operator',3);
/*!40000 ALTER TABLE `users_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-04 19:10:21
