package bd.ac.uiu.dto;

import java.util.Date;

public class IpcuDTO {

	private long ipcuID;
	private long ipcuCustomID;
	private double ipcuWattCapacity;

	private double ipcuDepth;

	private String waterDynamicHead;

	private String distanceFromCanel;

	private AreaDTO areaDTO;

	private OperatorDTO operatorDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public IpcuDTO() {
	}
	

	public IpcuDTO(long ipcuID, double ipcuWattCapacity, double ipcuDepth, String waterDynamicHead,
			String distanceFromCanel, AreaDTO areaDTO, OperatorDTO operatorDTO, boolean enabled) {
		super();
		this.ipcuID = ipcuID;
		this.ipcuWattCapacity = ipcuWattCapacity;
		this.ipcuDepth = ipcuDepth;
		this.waterDynamicHead = waterDynamicHead;
		this.distanceFromCanel = distanceFromCanel;
		this.areaDTO = areaDTO;
		this.operatorDTO = operatorDTO;
		this.enabled = enabled;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public double getIpcuWattCapacity() {
		return ipcuWattCapacity;
	}

	public void setIpcuWattCapacity(double ipcuWattCapacity) {
		this.ipcuWattCapacity = ipcuWattCapacity;
	}

	public double getIpcuDepth() {
		return ipcuDepth;
	}

	public void setIpcuDepth(double ipcuDepth) {
		this.ipcuDepth = ipcuDepth;
	}

	public String getWaterDynamicHead() {
		return waterDynamicHead;
	}

	public void setWaterDynamicHead(String waterDynamicHead) {
		this.waterDynamicHead = waterDynamicHead;
	}

	public String getDistanceFromCanel() {
		return distanceFromCanel;
	}

	public void setDistanceFromCanel(String distanceFromCanel) {
		this.distanceFromCanel = distanceFromCanel;
	}

	public AreaDTO getAreaDTO() {
		return areaDTO;
	}

	public void setAreaDTO(AreaDTO areaDTO) {
		this.areaDTO = areaDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
}
