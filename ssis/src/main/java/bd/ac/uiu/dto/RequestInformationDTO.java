package bd.ac.uiu.dto;

import java.util.Date;

import javax.persistence.Column;

public class RequestInformationDTO {

	private long requestInformationID;
	private int operatorID;
	private int ipcuID;
	private int customerID;
	private int fieldID;
	private int soilMoisture;
	private int motorIpcu;
	private int waterLevelSensor;
	private int fieldValve;
	private int mainValve;
	private int waterCredit;
	private int flowRate;
	private int temperature;
	private int humidity;
	private int deviceStatus;
	private int nodeID;
	private int season;
	private String recordNote;
	private String userExecuted;
	private Date dateExecuted;
	private String ipExecuted;
	private boolean enabled;

	public RequestInformationDTO() {
	}

	public long getRequestInformationID() {
		return requestInformationID;
	}

	public void setRequestInformationID(long requestInformationID) {
		this.requestInformationID = requestInformationID;
	}

	public int getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(int operatorID) {
		this.operatorID = operatorID;
	}

	public int getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(int ipcuID) {
		this.ipcuID = ipcuID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getFieldID() {
		return fieldID;
	}

	public void setFieldID(int fieldID) {
		this.fieldID = fieldID;
	}

	public int getSoilMoisture() {
		return soilMoisture;
	}

	public void setSoilMoisture(int soilMoisture) {
		this.soilMoisture = soilMoisture;
	}

	public int getMotorIpcu() {
		return motorIpcu;
	}

	public void setMotorIpcu(int motorIpcu) {
		this.motorIpcu = motorIpcu;
	}

	public int getWaterLevelSensor() {
		return waterLevelSensor;
	}

	public void setWaterLevelSensor(int waterLevelSensor) {
		this.waterLevelSensor = waterLevelSensor;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getMainValve() {
		return mainValve;
	}

	public void setMainValve(int mainValve) {
		this.mainValve = mainValve;
	}

	public int getWaterCredit() {
		return waterCredit;
	}

	public void setWaterCredit(int waterCredit) {
		this.waterCredit = waterCredit;
	}

	public int getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(int flowRate) {
		this.flowRate = flowRate;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(int deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
