package bd.ac.uiu.dto;

import java.util.Date;

public class OperatorDTO {
	private long operatorID;

	private String operatorName;

	private String mobile;

	private String email;

	private String presentAddress;

	private String permanentAddress;

	private String voterID;

	private String passport;

	private String birthCerNumber;

	private String tinCerNumber;

	private Date establishDate;

	private String imageName;

	private String sizeOfImage;

	private String imagePath;

	private String imageTitle;

	private String registrationNumber;

	private OrganizationDTO organizationDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public OperatorDTO() {
	}

	

	public OperatorDTO(long operatorID, String operatorName, String mobile, String email, String presentAddress,
			String permanentAddress, String voterID, String passport, String birthCerNumber, String tinCerNumber,
			Date establishDate, String registrationNumber, OrganizationDTO organizationDTO, boolean enabled) {
		super();
		this.operatorID = operatorID;
		this.operatorName = operatorName;
		this.mobile = mobile;
		this.email = email;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.voterID = voterID;
		this.passport = passport;
		this.birthCerNumber = birthCerNumber;
		this.tinCerNumber = tinCerNumber;
		this.establishDate = establishDate;
		this.registrationNumber = registrationNumber;
		this.organizationDTO = organizationDTO;
		this.enabled = enabled;
	}



	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getVoterID() {
		return voterID;
	}

	public void setVoterID(String voterID) {
		this.voterID = voterID;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getBirthCerNumber() {
		return birthCerNumber;
	}

	public void setBirthCerNumber(String birthCerNumber) {
		this.birthCerNumber = birthCerNumber;
	}

	public String getTinCerNumber() {
		return tinCerNumber;
	}

	public void setTinCerNumber(String tinCerNumber) {
		this.tinCerNumber = tinCerNumber;
	}

	public Date getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Date establishDate) {
		this.establishDate = establishDate;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getSizeOfImage() {
		return sizeOfImage;
	}

	public void setSizeOfImage(String sizeOfImage) {
		this.sizeOfImage = sizeOfImage;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public OrganizationDTO getOrganizationDTO() {
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}

}
