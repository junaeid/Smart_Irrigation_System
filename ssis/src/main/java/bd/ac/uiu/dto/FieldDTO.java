package bd.ac.uiu.dto;

import java.util.Date;

public class FieldDTO {
	private long fieldID;
	private double fieldMeasurement;

	private FieldTypeDTO fieldTypeDTO;

	private String reference;

	private IpcuDTO ipcuDTO;

	private CustomerDTO customerDTO;

	private OperatorDTO operatorDTO;

	private SoilDTO soilDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public FieldDTO() {

	}

	public FieldDTO(long fieldID, double fieldMeasurement, FieldTypeDTO fieldTypeDTO, IpcuDTO ipcuDTO,
			CustomerDTO customerDTO, SoilDTO soilDTO, boolean enabled) {
		super();
		this.fieldID = fieldID;
		this.fieldMeasurement = fieldMeasurement;
		this.fieldTypeDTO = fieldTypeDTO;
		this.ipcuDTO = ipcuDTO;
		this.customerDTO = customerDTO;
		this.soilDTO = soilDTO;
		this.enabled = enabled;
	}

	public IpcuDTO getIpcuDTO() {
		return ipcuDTO;
	}

	public void setIpcuDTO(IpcuDTO ipcuDTO) {
		this.ipcuDTO = ipcuDTO;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public long getFieldID() {
		return fieldID;
	}

	public void setFieldID(long fieldID) {
		this.fieldID = fieldID;
	}

	public double getFieldMeasurement() {
		return fieldMeasurement;
	}

	public void setFieldMeasurement(double fieldMeasurement) {
		this.fieldMeasurement = fieldMeasurement;
	}

	public FieldTypeDTO getFieldTypeDTO() {
		return fieldTypeDTO;
	}

	public void setFieldTypeDTO(FieldTypeDTO fieldTypeDTO) {
		this.fieldTypeDTO = fieldTypeDTO;
	}

	public SoilDTO getSoilDTO() {
		return soilDTO;
	}

	public void setSoilDTO(SoilDTO soilDTO) {
		this.soilDTO = soilDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}