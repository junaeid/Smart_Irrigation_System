package bd.ac.uiu.dto;

import java.util.Date;

import javax.persistence.Column;

public class SMSToDTO {
	private long smsToID;

	private String smsToName;

	private String smsToMobile;
	
	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;
	
	

	public SMSToDTO() {
		
	}

	public SMSToDTO(String smsToName, String smsToMobile, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.smsToName = smsToName;
		this.smsToMobile = smsToMobile;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSmsToID() {
		return smsToID;
	}

	public void setSmsToID(long smsToID) {
		this.smsToID = smsToID;
	}

	public String getSmsToName() {
		return smsToName;
	}

	public void setSmsToName(String smsToName) {
		this.smsToName = smsToName;
	}

	public String getSmsToMobile() {
		return smsToMobile;
	}

	public void setSmsToMobile(String smsToMobile) {
		this.smsToMobile = smsToMobile;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
