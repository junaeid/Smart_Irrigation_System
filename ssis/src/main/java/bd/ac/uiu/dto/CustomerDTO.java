package bd.ac.uiu.dto;

import java.util.Date;

public class CustomerDTO {

	private long customerID;

	private String customerName;

	private String nationalID;

	private Date dateOfBirth;

	private String mobile;

	private String vulveNumber;

	private String birthCertificate;

	private String religion;

	private String gender;

	private String maritalStatus;

	private String nationality;

	private String bloodGroup;

	private AreaDTO areaDTO;

	private OperatorDTO operatorDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public CustomerDTO() {
	}

	public CustomerDTO(long customerID, String customerName, String nationalID, Date dateOfBirth, String mobile,
			String vulveNumber, String birthCertificate, String religion, String gender, String maritalStatus,
			String nationality, String bloodGroup, AreaDTO areaDTO, OperatorDTO operatorDTO, boolean enabled) {
		super();
		this.customerID = customerID;
		this.customerName = customerName;
		this.nationalID = nationalID;
		this.dateOfBirth = dateOfBirth;
		this.mobile = mobile;
		this.vulveNumber = vulveNumber;
		this.birthCertificate = birthCertificate;
		this.religion = religion;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.nationality = nationality;
		this.bloodGroup = bloodGroup;
		this.areaDTO = areaDTO;
		this.operatorDTO = operatorDTO;
		this.enabled = enabled;
	}

	public CustomerDTO(long customerID, String customerName, String nationalID, Date dateOfBirth, String mobile,
			String vulveNumber, String birthCertificate, String religion, String gender, String maritalStatus,
			String nationality, String bloodGroup, boolean enabled, OperatorDTO operatorDTO) {
		super();
		this.customerID = customerID;
		this.customerName = customerName;
		this.nationalID = nationalID;
		this.dateOfBirth = dateOfBirth;
		this.mobile = mobile;
		this.vulveNumber = vulveNumber;
		this.birthCertificate = birthCertificate;
		this.religion = religion;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.nationality = nationality;
		this.bloodGroup = bloodGroup;
		this.enabled = enabled;
		this.operatorDTO = operatorDTO;
	}

	public long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getNationalID() {
		return nationalID;
	}

	public void setNationalID(String nationalID) {
		this.nationalID = nationalID;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getVulveNumber() {
		return vulveNumber;
	}

	public void setVulveNumber(String vulveNumber) {
		this.vulveNumber = vulveNumber;
	}

	public String getBirthCertificate() {
		return birthCertificate;
	}

	public void setBirthCertificate(String birthCertificate) {
		this.birthCertificate = birthCertificate;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public AreaDTO getAreaDTO() {
		return areaDTO;
	}

	public void setAreaDTO(AreaDTO areaDTO) {
		this.areaDTO = areaDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
