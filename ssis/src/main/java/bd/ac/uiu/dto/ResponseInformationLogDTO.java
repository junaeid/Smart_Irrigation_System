package bd.ac.uiu.dto;

import java.util.Date;

public class ResponseInformationLogDTO {

	private long responseID;

	private int fieldValve;

	private int sensorStatus;

	private int nodeID;

	private double balance;

	private int mainValve;

	private FieldDTO fieldDTO;

	private OperatorDTO operatorDTO;

	private IpcuDTO ipcuDTO;

	private CustomerDTO customerDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public ResponseInformationLogDTO() {

	}

	public long getResponseID() {
		return responseID;
	}

	public void setResponseID(long responseID) {
		this.responseID = responseID;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(int sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getMainValve() {
		return mainValve;
	}

	public void setMainValve(int mainValve) {
		this.mainValve = mainValve;
	}

	public FieldDTO getFieldDTO() {
		return fieldDTO;
	}

	public void setFieldDTO(FieldDTO fieldDTO) {
		this.fieldDTO = fieldDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public IpcuDTO getIpcuDTO() {
		return ipcuDTO;
	}

	public void setIpcuDTO(IpcuDTO ipcuDTO) {
		this.ipcuDTO = ipcuDTO;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
