package bd.ac.uiu.dto;

import java.util.Date;

public class SMSDTO {

	private long smsID;

	private String smsTo;

	private String smsFrom;

	private int availableWaterCredit;

	private String smsBody;

	private OperatorDTO operatorDTO;

	private CustomerDTO customerDTO;

	private Date dateExecuted;

	public SMSDTO() {
		super();
	}

	public SMSDTO(long smsID, String smsTo, String smsFrom, int availableWaterCredit, String smsBody,
			OperatorDTO operatorDTO, CustomerDTO customerDTO, Date dateExecuted) {
		super();
		this.smsID = smsID;
		this.smsTo = smsTo;
		this.smsFrom = smsFrom;
		this.availableWaterCredit = availableWaterCredit;
		this.smsBody = smsBody;
		this.operatorDTO = operatorDTO;
		this.customerDTO = customerDTO;
		this.dateExecuted = dateExecuted;
	}

	public long getSmsID() {
		return smsID;
	}

	public void setSmsID(long smsID) {
		this.smsID = smsID;
	}

	public String getSmsTo() {
		return smsTo;
	}

	public void setSmsTo(String smsTo) {
		this.smsTo = smsTo;
	}

	public String getSmsFrom() {
		return smsFrom;
	}

	public void setSmsFrom(String smsFrom) {
		this.smsFrom = smsFrom;
	}

	public int getAvailableWaterCredit() {
		return availableWaterCredit;
	}

	public void setAvailableWaterCredit(int availableWaterCredit) {
		this.availableWaterCredit = availableWaterCredit;
	}

	public String getSmsBody() {
		return smsBody;
	}

	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

}
