package bd.ac.uiu.dto;

import java.util.Date;


public class OrganizationDTO {

	private long organizationID;

	private String organizationName;

	private String mobile;

	private String email;

	private String presentAddress;

	private String permanentAddress;

	private String tinCerNumber;

	private Date establishDate;

	private String imageName;

	private String sizeOfImage;

	private String imagePath;

	private String imageTitle;

	private String registrationNumber;

	private CerDTO cerDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public OrganizationDTO() {

	}

	

	public OrganizationDTO(long organizationID, String organizationName, String mobile, String email, CerDTO cerDTO) {
		super();
		this.organizationID = organizationID;
		this.organizationName = organizationName;
		this.mobile = mobile;
		this.email = email;
		this.cerDTO = cerDTO;
	}



	public long getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(long organizationID) {
		this.organizationID = organizationID;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getTinCerNumber() {
		return tinCerNumber;
	}

	public void setTinCerNumber(String tinCerNumber) {
		this.tinCerNumber = tinCerNumber;
	}

	public Date getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Date establishDate) {
		this.establishDate = establishDate;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getSizeOfImage() {
		return sizeOfImage;
	}

	public void setSizeOfImage(String sizeOfImage) {
		this.sizeOfImage = sizeOfImage;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	
	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}



	public CerDTO getCerDTO() {
		return cerDTO;
	}



	public void setCerDTO(CerDTO cerDTO) {
		this.cerDTO = cerDTO;
	}









}
