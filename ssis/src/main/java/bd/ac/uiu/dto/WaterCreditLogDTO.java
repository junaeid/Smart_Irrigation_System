package bd.ac.uiu.dto;

import java.util.Date;

public class WaterCreditLogDTO {

	private long waterCreditID;

	private int usedWaterCredit;

	private int availableWaterCredit;

	private Date usedDate;

	private String year;

	private String month;

	private long fieldID;

	private long ipcuID;

	private CustomerDTO customerDTO;

	private OperatorDTO operatorDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public WaterCreditLogDTO() {
	}

	public long getWaterCreditID() {
		return waterCreditID;
	}

	public void setWaterCreditID(long waterCreditID) {
		this.waterCreditID = waterCreditID;
	}

	public double getUsedWaterCredit() {
		return usedWaterCredit;
	}

	public int getAvailableWaterCredit() {
		return availableWaterCredit;
	}

	public void setAvailableWaterCredit(int availableWaterCredit) {
		this.availableWaterCredit = availableWaterCredit;
	}

	public void setUsedWaterCredit(int usedWaterCredit) {
		this.usedWaterCredit = usedWaterCredit;
	}

	public Date getUsedDate() {
		return usedDate;
	}

	public void setUsedDate(Date usedDate) {
		this.usedDate = usedDate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public long getFieldID() {
		return fieldID;
	}

	public void setFieldID(long fieldID) {
		this.fieldID = fieldID;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
