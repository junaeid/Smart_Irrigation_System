package bd.ac.uiu.dto;

import java.util.Date;

public class ResponseInformationDTO {

	private long responseID;

	private int operatorID;

	private int ipcuID;

	private int customerID;

	private int fieldID;

	private int fieldVulve;

	private int mainVulve;

	private int sensorStatus;

	private int nodeID;

	private double balance;

	private long paymentSummaryID;

	// private FieldDTO fieldDTO;

	// private OperatorDTO operatorDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public ResponseInformationDTO() {

	}

	public ResponseInformationDTO(int fieldVulve, int sensorStatus, int nodeID, double balance, long paymentSummaryID,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.fieldVulve = fieldVulve;
		this.sensorStatus = sensorStatus;
		this.nodeID = nodeID;
		this.balance = balance;
		this.paymentSummaryID = paymentSummaryID;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public ResponseInformationDTO(long responseID, int operatorID, int ipcuID, int customerID, int fieldID,
			int fieldVulve, int sensorStatus, int nodeId, double balance, long paymentSummaryID, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.responseID = responseID;
		this.operatorID = operatorID;
		this.ipcuID = ipcuID;
		this.customerID = customerID;
		this.fieldID = fieldID;
		this.fieldVulve = fieldVulve;
		this.sensorStatus = sensorStatus;
		this.nodeID = nodeID;
		this.balance = balance;
		this.paymentSummaryID = paymentSummaryID;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getResponseID() {
		return responseID;
	}

	public void setResponseID(long responseID) {
		this.responseID = responseID;
	}

	public int getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(int operatorID) {
		this.operatorID = operatorID;
	}

	public int getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(int ipcuID) {
		this.ipcuID = ipcuID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getFieldID() {
		return fieldID;
	}

	public void setFieldID(int fieldID) {
		this.fieldID = fieldID;
	}

	public int getFieldVulve() {
		return fieldVulve;
	}

	public void setFieldVulve(int fieldVulve) {
		this.fieldVulve = fieldVulve;
	}

	public int getMainVulve() {
		return mainVulve;
	}

	public void setMainVulve(int mainVulve) {
		this.mainVulve = mainVulve;
	}

	public int getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(int sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public long getPaymentSummaryID() {
		return paymentSummaryID;
	}

	public void setPaymentSummaryID(long paymentSummaryID) {
		this.paymentSummaryID = paymentSummaryID;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
