package bd.ac.uiu.dto;

import java.util.Date;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;

public class PaymentSummaryDTO {

	private long paymentSummaryId;

	private double totalDepositeAmount;

	private double totalExpenseAmount;

	private double balanceAmount;

	private OperatorDTO operatorDTO;

	private CustomerDTO customerDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	private double amount;

	public PaymentSummaryDTO() {

	}

	

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}



	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}



	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}



	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}



	public long getPaymentSummaryId() {
		return paymentSummaryId;
	}

	public void setPaymentSummaryId(long paymentSummaryId) {
		this.paymentSummaryId = paymentSummaryId;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public double getTotalDepositeAmount() {
		return totalDepositeAmount;
	}

	public void setTotalDepositeAmount(double totalDepositeAmount) {
		this.totalDepositeAmount = totalDepositeAmount;
	}

	public double getTotalExpenseAmount() {
		return totalExpenseAmount;
	}

	public void setTotalExpenseAmount(double totalExpenseAmount) {
		this.totalExpenseAmount = totalExpenseAmount;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
