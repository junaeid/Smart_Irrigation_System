package bd.ac.uiu.dto;

import java.io.Serializable;
import java.util.Date;

public class CerDTO implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;

	private long cerID;

	private String cerName;

	private String mobile;

	private String email;

	private String presentAddress;

	private String permanentAddress;

	private String tinCerNumber;

	private Date establishDate;

	private String imageName;

	private String sizeOfImage;

	private String imagePath;

	private String imageTitle;

	private String registrationNumber;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public CerDTO() {
	}

	public CerDTO(long cerID, String cerName, String mobile, String email, String presentAddress,
			String permanentAddress, String tinCerNumber, Date establishDate, String registrationNumber,
			boolean enabled) {
		super();
		this.cerID = cerID;
		this.cerName = cerName;
		this.mobile = mobile;
		this.email = email;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.tinCerNumber = tinCerNumber;
		this.establishDate = establishDate;
		this.registrationNumber = registrationNumber;
		this.enabled = enabled;
	}

	public CerDTO(long cerID, String cerName, String mobile, String email, String presentAddress,
			String permanentAddress, String tinCerNumber, Date establishDate, String imageName, String sizeOfImage,
			String imagePath, String imageTitle, String registrationNumber, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.cerID = cerID;
		this.cerName = cerName;
		this.mobile = mobile;
		this.email = email;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.tinCerNumber = tinCerNumber;
		this.establishDate = establishDate;
		this.imageName = imageName;
		this.sizeOfImage = sizeOfImage;
		this.imagePath = imagePath;
		this.imageTitle = imageTitle;
		this.registrationNumber = registrationNumber;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getCerID() {
		return cerID;
	}

	public void setCerID(long cerID) {
		this.cerID = cerID;
	}

	public String getCerName() {
		return cerName;
	}

	public void setCerName(String cerName) {
		this.cerName = cerName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getTinCerNumber() {
		return tinCerNumber;
	}

	public void setTinCerNumber(String tinCerNumber) {
		this.tinCerNumber = tinCerNumber;
	}

	public Date getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Date establishDate) {
		this.establishDate = establishDate;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getSizeOfImage() {
		return sizeOfImage;
	}

	public void setSizeOfImage(String sizeOfImage) {
		this.sizeOfImage = sizeOfImage;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
