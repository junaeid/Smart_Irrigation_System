package bd.ac.uiu.dto;

import java.util.Date;

import javax.persistence.Column;

public class SMSTemplateDTO {
	private long smsTemplateID;

	private String smsTemplateType;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public SMSTemplateDTO() {

	}

	public SMSTemplateDTO(String smsTemplateType, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.smsTemplateType = smsTemplateType;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSmsTemplateID() {
		return smsTemplateID;
	}

	public void setSmsTemplateID(long smsTemplateID) {
		this.smsTemplateID = smsTemplateID;
	}

	public String getSmsTemplateType() {
		return smsTemplateType;
	}

	public void setSmsTemplateType(String smsTemplateType) {
		this.smsTemplateType = smsTemplateType;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
