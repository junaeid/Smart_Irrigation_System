package bd.ac.uiu.dto;

import java.util.Date;

public class RequestInformationLogDTO {

	private long requestInformationLogID;

	private int soilMoisture;

	private int motoIPCU;

	private int waterLevelSensor;

	private int fieldValve;
	
	private int mainValve;

	private int waterCredit;

	private int flowRate;

	private int temperature;

	private int humidity;

	private int deviceStatus;

	private int nodeID;

	private int season;

	private FieldDTO fieldDTO;

	private OperatorDTO operatorDTO;

	private IpcuDTO ipcuDTO;

	private CustomerDTO customerDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public RequestInformationLogDTO() {
	}

	public long getRequestInformationLogID() {
		return requestInformationLogID;
	}

	public void setRequestInformationLogID(long requestInformationLogID) {
		this.requestInformationLogID = requestInformationLogID;
	}

	public int getSoilMoisture() {
		return soilMoisture;
	}

	public void setSoilMoisture(int soilMoisture) {
		this.soilMoisture = soilMoisture;
	}

	public int getMotoIPCU() {
		return motoIPCU;
	}

	public void setMotoIPCU(int motoIPCU) {
		this.motoIPCU = motoIPCU;
	}

	public int getWaterLevelSensor() {
		return waterLevelSensor;
	}

	public void setWaterLevelSensor(int waterLevelSensor) {
		this.waterLevelSensor = waterLevelSensor;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getWaterCredit() {
		return waterCredit;
	}

	public void setWaterCredit(int waterCredit) {
		this.waterCredit = waterCredit;
	}

	public int getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(int flowRate) {
		this.flowRate = flowRate;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(int deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public FieldDTO getFieldDTO() {
		return fieldDTO;
	}

	public void setFieldDTO(FieldDTO fieldDTO) {
		this.fieldDTO = fieldDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public IpcuDTO getIpcuDTO() {
		return ipcuDTO;
	}

	public void setIpcuDTO(IpcuDTO ipcuDTO) {
		this.ipcuDTO = ipcuDTO;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getMainValve() {
		return mainValve;
	}

	public void setMainValve(int mainValve) {
		this.mainValve = mainValve;
	}

}
