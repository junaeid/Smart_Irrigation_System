package bd.ac.uiu.dto;

import java.util.Date;

import bd.ac.uiu.entity.ContactPerson;

public class ContactPersonDTO {

	private long contactPersonID;

	private String contactPersonName;

	private CerDTO cerDTO;

	private OrganizationDTO organizationDTO;

	private OperatorDTO operatorDTO;

	private ContactPerson contactPerson;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private String userType;

	private boolean enabled;

	public ContactPersonDTO() {
	}

	public ContactPersonDTO(long contactPersonID, String contactPersonName, CerDTO cerDTO, OrganizationDTO organization,
			OperatorDTO operatorDTO, String recordNote, String userExecuted, Date dateExecuted, String ipExecuted,
			boolean enabled) {
		super();
		this.contactPersonID = contactPersonID;
		this.contactPersonName = contactPersonName;
		this.cerDTO = cerDTO;
		this.organizationDTO = organization;
		this.operatorDTO = operatorDTO;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public ContactPersonDTO(long contactPersonID, String contactPersonName, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.contactPersonID = contactPersonID;
		this.contactPersonName = contactPersonName;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getContactPersonID() {
		return contactPersonID;
	}

	public void setContactPersonID(long contactPersonID) {
		this.contactPersonID = contactPersonID;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public CerDTO getCerDTO() {
		return cerDTO;
	}

	public void setCerDTO(CerDTO cerDTO) {
		this.cerDTO = cerDTO;
	}

	public OrganizationDTO getOrganizationDTO() {
		return organizationDTO;
	}

	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	public void setOrganizationDTO(OrganizationDTO organization) {
		this.organizationDTO = organization;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
