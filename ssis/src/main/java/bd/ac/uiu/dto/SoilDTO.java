package bd.ac.uiu.dto;

import java.util.Date;

public class SoilDTO {

	private long soilID;
	private String soilType;
	private String geographicalInformation;
	private String fieldDescription;

	private OperatorDTO operatorDTO;

	private FieldDTO fieldDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public SoilDTO() {

	}

	public SoilDTO(long soilID, String soilType, String geographicalInformation, String fieldDescription,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled, OperatorDTO operatorDTO) {
		super();
		this.soilID = soilID;
		this.soilType = soilType;
		this.geographicalInformation = geographicalInformation;
		this.fieldDescription = fieldDescription;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.operatorDTO =operatorDTO;
	}

	public SoilDTO(String soilType, String geographicalInformation, String fieldDescription, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.soilType = soilType;
		this.geographicalInformation = geographicalInformation;
		this.fieldDescription = fieldDescription;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSoilID() {
		return soilID;
	}

	public void setSoilID(long soilID) {
		this.soilID = soilID;
	}

	public String getSoilType() {
		return soilType;
	}

	public void setSoilType(String soilType) {
		this.soilType = soilType;
	}

	public String getGeographicalInformation() {
		return geographicalInformation;
	}

	public void setGeographicalInformation(String geographicalInformation) {
		this.geographicalInformation = geographicalInformation;
	}

	public String getFieldDescription() {
		return fieldDescription;
	}

	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public FieldDTO getFieldDTO() {
		return fieldDTO;
	}

	public void setFieldDTO(FieldDTO fieldDTO) {
		this.fieldDTO = fieldDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

}
