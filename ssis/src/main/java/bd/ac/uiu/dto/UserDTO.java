package bd.ac.uiu.dto;

import java.util.Date;

import bd.ac.uiu.entity.UserRole;
import bd.ac.uiu.entity.Users;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */
public class UserDTO {
	private long id;
	private String name;
	private String username;
	private String password;
	private String mobile;
	private boolean enabled;
	private long organizationID;
	private String recordNote;
	private String userExecuted;
	private Date dateExecuted;
	private String ipExecuted;
	private int recordStatus;

	private Users users;

	private UserRole userRole;

	private String rolename;
	
	private String userType;
	
	private CerDTO cerDTO;
	

	private OrganizationDTO organizationDTO;
	

	private OperatorDTO operatorDTO;
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserDTO(long id, String name, String username, String password, boolean enabled, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, int recordStatus, String rolename) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.rolename = rolename;
	}

	public UserDTO(String name, String username, String rolename) {
		super();
		this.name = name;
		this.username = username;
		this.rolename = rolename;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public long getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(long organizationID) {
		this.organizationID = organizationID;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public CerDTO getCerDTO() {
		return cerDTO;
	}

	public void setCerDTO(CerDTO cerDTO) {
		this.cerDTO = cerDTO;
	}

	public OrganizationDTO getOrganizationDTO() {
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	

}
