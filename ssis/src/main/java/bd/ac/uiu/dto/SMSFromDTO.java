package bd.ac.uiu.dto;

import java.util.Date;

import javax.persistence.Column;

public class SMSFromDTO {
	private long smsFromID;

	private String smsFromName;

	private String smsFromMobile;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	public SMSFromDTO() {

	}

	public SMSFromDTO(String smsFromName, String smsFromMobile, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.smsFromName = smsFromName;
		this.smsFromMobile = smsFromMobile;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSmsFromID() {
		return smsFromID;
	}

	public void setSmsFromID(long smsFromID) {
		this.smsFromID = smsFromID;
	}

	public String getSmsFromName() {
		return smsFromName;
	}

	public void setSmsFromName(String smsFromName) {
		this.smsFromName = smsFromName;
	}

	public String getSmsFromMobile() {
		return smsFromMobile;
	}

	public void setSmsFromMobile(String smsFromMobile) {
		this.smsFromMobile = smsFromMobile;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
