package bd.ac.uiu.dto;

import java.util.Date;


public class TransactionLogDTO {

	private long transationLogID;

	private String paymentTypeName;

	private Date paymentDate;

	private double amount;

	private double debitAmount;

	private double creditAmount;

	private CustomerDTO customerDTO;

	private OperatorDTO operatorDTO;

	private String year;

	private String month;

	private String trxStatus;

	private String trxId;

	private String counter;

	private String reference;

	private String sender;

	private String service;

	private String currency;

	private String receiver;

	private String trxTimeStamp;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;

	// for water creadit summary
	private int noOfUnit;

	public TransactionLogDTO() {
	}

	public TransactionLogDTO(long transationLogID, String paymentTypeName, Date paymentDate, double debitAmount,
			double creditAmount, CustomerDTO customerDTO, OperatorDTO operatorDTO, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.transationLogID = transationLogID;
		this.paymentTypeName = paymentTypeName;
		this.paymentDate = paymentDate;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.customerDTO = customerDTO;
		this.operatorDTO = operatorDTO;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getTransationLogID() {
		return transationLogID;
	}

	public void setTransationLogID(long transationLogID) {
		this.transationLogID = transationLogID;
	}

	public String getPaymentTypeName() {
		return paymentTypeName;
	}

	public void setPaymentTypeName(String paymentTypeName) {
		this.paymentTypeName = paymentTypeName;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getNoOfUnit() {
		return noOfUnit;
	}

	public void setNoOfUnit(int noOfUnit) {
		this.noOfUnit = noOfUnit;
	}

	public String getTrxStatus() {
		return trxStatus;
	}

	public void setTrxStatus(String trxStatus) {
		this.trxStatus = trxStatus;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getTrxTimeStamp() {
		return trxTimeStamp;
	}

	public void setTrxTimeStamp(String trxTimeStamp) {
		this.trxTimeStamp = trxTimeStamp;
	}

}
