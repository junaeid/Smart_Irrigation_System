package bd.ac.uiu.dto;

public class DashBoardDTO {

	long totalOperator;
	long totalIpcu;
	long totalCustomer;
	long totalField;

	long availableWaterCreditCustomerCount;
	long notAvailableWaterCreditCustomerCount;

	private CustomerDTO customerDTO;
	private FieldDTO fieldDTO;

	public long getTotalOperator() {
		return totalOperator;
	}

	public void setTotalOperator(long totalOperator) {
		this.totalOperator = totalOperator;
	}

	public long getTotalIpcu() {
		return totalIpcu;
	}

	public void setTotalIpcu(long totalIpcu) {
		this.totalIpcu = totalIpcu;
	}

	public long getTotalCustomer() {
		return totalCustomer;
	}

	public void setTotalCustomer(long totalCustomer) {
		this.totalCustomer = totalCustomer;
	}

	public long getTotalField() {
		return totalField;
	}

	public void setTotalField(long totalField) {
		this.totalField = totalField;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public FieldDTO getFieldDTO() {
		return fieldDTO;
	}

	public void setFieldDTO(FieldDTO fieldDTO) {
		this.fieldDTO = fieldDTO;
	}

	public long getAvailableWaterCreditCustomerCount() {
		return availableWaterCreditCustomerCount;
	}

	public void setAvailableWaterCreditCustomerCount(long availableWaterCreditCustomerCount) {
		this.availableWaterCreditCustomerCount = availableWaterCreditCustomerCount;
	}

	public long getNotAvailableWaterCreditCustomerCount() {
		return notAvailableWaterCreditCustomerCount;
	}

	public void setNotAvailableWaterCreditCustomerCount(long notAvailableWaterCreditCustomerCount) {
		this.notAvailableWaterCreditCustomerCount = notAvailableWaterCreditCustomerCount;
	}

}
