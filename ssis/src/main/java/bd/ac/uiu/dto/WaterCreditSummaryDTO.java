package bd.ac.uiu.dto;

import java.util.Date;

public class WaterCreditSummaryDTO {
	private long waterCreditSummaryID;

	private int usedWaterCredit;


	private int availableWaterCredit;


	private CustomerDTO customerDTO;

	private OperatorDTO operatorDTO;

	private String recordNote;

	private String userExecuted;

	private Date dateExecuted;

	private String ipExecuted;

	private boolean enabled;
	
	private int noOfUnit;

	public WaterCreditSummaryDTO() {

	}

	public long getWaterCreditSummaryID() {
		return waterCreditSummaryID;
	}

	public void setWaterCreditSummaryID(long waterCreditSummaryID) {
		this.waterCreditSummaryID = waterCreditSummaryID;
	}

	public double getUsedWaterCredit() {
		return usedWaterCredit;
	}

	

	public int getAvailableWaterCredit() {
		return availableWaterCredit;
	}

	public void setAvailableWaterCredit(int availableWaterCredit) {
		this.availableWaterCredit = availableWaterCredit;
	}

	public void setUsedWaterCredit(int usedWaterCredit) {
		this.usedWaterCredit = usedWaterCredit;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public int getNoOfUnit() {
		return noOfUnit;
	}

	public void setNoOfUnit(int noOfUnit) {
		this.noOfUnit = noOfUnit;
	}

	

}
