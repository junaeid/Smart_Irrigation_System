package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.PaymentTypeDAO;
import bd.ac.uiu.entity.PaymentType;
import bd.ac.uiu.generic.EntityService;
@Component
@Transactional
public class PaymentTypeDAOImpl extends EntityService<PaymentType> implements PaymentTypeDAO{

}
