package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.RequestInformationLogDAO;
import bd.ac.uiu.entity.RequestInformationLog;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class RequestInformationLogDAOImpl extends EntityService<RequestInformationLog>
		implements RequestInformationLogDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<RequestInformationLog> findRequestInformationLogByOperatorID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from RequestInformationLog t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

	@Override
	public RequestInformationLog checkOperatorIDPumpIDCustomerIDAndFieldIDExistInRequestInformation(long operatorID,
			long ipcuID, long customerID, long fieldID) {

		try {
			return (RequestInformationLog) entityManager
					.createQuery("Select t from RequestInformationLog t where t.operator.operatorID=" + operatorID
							+ "and t.ipcu.ipcuID=" + ipcuID + "and t.customer.customerID=" + customerID
							+ "and t.field.fieldID=" + fieldID + "")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void updateRequestInformation(int soilMoisture, int motoIPCU, int waterLevelSensor, int fieldValve,
			int waterCredit, int flowRate, int temperature, int humidity, int deviceStatus, int nodeID, int season,
			long fieldID, long operatorID, long ipcuID, long customerID, int mainValve) throws Exception {

		entityManager
				.createQuery("Update RequestInformationLog t SET t.soilMoisture= " + soilMoisture + ", t.motoIPCU= "
						+ motoIPCU + ",t.waterLevelSensor=" + waterLevelSensor + ",t.fieldValve=" + fieldValve
						+ ",t.waterCredit=" + waterCredit + ",t.flowRate=" + flowRate + ",t.temperature=" + temperature
						+ ",t.humidity=" + humidity + ",t.deviceStatus=" + deviceStatus + ",t.nodeID=" + nodeID
						+ ",t.season=" + season + ",t.mainValve=" + mainValve + "  where t.operator.operatorID="
						+ operatorID + "and t.ipcu.ipcuID=" + ipcuID + "and t.customer.customerID=" + customerID
						+ "and t.field.fieldID=" + fieldID + "")
				.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RequestInformationLog> findRequestInformationLogByOperatorIDAndCustomerID(long operatorID,
			long customerID) throws Exception {
		return getEntityManager().createQuery("Select t from RequestInformationLog t where t.operator.operatorID='" + operatorID
				+ "' and t.customer.customerID='" + customerID + "'").getResultList();
	}

}
