package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.IpcuDAO;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class IpcuDAOImpl extends EntityService<Ipcu> implements IpcuDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Ipcu> findIpcusByOperator(long operatorID) throws Exception {
		return getEntityManager().createQuery("Select t from Ipcu t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

	@Override
	public Ipcu findIpcuByOperatorAndIpcu(long operatorID, long ipcuID) throws Exception {

		return (Ipcu) getEntityManager().createQuery(
				"Select t from Ipcu t where t.operator.operatorID='" + operatorID + "' and t.ipcuID='" + ipcuID + "'")
				.getSingleResult();
	}
	
	@Override
	public Ipcu findIpcuByOperator(long operatorID) throws Exception {
		return (Ipcu) getEntityManager().createQuery("Select t from Ipcu t where t.operator.operatorID='" + operatorID
				+ "'and t.ipcuCustomID=(Select MAX(t.ipcuCustomID) from Ipcu t)").getSingleResult();
	}

	@Override
	public List<Ipcu> findIpcusByAreaIDAndOperatorID(long areaID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from Ipcu t where t.area.areaID='" + areaID +"'")
				.getResultList();
	}

}
