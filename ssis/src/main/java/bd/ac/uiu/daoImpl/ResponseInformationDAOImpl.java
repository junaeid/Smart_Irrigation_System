package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.ResponseInformationDAO;
import bd.ac.uiu.entity.RequestInformation;
import bd.ac.uiu.entity.ResponseInformation;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class ResponseInformationDAOImpl extends EntityService<ResponseInformation> implements ResponseInformationDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ResponseInformation> findResponseInformationByOperatorID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from ResponseInformation t where t.operatorID='" + operatorID + "'ORDER BY t.responseID DESC")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ResponseInformation> findResListByOpAndCustID(long operatorID, long customerID) throws Exception {
		return getEntityManager().createQuery("Select t from ResponseInformation t where t.operatorID='" + operatorID
				+ "' and t.customerID='" + customerID + "'ORDER BY t.responseID DESC").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<ResponseInformation> findAllForCERAndOrg() throws Exception{
        return getEntityManager().createQuery("Select t from " + getEntityClass().getSimpleName() + " t ORDER BY t.responseID DESC").getResultList();
	}

}
