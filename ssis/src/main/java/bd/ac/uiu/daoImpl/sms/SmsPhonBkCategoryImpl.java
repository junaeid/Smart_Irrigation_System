package bd.ac.uiu.daoImpl.sms;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.sms.SmsPhonBkCategoryDao;
import bd.ac.uiu.entity.sms.SmsPhonBkCategory;
import bd.ac.uiu.generic.EntityService;

@Transactional
@Component
public class SmsPhonBkCategoryImpl extends EntityService<SmsPhonBkCategory> implements SmsPhonBkCategoryDao{

}
