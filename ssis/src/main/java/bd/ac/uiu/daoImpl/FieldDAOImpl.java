package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.FieldDAO;
import bd.ac.uiu.entity.Field;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class FieldDAOImpl extends EntityService<Field> implements FieldDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Field> findFieldsByOperator(long operatorID) throws Exception {

		return getEntityManager().createQuery("Select t from Field t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

}
