package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.SensorDAO;
import bd.ac.uiu.entity.Sensor;
import bd.ac.uiu.generic.EntityService;
@Component
@Transactional
public class SensorDAOImpl extends EntityService<Sensor> implements SensorDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Sensor> findSensorsByOperator(long operatorID) throws Exception {
		// TODO Auto-generated method stub
		return getEntityManager().createQuery("Select t from Sensor t where t.operator.operatorID='"+operatorID+"'").getResultList();
	}

}
