package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.sun.glass.ui.GestureSupport;

import bd.ac.uiu.dao.RequestInformationDAO;
import bd.ac.uiu.entity.RequestInformation;
import bd.ac.uiu.entity.Users;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class RequestInformationDAOImpl extends EntityService<RequestInformation> implements RequestInformationDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<RequestInformation> findRequestInformationByOperatorID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from RequestInformation t where t.operatorID='" + operatorID + "'ORDER BY t.requestInformationID DESC")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RequestInformation> findRequestInformationByOperatorIDAndCustomerID(long operatorID, long customerID)
			throws Exception {
		return getEntityManager().createQuery("Select t from RequestInformation t where t.operatorID='"
				+ operatorID + "' and t.customerID='" + customerID + "'ORDER BY t.requestInformationID DESC").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<RequestInformation> findAllForCERAndOrg() throws Exception{
        return getEntityManager().createQuery("Select t from " + getEntityClass().getSimpleName() + " t ORDER BY t.requestInformationID DESC").getResultList();
	}


}
