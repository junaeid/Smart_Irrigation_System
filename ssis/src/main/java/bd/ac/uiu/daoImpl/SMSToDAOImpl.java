package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.SMSToDAO;
import bd.ac.uiu.entity.SMSTo;
import bd.ac.uiu.generic.EntityService;
@Component
@Transactional
public class SMSToDAOImpl extends EntityService<SMSTo> implements SMSToDAO{

}
