package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.SMSTemplateDAO;
import bd.ac.uiu.entity.SMSTemplate;
import bd.ac.uiu.generic.EntityService;
@Component
@Transactional
public class SMSTemplateDAOImpl extends EntityService<SMSTemplate>  implements SMSTemplateDAO {

}
