package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.ContactPersonDAO;
import bd.ac.uiu.entity.ContactPerson;
import bd.ac.uiu.generic.EntityService;


@Component
@Transactional
public class ContactPersonDAOImpl extends EntityService<ContactPerson> implements ContactPersonDAO {

}
