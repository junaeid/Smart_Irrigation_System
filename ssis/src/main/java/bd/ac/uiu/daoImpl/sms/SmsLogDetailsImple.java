package bd.ac.uiu.daoImpl.sms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.sms.SmsLogDetailsDao;
import bd.ac.uiu.entity.sms.SmsLogDetails;
import bd.ac.uiu.generic.EntityService;

@Transactional
@Component
public class SmsLogDetailsImple extends EntityService<SmsLogDetails> implements SmsLogDetailsDao {
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<SmsLogDetails> findSmsLogUnsuccessStatusList(Object operatorID) {
		List<SmsLogDetails> list = new ArrayList<>();
		try {
			list = getEntityManager().createQuery(
					"Select t from SmsLogDetails t where t.recordStatus=1 and t.successfulStatus=0 and t.operatorID='"
							+ operatorID + "'")
					.getResultList();

			return list;
		}

		catch (Exception e) {

			System.out.println(e.getMessage());

			return list;
		}
	}

	@Transactional
	public int updateSmsLogDetails(Object smsLogID, Date date, Object monthName, Object year) {

		try {

			Query query;

			query = getEntityManager().createQuery(
					"Update SmsLogDetails t set t.successfulStatus=1, t.successfulDate=?1, t.successfulMonth=?2, t.sucessfulYear=?3 where t.smsLogID=?4");
			query.setParameter(1, date);
			query.setParameter(2, monthName);
			query.setParameter(3, year);
			query.setParameter(4, smsLogID);

			return query.executeUpdate();
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return 0;

	}

	@Transactional
	public int updateSmsLogDetailsBySmsFail(Object smsLogID, Date date, Object monthName, Object year) {

		try {

			Query query;

			query = getEntityManager().createQuery(
					"Update SmsLogDetails t set t.successfulStatus=2, t.successfulDate=?1, t.successfulMonth=?2, t.sucessfulYear=?3 where t.smsLogID=?4");
			query.setParameter(1, date);
			query.setParameter(2, monthName);
			query.setParameter(3, year);
			query.setParameter(4, smsLogID);

			return query.executeUpdate();
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return 0;

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<SmsLogDetails> findAllSmsLogByDateRang(Date fromDat, Date toDat, Object operatorID) throws Exception {

		Query query = entityManager.createQuery("select t from SmsLogDetails t" + " where t.recordStatus=1"
				+ " and t.sendDate between ?1 and ?2" + " and t.operatorID=?3");

		query.setParameter(1, fromDat);
		query.setParameter(2, toDat);
		query.setParameter(3, operatorID);

		return query.getResultList();
	}

}
