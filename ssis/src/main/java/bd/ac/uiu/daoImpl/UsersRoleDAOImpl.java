package bd.ac.uiu.daoImpl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.UsersRoleDAO;
import bd.ac.uiu.entity.UserRole;
import bd.ac.uiu.generic.EntityService;
/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */
@Component
@Transactional
public class UsersRoleDAOImpl extends EntityService<UserRole> implements UsersRoleDAO {

}
