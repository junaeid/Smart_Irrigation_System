package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.PaymentSummaryDAO;
import bd.ac.uiu.entity.PaymentSummary;
import bd.ac.uiu.generic.EntityService;
@Component
@Transactional
public class PaymentSummaryDAOImpl extends EntityService<PaymentSummary> implements PaymentSummaryDAO{

	@Override
	public PaymentSummary checkCustomerIDExistInPaymentSummary(long customerID) {
		try {
			return (PaymentSummary) entityManager
					.createQuery(
							"Select t from PaymentSummary t where t.customer.customerID=" + customerID + "")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void updateDepositPaymentSummary(long customerID, double amount) throws Exception {
		entityManager.createQuery("Update PaymentSummary t SET t.totalDepositeAmount= t.totalDepositeAmount+" + amount
				+ ", t.balanceAmount=t.totalDepositeAmount - t.totalExpenseAmount where t.customer.customerID=" + customerID
				+ "").executeUpdate();
		
	}

	@Override
	public void updateExpensePaymentSummary(long customerID, double amount) throws Exception {
		entityManager.createQuery("Update PaymentSummary t SET t.totalExpenseAmount= t.totalExpenseAmount+" + amount
				+ ", t.balanceAmount=t.totalDepositeAmount - t.totalExpenseAmount where t.customer.customerID=" + customerID
				+ "").executeUpdate();
		
	}

}
