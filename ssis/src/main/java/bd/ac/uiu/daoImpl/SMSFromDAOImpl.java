package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.SMSFromDAO;
import bd.ac.uiu.entity.SMSFrom;
import bd.ac.uiu.generic.EntityService;
@Component
@Transactional
public class SMSFromDAOImpl extends EntityService<SMSFrom> implements SMSFromDAO {

}
