package bd.ac.uiu.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.TransactionLogDAO;
import bd.ac.uiu.entity.TransactionLog;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class TransactionLogDAOImpl extends EntityService<TransactionLog> implements TransactionLogDAO {

	@SuppressWarnings("unchecked")
	public List<TransactionLog> findAllForCerAndOrg() throws Exception {
		return getEntityManager()
				.createQuery("Select t from " + getEntityClass().getSimpleName() + " t ORDER BY t.transationLogID DESC")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionLog> findAllTransactionLogByOperatorID(long operatorID) throws Exception {
		return getEntityManager().createQuery("Select t from TransactionLog t where t.operator.operatorID='"
				+ operatorID + "'ORDER BY t.transationLogID DESC").getResultList();
	}

	@Override
	public double findMaxTransactionByOpID(long operatorID, String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager()
						.createQuery("Select max(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "' and date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {
			}

		} else {
			try {
				value = (double) getEntityManager()
						.createQuery("Select max(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;
	}

	@Override
	public double findMinTransactionByOpID(long operatorID, String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager()
						.createQuery("Select min(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "' and date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (double) getEntityManager()
						.createQuery("Select min(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;
	}

	@Override
	public double findTotalTransactionByOpID(long operatorID, String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager()
						.createQuery("Select sum(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "' and date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {

			}

		} else {
			try {
				value = (double) getEntityManager()
						.createQuery("Select sum(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {

			}
		}
		return value;
	}

	@Override
	public double findAvgTransactionByOpID(long operatorID, String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager()
						.createQuery("Select avg(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "' and date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {

			}
		} else {
			try {
				value = (double) getEntityManager()
						.createQuery("Select avg(t.debitAmount)  from TransactionLog t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {

			}

		}
		return value;
	}

	@Override
	public double findMaxTransOrg(String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager().createQuery(
						"Select max(t.debitAmount)  from TransactionLog t where  date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {

			}
		} else {
			try {
				value = (double) getEntityManager().createQuery("Select max(t.debitAmount)  from TransactionLog t ")
						.getSingleResult();
			} catch (Exception e) {

			}

		}
		return value;
	}

	@Override
	public double findMinTransOrg(String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager().createQuery(
						"Select min(t.debitAmount)  from TransactionLog t where  date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {

			}
		} else {
			try {
				value = (double) getEntityManager().createQuery("Select min(t.debitAmount)  from TransactionLog t ")
						.getSingleResult();
			} catch (Exception e) {

			}

		}
		return value;
	}

	@Override
	public double findTotalTransOrg(String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager().createQuery(
						"Select sum(t.debitAmount)  from TransactionLog t where  date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {

			}
		} else {
			try {
				value = (double) getEntityManager().createQuery("Select sum(t.debitAmount)  from TransactionLog t ")
						.getSingleResult();
			} catch (Exception e) {

			}

		}
		return value;
	}

	@Override
	public double findAvgTransOrg(String date) throws Exception {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager().createQuery(
						"Select avg(t.debitAmount)  from TransactionLog t where  date(paymentDate)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {

			}
		} else {
			try {
				value = (double) getEntityManager().createQuery("Select avg(t.debitAmount)  from TransactionLog t ")
						.getSingleResult();
			} catch (Exception e) {

			}

		}
		return value;
	}

	@Override
	public TransactionLog duplicateOrNot(long operatorID, long customerID) throws Exception {

		System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		try {
			long id = (long) getEntityManager()
					.createQuery("Select max(t.transationLogID)  from TransactionLog t where t.operator.operatorID="
							+ operatorID + "and t.customer.customerID=" + customerID)
					.getSingleResult();

			System.out.println("transaction Id " + id);

			return (TransactionLog) getEntityManager()
					.createQuery("select t from TransactionLog t where t.transationLogID=" + id).getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionLog> sumOfTransactionByDate(String date) throws Exception {

		// return getEntityManager().createQuery("Select sum(t.debitAmount) as
		// debitAmount,t.operatorID,t.sender from TransactionLog t group by
		// t.operatorID").getResultList();
		/*
		 * return getEntityManager() .createQuery(
		 * "Select t.operator.operatorID,t.sender from TransactionLog t group by t.operator.operatorID "
		 * ).getResultList();
		 */
		/*
		 * double sum = (double) getEntityManager()
		 * .createQuery("Select sum(t.debitAmount) from TransactionLog where t.operator.operatorID ='"
		 * + 1 + "'") .getSingleResult();
		 * 
		 * System.out.println("SUM :" + sum);
		 */

		return getEntityManager().createQuery("Select t.debitAmount from TransactionLog t group by operatorID ")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionLog> findTransactionDateBetween(Date startDate, Date endDate, long operatorID)
			throws Exception {
		// TODO Auto-generated method stub
		return getEntityManager().createQuery(
				"' select t from TransactionLog t where date(paymentDate) BETWEEN : startDate '' AND endDate ''" + "'")
				.setParameter("startDate", startDate, TemporalType.DATE)
				.setParameter("endDate", endDate, TemporalType.DATE).getResultList();
	}

	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionLog> dateWiseTransactionLogList(long operatorID, Date fromDate, Date toDate)
			throws Exception {
		List<TransactionLog> list = new ArrayList<>();
		Query query = getEntityManager().createQuery(
				"Select t from TransactionLog t where t.paymentDate between ?1 and ?2 and t.operator.operatorID=?3");
		query.setParameter(1, fromDate);
		query.setParameter(2, toDate);
		query.setParameter(3, operatorID);
		list = query.getResultList();

		return list;
	}

}
