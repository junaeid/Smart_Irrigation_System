package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.CustomerDAO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class CustomerDAOImpl extends EntityService<Customer> implements CustomerDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> findCustomerByOperatorID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from Customer t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

	@Override
	public Customer findCustomerDetailByOperatorIDAndCustomerID(long operatorID, long customerID) throws Exception {
		return (Customer) getEntityManager().createQuery("Select t from Customer t where t.operator.operatorID='"
				+ operatorID + "' and t.customerID='" + customerID + "'").getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> searchCustomerByName(String customerName) throws Exception {
		return getEntityManager().createQuery("Select t from Customer t where t.customerName='" + customerName + "'")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> findLatestThreeCustomersByOperatorID(long operatorID) throws Exception {

		return getEntityManager()
				.createQuery(
						"Select t from(Select t from Customer t order by t.customerID desc limit 1,3) t order by customerID asc")
				.getResultList();
	}

	public long customerMaxID() throws Exception {
		return (long) getEntityManager().createQuery("SELECT max(o.customerID) FROM Customer o").getSingleResult();
	}

	@Override
	public List<Customer> seachCustomersByArea(long areaID) throws Exception {

		return getEntityManager().createQuery("Select t from Customer t where t.area.areaID='" + areaID + "'")
				.getResultList();
	}

}
