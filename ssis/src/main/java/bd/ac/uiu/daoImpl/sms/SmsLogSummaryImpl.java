package bd.ac.uiu.daoImpl.sms;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import bd.ac.uiu.dao.sms.SmsLogSummaryDao;
import bd.ac.uiu.entity.sms.SmsLogSummary;
import bd.ac.uiu.generic.EntityService;

@Transactional
@Component
public class SmsLogSummaryImpl extends EntityService<SmsLogSummary> implements SmsLogSummaryDao {
	@SuppressWarnings("unchecked")
	@Transactional
	public List<SmsLogSummary> findAllLogSummryByMonth(Object year, Object operatorID)
			throws Exception {
		
		Query query=entityManager.createQuery("select t from SmsLogSummary t"
				+ " where t.sendYear=?1"
				+ " and t.instituteID=?2"
				+ " and t.recordStatus=1");

		query.setParameter(1, year);
		query.setParameter(2, operatorID);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<SmsLogSummary> findAllSmsLogSumryByDatRng(Date fromDat,
			Date toDat, Object operatorID) throws Exception {

		Query query=entityManager.createQuery("select t from SmsLogSummary t"
				+ " where t.recordStatus=1"
				+ " and t.sendDate between ?1 and ?2"
				+ " and t.operatorID=?3");
		query.setParameter(1, fromDat);
		query.setParameter(2, toDat);
		query.setParameter(3, operatorID);
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<SmsLogSummary> findAllLogSummryByMonthYearWise(Object year)
			throws Exception {
		
		Query query=entityManager.createQuery("select t from SmsLogSummary t"
				+ " where t.sendYear=?1"
				+ " and t.recordStatus=1");

		query.setParameter(1, year);
		return query.getResultList();
	}
	

}
