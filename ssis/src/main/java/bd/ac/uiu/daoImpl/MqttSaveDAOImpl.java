package bd.ac.uiu.daoImpl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.MqttSaveDAO;
import bd.ac.uiu.entity.MqttSave;
import bd.ac.uiu.generic.EntityService;

@Transactional
@Component
public class MqttSaveDAOImpl extends EntityService<MqttSave> implements MqttSaveDAO {

}
