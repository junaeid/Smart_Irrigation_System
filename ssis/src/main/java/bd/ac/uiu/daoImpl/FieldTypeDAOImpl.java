package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.FieldTypeDAO;
import bd.ac.uiu.entity.FieldType;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class FieldTypeDAOImpl extends EntityService<FieldType> implements FieldTypeDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<FieldType> findFieldTypeByOperatorID(long operatorID) throws Exception {

		return getEntityManager().createQuery("Select t from FieldType t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

}
