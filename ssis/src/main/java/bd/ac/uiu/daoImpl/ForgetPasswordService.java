package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bd.ac.uiu.entity.ForgetPassword;


@Service
@Transactional
public class ForgetPasswordService extends bd.ac.uiu.generic.EntityService<ForgetPassword>{


	public ForgetPassword findForgetPassword(Object userName,Object resectCode){
		try{
		    return (ForgetPassword)getEntityManager().createQuery("select t from ForgetPassword t where t.userName='"+userName+"' and t.resetCode='"+resectCode+"' and t.active=true ").getSingleResult();    
		}
		
		catch(Exception e){
		return null;	
		}
		}
		
	

	public int  forgetPasswordAfterReset(Object userName){
		try{
	    
		return getEntityManager().createQuery("Update ForgetPassword t set t.active=false where t.userName='"+userName+"'").executeUpdate();    
	
		}
	
	catch(Exception e){
		return 0;	
		}
	
	}
	
	

}
