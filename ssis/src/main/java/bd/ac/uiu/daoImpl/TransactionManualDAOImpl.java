package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.TransactionManualDAO;
import bd.ac.uiu.entity.TransactionLog;
import bd.ac.uiu.entity.TransactionManual;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class TransactionManualDAOImpl extends EntityService<TransactionManual> implements TransactionManualDAO {

	@Override
	public TransactionManual findLatestTransactionManual() throws Exception {
		
		System.out.println("transactionManualID");
		try {
			long id = (long) getEntityManager()
					.createQuery("Select max(t.transactionManualID)  from TransactionManual t")
					.getSingleResult();

			System.out.println("transactionManualID Id " + id);

			return (TransactionManual) getEntityManager()
					.createQuery("select t from TransactionManual t where t.transactionManualID=" + id).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
