package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.CerDAO;
import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.generic.EntityService;

@Service
@Transactional
public class CerDAOImpl extends EntityService<Cer> implements CerDAO {
	
	@Transactional
	public Cer findCerByuserName(Object userName) throws Exception {     
	    return (Cer)getEntityManager().createQuery("SELECT t from Cer t, Users u where t.cerID=u.cer.cerID and u.username='"+userName+"'").getSingleResult();    
	}
	
	

}
