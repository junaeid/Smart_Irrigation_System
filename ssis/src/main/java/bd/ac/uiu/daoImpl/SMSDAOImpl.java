package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.SMSDAO;
import bd.ac.uiu.entity.SMS;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class SMSDAOImpl  extends EntityService<SMS> implements SMSDAO{

}
