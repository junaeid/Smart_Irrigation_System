package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.WaterCreditSummaryDAO;
import bd.ac.uiu.entity.WaterCreditSummary;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class WaterCreditSummaryDAOImpl extends EntityService<WaterCreditSummary> implements WaterCreditSummaryDAO {

	@Override
	public WaterCreditSummary checkCustomerIDExistInWaterCreditSummary(long customerID) {

		try {
			return (WaterCreditSummary) entityManager
					.createQuery("Select t from WaterCreditSummary t where t.customer.customerID=" + customerID + "")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void updateAvailableWaterCreditAdd(long customerID, double numberOfWaterCredit) throws Exception {
		entityManager.createQuery("Update WaterCreditSummary t SET t.availableWaterCredit= t.availableWaterCredit+"
				+ numberOfWaterCredit + " where t.customer.customerID=" + customerID + "").executeUpdate();
	}

	@Override
	public void updateUsedWaterCreditMinus(long customerID, double numberOfWaterCredit) throws Exception {
		entityManager.createQuery("Update WaterCreditSummary t SET t.usedWaterCredit= t.usedWaterCredit+"
				+ numberOfWaterCredit + ", t.availableWaterCredit=t.availableWaterCredit -" + numberOfWaterCredit
				+ "  where t.customer.customerID=" + customerID + "").executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WaterCreditSummary> findWaterCreditSummaryByOperatorID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from WaterCreditSummary t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

	public WaterCreditSummary findWaterCreditSummaryByOperatorIDAndCustomerID(long operatorID, long customerID)
			throws Exception {

		try {
			return (WaterCreditSummary) entityManager
					.createQuery("Select t from WaterCreditSummary t where t.operator.operatorID=" + operatorID
							+ "and t.customer.customerID=" + customerID + "")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WaterCreditSummary> findWacreSumByOpAndCustIDList(long operatorID, long customerID) throws Exception {

		return getEntityManager().createQuery("Select t from WaterCreditSummary t where t.operator.operatorID="
				+ operatorID + "and t.customer.customerID=" + customerID + "").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WaterCreditSummary> findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(long operatorID)
			throws Exception {

		return getEntityManager().createQuery(
				"Select t from WaterCreditSummary t where t.availableWaterCredit < 100 and  t.operator.operatorID='"
						+ operatorID + "'")
				.getResultList();
	}

	@Override
	public void updateWaterCreditEnableStatus(long operatorID, long customerID, boolean isEnable) throws Exception {
		entityManager.createQuery("Update WaterCreditSummary t SET t.enabled=" + isEnable
				+ " where t.operator.operatorID=" + operatorID + "and t.customer.customerID=" + customerID + "")
				.executeUpdate();
	}

	@Override
	public double maxWaterCreditSummaryByOpId(long operatorID, String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (int) getEntityManager().createQuery(
						"Select max(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "' and date(dateExecuted)='" + date + "'")
						.getSingleResult();

				System.out.println("From IF " + getEntityManager().createQuery(
						"Select max(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "' and date(dateExecuted)='" + date + "'")
						.getSingleResult());
			} catch (Exception e) {
			}
		} else {
			try {
				value = (int) getEntityManager().createQuery(
						"Select max(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();

				System.out.println("From Else " + getEntityManager().createQuery(
						"Select max(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult());
			} catch (Exception e) {
			}
		}
		return value;

	}

	@Override
	public double minWaterCreditSummaryByOpId(long operatorID, String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (int) getEntityManager().createQuery(
						"Select min(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "' and date(dateExecuted)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (int) getEntityManager().createQuery(
						"Select min(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;

	}

	@Override
	public double totalWaterCreditSummaryByOpId(long operatorID, String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (long) getEntityManager().createQuery(
						"Select sum(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "' and date(dateExecuted)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (long) getEntityManager().createQuery(
						"Select sum(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;

	}

	@Override
	public double avgWaterCreditSummaryByOpId(long operatorID, String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager().createQuery(
						"Select avg(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "' and date(dateExecuted)='" + date + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (double) getEntityManager().createQuery(
						"Select avg(t.availableWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
								+ operatorID + "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;

	}

	@Override
	public double maxWaterCreditSummary(String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (int) getEntityManager().createQuery(
						"Select max(t.availableWaterCredit)  from WaterCreditSummary t date(dateExecuted)='" + date
								+ "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (int) getEntityManager()
						.createQuery("Select max(t.availableWaterCredit)  from WaterCreditSummary t ")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;
	}

	@Override
	public double minWaterCreditSummary(String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (int) getEntityManager().createQuery(
						"Select min(t.availableWaterCredit)  from WaterCreditSummary t date(dateExecuted)='" + date
								+ "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (int) getEntityManager()
						.createQuery("Select min(t.availableWaterCredit)  from WaterCreditSummary t ")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;
	}

	@Override
	public double totalWaterCreditSummary(String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (long) getEntityManager().createQuery(
						"Select sum(t.availableWaterCredit)  from WaterCreditSummary t date(dateExecuted)='" + date
								+ "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (long) getEntityManager()
						.createQuery("Select sum(t.availableWaterCredit)  from WaterCreditSummary t ")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;
	}

	@Override
	public double avgWaterCreditSummary(String date) {
		double value = 0;
		if (date != null) {
			try {
				value = (double) getEntityManager().createQuery(
						"Select avg(t.availableWaterCredit)  from WaterCreditSummary t date(dateExecuted)='" + date
								+ "'")
						.getSingleResult();
			} catch (Exception e) {
			}
		} else {
			try {
				value = (double) getEntityManager()
						.createQuery("Select avg(t.availableWaterCredit)  from WaterCreditSummary t ")
						.getSingleResult();
			} catch (Exception e) {
			}
		}
		return value;
	}

	@Override
	public long findTotalWaterCreditByOperatorID(long operatorID) {

		return (long) getEntityManager()
				.createQuery("Select sum(t.usedWaterCredit)  from WaterCreditSummary t where t.operator.operatorID='"
						+ operatorID + "'")
				.getSingleResult();
	}

}
