package bd.ac.uiu.daoImpl.sms;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.sms.SmsPhonBkDao;
import bd.ac.uiu.entity.sms.SmsPhonBk;
import bd.ac.uiu.generic.EntityService;

@Transactional
@Component
public class SmsPhonBkImpl extends EntityService<SmsPhonBk> implements SmsPhonBkDao{

}
