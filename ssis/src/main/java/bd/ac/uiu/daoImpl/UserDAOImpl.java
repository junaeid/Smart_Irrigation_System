package bd.ac.uiu.daoImpl;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.UsersDAO;
import bd.ac.uiu.entity.Users;
import bd.ac.uiu.generic.EntityService;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

@Component
@Transactional
public class UserDAOImpl extends EntityService<Users> implements UsersDAO {
	public String checkUser(String username) {
		try {
			return (String) entityManager
					.createQuery("select t.username from Users t where t.username='" + username + "'")
					.getSingleResult();
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	public Users userByUserNameAndPassword(String username, String password) {

		return (Users) entityManager
				.createQuery(
						"select t from Users t where t.username='" + username + "' and t.password='" + password + "'")
				.getSingleResult();
	}

	@Override
	public Users findPasswordByUserName(String username) {

		return (Users) getEntityManager().createQuery("select t from Users t where t.username='" + username + "'")
				.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Users> findAllUsersByCerIDs(long cerID, String userType) throws Exception {
		return getEntityManager()
				.createQuery(
						"SELECT t from Users t where t.cer.cerID='" + cerID + "' and t.userType='" + userType + "'")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Users> findAllUsersByOrgIDs(long organizationID, String userType) throws Exception {
		return getEntityManager().createQuery("SELECT t from Users t where t.organization.organizationID='"
				+ organizationID + "' and t.userType='" + userType + "'").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Users> findAllUsersByOpIDs(long operatorID, String userType) throws Exception {
		return getEntityManager().createQuery("SELECT t from Users t where t.operator.operatorID='" + operatorID
				+ "' and t.userType='" + userType + "'").getResultList();
	}

	@Override
	public void updatePassword(String username, String password) throws Exception{
		
			entityManager
					.createQuery("Update Users t SET t.password='"+password+"' where t.username='"+username +"'")
					.executeUpdate();

		

	}

}
