package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.ForgetPasswordDao;
import bd.ac.uiu.entity.ForgetPassword;
import bd.ac.uiu.generic.EntityService;

@SuppressWarnings("unused")
@Component
@Transactional
public class ForgetPasswordImpl extends EntityService<ForgetPassword> implements ForgetPasswordDao {

	@Override
	public ForgetPasswordDao findForgetPassword(Object userName, Object resectCode) {
		return null;
	}

	@Override
	public int forgetPasswordAfterReset(Object userName) {
		return 0;
	}



	

}
