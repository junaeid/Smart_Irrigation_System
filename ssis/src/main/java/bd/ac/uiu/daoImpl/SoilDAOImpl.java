package bd.ac.uiu.daoImpl;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.SoilDAO;
import bd.ac.uiu.entity.Soil;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class SoilDAOImpl extends EntityService<Soil> implements SoilDAO{

	@Override
	public String checkSoil(String soilType) {
		try{
			return (String) entityManager.createQuery("select s.soilType from Soil s where s.soilType='"+soilType+"'")
					.getSingleResult();
		}catch(Exception e){
			System.out.println(e.getMessage());
			return null;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Soil> findSoilByOperator(long operatorID) throws Exception {
		
		return getEntityManager().createQuery("Select t from Soil t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

}
