package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.OperatorDAO;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.generic.EntityService;

@Service
@Transactional
public class OperatorDAOImpl extends EntityService<Operator> implements OperatorDAO {
	@Transactional
	public Operator findOperatorByuserName(Object userName) throws Exception {
		return (Operator) getEntityManager().createQuery(
				"SELECT t from Operator t, Users u where t.operatorID=u.operator.operatorID and u.username='" + userName
						+ "'")
				.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Operator> findAllOperatorsByOragnization(long organizationID) throws Exception {
		return getEntityManager()
				.createQuery("SELECT t from Operator t where t.enabled=1 and t.organization.organizationID='"
						+ organizationID + "' ")
				.getResultList();
	}

	@Override
	public long operatorMaxID() throws Exception {
		return getEntityManager().createQuery("SELECT max(o.operatorID) FROM Operator o").getFirstResult();
	}

	@Override
	public Operator findOperatorByOperatorID(long operatorID) throws Exception {

		return (Operator) getEntityManager()
				.createQuery("Select t from Operator t where t.operatorID='" + operatorID + "'")
				.getSingleResult();
	}

}
