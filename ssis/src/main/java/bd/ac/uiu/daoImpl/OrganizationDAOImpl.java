package bd.ac.uiu.daoImpl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.OrganizationDAO;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.generic.EntityService;

@Service
@Transactional
public class OrganizationDAOImpl extends EntityService<Organization> implements OrganizationDAO{

	@Transactional
	public Organization findOrganizationByuserName(Object userName) throws Exception {     
	    return (Organization)getEntityManager().createQuery("SELECT t from Organization t,Users u where t.organizationID=u.organization.organizationID and u.username='"+userName+"'").getSingleResult();    
	}

}
