package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.WaterCreditLogDAO;

import bd.ac.uiu.entity.WaterCreditLog;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class WaterCreditLogDAOImpl extends EntityService<WaterCreditLog> implements WaterCreditLogDAO {
	@SuppressWarnings("unchecked")
	public List<WaterCreditLog> findWaterCreditLogByOperatorID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from WaterCreditLog t where t.operator.operatorID='" + operatorID + "' ORDER BY t.waterCreditID DESC")
				.getResultList();
	}

	@Override
	public List<WaterCreditLog> findWaterCreditIsLessThenHunByOpID(long operatorID) throws Exception {
		return getEntityManager()
				.createQuery("Select t from WaterCreditLog t where t.availableWaterCredit < 100 and  t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<WaterCreditLog> findAllForCERAndOrg() throws Exception{
        return getEntityManager().createQuery("Select t from " + getEntityClass().getSimpleName() + " t ORDER BY t.waterCreditID DESC").getResultList();
	}
}
