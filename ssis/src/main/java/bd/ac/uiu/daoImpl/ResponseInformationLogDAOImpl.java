package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.ResponseInformationLogDAO;
import bd.ac.uiu.entity.ResponseInformationLog;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class ResponseInformationLogDAOImpl extends EntityService<ResponseInformationLog>
		implements ResponseInformationLogDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ResponseInformationLog> findRequestInformationLogByOperatorID(long operatorID) throws Exception {

		return getEntityManager()
				.createQuery("Select t from ResponseInformationLog t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

	@Override
	public ResponseInformationLog checkOperatorIDPumpIDCustomerIDAndFieldIDExistInResponseInformation(long operatorID,
			long ipcuID, long customerID, long fieldID) {
		try {
			return (ResponseInformationLog) entityManager
					.createQuery("Select t from ResponseInformationLog t where t.operator.operatorID=" + operatorID
							+ "and t.ipcu.ipcuID=" + ipcuID + "and t.customer.customerID=" + customerID
							+ "and t.field.fieldID=" + fieldID + "")
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void updateResponseInformation(int fieldValve, int sensorStatus, int nodeID, double balance, int mainValve,
			long fieldID, long operatorID, long ipcuID, long customerID) throws Exception {

		entityManager
				.createQuery("Update ResponseInformationLog t SET t.fieldValve=" + fieldValve + ",t.sensorStatus="
						+ sensorStatus + ",t.nodeID=" + nodeID + ",t.balance=" + balance + ",t.mainValve=" + mainValve
						+ "  where t.operator.operatorID=" + operatorID + "and t.ipcu.ipcuID=" + ipcuID
						+ "and t.customer.customerID=" + customerID + "and t.field.fieldID=" + fieldID + "")
				.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ResponseInformationLog> findResquestInformationListByOperatorIDAndCustomerID(long operatorID,
			long customerID) throws Exception {
		return getEntityManager().createQuery("Select t from ResponseInformationLog t where t.operator.operatorID='" + operatorID
				+ "' and t.customer.customerID='" + customerID + "'").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ResponseInformationLog> findResponseInformationListByOperatorIDAndIpcuID(long operatorID, long ipcuID)
			throws Exception {
		return getEntityManager().createQuery("Select t from ResponseInformationLog t where t.operator.operatorID='" + operatorID
				+ "' and t.ipcu.ipcuID='" + ipcuID + "' and DATE(t.dateExecuted) = CURDATE()").getResultList();
	}
	
	

}
