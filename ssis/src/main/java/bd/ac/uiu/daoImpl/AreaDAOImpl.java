package bd.ac.uiu.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import bd.ac.uiu.dao.AreaDAO;
import bd.ac.uiu.entity.Area;
import bd.ac.uiu.generic.EntityService;

@Component
@Transactional
public class AreaDAOImpl extends EntityService<Area> implements AreaDAO {

	@Override
	public String checkArea(String areaName) {
		try {
			return (String) entityManager
					.createQuery("select t.areaName from cer_area t where t.areaName='" + areaName + "'")
					.getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Area> findAreaByOperatorID(long operatorID) throws Exception {
		return getEntityManager().createQuery("Select t from Area t where t.operator.operatorID='" + operatorID + "'")
				.getResultList();
	}

}
