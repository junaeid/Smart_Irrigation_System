package bd.ac.uiu.mqtt;

import javax.faces.bean.ManagedProperty;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.stereotype.Service;

import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.FieldService;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.RequestInformationLogService;
import bd.ac.uiu.service.RequestInformationService;
import bd.ac.uiu.service.ResponseInformationLogService;
import bd.ac.uiu.service.ResponseInformationService;
import bd.ac.uiu.service.SmsService;
import bd.ac.uiu.service.TransactionLogService;
import bd.ac.uiu.service.WaterCreditLogService;
import bd.ac.uiu.service.WaterCreditSummaryService;

@Service
public class MqttService {

	@ManagedProperty("#{requestInformationService}")
	private RequestInformationService requestInformationService;

	@ManagedProperty("#{requestInformationLogService}")
	private RequestInformationLogService requestInformationLogService;

	@ManagedProperty("#{responseInformationService}")
	private ResponseInformationService responseInformationService;

	@ManagedProperty("#{responseInformationLogService}")
	private ResponseInformationLogService responseInformationLogService;

	@ManagedProperty("#{waterCreditSummaryService}")
	private WaterCreditSummaryService waterCreditSummaryService;

	@ManagedProperty("#{waterCreditLogService}")
	private WaterCreditLogService waterCreditLogService;

	@ManagedProperty("#{fieldService}")
	private FieldService fieldService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{transactionLogService}")
	private TransactionLogService transactionLogService;

	@ManagedProperty("#{smsService}")
	private SmsService smsService;

	public void runMqtt() throws MqttException {
		String url = "tcp://m13.cloudmqtt.com:13323";
		// String url = "tcp://172.16.9.23:1883";
		String clientID = "001";
		String password = "ZPd5pta5XVVr";
		String userName = "xptxwdwj";
		String subTopic = "xptxwdwj/feeds/trans";
		String pubTopic = "xptxwdwj/feeds/recv";

		String message = "test Message ";

		MqttSIS mqtt;
		try {
			mqtt = new MqttSIS(url, clientID, userName, password);
			mqtt.mqtt(pubTopic, subTopic, requestInformationService, responseInformationService,
					waterCreditSummaryService, waterCreditLogService, requestInformationLogService,
					responseInformationLogService, fieldService, ipcuService, operatorService, customerService,
					smsService, transactionLogService);
			RequestInformationService req = new RequestInformationService();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public RequestInformationService getRequestInformationService() {
		return requestInformationService;
	}

	public void setRequestInformationService(RequestInformationService requestInformationService) {
		this.requestInformationService = requestInformationService;
	}

	public ResponseInformationService getResponseInformationService() {
		return responseInformationService;
	}

	public void setResponseInformationService(ResponseInformationService responseInformationService) {
		this.responseInformationService = responseInformationService;
	}

	public WaterCreditSummaryService getWaterCreditSummaryService() {
		return waterCreditSummaryService;
	}

	public void setWaterCreditSummaryService(WaterCreditSummaryService waterCreditSummaryService) {
		this.waterCreditSummaryService = waterCreditSummaryService;
	}

	public WaterCreditLogService getWaterCreditLogService() {
		return waterCreditLogService;
	}

	public void setWaterCreditLogService(WaterCreditLogService waterCreditLogService) {
		this.waterCreditLogService = waterCreditLogService;
	}

	public RequestInformationLogService getRequestInformationLogService() {
		return requestInformationLogService;
	}

	public void setRequestInformationLogService(RequestInformationLogService requestInformationLogService) {
		this.requestInformationLogService = requestInformationLogService;
	}

	public ResponseInformationLogService getResponseInformationLogService() {
		return responseInformationLogService;
	}

	public void setResponseInformationLogService(ResponseInformationLogService responseInformationLogService) {
		this.responseInformationLogService = responseInformationLogService;
	}

	public FieldService getFieldService() {
		return fieldService;
	}

	public void setFieldService(FieldService fieldService) {
		this.fieldService = fieldService;
	}

	public IpcuService getIpcuService() {
		return ipcuService;
	}

	public void setIpcuService(IpcuService ipcuService) {
		this.ipcuService = ipcuService;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public SmsService getSmsService() {
		return smsService;
	}

	public void setSmsService(SmsService smsService) {
		this.smsService = smsService;
	}

	public TransactionLogService getTransactionLogService() {
		return transactionLogService;
	}

	public void setTransactionLogService(TransactionLogService transactionLogService) {
		this.transactionLogService = transactionLogService;
	}

}
