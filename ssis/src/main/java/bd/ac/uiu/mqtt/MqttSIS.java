package bd.ac.uiu.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import com.sun.jmx.snmp.Timestamp;

import bd.ac.uiu.api.bKash.BkashApi;
import bd.ac.uiu.decesion.Decesion;
import bd.ac.uiu.decesion.FieldStatusBruteForce;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.RequestInformationDTO;
import bd.ac.uiu.dto.RequestInformationLogDTO;
import bd.ac.uiu.dto.ResponseInformationDTO;
import bd.ac.uiu.dto.ResponseInformationLogDTO;
import bd.ac.uiu.dto.TransactionLogDTO;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.FieldService;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.RequestInformationLogService;
import bd.ac.uiu.service.RequestInformationService;
import bd.ac.uiu.service.ResponseInformationLogService;
import bd.ac.uiu.service.ResponseInformationService;
import bd.ac.uiu.service.SmsService;
import bd.ac.uiu.service.TransactionLogService;
import bd.ac.uiu.service.WaterCreditLogService;
import bd.ac.uiu.service.WaterCreditSummaryService;

@SuppressWarnings("restriction")
public class MqttSIS implements MqttCallback {

	private static String str = "";

	FieldStatusBruteForce fieldStatusBruteForce;
	private MqttClient client;
	private MqttConnectOptions connectOptions;
	private String brokerUrl;
	private boolean clean;
	private String password;
	private String userName;
	Decesion decesion;
	RequestInformationDTO requestInformationDTO;
	ResponseInformationDTO responseInformationDTO;
	RequestInformationLogDTO requestInformationLogDTO;
	ResponseInformationLogDTO responseInformationLogDTO;

	private static int qos = 2;

	public void mqtt(String pubTopic, String subTopic, RequestInformationService requestInformationService,
			ResponseInformationService responseInformationService, WaterCreditSummaryService waterCreditSummaryService,
			WaterCreditLogService waterCreditLogService, RequestInformationLogService requestInformationLogService,
			ResponseInformationLogService responseInformationLogService, FieldService fieldService,
			IpcuService ipcuService, OperatorService operatorService, CustomerService customerService,
			SmsService smsService, TransactionLogService transactionLogService) throws MqttException {
		try {
			boolean tr = false;
			doConnect();
			while (!tr) {
				if (!str.equals("") && str.length() == 73) {
					String packet = str;

					fieldStatusBruteForce = new FieldStatusBruteForce();
					fieldStatusBruteForce.setPacket(packet);
					boolean generatedValueCheck = fieldStatusBruteForce.generateValue();

					System.out.println(fieldStatusBruteForce.getFieldStatusObj().toString());

					if (generatedValueCheck == true) {

						requestInformationDTO = new RequestInformationDTO();

						requestInformationDTO.setOperatorID(fieldStatusBruteForce.getFieldStatusObj().getOperator());
						requestInformationDTO.setIpcuID(fieldStatusBruteForce.getFieldStatusObj().getIpcu());
						requestInformationDTO.setCustomerID(fieldStatusBruteForce.getFieldStatusObj().getCustomer());
						requestInformationDTO.setFieldID(fieldStatusBruteForce.getFieldStatusObj().getField());

						requestInformationDTO
								.setSoilMoisture(fieldStatusBruteForce.getFieldStatusObj().getSoilMoisture());
						requestInformationDTO.setMotorIpcu(fieldStatusBruteForce.getFieldStatusObj().getMotorPump());
						requestInformationDTO
								.setWaterLevelSensor(fieldStatusBruteForce.getFieldStatusObj().getWaterLevel());
						requestInformationDTO.setFieldValve(fieldStatusBruteForce.getFieldStatusObj().getFieldValve());
						requestInformationDTO.setMainValve(fieldStatusBruteForce.getFieldStatusObj().getMainValve());
						requestInformationDTO
								.setWaterCredit(fieldStatusBruteForce.getFieldStatusObj().getWaterCredit());
						requestInformationDTO.setFlowRate(fieldStatusBruteForce.getFieldStatusObj().getFlowRate());
						requestInformationDTO
								.setTemperature(fieldStatusBruteForce.getFieldStatusObj().getTemperature());
						requestInformationDTO.setHumidity(fieldStatusBruteForce.getFieldStatusObj().getHumidity());
						requestInformationDTO
								.setDeviceStatus(fieldStatusBruteForce.getFieldStatusObj().getSensorStatus());
						requestInformationDTO.setNodeID(fieldStatusBruteForce.getFieldStatusObj().getNodeID());
						requestInformationDTO.setSeason(fieldStatusBruteForce.getFieldStatusObj().getSeason());

						requestInformationLogDTO = new RequestInformationLogDTO();

						OperatorDTO operatorDTO = new OperatorDTO();
						operatorDTO.setOperatorID(fieldStatusBruteForce.getFieldStatusObj().getOperator());
						requestInformationLogDTO.setOperatorDTO(operatorDTO);

						IpcuDTO ipcuDTO = new IpcuDTO();
						ipcuDTO.setIpcuID(fieldStatusBruteForce.getFieldStatusObj().getIpcu());
						requestInformationLogDTO.setIpcuDTO(ipcuDTO);

						CustomerDTO customerDTO = new CustomerDTO();
						customerDTO.setCustomerID(fieldStatusBruteForce.getFieldStatusObj().getCustomer());
						requestInformationLogDTO.setCustomerDTO(customerDTO);

						FieldDTO fieldDTO = new FieldDTO();
						fieldDTO.setFieldID(fieldStatusBruteForce.getFieldStatusObj().getField());
						requestInformationLogDTO.setFieldDTO(fieldDTO);

						requestInformationLogDTO
								.setSoilMoisture(fieldStatusBruteForce.getFieldStatusObj().getSoilMoisture());
						requestInformationLogDTO.setMotoIPCU(fieldStatusBruteForce.getFieldStatusObj().getMotorPump());
						requestInformationLogDTO
								.setWaterLevelSensor(fieldStatusBruteForce.getFieldStatusObj().getWaterLevel());
						requestInformationLogDTO
								.setFieldValve(fieldStatusBruteForce.getFieldStatusObj().getFieldValve());
						requestInformationLogDTO.setMainValve(fieldStatusBruteForce.getFieldStatusObj().getMainValve());
						requestInformationLogDTO
								.setWaterCredit(fieldStatusBruteForce.getFieldStatusObj().getWaterCredit());
						requestInformationLogDTO.setFlowRate(fieldStatusBruteForce.getFieldStatusObj().getFlowRate());
						requestInformationLogDTO
								.setTemperature(fieldStatusBruteForce.getFieldStatusObj().getTemperature());
						requestInformationLogDTO.setHumidity(fieldStatusBruteForce.getFieldStatusObj().getHumidity());
						requestInformationLogDTO
								.setDeviceStatus(fieldStatusBruteForce.getFieldStatusObj().getSensorStatus());
						requestInformationLogDTO.setNodeID(fieldStatusBruteForce.getFieldStatusObj().getNodeID());
						requestInformationLogDTO.setSeason(fieldStatusBruteForce.getFieldStatusObj().getSeason());

						decesion = new Decesion();

						BkashApi bkashApi = new BkashApi();

						String reference = decesion.getReferenceID(
								Integer.toString(fieldStatusBruteForce.getFieldStatusObj().getOperator()),
								Integer.toString(fieldStatusBruteForce.getFieldStatusObj().getCustomer()));
						bkashApi.setReference(reference);

						bkashApi.bKashServiceWithDate();

						if (bkashApi.getBkashTransactionData() != null) {
							TransactionLogDTO transactionLogDTO = new TransactionLogDTO();

							transactionLogDTO.setPaymentTypeName("bKash");
							transactionLogDTO
									.setAmount(Double.parseDouble(bkashApi.getBkashTransactionData().getAmount()));
							transactionLogDTO.setCounter(bkashApi.getBkashTransactionData().getCounter());
							transactionLogDTO.setTrxStatus(bkashApi.getBkashTransactionData().getTrxStatus());
							transactionLogDTO.setTrxId(bkashApi.getBkashTransactionData().getTrxId());
							transactionLogDTO.setReference(bkashApi.getBkashTransactionData().getReference());
							transactionLogDTO.setSender(bkashApi.getBkashTransactionData().getSender());
							transactionLogDTO.setService(bkashApi.getBkashTransactionData().getService());
							transactionLogDTO.setCurrency(bkashApi.getBkashTransactionData().getCurrency());
							transactionLogDTO.setReceiver(bkashApi.getBkashTransactionData().getReceiver());
							transactionLogDTO.setTrxTimeStamp(bkashApi.getBkashTransactionData().getTrxTimeStamp());
							transactionLogDTO.setOperatorDTO(operatorDTO);
							transactionLogDTO.setCustomerDTO(customerDTO);
							transactionLogDTO
									.setDebitAmount(Double.parseDouble(bkashApi.getBkashTransactionData().getAmount()));

							if (transactionLogService.paymentStatusExistOrNot(
									fieldStatusBruteForce.getFieldStatusObj().getOperator(),
									fieldStatusBruteForce.getFieldStatusObj().getCustomer(),
									transactionLogDTO.getTrxTimeStamp())) {
								try {
									transactionLogService.saveTransaction(transactionLogDTO);
									System.out.println("ccccccccccccccccccccccccccccccccccccccc");
								} catch (Exception e2) {
									// TODO Auto-generated catch block

								}
							}
						} else {

						}

						decesion.setUpdatePacket(fieldStatusBruteForce.getUpdatePacket());

						if (decesion.setFieldStatusObjSuscribe(fieldStatusBruteForce.getFieldStatusObj(),
								waterCreditSummaryService, waterCreditLogService, responseInformationLogService,
								fieldService, ipcuService, operatorService, customerService, smsService,
								transactionLogService) == true) {

							

							System.out.println(decesion.getDecesionPacket());

							responseInformationDTO = new ResponseInformationDTO();

							responseInformationDTO.setOperatorID(decesion.getFieldStatusObjPublish().getOperator());
							responseInformationDTO.setIpcuID(decesion.getFieldStatusObjPublish().getIpcu());
							responseInformationDTO.setCustomerID(decesion.getFieldStatusObjPublish().getCustomer());
							responseInformationDTO.setFieldID(decesion.getFieldStatusObjPublish().getField());
							responseInformationDTO.setFieldVulve(decesion.getFieldStatusObjPublish().getFieldValve());
							responseInformationDTO.setMainVulve(decesion.getFieldStatusObjPublish().getMainValve());
							responseInformationDTO
									.setSensorStatus(decesion.getFieldStatusObjPublish().getSensorStatus());
							responseInformationDTO.setNodeID(decesion.getFieldStatusObjPublish().getNodeID());
							responseInformationDTO
									.setBalance(decesion.getWaterCreditSummaryDTO().getAvailableWaterCredit());
							System.out.println("decesion.getWaterCreditSummaryDTO().getAvailableWaterCredit()"
									+ decesion.getWaterCreditSummaryDTO().getAvailableWaterCredit());
							responseInformationDTO.setPaymentSummaryID(1);// Manual

							responseInformationLogDTO = new ResponseInformationLogDTO();

							operatorDTO.setOperatorID(decesion.getFieldStatusObjPublish().getOperator());

							ipcuDTO.setIpcuID(decesion.getFieldStatusObjPublish().getIpcu());

							customerDTO.setCustomerID(decesion.getFieldStatusObjPublish().getCustomer());

							fieldDTO.setFieldID(decesion.getFieldStatusObjPublish().getField());

							responseInformationLogDTO.setOperatorDTO(operatorDTO);
							responseInformationLogDTO.setIpcuDTO(ipcuDTO);
							responseInformationLogDTO.setCustomerDTO(customerDTO);
							responseInformationLogDTO.setFieldDTO(fieldDTO);
							responseInformationLogDTO
									.setFieldValve(decesion.getFieldStatusObjPublish().getFieldValve());
							responseInformationLogDTO.setMainValve(decesion.getFieldStatusObjPublish().getMainValve());
							responseInformationLogDTO
									.setSensorStatus(decesion.getFieldStatusObjPublish().getSensorStatus());
							responseInformationLogDTO.setNodeID(decesion.getFieldStatusObjPublish().getNodeID());
							responseInformationLogDTO
									.setBalance(decesion.getWaterCreditSummaryDTO().getAvailableWaterCredit());

							try {
								requestInformationService.saveRequestInformation(requestInformationDTO);
								requestInformationLogService.saveRequestInformationLog(requestInformationLogDTO);
								requestInformationDTO = null;
								requestInformationLogDTO = null;
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							try {
								responseInformationService.saveResponseInformation(responseInformationDTO);
								responseInformationLogService.saveResponseInformationLog(responseInformationLogDTO);
								responseInformationDTO = null;
								responseInformationLogDTO = null;

							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							doSubscribe(subTopic, qos);
						}
					} else {
						System.out
								.println("..................This date is not available in the System................");
						doSubscribe(subTopic, qos);
					}

					doPublish(pubTopic, qos, decesion.getDecesionPacket().getBytes());

					str = "";
				} else {
					doSubscribe(subTopic, qos);
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			doSubscribe(subTopic, qos);
		} finally {
			mqtt(pubTopic, subTopic, requestInformationService, responseInformationService, waterCreditSummaryService,
					waterCreditLogService, requestInformationLogService, responseInformationLogService, fieldService,
					ipcuService, operatorService, customerService, smsService, transactionLogService);
		}
	}

	public MqttSIS() {
	}

	public MqttSIS(String brokerUrl, String clientID, String userName, String password) throws MqttException {
		this.brokerUrl = brokerUrl;
		this.password = password;
		this.userName = userName;

		this.clean = true;

		String tmpDir = System.getProperty("java.io.tmpdir");

		MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir);
		try {
			connectOptions = new MqttConnectOptions();
			connectOptions.setCleanSession(clean);
			if (password != null) {
				connectOptions.setPassword(this.password.toCharArray());
			}
			if (userName != null) {
				connectOptions.setUserName(this.userName);
			}
			client = new MqttClient(this.brokerUrl, clientID, dataStore);
			client.setCallback(this);
		} catch (MqttException me) {
			me.printStackTrace();
			log("unable to set up client : " + me.toString());
			System.exit(1);
		}
	}

	private void doConnect() throws MqttException {
		log("Connecting to " + brokerUrl + " with client ID " + client.getClientId());
		client.connect(connectOptions);
		log("Connected");
	}

	private void doSubscribe(String topic, int qos) throws MqttException {
		client.subscribe(topic, qos);
	}

	private void doPublish(String topic, int qos, byte[] payload) throws MqttException {

		MqttMessage message = new MqttMessage(payload);
		message.setQos(qos);

		client.publish(topic, message);
	}

	private void doDisconnect() throws MqttException {
		client.disconnect();
		log("Disconnected");
	}

	private void log(String message) {
		System.out.println(message);
	}

	public void connectionLost(Throwable cause) {

	}

	@SuppressWarnings("restriction")
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String time = new Timestamp(System.currentTimeMillis()).toString();
		System.out.println("Time : \t " + time + "Topic:\t " + topic + "Message :\t " + new String(message.getPayload())
				+ " Qos :\t " + message.getQos());
		str = String.valueOf(message);
	}

	public void deliveryComplete(IMqttDeliveryToken token) {

	}
}
