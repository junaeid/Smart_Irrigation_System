package bd.ac.uiu.utility;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import bd.ac.uiu.entity.UserRole;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

public class ApplicationUtility {


	public static String getCerOrOrgOrOpID() {

		FacesContext context = FacesContext.getCurrentInstance();

		String cerOrOrgOrOpID = context.getExternalContext().getSessionMap().get("CerOrOrgOrOpID").toString();

		return cerOrOrgOrOpID;

	}
	
	public static String getPagetitle() {

		FacesContext context = FacesContext.getCurrentInstance();

		String pagetitle = context.getExternalContext().getSessionMap().get("Pagetitle").toString();

		return pagetitle;

	}
	
	
	public static String getIpAddress() {

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();

		String remothost = request.getRemoteHost();

		return remothost;

	}

	public static String getUserID() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userID = auth.getName();

		return userID;

	}
	
	public static String getUserRole() {
		FacesContext context = FacesContext.getCurrentInstance();
		String userRole = null;
		try {
			userRole = context.getExternalContext().getSessionMap().get("UserRole").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return userRole;
	}

	public static String getUserType() {
		FacesContext context = FacesContext.getCurrentInstance();
		String userType = null;
		try {
			userType = context.getExternalContext().getSessionMap().get("UserType").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return userType;
	}

}
