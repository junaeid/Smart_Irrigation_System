package bd.ac.uiu.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
/**
 * 
 * @author Rajaul Islam
 *
 */

public class PasswordEncryption {
	
	final static Logger logger = Logger.getLogger(PasswordEncryption.class);
	
	
	private static MessageDigest md;

	   public String cryptWithMD5(String pass){
	    try {
	        md = MessageDigest.getInstance("MD5");
	        byte[] passBytes = pass.getBytes();
	        md.reset();
	        byte[] digested = md.digest(passBytes);
	        StringBuilder sb = new StringBuilder();
	        for(int i=0;i<digested.length;i++){
	            sb.append(Integer.toHexString(0xff & digested[i]));
	        }
	        return sb.toString();
	    } catch (NoSuchAlgorithmException ex) {
	    	logger.debug("This is debug :" + ex.getMessage());
			logger.error("This is error : " + ex);
	    }
	        return null;


	   }

	   
	   public String  getHashPassword(String password) {  
		  
		   Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();  
		   
		   String hashedPassword = passwordEncoder.encodePassword(password, null);
		   
		   System.out.println(hashedPassword);  
		   
		   return hashedPassword;  
		  
	   }  
}
