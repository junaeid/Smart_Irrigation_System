package bd.ac.uiu.utility;

import java.io.Serializable;
import java.net.InetAddress;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import bd.ac.uiu.dao.CerDAO;
import bd.ac.uiu.dao.OperatorDAO;
import bd.ac.uiu.dao.OrganizationDAO;
import bd.ac.uiu.dao.UsersDAO;
import bd.ac.uiu.dao.UsersRoleDAO;
import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.entity.UserRole;
import bd.ac.uiu.entity.Users;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

@ManagedBean
@Scope("session")
@Controller(value = "sessionsData")
public class SessionData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long cerID;
	private String cerName;
	private long organizationID;
	private String organizationName;
	private String userName;
	private long operatorID;
	private String operatorName;
	private String pageRedirector;

	private Cer cer;
	private Organization organization;
	private Operator operator;
	private Users user;
	private UserRole userRole;
	private String pagetitle;
	private long cerOrOrgOrOpID;

	@Autowired
	UsersDAO usersDAO;

	@Autowired
	CerDAO cerDAO;

	@Autowired
	OrganizationDAO organizationDAO;

	@Autowired
	OperatorDAO operatorDAO;
	
	@Autowired
	UsersRoleDAO usersRoleDAO;

	public void findCerOrgOpInfo() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Object username = auth.getName();

		FacesContext context = FacesContext.getCurrentInstance();

		try {
			context.getExternalContext().getSessionMap().put("UserName", username);

			this.user = new Users();
			this.user = (Users) usersDAO.findSingleObjectByProperty("username", username);
			System.out.println("UserName1001... " + username);
			System.out.println("user1002... " + user.getUserType());

			if (user.getUserType() != "")
				context.getExternalContext().getSessionMap().put("UserType", user.getUserType());

			this.userRole = new UserRole();
			this.userRole = (UserRole) usersRoleDAO.findSingleObjectByProperty("username", username);
			
			System.out.println("role... " + userRole.getRolename());
			if( userRole.getRolename() != null )
				context.getExternalContext().getSessionMap().put("UserRole",  userRole.getRolename());
			
			if (user.getUserType().equals("cer")) {

				cer = new Cer();
				cer = cerDAO.findCerByuserName(username);

				System.out.println("cer name " + cer.getCerName());
				if (pagetitle == null) {
					pagetitle = cer.getCerName();
					context.getExternalContext().getSessionMap().put("Pagetitle", pagetitle);

				}
				if (cerOrOrgOrOpID == 0) {
					cerOrOrgOrOpID = cer.getCerID();
					context.getExternalContext().getSessionMap().put("CerOrOrgOrOpID", cerOrOrgOrOpID);
				}
				if (cer.getCerID() != 0)
					context.getExternalContext().getSessionMap().put("CerID", cer.getCerID());

				if (cer.getCerName() != "")
					context.getExternalContext().getSessionMap().put("CerName", cer.getCerName());
				if (cer.getIpExecuted() != "")
					context.getExternalContext().getSessionMap().put("ipAddress", getIp());
			}
			if (user.getUserType().equals("organization")) {
				organization = organizationDAO.findOrganizationByuserName(username);
				System.out.println("organization name " + organization.getOrganizationName());
				if (pagetitle == null) {
					pagetitle = organization.getOrganizationName();
					context.getExternalContext().getSessionMap().put("Pagetitle", pagetitle);

				}
				if (cerOrOrgOrOpID == 0) {
					cerOrOrgOrOpID = organization.getOrganizationID();
					context.getExternalContext().getSessionMap().put("CerOrOrgOrOpID", cerOrOrgOrOpID);
				}
				if (organization.getOrganizationID() != 0)
					context.getExternalContext().getSessionMap().put("OrganizationID",
							organization.getOrganizationID());

				if (cer.getCerName() != "")
					context.getExternalContext().getSessionMap().put("OrganizationName",
							organization.getOrganizationName());
				if (cer.getIpExecuted() != "")
					context.getExternalContext().getSessionMap().put("ipAddress", getIp());

			}
			if (user.getUserType().equals("operator")) {
				operator = operatorDAO.findOperatorByuserName(username);
				System.out.println("operator name " + operator.getOperatorName());
				if (pagetitle == null) {
					pagetitle = operator.getOperatorName();
					context.getExternalContext().getSessionMap().put("Pagetitle", pagetitle);
				}
				if (cerOrOrgOrOpID == 0) {
					cerOrOrgOrOpID = operator.getOperatorID();
					context.getExternalContext().getSessionMap().put("CerOrOrgOrOpID", cerOrOrgOrOpID);
				}
				if (operator.getOperatorID() != 0)
					context.getExternalContext().getSessionMap().put("OperatorID", operator.getOperatorID());

				if (operator.getOperatorName() != "")
					context.getExternalContext().getSessionMap().put("OperatorName", operator.getOperatorName());
				if (cer.getIpExecuted() != "")
					context.getExternalContext().getSessionMap().put("ipAddress", getIp());

			}

		} catch (Exception e) {
			System.out.println("" + e.getMessage());
		}

	}

	public String getIp() {
		FacesContext context1 = FacesContext.getCurrentInstance();

		HttpServletRequest request = (HttpServletRequest) context1.getExternalContext().getRequest();

		String ipAddress = request.getRemoteAddr();
		return ipAddress;

	}

	public String findIPAddress(String x) throws Exception {
		InetAddress addr = InetAddress.getByName(x);
		return addr.getHostAddress();
	}

	public long getCerID() {
				return cerID;
	}

	public void setCerID(long cerID) {

		this.cerID = cerID;
	}

	public String getCerName() {
				return cerName;
	}

	public void setCerName(String cerName) {
		this.cerName = cerName;
	}

	public long getOrganizationID() {
				return organizationID;
	}

	public void setOrganizationID(long organizationID) {
		this.organizationID = organizationID;
	}

	public String getOrganizationName() {
				return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public long getOperatorID() {
				return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public String getOperatorName() {
				return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getPageRedirector() {
		findCerOrgOpInfo();
		System.out.println("Page Is redirected.");
		return pageRedirector;
	}

	public void setPageRedirector(String pageRedirector) {
		this.pageRedirector = pageRedirector;
	}

	public Cer getCer() {
		if (cer == null) {

			cer = new Cer();
		}
		return cer;
	}

	public void setCer(Cer cer) {
		this.cer = cer;
	}

	public Organization getOrganization() {
		if (organization == null) {

			organization = new Organization();
		}
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Operator getOperator() {
		if (operator == null) {
			operator = new Operator();
		}
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Users getUser() {
		if (user == null) {

			user = new Users();
		}
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getUserName() {
		userName = ApplicationUtility.getUserID();
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPagetitle() {
		return pagetitle;
	}

	public void setPagetitle(String pagetitle) {
		this.pagetitle = pagetitle;
	}

	public long getCerOrOrgOrOpID() {
		return cerOrOrgOrOpID;
	}

	public void setCerOrOrgOrOpID(long cerOrOrgOrOpID) {
		this.cerOrOrgOrOpID = cerOrOrgOrOpID;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	

}
