package bd.ac.uiu.utility;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletResponse;

import com.mysql.fabric.Response;

@ManagedBean
@ViewScoped
public class SessionCheckerController {

	@PostConstruct
	public void init() {
		try {
			getDataFromSessionasTest();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String pagetitle;

	private long cerOrOrgOrOpIDLogin;

	public void getDataFromSessionasTest() throws Exception {

		pagetitle = ApplicationUtility.getPagetitle();
		System.out.println("Cer name as test from Session: " + pagetitle);
		cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		System.out.println("cerOrOrgOrOpIDLogin as Long from Session: " + cerOrOrgOrOpIDLogin);
		//////////////////////////////////////////////////

		/*
		 * 
		 * if you need login cer or operator or organization ID, then just use
		 * bellow line in service save method
		 * 
		 * long cerOrOrgOrOpIDLogin=Long.parseLong(ApplicationUtility.
		 * getCerOrOrgOrOpID());
		 * 
		 * 
		 */
		///////////////////////////////////

		String userType = ApplicationUtility.getUserType();
		System.out.println("userType... from Session: " + userType);

		String userRoleName = ApplicationUtility.getUserRole();
		System.out.println("userRoleName... from Session: " + userRoleName);



	}

	public String getPagetitle() {
		return pagetitle;
	}

	public void setPagetitle(String pagetitle) {
		this.pagetitle = pagetitle;
	}

	public long getCerOrOrgOrOpIDLogin() {
		return cerOrOrgOrOpIDLogin;
	}

	public void setCerOrOrgOrOpIDLogin(long cerOrOrgOrOpIDLogin) {
		this.cerOrOrgOrOpIDLogin = cerOrOrgOrOpIDLogin;
	}

}
