package bd.ac.uiu.utility.validation.test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.validator.FacesValidator;


@ManagedBean
@RequestScoped
public class TestController {


	private Test test;

	public Test getTest() {
		if(test == null ){
			
			test=new Test();
		}
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}
	
	
}
