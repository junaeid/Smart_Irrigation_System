package bd.ac.uiu.utility.validation.test;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import javax.validation.constraints.*;



@Entity
@ManagedBean
@RequestScoped
public class Test {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Size(min=2,max=5, message="okkkkkkkkkk")
	@Column(name = "name")
    private String name;
     
    @Min(10) @Max(20)
    @Column(name = "age")
    private Integer age;
     
    @DecimalMax(value= "99.9", message = "Shold not exceed 99.9")
    @Column(name = "amount")
    private Double amount;
     
    @Digits(integer=3,fraction=2)
    @Column(name = "amount2")
    private Double amount2;
     
    @AssertTrue
    @Column(name = "checked")
    private boolean checked;
 
    @Past
    @Column(name = "pastDate")
    private Date pastDate;
     
    @Future
    @Column(name = "futureDate")
    private Date futureDate;
     
    @Pattern(regexp="^[-+]?\\d+$")
    @Column(name = "pattern")
    private String pattern;
 
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
 
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
 
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }
 
    public Double getAmount2() {
        return amount2;
    }
    public void setAmount2(Double amount2) {
        this.amount2 = amount2;
    }
 
    public boolean isChecked() {
        return checked;
    }
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
 
    public Date getPastDate() {
        return pastDate;
    }
    public void setPastDate(Date pastDate) {
        this.pastDate = pastDate;
    }
 
    public Date getFutureDate() {
        return futureDate;
    }
    public void setFutureDate(Date futureDate) {
        this.futureDate = futureDate;
    }
 
    public String getPattern() {
        return pattern;
    }
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
	
	
}
