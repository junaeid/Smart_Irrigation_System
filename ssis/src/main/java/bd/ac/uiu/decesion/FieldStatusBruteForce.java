package bd.ac.uiu.decesion;

public class FieldStatusBruteForce {

	private String packet;
	private FieldStatus fieldStatus;
	private String[] updatePacket;
	private FieldStatusObj fieldStatusObj;
	private char[] bitPacket;

	public FieldStatusBruteForce() {

	}

	public String[] getUpdatePacket() {
		return updatePacket;
	}

	public FieldStatusBruteForce(String packet) {
		this.packet = packet;
	}

	public String getPacket() {
		return packet;
	}

	public void setPacket(String packet) {
		this.packet = packet;
		processPacket();
	}

	public void processPacket() {
		String newPacket = packet.substring(1, packet.length() - 1);
		updatePacket = newPacket.split("\\.");
		savePacketValue();
	}

	public void savePacketValue() {
		fieldStatus = new FieldStatus(updatePacket[0], updatePacket[1], updatePacket[2], updatePacket[3],
				updatePacket[4], updatePacket[5], updatePacket[6], updatePacket[7], updatePacket[8], updatePacket[9],
				updatePacket[10], updatePacket[11], updatePacket[12], updatePacket[13], updatePacket[14],
				updatePacket[15]);
		

	}

	public boolean generateValue() {
		fieldStatusObj = new FieldStatusObj();

		try {
			fieldStatusObj.setOperator(
					Integer.parseInt(fieldStatus.getOperator().substring(1, fieldStatus.getSensorStatus().length())));
			fieldStatusObj
					.setIpcu(Integer.parseInt(fieldStatus.getIpcu().substring(1, fieldStatus.getIpcu().length())));
			fieldStatusObj
					.setCustomer(Integer.parseInt(fieldStatus.getCustomer().substring(1, fieldStatus.getCustomer().length())));
			fieldStatusObj
					.setField(Integer.parseInt(fieldStatus.getField().substring(1, fieldStatus.getField().length())));
			fieldStatusObj.setSoilMoisture(
					Integer.parseInt(fieldStatus.getSoilMoisture().substring(1, fieldStatus.getSoilMoisture().length())));
			fieldStatusObj.setWaterLevel(
					Integer.parseInt(fieldStatus.getWaterLevel().substring(1, fieldStatus.getWaterLevel().length())));
			fieldStatusObj.setFieldValve(
					Integer.parseInt(fieldStatus.getFieldValve().substring(1, fieldStatus.getFieldValve().length())));
			fieldStatusObj.setMotorPump(
					Integer.parseInt(fieldStatus.getMotorPump().substring(1, fieldStatus.getMotorPump().length())));
			fieldStatusObj.setMainValve(
					Integer.parseInt(fieldStatus.getMainValve().substring(1, fieldStatus.getMainValve().length())));
			fieldStatusObj.setWaterCredit(
					Integer.parseInt(fieldStatus.getWaterCredit().substring(1, fieldStatus.getWaterCredit().length())));
			fieldStatusObj.setFlowRate(
					Integer.parseInt(fieldStatus.getFlowRate().substring(1, fieldStatus.getFlowRate().length())));
			fieldStatusObj.setTemperature(
					Integer.parseInt(fieldStatus.getTemperature().substring(1, fieldStatus.getTemperature().length())));
			fieldStatusObj.setHumidity(
					Integer.parseInt(fieldStatus.getHumidity().substring(1, fieldStatus.getHumidity().length())));
			fieldStatusObj.setSensorStatus(Integer
					.parseInt(fieldStatus.getSensorStatus().substring(1, fieldStatus.getSensorStatus().length())));
			fieldStatusObj.setNodeID(
					Integer.parseInt(fieldStatus.getNodeID().substring(1, fieldStatus.getNodeID().length())));
			fieldStatusObj.setSeason(
					Integer.parseInt(fieldStatus.getSeason().substring(1, fieldStatus.getSeason().length())));
			return true;
		} catch (NumberFormatException e) {
			System.err.println("The packet format is not correct....");
			return false;
		}
	}

	public FieldStatusObj getFieldStatusObj() {
		return fieldStatusObj;
	}

}
