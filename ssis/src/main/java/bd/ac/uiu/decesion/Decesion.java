package bd.ac.uiu.decesion;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import bd.ac.uiu.api.bKash.BkashApi;
import bd.ac.uiu.api.sms.ShortMessageSystem;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.ResponseInformationLogDTO;
import bd.ac.uiu.dto.SMSDTO;
import bd.ac.uiu.dto.TransactionLogDTO;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.FieldService;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.ResponseInformationLogService;
import bd.ac.uiu.service.SmsService;
import bd.ac.uiu.service.TransactionLogService;
import bd.ac.uiu.service.WaterCreditLogService;
import bd.ac.uiu.service.WaterCreditSummaryService;

public class Decesion {
	private FieldStatusObj fieldStatusObjSuscribe;
	private FieldStatusObj fieldStatusObjPublish;
	private String decesionPacket;
	private String[] updatePacket;
	List<ResponseInformationLogDTO> responseInformationLogs;

	WaterCreditSummaryDTO waterCreditSummaryDTO;
	private IpcuDTO ipcuDTO;
	private CustomerDTO customerDTO;
	private OperatorDTO operatorDTO;
	private SMSDTO smsDTO;
	private TransactionLogDTO transactionLogDTO;

	public Decesion() {
	}

	public boolean publishPacketInit(WaterCreditSummaryService waterCreditSummaryService,
			WaterCreditLogService waterCreditLogService, ResponseInformationLogService responseInformationLogService,
			FieldService fieldService, IpcuService ipcuService, OperatorService operatorService,
			CustomerService customerService, SmsService smsService, TransactionLogService transactionLogService) {
		fieldStatusObjPublish = new FieldStatusObj();
		fieldStatusObjPublish.setOperator(fieldStatusObjSuscribe.getOperator());
		fieldStatusObjPublish.setIpcu(fieldStatusObjSuscribe.getIpcu());
		fieldStatusObjPublish.setCustomer(fieldStatusObjSuscribe.getCustomer());
		fieldStatusObjPublish.setField(fieldStatusObjSuscribe.getField());
		fieldStatusObjPublish.setSoilMoisture(fieldStatusObjSuscribe.getSoilMoisture());
		fieldStatusObjPublish.setWaterLevel(fieldStatusObjSuscribe.getWaterLevel());
		fieldStatusObjPublish.setMotorPump(fieldStatusObjSuscribe.getMotorPump());
		fieldStatusObjPublish.setMainValve(fieldStatusObjSuscribe.getMainValve());
		fieldStatusObjPublish.setWaterCredit(fieldStatusObjSuscribe.getWaterCredit());
		fieldStatusObjPublish.setFlowRate(fieldStatusObjSuscribe.getFlowRate());
		fieldStatusObjPublish.setTemperature(fieldStatusObjSuscribe.getTemperature());
		fieldStatusObjPublish.setHumidity(fieldStatusObjSuscribe.getHumidity());
		fieldStatusObjPublish.setSensorStatus(fieldStatusObjSuscribe.getSensorStatus());
		fieldStatusObjPublish.setNodeID(fieldStatusObjSuscribe.getNodeID());
		fieldStatusObjPublish.setSeason(fieldStatusObjSuscribe.getSeason());
		boolean bool =makeDecesion(waterCreditSummaryService, waterCreditLogService, responseInformationLogService, fieldService,
				ipcuService, operatorService, customerService, smsService, transactionLogService);
		return bool;

	}

	public void setUpdatePacket(String[] updatePacket) {
		this.updatePacket = updatePacket;
	}

	public FieldStatusObj getFieldStatusObjSuscribe() {
		return fieldStatusObjSuscribe;
	}

	public boolean setFieldStatusObjSuscribe(FieldStatusObj fieldStatusObjSuscribe,
			WaterCreditSummaryService waterCreditSummaryService, WaterCreditLogService waterCreditLogService,
			ResponseInformationLogService responseInformationLogService, FieldService fieldService,
			IpcuService ipcuService, OperatorService operatorService, CustomerService customerService,
			SmsService smsService, TransactionLogService transactionLogService) {
		this.fieldStatusObjSuscribe = fieldStatusObjSuscribe;
		boolean bool = publishPacketInit(waterCreditSummaryService, waterCreditLogService, responseInformationLogService, fieldService,
				ipcuService, operatorService, customerService, smsService, transactionLogService);
		return bool;
	}

	public boolean makeDecesion(WaterCreditSummaryService waterCreditSummaryService,
			WaterCreditLogService waterCreditLogService, ResponseInformationLogService responseInformationLogService,
			FieldService fieldService, IpcuService ipcuService, OperatorService operatorService,
			CustomerService customerService, SmsService smsService, TransactionLogService transactionLogService) {

		long operatorID = (long) fieldStatusObjSuscribe.getOperator();
		long customerID = (long) fieldStatusObjSuscribe.getCustomer();
		long ipcuID = (long) fieldStatusObjSuscribe.getIpcu();

		ShortMessageSystem shortMessageSystem = new ShortMessageSystem();

		BkashApi bkashApi = new BkashApi();
		
		try{
		waterCreditSummaryDTO = waterCreditSummaryService.findWaterCreditSummaryByOperatorIDAndCustomerID(operatorID,
				customerID);
		}catch(Exception e){
			return false;
		}

		System.out.println("Available water credit : " + waterCreditSummaryDTO.getAvailableWaterCredit());

		try {
			customerDTO = customerService.findCustomerByOperatorIDAndCustomerID(operatorID, customerID);
		} catch (Exception e2) {

			e2.printStackTrace();
			return false;
		}
		// customer.setCustomerID(customerID);

		// operator.setOperatorID(operatorID);

		System.out.println("Customer name :" + customerDTO.getCustomerName());
		System.out.println("Customer mobile :" + customerDTO.getMobile());
		System.out.println("Operator name :" + customerDTO.getOperatorDTO().getOperatorName());

		try {
			operatorDTO = operatorService.findOperatorByOperatorID(operatorID);
		} catch (Exception e2) {
			
			e2.printStackTrace();
			return false;
		}

		int offOn = 0;
		/* if all field valve is 0 thn the pump should be off */

		try {
			responseInformationLogs = responseInformationLogService.findResponseInformationListByOperatorIDAndIpcuID(
					fieldStatusObjSuscribe.getOperator(), fieldStatusObjSuscribe.getIpcu());
		} catch (Exception e) {

			e.printStackTrace();
			
		}

		for (ResponseInformationLogDTO responseInformationLogDTO : responseInformationLogs) {
			if (responseInformationLogDTO.getFieldValve() == 1) {
				offOn = 1;
			}
		}

		try {
			ipcuDTO = ipcuService.findIpcuByOperatorAndIpcu(operatorID, ipcuID);
		} catch (Exception e1) {

			e1.printStackTrace();
			return false;
		}

		if (fieldStatusObjSuscribe.getWaterLevel() > 60 && fieldStatusObjSuscribe.getSoilMoisture() > 60 && fieldStatusObjSuscribe.getSeason()==0) {
			fieldStatusObjPublish.setFieldValve(0);

			if (ipcuDTO.isEnabled() == true) {
				fieldStatusObjPublish.setMainValve(offOn);
				fieldStatusObjPublish.setMotorPump(offOn);
			} else {
				fieldStatusObjPublish.setMainValve(0);
				fieldStatusObjPublish.setMotorPump(0);
			}

			fieldStatusObjPublish.setWaterCredit(0);
		}else if (fieldStatusObjSuscribe.getSoilMoisture() >= 60 && fieldStatusObjSuscribe.getSeason()==1) {
			fieldStatusObjPublish.setFieldValve(0);

			if (ipcuDTO.isEnabled() == true) {
				fieldStatusObjPublish.setMainValve(offOn);
				fieldStatusObjPublish.setMotorPump(offOn);
			} else {
				fieldStatusObjPublish.setMainValve(0);
				fieldStatusObjPublish.setMotorPump(0);
			}

			fieldStatusObjPublish.setWaterCredit(0);
		}else if (fieldStatusObjSuscribe.getSoilMoisture() <= 30
				&& waterCreditSummaryDTO.getAvailableWaterCredit() > 100 && fieldStatusObjSuscribe.getSeason()==1) {

			if (fieldStatusObjSuscribe.getFieldValve() == 0) {
				fieldStatusObjPublish.setFieldValve(1);

				if (ipcuDTO.isEnabled() == true) {
					fieldStatusObjPublish.setMainValve(1);
					fieldStatusObjPublish.setMotorPump(1);
				} else {
					fieldStatusObjPublish.setMainValve(0);
					fieldStatusObjPublish.setMotorPump(0);
				}

			} else {
				fieldStatusObjPublish.setFieldValve(fieldStatusObjSuscribe.getFieldValve());
			}

			try {
				waterCreditSummaryService.waterCreditSummaryUpdateByUsedWaterCredit(customerID,
						fieldStatusObjSuscribe.getWaterCredit());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			fieldStatusObjPublish.setWaterCredit(0);

			waterCreditLogService.saveWaterCreditLogWhenWaterCreditIsUsedInField(customerID, operatorID,
					fieldStatusObjSuscribe.getField(), fieldStatusObjSuscribe.getIpcu(),
					fieldStatusObjSuscribe.getWaterCredit(), waterCreditSummaryDTO.getAvailableWaterCredit());

		}else if (fieldStatusObjSuscribe.getSoilMoisture() < 50
				&& waterCreditSummaryDTO.getAvailableWaterCredit() > 100 && fieldStatusObjSuscribe.getSeason()==0) {

			if (fieldStatusObjSuscribe.getFieldValve() == 0) {
				fieldStatusObjPublish.setFieldValve(1);

				if (ipcuDTO.isEnabled() == true) {
					fieldStatusObjPublish.setMainValve(1);
					fieldStatusObjPublish.setMotorPump(1);
				} else {
					fieldStatusObjPublish.setMainValve(0);
					fieldStatusObjPublish.setMotorPump(0);
				}

			} else {
				fieldStatusObjPublish.setFieldValve(fieldStatusObjSuscribe.getFieldValve());
			}

			try {
				waterCreditSummaryService.waterCreditSummaryUpdateByUsedWaterCredit(customerID,
						fieldStatusObjSuscribe.getWaterCredit());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			fieldStatusObjPublish.setWaterCredit(0);

			waterCreditLogService.saveWaterCreditLogWhenWaterCreditIsUsedInField(customerID, operatorID,
					fieldStatusObjSuscribe.getField(), fieldStatusObjSuscribe.getIpcu(),
					fieldStatusObjSuscribe.getWaterCredit(), waterCreditSummaryDTO.getAvailableWaterCredit());

		}

		else if (waterCreditSummaryDTO.getAvailableWaterCredit() < 100) {
			/* if water credit is less then 100 thn the field should be off */
			fieldStatusObjPublish.setFieldValve(0);

			if (ipcuDTO.isEnabled() == true) {
				fieldStatusObjPublish.setMainValve(offOn);
				fieldStatusObjPublish.setMotorPump(offOn);
			} else {
				fieldStatusObjPublish.setMainValve(0);
				fieldStatusObjPublish.setMotorPump(0);
			}

			/*String reference = getReference(operatorID, customerID);
			bkashApi.setReference(reference);

			transactionLogDTO = new TransactionLogDTO();

			transactionLogDTO.setPaymentTypeName("bKash");
			transactionLogDTO.setAmount(Double.parseDouble(bkashApi.bKashService().getAmount()));
			transactionLogDTO.setCounter(bkashApi.bKashService().getCounter());
			transactionLogDTO.setTrxStatus(bkashApi.bKashService().getTrxStatus());
			transactionLogDTO.setTrxId(bkashApi.bKashService().getTrxId());
			transactionLogDTO.setReference(bkashApi.bKashService().getReference());
			transactionLogDTO.setSender(bkashApi.bKashService().getSender());
			transactionLogDTO.setService(bkashApi.bKashService().getService());
			transactionLogDTO.setCurrency(bkashApi.bKashService().getCurrency());
			transactionLogDTO.setReceiver(bkashApi.bKashService().getReceiver());
			transactionLogDTO.setTrxTimeStamp(bkashApi.bKashService().getTrxTimeStamp());
*/
			/*if (transactionLogService.paymentStatusExistOrNot(operatorID, customerID,
					transactionLogDTO.getTrxTimeStamp()) == false) {
				try {
					transactionLogService.saveTransaction(transactionLogDTO);
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			}*/

			fieldStatusObjPublish.setWaterCredit(0);

			if (waterCreditSummaryDTO.isEnabled() == true) {
				System.out.println("Your water Credit is low! Please Recharge... ");

				smsDTO = new SMSDTO();
				smsDTO.setCustomerDTO(customerDTO);
				smsDTO.setOperatorDTO(operatorDTO);
				smsDTO.setSmsTo(customerDTO.getMobile());
				smsDTO.setSmsFrom("SSIS");
				smsDTO.setSmsBody("N/A");
				smsDTO.setAvailableWaterCredit(waterCreditSummaryDTO.getAvailableWaterCredit());
				smsDTO.setDateExecuted(new Date());

				try {
					smsService.saveSms(smsDTO);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				shortMessageSystem.lowCreditWarning(customerDTO.getOperatorDTO().getOperatorID(),
						customerDTO.getCustomerName(), customerDTO.getMobile(),
						waterCreditSummaryDTO.getAvailableWaterCredit());

				try {
					waterCreditSummaryService.updateWaterCreditEnableStatus(operatorID, customerID, false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {

			if (fieldStatusObjSuscribe.getFieldValve() == 0) {
				fieldStatusObjPublish.setFieldValve(1);

				if (ipcuDTO.isEnabled() == true) {
					fieldStatusObjPublish.setMainValve(1);
					fieldStatusObjPublish.setMotorPump(1);
				} else {
					fieldStatusObjPublish.setMainValve(0);
					fieldStatusObjPublish.setMotorPump(0);
				}

			} else {
				fieldStatusObjPublish.setFieldValve(fieldStatusObjSuscribe.getFieldValve());
			}

			try {
				waterCreditSummaryService.waterCreditSummaryUpdateByUsedWaterCredit(customerID,
						fieldStatusObjSuscribe.getWaterCredit());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fieldStatusObjPublish.setWaterCredit(0);

			waterCreditLogService.saveWaterCreditLogWhenWaterCreditIsUsedInField(customerID, operatorID,
					fieldStatusObjSuscribe.getField(), fieldStatusObjSuscribe.getIpcu(),
					fieldStatusObjSuscribe.getWaterCredit(), waterCreditSummaryDTO.getAvailableWaterCredit());
		}
		return true;

	}

	public String getDecesionPacket() {
		decesionPacket = "#";
		for (String str : updatePacket) {
			if (str.equals("v1") || str.equals("v0")) {
				decesionPacket += "v" + fieldStatusObjPublish.getFieldValve() + ".";
			} else if (str.equals("m0") || str.equals("m1")) {
				// if(fieldStatusObjPublish.getFieldValve()==1){
				decesionPacket += "m" + fieldStatusObjPublish.getMotorPump() + ".";
				// }
			} else if (str.equals("V0") || str.equals("V1")) {
				// if(fieldStatusObjPublish.getFieldValve()==1){
				decesionPacket += "V" + fieldStatusObjPublish.getMainValve() + ".";
				// }
			}

			else if (str.startsWith("l")) {
				decesionPacket += "l0000.";
			}

			else {
				if (str.equals("s0") || str.equals("s1")) {
					decesionPacket += str;
				} else {
					decesionPacket += str + ".";
				}
			}

		}
		decesionPacket += "#";
		return decesionPacket;

	}

	public FieldStatusObj getFieldStatusObjPublish() {
		return fieldStatusObjPublish;
	}

	public WaterCreditSummaryDTO getWaterCreditSummaryDTO() {

		if (waterCreditSummaryDTO == null) {
			waterCreditSummaryDTO = new WaterCreditSummaryDTO();
		}
		return waterCreditSummaryDTO;
	}

	public void setWaterCreditSummaryDTO(WaterCreditSummaryDTO waterCreditSummaryDTO) {
		this.waterCreditSummaryDTO = waterCreditSummaryDTO;
	}

	public List<ResponseInformationLogDTO> getResponseInformationLogs() {
		return responseInformationLogs;
	}

	public void setResponseInformationLogs(List<ResponseInformationLogDTO> responseInformationLogs) {
		this.responseInformationLogs = responseInformationLogs;
	}

	public IpcuDTO getIpcuDTO() {
		return ipcuDTO;
	}

	public void setIpcuDTO(IpcuDTO ipcuDTO) {
		this.ipcuDTO = ipcuDTO;
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public SMSDTO getSmsDTO() {
		return smsDTO;
	}

	public void setSmsDTO(SMSDTO smsDTO) {
		this.smsDTO = smsDTO;
	}

	public TransactionLogDTO getTransactionLogDTO() {
		return transactionLogDTO;
	}

	public void setTransactionLogDTO(TransactionLogDTO transactionLogDTO) {
		this.transactionLogDTO = transactionLogDTO;
	}
	public String getReferenceID(String operatorID, String customerID) {

		if (operatorID.length() < 3) {
			if (operatorID.length() == 2) {
				operatorID = "0" + operatorID;
			} else {
				operatorID = "00" + operatorID;
			}
		}
		if (customerID.length() < 3) {
			if (customerID.length() == 2) {
				customerID = "0" + customerID;
			} else {
				customerID = "00" + customerID;
			}
		}

		return operatorID + customerID;
	}

	public String getReference(long operatorID, long customerID) {

		char[] customCustomerID = { '0', '0', '0' };
		char[] customOperatorID = { '0', '0', '0' };

		String finalCustID = new String();
		String finalOperID = new String();

		String reference;

		String str = Integer.toString((int) customerID);
		for (int i = str.length() - 1; i >= 0; i--) {
			System.out.println(str.charAt(i));
			customCustomerID[i] = str.charAt(i);
		}

		String str1 = Integer.toString((int) operatorID);
		for (int i = str1.length() - 1; i >= 0; i--) {
			System.out.println(str1.charAt(i));
			customOperatorID[i] = str1.charAt(i);
		}

		for (int i = customCustomerID.length - 1; i >= 0; i--) {
			finalCustID += customCustomerID[i];
		}

		for (int i = customOperatorID.length - 1; i >= 0; i--) {
			finalOperID += customOperatorID[i];
		}

		reference = finalOperID + finalCustID;

		System.out.println(reference);

		return reference;
	}

}
