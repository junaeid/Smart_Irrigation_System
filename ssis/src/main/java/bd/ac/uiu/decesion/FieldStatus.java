package bd.ac.uiu.decesion;

public class FieldStatus {

	private String operator;
	private String ipcu;
	private String customer;
	private String field;
	private String soilMoisture;
	private String waterLevel;
	private String fieldValve;
	private String motorPump;
	private String mainValve;
	private String waterCredit;
	private String flowRate;
	private String temperature;
	private String humidity;
	private String sensorStatus;
	private String nodeID;
	private String season;

	public FieldStatus() {

	}



	public String getOperator() {
		return operator;
	}



	public void setOperator(String operator) {
		this.operator = operator;
	}



	public String getIpcu() {
		return ipcu;
	}



	public void setIpcu(String ipcu) {
		this.ipcu = ipcu;
	}



	public String getCustomer() {
		return customer;
	}



	public void setCustomer(String customer) {
		this.customer = customer;
	}



	public String getField() {
		return field;
	}



	public void setField(String field) {
		this.field = field;
	}



	public String getSoilMoisture() {
		return soilMoisture;
	}



	public void setSoilMoisture(String soilMoisture) {
		this.soilMoisture = soilMoisture;
	}



	public String getWaterLevel() {
		return waterLevel;
	}



	public void setWaterLevel(String waterLevel) {
		this.waterLevel = waterLevel;
	}



	public String getFieldValve() {
		return fieldValve;
	}



	public void setFieldValve(String fieldValve) {
		this.fieldValve = fieldValve;
	}



	public String getMotorPump() {
		return motorPump;
	}



	public void setMotorPump(String motorPump) {
		this.motorPump = motorPump;
	}



	public String getMainValve() {
		return mainValve;
	}



	public void setMainValve(String mainValve) {
		this.mainValve = mainValve;
	}



	public String getWaterCredit() {
		return waterCredit;
	}



	public void setWaterCredit(String waterCredit) {
		this.waterCredit = waterCredit;
	}



	public String getFlowRate() {
		return flowRate;
	}



	public void setFlowRate(String flowRate) {
		this.flowRate = flowRate;
	}



	public String getTemperature() {
		return temperature;
	}



	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}



	public String getHumidity() {
		return humidity;
	}



	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}



	public String getSensorStatus() {
		return sensorStatus;
	}



	public void setSensorStatus(String sensorStatus) {
		this.sensorStatus = sensorStatus;
	}



	public String getNodeID() {
		return nodeID;
	}



	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}



	public String getSeason() {
		return season;
	}



	public void setSeason(String season) {
		this.season = season;
	}



	public FieldStatus(String operator, String ipcu, String customer, String field, String soilMoisture, String waterLevel,
			String fieldValve, String motorPump, String mainValve, String waterCredit, String flowRate,
			String temperature, String humidity, String sensorStatus, String nodeID, String season) {
		super();
		this.operator = operator;
		this.ipcu = ipcu;
		this.customer = customer;
		this.field = field;
		this.soilMoisture = soilMoisture;
		this.waterLevel = waterLevel;
		this.fieldValve = fieldValve;
		this.motorPump = motorPump;
		this.mainValve = mainValve;
		this.waterCredit = waterCredit;
		this.flowRate = flowRate;
		this.temperature = temperature;
		this.humidity = humidity;
		this.sensorStatus = sensorStatus;
		this.nodeID = nodeID;
		this.season = season;
	}

	@Override
	public String toString() {
		return "FieldStatus [Operator=" + operator + ", Ipcu=" + ipcu + ", Customer=" + customer + ", field=" + field
				+ ", SoilMoisture=" + soilMoisture + ", waterLevel=" + waterLevel + ", fieldValve=" + fieldValve
				+ ", motorPump=" + motorPump + ", mainValve=" + mainValve + ", waterCredit=" + waterCredit
				+ ", flowRate=" + flowRate + ", temperature=" + temperature + ", humidity=" + humidity
				+ ", sensorStatus=" + sensorStatus + ", nodeID=" + nodeID + ", season=" + season + "]";
	}

}
