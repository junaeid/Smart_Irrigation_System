package bd.ac.uiu.decesion;

public class FieldStatusObj {
	private int operator;
	private int ipcu;
	private int customer;
	private int field;
	private int soilMoisture;
	private int waterLevel;
	private int fieldValve;
	private int motorPump;
	private int mainValve;
	private int waterCredit;
	private int flowRate;
	private int temperature;
	private int humidity;
	private int sensorStatus;
	private int nodeID;
	private int season;

	public FieldStatusObj() {

	}

	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}

	public int getIpcu() {
		return ipcu;
	}

	public void setIpcu(int ipcu) {
		this.ipcu = ipcu;
	}

	public int getCustomer() {
		return customer;
	}

	public void setCustomer(int customer) {
		this.customer = customer;
	}

	public int getField() {
		return field;
	}

	public void setField(int field) {
		this.field = field;
	}

	public int getSoilMoisture() {
		return soilMoisture;
	}

	public void setSoilMoisture(int soilMoisture) {
		this.soilMoisture = soilMoisture;
	}

	public int getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(int waterLevel) {
		this.waterLevel = waterLevel;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getMotorPump() {
		return motorPump;
	}

	public void setMotorPump(int motorPump) {
		this.motorPump = motorPump;
	}

	public int getMainValve() {
		return mainValve;
	}

	public void setMainValve(int mainValve) {
		this.mainValve = mainValve;
	}

	public int getWaterCredit() {
		return waterCredit;
	}

	public void setWaterCredit(int waterCredit) {
		this.waterCredit = waterCredit;
	}

	public int getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(int flowRate) {
		this.flowRate = flowRate;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(int sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	@Override
	public String toString() {
		return "FieldStatusObj [Operator=" + operator + ", IPCU=" + ipcu + ", Customer=" + customer + ", field=" + field
				+ ", SoilMoisture=" + soilMoisture + ", waterLevel=" + waterLevel + ", fieldValve=" + fieldValve
				+ ", motorPump=" + motorPump + ", mainValve=" + mainValve + ", waterCredit=" + waterCredit
				+ ", flowRate=" + flowRate + ", temperature=" + temperature + ", humidity=" + humidity
				+ ", sensorStatus=" + sensorStatus + ", nodeID=" + nodeID + ",season=" + season + "]";
	}

}
