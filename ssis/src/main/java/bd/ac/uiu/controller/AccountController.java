package bd.ac.uiu.controller;

import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.api.sms.ShortMessageSystem;
import bd.ac.uiu.dto.UserDTO;
import bd.ac.uiu.service.UserService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class AccountController {
	@ManagedProperty("#{userService}")
	private UserService userService;

	@ManagedProperty("#{shortMessageSystem}")
	ShortMessageSystem shortMessageSystem;

	private String userName;

	private String currentPassword;

	private String newPassword;
	private String confirmPassword;;
	private int sCode;
	UserDTO userDTO;

	private boolean renForm = false;
	private boolean renResetBtn = false;

	/*
	 * Used to find Users by their name using
	 * userService.findUserByName(userName);
	 * 
	 */
	public void findUsersByUserName() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		userDTO = userService.findUsersByUserName(userName);
		if (userDTO != null) {
			ShortMessageSystem shortMessageSystem = new ShortMessageSystem();
			// shortMessageSystem.forgetPassword(userDTO.getMobile(),userDTO.getPassword());
			context.addMessage(null, new FacesMessage("Successfully Sent"));
			userDTO = null;

		} else {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "User name does not Exist", ""));
		}

	}

	/*
	 * Used to changePassword by userID and currentPassword get from session
	 * userService.findUserByUserNameAndPassword(userName,currentPassword) and
	 * set new password using userDTO.setPassword(newPassword); then
	 * userService.changePassword(userDTO)
	 */

	public void changePassword() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		String userName = ApplicationUtility.getUserID();
		System.out.println("ccccdddd"+confirmPassword);
		System.out.println("ccccdddd"+userName);
		
		userDTO = new UserDTO();
		userDTO = userService.findUserByUserNameAndPassword(userName, currentPassword);
		currentPassword=null;

		if (userDTO.getUsername() != null && userDTO.getPassword() != null) {

			System.out.println("====UserDTO username:" + userDTO.getUsername());
			System.out.println("====UserDTO password:" + userDTO.getPassword());

			if (newPassword.equals(confirmPassword)) {
				System.out.println("newPassword :" + newPassword);
				System.out.println("confirmpassword :" + confirmPassword);
				userDTO.setPassword(newPassword);
				userService.changePassword(userDTO);

				context.addMessage(null, new FacesMessage("Successfully changed the password."));

			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_FATAL, "Both passwords do not match.", ""));
			}
			userDTO = null;

		} else {
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Password does not match for this user.", ""));
		}
	}

	private int genSecretCode;

	/*
	 * sendSMS is Used send message to Users if they forgot password
	 */

	public void sendSms() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {

			userDTO = userService.findUsersByUserName(userName);

			if (userDTO != null) {

				genSecretCode = gen();
				System.out.println("genSecretCode : " + genSecretCode);
				System.out.println("userDTO.getMobile() : " + userDTO.getMobile());
				ShortMessageSystem shortMessageSystem = new ShortMessageSystem();
				shortMessageSystem.sendSMS(userDTO.getMobile(), genSecretCode);
				System.out.println("secretCode" + genSecretCode);
				renForm = true;

				context.addMessage(null, new FacesMessage("Successfully Send Secret Code"));
				userDTO = null;

			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Code Does not Match", ""));
			}

		} catch (Exception e) {
			System.out.println(e);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "User does not exist.", ""));
		}

	}

	/*
	 * random number generator for forgot password
	 */

	public int gen() {
		Random r = new Random(System.currentTimeMillis());
		return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
	}

	/*
	 * Used to change the password and check the password they put is corrected
	 */
	public void enabledResetButton() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			System.out.println("newPassword :" + newPassword);
			System.out.println("confirmpassword :" + confirmPassword);
			if (genSecretCode == sCode && newPassword.equals(confirmPassword)) {
				renResetBtn = true;
			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_FATAL, "Both passwords do not match.", ""));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * check the password inputed is both same or not then update the password
	 * after confirmation
	 */
	public void resetPassword() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		userDTO = new UserDTO();
		userDTO = userService.findUsersByUserName(userName);
		try {
			if (genSecretCode == sCode) {
				if (newPassword.equals(confirmPassword)) {
					System.out.println("newPassword :" + newPassword);
					System.out.println("confirmpassword :" + confirmPassword);
					userDTO.setPassword(newPassword);
					userService.changePassword(userDTO);

					context.addMessage(null, new FacesMessage("Successfully Reset the password."));

				} else {
					context.addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_FATAL, "Both passwords do not match.", ""));
				}
				userDTO = null;

			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_FATAL, "Password does not match for this user.", ""));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public ShortMessageSystem getShortMessageSystem() {
		return shortMessageSystem;
	}

	public void setShortMessageSystem(ShortMessageSystem shortMessageSystem) {
		this.shortMessageSystem = shortMessageSystem;
	}

	public UserDTO getUserDTO() {
		if (userDTO == null) {

			userDTO = new UserDTO();
		}
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public int getsCode() {
		return sCode;
	}

	public void setsCode(int sCode) {
		this.sCode = sCode;
	}

	public boolean isRenForm() {
		return renForm;
	}

	public void setRenForm(boolean renForm) {
		this.renForm = renForm;
	}

	public int getGenSecretCode() {
		return genSecretCode;
	}

	public void setGenSecretCode(int genSecretCode) {
		this.genSecretCode = genSecretCode;
	}

	public boolean isRenResetBtn() {
		return renResetBtn;
	}

	public void setRenResetBtn(boolean renResetBtn) {
		this.renResetBtn = renResetBtn;
	}

}
