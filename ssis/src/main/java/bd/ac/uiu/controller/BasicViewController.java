package bd.ac.uiu.controller;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class BasicViewController implements Serializable {

	private static final long serialVersionUID = 1L;
	private TreeNode root;

	@PostConstruct
	public void init() {

		FacesContext context = FacesContext.getCurrentInstance();

		String pagetitle = context.getExternalContext().getSessionMap().get("Pagetitle").toString();

		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Date date = new Date();

		root = new DefaultTreeNode(pagetitle, null);
		TreeNode node0 = new DefaultTreeNode(cerOrOrgOrOpIDLogin, root);

		TreeNode node00 = new DefaultTreeNode(cerOrOrgOrOpIDLogin + 1, node0);
		TreeNode node01 = new DefaultTreeNode(date, node0);

		node00.getChildren().add(new DefaultTreeNode("Node 0.0.0"));
		node00.getChildren().add(new DefaultTreeNode("Node 0.0.1"));
		node01.getChildren().add(new DefaultTreeNode("Node 0.1.0"));

		System.out.println("Root: " + root.getChildCount());
		root.setExpanded(true);
		node0.setExpanded(true);
		node00.setExpanded(true);
		node01.setExpanded(true);
	}

	public TreeNode getRoot() {
		return root;
	}
}
