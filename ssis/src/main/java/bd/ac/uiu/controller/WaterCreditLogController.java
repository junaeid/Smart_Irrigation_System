package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.WaterCreditLogDTO;
import bd.ac.uiu.service.WaterCreditLogService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class WaterCreditLogController {

	private WaterCreditLogDTO waterCreditLogDTO;

	private List<WaterCreditLogDTO> waterCreditLogList;
	private List<WaterCreditLogDTO> waCreLogOrgList;

	@ManagedProperty("#{waterCreditLogService}")
	private WaterCreditLogService waterCreditLogService;

	public void saveWaterCreditSummaryLog() {
		/*
		 * FacesContext context = FacesContext.getCurrentInstance(); try { if
		 * (waterCreditLogService.saveWaterCreditLog(waterCreditLogDTO)) {
		 * context.addMessage(null, new FacesMessage("Successfully Saved"));
		 * waterCreditLogDTO = null; } else { context.addMessage(null, new
		 * FacesMessage(FacesMessage.SEVERITY_FATAL,
		 * "waterCreditSummaryLog Exist", "")); } } catch (Exception e) { //
		 * TODO: handle exception
		 * 
		 * System.out.println(e.getMessage()); context.addMessage(null, new
		 * FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", "")); }
		 */
	}

	public void updateWaterCreditSummaryLog() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			waterCreditLogService.updateWaterCreditLog(waterCreditLogDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			waterCreditLogDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deleteWaterCreditSummaryLog() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			waterCreditLogService.deleteWaterCreditLog(waterCreditLogDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			waterCreditLogDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public WaterCreditLogDTO getWaterCreditSummaryLogDTO() {
		if (waterCreditLogDTO == null) {
			waterCreditLogDTO = new WaterCreditLogDTO();
		}
		return waterCreditLogDTO;
	}

	public void setWaterCreditLogDTO(WaterCreditLogDTO waterCreditLogDTO) {
		this.waterCreditLogDTO = waterCreditLogDTO;
	}

	public List<WaterCreditLogDTO> getWaterCreditLogList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			waterCreditLogList = new ArrayList<>();
			waterCreditLogList = waterCreditLogService.findWaterCreditLogByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return waterCreditLogList;
	}

	public void setWaterCreditLogList(List<WaterCreditLogDTO> waterCreditLogList) {
		this.waterCreditLogList = waterCreditLogList;
	}

	public List<WaterCreditLogDTO> getWaCreLogOrgList() {
		try {
			waCreLogOrgList = new ArrayList<>();
			waCreLogOrgList = waterCreditLogService.findWaterCreditLogList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return waCreLogOrgList;
	}

	public void setWaCreLogOrgList(List<WaterCreditLogDTO> waCreLogOrgList) {
		this.waCreLogOrgList = waCreLogOrgList;
	}

	public WaterCreditLogService getWaterCreditSummaryLogService() {
		return waterCreditLogService;
	}

	public void setWaterCreditLogService(WaterCreditLogService waterCreditLogService) {
		this.waterCreditLogService = waterCreditLogService;
	}

}
