package bd.ac.uiu.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import bd.ac.uiu.dao.ForgetPasswordDao;
import bd.ac.uiu.dao.UsersDAO;
import bd.ac.uiu.dao.sms.OperatorSmsBalanceDao;
import bd.ac.uiu.dao.sms.SmsLogDetailsDao;
import bd.ac.uiu.dao.sms.SmsLogSummaryDao;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.UserDTO;
import bd.ac.uiu.entity.ForgetPassword;
import bd.ac.uiu.entity.Users;
import bd.ac.uiu.entity.sms.SMS_Service;
import bd.ac.uiu.entity.sms.SMS_ServiceImpl;
import bd.ac.uiu.entity.sms.SmsLogDetails;
import bd.ac.uiu.entity.sms.SmsLogSummary;
import bd.ac.uiu.service.UserService;
import bd.ac.uiu.utility.ApplicationUtility;
import bd.ac.uiu.utility.PasswordEncryption;

@ManagedBean
@Scope("session")
@Controller(value="passwordChangeBean")
public class PasswordController implements Serializable {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(PasswordController.class);
/*	
	@ManagedProperty("#{userService}")
	private UserService userService;

	private UserDTO userDTO;
	private List<UserDTO> userList;
	
	private OperatorDTO operatorDTO;
	
	

	private String userName;
		
	private String renderResetCode="false";
	private String rederPassword="false";
	
	
	private String resetCode;
	private String resetPassword;
	
	private String currentPassword;
	
	private String sendingMessage;
	
	public SMS_Service smsDao=new SMS_ServiceImpl();
	
	@Autowired
	SmsLogDetailsDao smsLogDetailsDao;
	
	@Autowired
	SmsLogSummaryDao smsLogSummaryDao;
	
	@Autowired
	OperatorSmsBalanceDao operatorSmsBalanceDao;
	
	@Autowired
	ForgetPasswordDao forgetPasswordDao;
	
	@Autowired
	UsersDAO usersDao;
	
	private long operatorID;
		

	
	public void sendMessageFromApi(){
		
	
		List<SmsLogDetails> smsLogDetailsList=new ArrayList<>();
		
		Date date=new Date();
		
		String monthName=new SimpleDateFormat("MMMM").format(date.getTime());
		String year=new SimpleDateFormat("yyyy").format(date.getTime());
		
		while(true){
		
			smsLogDetailsList=smsLogDetailsDao.findSmsLogUnsuccessStatusList(operatorDTO.getOperatorID());
			
			if(smsLogDetailsList.size()>0){
				
			
			for(SmsLogDetails sm:smsLogDetailsList){
				
				if(smsDao.sendIndividual_Sms(sm.getToNumber(), sm.getSmsBody())==200){
					
					if(smsLogDetailsDao.updateSmsLogDetails(sm.getSmsLogID(), date, monthName, year)==0){
						
						break;
					}
					
				//---------For Sleep Start -----//
					try {
						
						Thread.sleep(1500);
						
					} catch (InterruptedException e) {
						System.out.println(e.getMessage());
					}
					
				//---------For Sleep End -----//	
				
				}
				
				else{
					
					smsLogDetailsDao.updateSmsLogDetailsBySmsFail(sm.getSmsLogID(), date, monthName, year);
					
				}
			}
				
				
			
			}
			
			
			else{
				
				break;
			}
		}
		
		smsLogDetailsList=null;
		date=null;
		monthName=null;
		year=null;
		
		System.out.println("SMS Sending End");
	}
	
	
	
	public void sendResetCodeToMobile(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		Date dt=new Date();
		String monthName=new SimpleDateFormat("MMMM").format(dt.getTime());
        String year=new SimpleDateFormat("yyyy").format(dt.getTime());
		long smsBalance=0;
		Users users=new Users();
		SmsLogSummary summary=new SmsLogSummary();
		SmsLogDetails details=new SmsLogDetails();
		ForgetPassword forgetPassword=new ForgetPassword();
		String operatorName="Unkwown";
		String smsBody;
		
		String ipAddress=ApplicationUtility.getIpAddress();
		
		Random ran=new Random();
		String resetCode=""+ran.nextInt(10000);
		smsBody="Your Password Reset Code is "+resetCode;
		
		try {
			
		smsBalance=operatorSmsBalanceDao.smsBalanceCheck(this.operatorID, dt);
		
		if(smsBalance>0){
		
			users=usersDao.findUsers(userName, operatorID);
		
			if(users==null){
				
				context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,"Information is not matching",""));	
				
			}
			
			
			else{

				summary.setSendDate(dt);
				summary.setSendMonth(monthName);
				summary.setSendYear(year);
				summary.setSmsBodySize(160);
				summary.setNumberOfSMS(1);
				summary.setDateExecuted(dt);
				
				summary.setUserExecuted(this.userName);
				summary.setIpExecuted(ipAddress);
				summary.setSmsCategory("Forget Password");
				summary.setRecordStatus(1);
				
				
				if(this.userName.startsWith("016")){
					operatorName="airtel";	
				}
				
				else if(this.userName.startsWith("017")){
					operatorName="GP";	
				}
				
				else if(this.userName.startsWith("019")){
					operatorName="BL";	
				}
				
				else if(this.userName.startsWith("018")){
					operatorName="Robi";	
				}
				
				else if(this.userName.startsWith("015")){
					operatorName="Teletalk";	
				}
				
				else if(this.userName.startsWith("011")){
					operatorName="Citycell";	
				}
				
				
				
				
				/*details=new SmsLogDetails(dt,monthName,year,"Forget Password","01678568240",
						this.userName,operatorName,smsBody,160,1,0,
						null,"", "", "Customer","",
						this.userName,"","",operatorID,"",this.userName,
						new Date(),ipAddress,1,summary);
				
			
				forgetPassword=new ForgetPassword(this.userName,users.getUserID(),resetCode,dt,monthName,
						year,true,this.operatorID,"N/A",this.userName,
						dt,ipAddress,1);*/
			
	/*			
				forgetPasswordDao.forgetPasswordAfterReset(userName);
				forgetPasswordDao.persist(forgetPassword);
				smsLogSummaryDao.persist(summary);
				smsLogDetailsDao.persist(details);
				operatorSmsBalanceDao.updateOperatorSMSBalanceBySend(operatorID,1);
				
				
				sendingMessage="A Reset Code is sent to your mobile";
				renderResetCode="true";
				rederPassword="true";

				
			
			}
			
		}
	    
		
		
		else{

			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,"Balance is not Sufficient",""));  
		}
	        
		}
		
		catch(Exception e){
			sendingMessage="";
			renderResetCode="false";
			rederPassword="false";
			logger.error("This is error : " + e);
			logger.fatal("This is fatal : " + e);
		}
		
		
		dt=null;
		monthName=null;
        year=null;
		smsBalance=0;
		users=null;
		summary=null;
		details=null;
		forgetPassword=null;
		operatorName=null;
		smsBody=null;
		ran=null;
		resetCode="";
		resetPassword="";
		this.resetCode="";
		
		sendMessageFromApi();
		
		
	}
	
	
	
	
	
	public void resetPassWordMethod(){
	
		FacesContext context = FacesContext.getCurrentInstance();
		
		ForgetPassword forgetPassword=new ForgetPassword();
		
		try {
			
			forgetPassword=forgetPasswordDao.findForgetPassword(userName, this.resetCode);
			
			
			if(forgetPassword!=null){
				
			String encpPass=PasswordEncryption.getHashPassword(resetPassword);

			int a=usersDao.changePassword(forgetPassword.getUsername(), encpPass);

			if(a>0){
				
			forgetPasswordDao.forgetPasswordAfterReset(userName);
			
			context.addMessage(null, new FacesMessage("Successfull Password Reset"));
			
			forgetPassword=null;
			resetCode="";			
			renderResetCode="false";
			rederPassword="false";						
			resetCode="";
			resetPassword="";			
			sendingMessage="";
			this.userName="";
			this.operatorID="";

			}
				
			
			}
		
			else{
			
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Failed",""));	
				
			}
	        
			}
		
		catch(Exception e){

			logger.error("This is error : " + e);
			logger.fatal("This is fatal : " + e);
		}

		}
	
	
	
	
	
	
	
	
	public void checkUserByUsernamePassword(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		
		Users users=new Users();
		
		String localInstituteID=ApplicationUtility.getCerOrOrgOrOpID();
		String localusername=auth.getName();
		String encpPassword=PasswordEncryption.getHashPassword(this.currentPassword);
		
		try{
			
		users=usersDao.checkUsersByUsernamePassword(localusername,encpPassword, localInstituteID);
			
		if(users!=null){
				
			rederPassword="true";
			encpPassword=null;

		}
		
		else{
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Information is not matching",""));			
			rederPassword="false";	
			encpPassword=null;
		}
		
		}
		
		catch(Exception e){
			System.out.println(e.getMessage());		
			rederPassword="false";
			encpPassword=null;
			
		}
		
	}
	
	
	
	
	
	public void changePassword(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		String localusername=auth.getName();
		
		String encpPassword;
		
		try{
		
			encpPassword=PasswordEncryption.getHashPassword(this.resetPassword);
		
			int a=usersDao.changePassword(localusername, encpPassword);
			
		
			if(a>0){
				
				context.addMessage(null, new FacesMessage("Password Successfully Updated",""));			
				rederPassword="false";	
				this.currentPassword="";
				this.resetPassword="";
				encpPassword=null;
				localusername=null;
			}
			
			else{
				encpPassword=null;
				localusername=null;
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Failed",""));
			}
		
		}
		
		catch(Exception e){
			encpPassword=null;
			localusername=null;
			System.out.println("changePassword "+e.getMessage());
		}
		
		
	}
	
	
/*	
	public String getSendingMessage() {
		return sendingMessage;
	}


	public void setSendingMessage(String sendingMessage) {
		this.sendingMessage = sendingMessage;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getRenderResetCode() {
		return renderResetCode;
	}
	public void setRenderResetCode(String renderResetCode) {
		this.renderResetCode = renderResetCode;
	}
	public String getRederPassword() {
		return rederPassword;
	}
	public void setRederPassword(String rederPassword) {
		this.rederPassword = rederPassword;
	}
	public String getResetCode() {
		return resetCode;
	}
	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}



	public String getResetPassword() {
		return resetPassword;
	}



	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}


	public String getCurrentPassword() {
		return currentPassword;
	}


	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public UserDTO getUserDTO() {
		if (userDTO == null) {

			userDTO = new UserDTO();
		}
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public List<UserDTO> getUserList() {
		return userList;
	}

	public void setUserList(List<UserDTO> userList) {
		this.userList = userList;
	}



	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}



	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}
	
	*/

}
