package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.SMSDTO;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.SmsService;

@ManagedBean
@ViewScoped
public class SmsController {

	private SMSDTO smsDTO;

	private List<SMSDTO> smsDTOList;

	@ManagedProperty("#{smsService}")
	private SmsService smsService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	public void saveSMS() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (smsDTO != null) {
				System.out.println("smsto " + smsDTO.getSmsTo());
				smsService.saveSms(smsDTO);
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				smsDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "area Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public SMSDTO getSmsDTO() {
		if (smsDTO == null) {
			this.smsDTO = new SMSDTO();
		}
		return smsDTO;
	}

	public void setSmsDTO(SMSDTO smsDTO) {
		this.smsDTO = smsDTO;
	}

	public List<SMSDTO> getSmsDTOList() {
		try {
			smsDTOList = new ArrayList<>();
			smsDTOList = smsService.findAllSMSList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return smsDTOList;
	}

	public void setSmsDTOList(List<SMSDTO> smsDTOList) {
		this.smsDTOList = smsDTOList;
	}

	public SmsService getSmsService() {
		return smsService;
	}

	public void setSmsService(SmsService smsService) {
		this.smsService = smsService;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}
