package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.TransactionManualDTO;
import bd.ac.uiu.service.TransactionManualService;
import bd.ac.uiu.service.UserService;

@ManagedBean
@ViewScoped
public class TransactionManualController {

	private TransactionManualDTO transactionManualDTO;

	@ManagedProperty("#{transactionManualService}")
	private TransactionManualService transactionManualService;

	private List<TransactionManualDTO> transactionManualDTOs;
	private int amountOfWaterCredit;
	private int unitPrice;
	private int waterCredit;

	/*
	 * get transactionManual Information from transactionManualDTO and save it
	 * using transactionManual.saveTransactionManual(transactionManualDTO);
	 */
	public void saveTransactionManual() {
		FacesContext context = FacesContext.getCurrentInstance();

		System.out.println((double) amountOfWaterCredit);
		System.out.println((double) unitPrice);

		transactionManualDTO = new TransactionManualDTO();

		transactionManualDTO.setAmountOfWater(amountOfWaterCredit);
		transactionManualDTO.setUnitPrice(unitPrice);
		transactionManualDTO.setWaterCredit(waterCredit);

		try {
			if (transactionManualService.saveTransactionManual(transactionManualDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				transactionManualDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * get inforamtion transactionManual information and update it using
	 * transactionManualService.updateTransactionmanual(transactionManual);
	 */

	public void updateTransactionManual() {
		FacesContext context = FacesContext.getCurrentInstance();
		transactionManualDTO.setRecordNote("N/A");
		try {
			transactionManualService.updateTransactionManual(transactionManualDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			transactionManualDTO = null;
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Update failed", ""));
		}
	}

	public List<TransactionManualDTO> getTransactionManualDTOs() {

		transactionManualDTOs = new ArrayList<>();
		try {
			transactionManualDTOs = transactionManualService.findTransactionManualList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return transactionManualDTOs;
	}

	public void setTransactionManualDTOs(List<TransactionManualDTO> transactionManualDTOs) {
		this.transactionManualDTOs = transactionManualDTOs;
	}

	public TransactionManualService getTransactionManualService() {
		return transactionManualService;
	}

	public void setTransactionManualService(TransactionManualService transactionManualService) {
		this.transactionManualService = transactionManualService;
	}

	public TransactionManualDTO getTransactionManualDTO() {
		if (transactionManualDTO == null) {
			transactionManualDTO = new TransactionManualDTO();
		}
		return transactionManualDTO;
	}

	public void setTransactionManualDTO(TransactionManualDTO transactionManualDTO) {
		this.transactionManualDTO = transactionManualDTO;
	}

	public int getAmountOfWaterCredit() {
		return amountOfWaterCredit;
	}

	public void setAmountOfWaterCredit(int amountOfWaterCredit) {
		this.amountOfWaterCredit = amountOfWaterCredit;
	}

	public int getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getWaterCredit() {
		return waterCredit;
	}

	public void setWaterCredit(int waterCredit) {
		this.waterCredit = waterCredit;
	}

}
