package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.RequestInformationDTO;
import bd.ac.uiu.dto.RequestInformationLogDTO;
import bd.ac.uiu.dto.ResponseInformationDTO;
import bd.ac.uiu.dto.ResponseInformationLogDTO;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.service.AreaService;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.RequestInformationLogService;
import bd.ac.uiu.service.RequestInformationService;
import bd.ac.uiu.service.ResponseInformationLogService;
import bd.ac.uiu.service.ResponseInformationService;
import bd.ac.uiu.service.WaterCreditSummaryService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@SessionScoped
public class CustomerController {
	private CustomerDTO customerDTO;
	private CustomerDTO customerDtoTarget;
	private List<CustomerDTO> customerList;
	private List<CustomerDTO> customerListOrganization;
	private List<CustomerDTO> customerSearchList;
	private List<AreaDTO> areaList;
	private List<AreaDTO> areaListOrga;
	private List<CustomerDTO> seaCustByAreaName;

	private BarChartModel barModel;

	private boolean rendersearchResult = false;
	private boolean renderSearchRest = false;

	private List<RequestInformationDTO> reqInfoListByOpAndCusID;
	private List<WaterCreditSummaryDTO> waCreSumListByOpAndCustID;
	private List<ResponseInformationDTO> resInfoListByOpAndCustID;

	private List<ResponseInformationLogDTO> resInfoLogListByOpAndCustID;
	private List<RequestInformationLogDTO> reqInfoLogListByOpAndCustID;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{requestInformationService}")
	private RequestInformationService requestInformationService;

	@ManagedProperty("#{waterCreditSummaryService}")
	private WaterCreditSummaryService waterCreditSummaryService;

	@ManagedProperty("#{responseInformationService}")
	private ResponseInformationService responseInformationService;

	@ManagedProperty("#{responseInformationLogService}")
	private ResponseInformationLogService responseInformationLogService;

	@ManagedProperty("#{requestInformationLogService}")
	private RequestInformationLogService requestInformationLogService;

	private boolean renOpCommon = false;

	private long areaID;

	private long ipcuID;

	private long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
	private long custID;
	private String searchCustomerName;

	@PostConstruct
	public void init() throws Exception {
		uiRenderView();
		createBarModels();
	}

	/*
	 * add new customer information with areaID and operatorID save the
	 * customerInformation using customerService.saveCustomer(customerDTO) if
	 * the data is saved then it will return successful message to the user
	 */

	public void saveCustomer() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			AreaDTO areaDTO = new AreaDTO();
			areaDTO.setAreaID(areaID);
			customerDTO.setAreaDTO(areaDTO);

			/*
			 * IpcuDTO ipcuDTO = new IpcuDTO(); ipcuDTO.setIpcuID(ipcuID);
			 * customerDTO.setIpcuDTO(ipcuDTO);
			 */
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(cerOrOrgOrOpIDLogin);
			customerDTO.setOperatorDTO(operatorDTO);

			if (customerService.saveCustomer(customerDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				customerDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Customer Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "National ID should be unique.", ""));
		}
	}

	/*
	 * update customer inforamtion by
	 * customerService.updateCustomer(customerDTO)
	 */
	public void updateCustomer() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			customerService.updateCustomer(customerDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			customerDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}

	}

	/*
	 * get all customer inforamtion from customerDTO and delete the information
	 * using customerService.deleteCustomer(customerDTO);
	 */
	public void deleteCustomer() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			customerService.deleteCustomer(customerDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			customerDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	/*
	 * based on areaID search customerInformation by from
	 * customerService.searcgCustomerByArea(areaID);
	 */
	public void searchCustomerByArea() {

		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.seaCustByAreaName = new ArrayList<>();
			this.seaCustByAreaName = customerService.seachCustomersByArea(areaID);

			// areaID = 0;

			/*
			 * if (seaCustByAreaName.isEmpty()) { context.addMessage(null, new
			 * FacesMessage(FacesMessage.SEVERITY_FATAL, "Area does not Exist",
			 * "")); } else { rendersearchResult = true; return
			 * seaCustByAreaName; }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * searchCustomerInforamtion by customerName from
	 * customerService.searchCustomerByName(searchCustomerName);
	 */
	public List<CustomerDTO> searchCustomer() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			customerSearchList = new ArrayList<>();
			customerSearchList = customerService.searchCustomerByName(searchCustomerName);
			searchCustomerName = null;

			if (customerSearchList.isEmpty()) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Customers does not Exist", ""));
			} else {
				renderSearchRest = true;
				return customerSearchList;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void uiRenderView() throws Exception {
		String userType = ApplicationUtility.getUserType();

		if (userType.equals("operator")) {
			renOpCommon = true;
		}
	}

	public CustomerDTO getCustomerDTO() {
		if (customerDTO == null) {
			this.customerDTO = new CustomerDTO();
		}
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public List<CustomerDTO> getCustomerList() {
		try {
			customerList = new ArrayList<>();
			customerList = customerService.findCustomersByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}
		return customerList;
	}

	public void setCustomerList(List<CustomerDTO> customerList) {
		this.customerList = customerList;
	}

	public List<CustomerDTO> getCustomerListOrganization() {
		try {
			customerListOrganization = new ArrayList<>();
			customerListOrganization = customerService.findCustomerList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerListOrganization;
	}

	public void setCustomerListOrganization(List<CustomerDTO> customerListOrganization) {
		this.customerListOrganization = customerListOrganization;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public List<AreaDTO> getAreaList() {
		try {
			areaList = new ArrayList<>();
			areaList = areaService.findAreaByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}
		return areaList;
	}

	public List<AreaDTO> getAreaListOrga() {
		try {
			areaListOrga = new ArrayList<>();
			areaListOrga = areaService.findAreaList();
		} catch (Exception e) {
		}
		return areaListOrga;
	}

	public void setAreaListOrga(List<AreaDTO> areaListOrga) {
		this.areaListOrga = areaListOrga;
	}

	public List<CustomerDTO> getSeaCustByAreaName() {
		try {
			seaCustByAreaName = new ArrayList<>();
			seaCustByAreaName = customerService.seachCustomersByArea(areaID);
			System.out.println("Size:==== " + seaCustByAreaName.size());

		} catch (Exception e) {
		}
		return seaCustByAreaName;
	}

	public void setSeaCustByAreaName(List<CustomerDTO> seaCustByAreaName) {
		this.seaCustByAreaName = seaCustByAreaName;
	}

	public void setAreaList(List<AreaDTO> areaList) {
		this.areaList = areaList;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public long getAreaID() {
		return areaID;
	}

	public void setAreaID(long areaID) {
		this.areaID = areaID;
	}

	public boolean isRenOpCommon() {
		return renOpCommon;
	}

	public void setRenOpCommon(boolean renOpCommon) {
		this.renOpCommon = renOpCommon;
	}

	public String jumpToCustomerProfile() {

		if (customerDtoTarget != null) {
			setCustID(customerDtoTarget.getCustomerID());
		}
		return "customerProfile.xhtml?faces-redirect=true";
	}

	public CustomerDTO getCustomerDtoTarget() {
		if (customerDtoTarget == null) {
			this.customerDtoTarget = new CustomerDTO();
		}
		return customerDtoTarget;
	}

	public void setCustomerDtoTarget(CustomerDTO customerDtoTarget) {
		this.customerDtoTarget = customerDtoTarget;
	}

	public List<RequestInformationDTO> getReqInfoListByOpAndCusID() {

		try {
			reqInfoListByOpAndCusID = new ArrayList<>();
			reqInfoListByOpAndCusID = requestInformationService.findRequestInfoByOpAndCustID(cerOrOrgOrOpIDLogin,
					getCustID());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return reqInfoListByOpAndCusID;
	}

	public void setReqInfoListByOpAndCusID(List<RequestInformationDTO> reqInfoListByOpAndCusID) {

		this.reqInfoListByOpAndCusID = reqInfoListByOpAndCusID;
	}

	public RequestInformationService getRequestInformationService() {
		return requestInformationService;
	}

	public void setRequestInformationService(RequestInformationService requestInformationService) {
		this.requestInformationService = requestInformationService;
	}

	public WaterCreditSummaryService getWaterCreditSummaryService() {
		return waterCreditSummaryService;
	}

	public void setWaterCreditSummaryService(WaterCreditSummaryService waterCreditSummaryService) {
		this.waterCreditSummaryService = waterCreditSummaryService;
	}

	public List<WaterCreditSummaryDTO> getWaCreSumListByOpAndCustID() {
		try {
			waCreSumListByOpAndCustID = new ArrayList<>();
			waCreSumListByOpAndCustID = waterCreditSummaryService
					.findWaterCreditSummaryByOpAndCustID(cerOrOrgOrOpIDLogin, getCustID());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return waCreSumListByOpAndCustID;
	}

	public void setWaCreSumListByOpAndCustID(List<WaterCreditSummaryDTO> waCreSumListByOpAndCustID) {
		this.waCreSumListByOpAndCustID = waCreSumListByOpAndCustID;
	}

	public ResponseInformationService getResponseInformationService() {
		return responseInformationService;
	}

	public void setResponseInformationService(ResponseInformationService responseInformationService) {
		this.responseInformationService = responseInformationService;
	}

	public List<ResponseInformationDTO> getResInfoListByOpAndCustID() {
		try {
			resInfoListByOpAndCustID = new ArrayList<>();
			resInfoListByOpAndCustID = responseInformationService.findResListByOpAndCustID(cerOrOrgOrOpIDLogin,
					getCustID());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resInfoListByOpAndCustID;
	}

	public void setResInfoListByOpAndCustID(List<ResponseInformationDTO> resInfoListByOpAndCustID) {
		this.resInfoListByOpAndCustID = resInfoListByOpAndCustID;
	}

	public List<ResponseInformationLogDTO> getResInfoLogListByOpAndCustID() {
		try {
			resInfoLogListByOpAndCustID = new ArrayList<>();
			resInfoLogListByOpAndCustID = responseInformationLogService
					.findResponseInformationLogListByOperatorIDAndCustomerID(cerOrOrgOrOpIDLogin, getCustID());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resInfoLogListByOpAndCustID;
	}

	public void setResInfoLogListByOpAndCustID(List<ResponseInformationLogDTO> resInfoLogListByOpAndCustID) {
		this.resInfoLogListByOpAndCustID = resInfoLogListByOpAndCustID;
	}

	public ResponseInformationLogService getResponseInformationLogService() {
		return responseInformationLogService;
	}

	public void setResponseInformationLogService(ResponseInformationLogService responseInformationLogService) {
		this.responseInformationLogService = responseInformationLogService;
	}

	public List<RequestInformationLogDTO> getReqInfoLogListByOpAndCustID() {
		try {
			reqInfoLogListByOpAndCustID = new ArrayList<>();
			reqInfoLogListByOpAndCustID = requestInformationLogService
					.findRequestInformationLogDTOByOperatorIDAndCustomerID(cerOrOrgOrOpIDLogin, getCustID());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reqInfoLogListByOpAndCustID;
	}

	public void setReqInfoLogListByOpAndCustID(List<RequestInformationLogDTO> reqInfoLogListByOpAndCustID) {
		this.reqInfoLogListByOpAndCustID = reqInfoLogListByOpAndCustID;
	}

	public RequestInformationLogService getRequestInformationLogService() {
		return requestInformationLogService;
	}

	public void setRequestInformationLogService(RequestInformationLogService requestInformationLogService) {
		this.requestInformationLogService = requestInformationLogService;
	}

	public long getCustID() {
		return custID;
	}

	public void setCustID(long custID) {
		this.custID = custID;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public String getSearchCustomerName() {
		return searchCustomerName;
	}

	public void setSearchCustomerName(String searchCustomerName) {
		this.searchCustomerName = searchCustomerName;
	}

	public boolean isRendersearchResult() {
		return rendersearchResult;
	}

	public void setRendersearchResult(boolean rendersearchResult) {
		this.rendersearchResult = rendersearchResult;
	}

	public boolean isRenderSearchRest() {
		return renderSearchRest;
	}

	public void setRenderSearchRest(boolean renderSearchRest) {
		this.renderSearchRest = renderSearchRest;
	}

	public List<CustomerDTO> getCustomerSearchList() {
		return customerSearchList;
	}

	public void setCustomerSearchList(List<CustomerDTO> customerSearchList) {
		this.customerSearchList = customerSearchList;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	private void createBarModels() {
		createBarModel();

	}

	private BarChartModel initBarModel() {
		BarChartModel model = new BarChartModel();

		ChartSeries boys = new ChartSeries();
		boys.setLabel("Boys");
		boys.set("2004", 120);
		boys.set("2005", 100);
		boys.set("2006", 44);
		boys.set("2007", 150);
		boys.set("2008", 25);

		ChartSeries girls = new ChartSeries();
		girls.setLabel("Girls");
		girls.set("2004", 52);
		girls.set("2005", 60);
		girls.set("2006", 110);
		girls.set("2007", 135);
		girls.set("2008", 120);

		model.addSeries(boys);
		model.addSeries(girls);

		return model;
	}

	private void createBarModel() {
		barModel = initBarModel();

		barModel.setTitle("Bar Chart");
		barModel.setLegendPosition("ne");

		Axis xAxis = barModel.getAxis(AxisType.X);
		xAxis.setLabel("Gender");

		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setLabel("Births");
		yAxis.setMin(0);
		yAxis.setMax(200);
	}
}
