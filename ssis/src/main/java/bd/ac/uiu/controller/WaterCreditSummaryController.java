package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.customClass.TotalWaterCreditConsumeByOperator;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.service.WaterCreditSummaryService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class WaterCreditSummaryController {

	private WaterCreditSummaryDTO waterCreditSummaryDTO;

	private List<WaterCreditSummaryDTO> waterCreditSummaryList;
	private List<WaterCreditSummaryDTO> wListOrganization;

	private WaterCreditSummaryDTO waCreSumByOpAndCusID;

	private TotalWaterCreditConsumeByOperator totalWaterCreditConsumeByOperators;

	private long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

	@ManagedProperty("#{waterCreditSummaryService}")
	private WaterCreditSummaryService waterCreditSummaryService;

	long operatorID;

	long totalusedWaterCredit;

	@PostConstruct
	public void init() {

	}

	public void saveWaterCreditSummary() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (waterCreditSummaryService.saveWaterCreditSummary(waterCreditSummaryDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				waterCreditSummaryDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "waterCreditSummary Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void updateWaterCreditSummary() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			waterCreditSummaryService.updateWaterCreditSummary(waterCreditSummaryDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			waterCreditSummaryDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deleteWaterCreditSummary() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			waterCreditSummaryService.deleteWaterCreditSummary(waterCreditSummaryDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			waterCreditSummaryDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public WaterCreditSummaryDTO getWaterCreditSummaryDTO() {
		if (waterCreditSummaryDTO == null) {
			waterCreditSummaryDTO = new WaterCreditSummaryDTO();
		}
		return waterCreditSummaryDTO;
	}

	public void setWaterCreditSummaryDTO(WaterCreditSummaryDTO waterCreditSummaryDTO) {
		this.waterCreditSummaryDTO = waterCreditSummaryDTO;
	}

	public List<WaterCreditSummaryDTO> getWaterCreditSummaryList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			waterCreditSummaryList = new ArrayList<>();
			waterCreditSummaryList = waterCreditSummaryService.findWaterCreditSummaryByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
		}
		return waterCreditSummaryList;
	}

	public void setWaterCreditSummaryList(List<WaterCreditSummaryDTO> waterCreditSummaryList) {
		this.waterCreditSummaryList = waterCreditSummaryList;
	}

	public List<WaterCreditSummaryDTO> getwListOrganization() {
		try {
			wListOrganization = new ArrayList<>();
			wListOrganization = waterCreditSummaryService.findWaterCreditSummaryList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return wListOrganization;
	}

	public void setwListOrganization(List<WaterCreditSummaryDTO> wListOrganization) {
		this.wListOrganization = wListOrganization;
	}

	public WaterCreditSummaryService getWaterCreditSummaryService() {
		return waterCreditSummaryService;
	}

	public void setWaterCreditSummaryService(WaterCreditSummaryService waterCreditSummaryService) {
		this.waterCreditSummaryService = waterCreditSummaryService;
	}

	public void findCusWaCreSumInformation(long customerID) {
		try {
			waCreSumByOpAndCusID = waterCreditSummaryService
					.findWaterCreditSummaryByOperatorIDAndCustomerID(cerOrOrgOrOpIDLogin, customerID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void totalWaterCreditConsumeOperatorWise() throws Exception {

		totalWaterCreditConsumeByOperators = new TotalWaterCreditConsumeByOperator();
		this.totalWaterCreditConsumeByOperators = waterCreditSummaryService.totalWaterCreditOperatorWise(operatorID);
	}

	public TotalWaterCreditConsumeByOperator getTotalWaterCreditConsumeByOperators() {
		return totalWaterCreditConsumeByOperators;
	}

	public void setTotalWaterCreditConsumeByOperators(
			TotalWaterCreditConsumeByOperator totalWaterCreditConsumeByOperators) {
		this.totalWaterCreditConsumeByOperators = totalWaterCreditConsumeByOperators;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public void findTotalWaterCreditCalculation() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.totalusedWaterCredit = waterCreditSummaryService.findTotalWaterCreditByOperatorID(operatorID);
			
			if(totalusedWaterCredit==0){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "This operator cannot have used water credit yet.", ""));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Empty...");
		}
	}

	public long getTotalusedWaterCredit() {
		return totalusedWaterCredit;
	}

	public void setTotalusedWaterCredit(long totalusedWaterCredit) {
		this.totalusedWaterCredit = totalusedWaterCredit;
	}

}
