package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import bd.ac.uiu.dto.RequestInformationLogDTO;
import bd.ac.uiu.service.RequestInformationLogService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class RequestInformationLogController {

	private RequestInformationLogDTO requestInformationLogDTO;
	private List<RequestInformationLogDTO> requestInformationLogList;
	@ManagedProperty("#{requestInformationLogService}")
	private RequestInformationLogService requestInformationLogService;

	public void saveRequestInformationLog() {

	}

	public void updateRequestInformationLog() {

	}

	public void deleteRequestInformationLog() {

	}

	public RequestInformationLogDTO getRequestInformationLogDTO() {
		if (requestInformationLogDTO == null) {
			requestInformationLogDTO = new RequestInformationLogDTO();
		}
		return requestInformationLogDTO;
	}

	public void setRequestInformationLogDTO(RequestInformationLogDTO requestInformationLogDTO) {
		this.requestInformationLogDTO = requestInformationLogDTO;
	}

	public List<RequestInformationLogDTO> getRequestInformationLogList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			requestInformationLogList = new ArrayList<>();
			requestInformationLogList = requestInformationLogService
					.findRequestInformationLogDTOByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}

		return requestInformationLogList;
	}
	
	public List<RequestInformationLogDTO> getRequestInformationLogListForOrganization() {
		
		try {
			requestInformationLogList = new ArrayList<>();
			requestInformationLogList = requestInformationLogService
					.findRequestInformationLogList();
		} catch (Exception e) {
			System.out.println(e);
		}

		return requestInformationLogList;
	}

	public void setRequestInformationLogList(List<RequestInformationLogDTO> requestInformationLogList) {
		this.requestInformationLogList = requestInformationLogList;
	}

	public RequestInformationLogService getRequestInformationService() {
		return requestInformationLogService;
	}

	public void setRequestInformationLogService(RequestInformationLogService requestInformationLogService) {
		this.requestInformationLogService = requestInformationLogService;
	}
}
