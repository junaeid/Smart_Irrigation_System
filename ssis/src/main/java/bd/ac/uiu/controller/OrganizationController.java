package bd.ac.uiu.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.service.CerService;
import bd.ac.uiu.service.OrganizationService;
import bd.ac.uiu.utility.ApplicationUtility;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */
@ManagedBean
@ViewScoped
public class OrganizationController implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(OrganizationController.class);

	@ManagedProperty("#{organizationService}")
	private OrganizationService organizationService;

	@ManagedProperty("#{cerService}")
	private CerService cerService;

	private OrganizationDTO organizationDTO;
	private List<OrganizationDTO> organizationDTOs;
	private List<CerDTO> cerDTOs;

	private long cerID;

	/*
	 * get cer id from season and set it to organizationDTO then organization
	 * information from organizationDTO then save it using
	 * organizationService.saveOrganization(organizationDTO);
	 */

	public void saveOrganizations() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			long cerID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
			CerDTO dTO = new CerDTO();
			dTO.setCerID(cerID);
			organizationDTO.setCerDTO(dTO);
			System.out.println("cer id: " + organizationDTO.getCerDTO().getCerID());
			if (organizationDTO != null) {
				organizationService.saveOrganization(organizationDTO);
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				organizationDTO = null;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.debug("This is Debug " + e);

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	/*
	 * get organization informaiton from organizationDTO and then update it using
	 * organizationService.updateOrganization(organizationDTO);
	 */
	public void updateOrganization() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			organizationService.updateOrganization(organizationDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated."));
			organizationDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public OrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public OrganizationDTO getOrganizationDTO() {
		if (organizationDTO == null) {

			organizationDTO = new OrganizationDTO();
		}
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}

	public List<OrganizationDTO> getOrganizationDTOs() {
		try {
			organizationDTOs = new ArrayList<>();
			organizationDTOs = organizationService.findAllOrganization();
			System.out.println("size of list: " + organizationDTOs.size());
		} catch (Exception e) {
			System.out.println(e);
		}

		return organizationDTOs;
	}

	public void setOrganizationDTOs(List<OrganizationDTO> organizationDTOs) {
		this.organizationDTOs = organizationDTOs;
	}

	public List<CerDTO> getCerDTOs() {
		try {
			cerDTOs = new ArrayList<>();
			cerDTOs = cerService.findAllCer();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return cerDTOs;
	}

	public void setCerDTOs(List<CerDTO> cerDTOs) {
		this.cerDTOs = cerDTOs;
	}

	public CerService getCerService() {
		return cerService;
	}

	public void setCerService(CerService cerService) {
		this.cerService = cerService;
	}

	public long getCerID() {
		return cerID;
	}

	public void setCerID(long cerID) {
		this.cerID = cerID;
	}

}
