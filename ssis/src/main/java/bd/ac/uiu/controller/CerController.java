package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;

import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.service.CerService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class CerController {

	private CerDTO cerDTO;

	@Autowired
	private UserController userController;

	@ManagedProperty("#{cerService}")
	private CerService cerService;
	private List<CerDTO> cerDTOs;

	/*
	 * save cer information by cerDTO using cerService.saveCER(cerDTO);
	 */

	public void saveCERs() {

		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (cerDTO != null) {
				cerService.saveCER(cerDTO);
				context.addMessage(null, new FacesMessage("Successfully Saved"));
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/*
	 * get cerInformation using cerDTO by cerID then update cer information by
	 * cerService.updateCre(cerDTO)
	 */

	public void updateCer() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			cerService.updateCer(cerDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			cerDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public CerDTO getCerDTO() {
		if (cerDTO == null) {
			this.cerDTO = new CerDTO();
		}
		return cerDTO;
	}

	public void setCerDTO(CerDTO cerDTO) {
		this.cerDTO = cerDTO;
	}

	public CerService getCerService() {
		return cerService;
	}

	public void setCerService(CerService cerService) {
		this.cerService = cerService;
	}

	public List<CerDTO> getCerDTOs() {
		try {
			cerDTOs = new ArrayList<>();
			cerDTOs = cerService.findAllCer();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return cerDTOs;
	}

	public void setCerDTOs(List<CerDTO> cerDTOs) {
		this.cerDTOs = cerDTOs;
	}

}
