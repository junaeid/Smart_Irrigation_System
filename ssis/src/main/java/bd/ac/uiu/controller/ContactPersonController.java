package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.dto.ContactPersonDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.service.CerService;
import bd.ac.uiu.service.ContactPersonService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.OrganizationService;

@ManagedBean
@ViewScoped
public class ContactPersonController {

	private ContactPersonDTO contactPersonDTO;
	private List<ContactPersonDTO> contactPersonList;

	private CerDTO cerDTO;
	private OrganizationDTO organizationDTO;
	private OperatorDTO operatorDTO;

	private List<CerDTO> cerDTOs;
	private List<OrganizationDTO> organizationDTOs;
	private List<OperatorDTO> operatorDTOs;

	@ManagedProperty("#{contactPersonService}")
	private ContactPersonService contactPersonService;

	@ManagedProperty("#{cerService}")
	private CerService cerService;

	@ManagedProperty("#{organizationService}")
	private OrganizationService organizationService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	private boolean renCer = false;
	private boolean renOrg = false;
	private boolean renOp = false;

	public void saveContactPerson() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (cerDTO != null) {
				CerDTO dto = new CerDTO();
				dto.setCerID(cerDTO.getCerID());
				contactPersonDTO.setCerDTO(dto);
			}
			if (organizationDTO != null) {
				OrganizationDTO dto1 = new OrganizationDTO();
				dto1.setOrganizationID(organizationDTO.getOrganizationID());
				contactPersonDTO.setOrganizationDTO(dto1);
			}
			if (operatorDTO != null) {
				OperatorDTO dto2 = new OperatorDTO();
				dto2.setOperatorID(operatorDTO.getOperatorID());
				contactPersonDTO.setOperatorDTO(dto2);

			}

			if (contactPersonService.saveContactPerson(contactPersonDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				contactPersonDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Contact Person Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void updateContactPerson() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			contactPersonService.updateContactPerson(contactPersonDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			contactPersonDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deleteContactPerson() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			contactPersonService.deleteContactPerson(contactPersonDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			contactPersonDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void uiRender() {

		switch (contactPersonDTO.getUserType()) {
		case "cer":
			renCer = true;
			renOrg = false;
			renOp = false;
			break;

		case "organization":
			renCer = false;
			renOrg = true;
			renOp = false;
			break;

		case "operator":
			renCer = false;
			renOrg = false;
			renOp = true;
			break;

		default:

			renCer = false;
			renOrg = false;
			renOp = false;
		}
	}

	public boolean isRenCer() {
		return renCer;
	}

	public void setRenCer(boolean renCer) {
		this.renCer = renCer;
	}

	public boolean isRenOrg() {
		return renOrg;
	}

	public void setRenOrg(boolean renOrg) {
		this.renOrg = renOrg;
	}

	public boolean isRenOp() {
		return renOp;
	}

	public void setRenOp(boolean renOp) {
		this.renOp = renOp;
	}

	public ContactPersonDTO getContactPersonDTO() {
		if (contactPersonDTO == null) {
			this.contactPersonDTO = new ContactPersonDTO();
		}
		return contactPersonDTO;
	}

	public void setContactPersonDTO(ContactPersonDTO contactPersonDTO) {
		this.contactPersonDTO = contactPersonDTO;
	}

	public List<ContactPersonDTO> getContactPersonList() {
		try {
			contactPersonList = new ArrayList<>();
			contactPersonList = contactPersonService.findContactPersonList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return contactPersonList;
	}

	public void setContactPersonList(List<ContactPersonDTO> contactPersonList) {
		this.contactPersonList = contactPersonList;
	}

	public ContactPersonService getContactPersonService() {
		return contactPersonService;
	}

	public void setContactPersonService(ContactPersonService contactPersonService) {
		this.contactPersonService = contactPersonService;
	}

	public CerDTO getCerDTO() {
		if (cerDTO == null) {

			cerDTO = new CerDTO();

		}
		return cerDTO;
	}

	public void setCerDTO(CerDTO cerDTO) {
		this.cerDTO = cerDTO;
	}

	public OrganizationDTO getOrganizationDTO() {
		if (organizationDTO == null) {
			organizationDTO = new OrganizationDTO();
		}
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}

	public OperatorDTO getOperatorDTO() {
		if (operatorDTO == null) {
			operatorDTO = new OperatorDTO();

		}
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public OrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public CerService getCerService() {
		return cerService;
	}

	public void setCerService(CerService cerService) {
		this.cerService = cerService;
	}

	public List<CerDTO> getCerDTOs() {
		try {
			cerDTOs = new ArrayList<>();
			cerDTOs = cerService.findAllCer();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return cerDTOs;
	}

	public void setCerDTOs(List<CerDTO> cerDTOs) {
		this.cerDTOs = cerDTOs;
	}

	public List<OperatorDTO> getOperatorDTOs() {
		try {
			operatorDTOs = new ArrayList<>();
			operatorDTOs = operatorService.findAllOperatorList();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return operatorDTOs;
	}

	public void setOperatorDTOs(List<OperatorDTO> operatorDTOs) {
		this.operatorDTOs = operatorDTOs;
	}

	public List<OrganizationDTO> getOrganizationDTOs() {
		try {
			organizationDTOs = new ArrayList<>();
			organizationDTOs = organizationService.findAllOrganization();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return organizationDTOs;
	}

	public void setOrganizationDTOs(List<OrganizationDTO> organizationDTOs) {
		this.organizationDTOs = organizationDTOs;
	}

}
