package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.PaymentTypeDTO;
import bd.ac.uiu.service.PaymentTypeService;

@ManagedBean
@ViewScoped
public class PaymentTypeController {

	private PaymentTypeDTO paymentTypeDTO;
	private List<PaymentTypeDTO> paymentTypeList;
	@ManagedProperty("#{paymentTypeService}")
	private PaymentTypeService paymentTypeService;

	public void savePaymentType() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (paymentTypeService.savePaymentType(paymentTypeDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				paymentTypeDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Payment Type Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));

		}
	}

	public void updatePaymentType() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			paymentTypeService.updatePaymentType(paymentTypeDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			paymentTypeDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public PaymentTypeDTO getPaymentTypeDTO() {
		if (paymentTypeDTO == null) {
			this.paymentTypeDTO = new PaymentTypeDTO();
		}
		return paymentTypeDTO;
	}

	public void setPaymentTypeDTO(PaymentTypeDTO paymentTypeDTO) {
		this.paymentTypeDTO = paymentTypeDTO;
	}

	public List<PaymentTypeDTO> getPaymentTypeList() {
		try {
			paymentTypeList = new ArrayList<>();
			paymentTypeList = paymentTypeService.findPaymentTypeList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return paymentTypeList;
	}

	public void setPaymentTypeList(List<PaymentTypeDTO> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}

	public PaymentTypeService getPaymentTypeService() {
		return paymentTypeService;
	}

	public void setPaymentTypeService(PaymentTypeService paymentTypeService) {
		this.paymentTypeService = paymentTypeService;
	}

}
