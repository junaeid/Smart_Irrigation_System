package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.RequestInformationDTO;
import bd.ac.uiu.service.RequestInformationService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class RequestInformationController {

	private RequestInformationDTO requestInformationDTO;
	private List<RequestInformationDTO> requestInformationList;

	private CustomerDTO customerDtoTarget;
	@ManagedProperty("#{requestInformationService}")
	private RequestInformationService requestInformationService;

	public void saveRequestInformation() {
		FacesContext context = FacesContext.getCurrentInstance();
		// requestInformationDTO = new RequestInformationDTO();
		// requestInformationDTO.setSoilMoisture(50);
		// requestInformationDTO.setMotoIPCU(12);
		// requestInformationDTO.setWaterLevelSensor(200);
		// requestInformationDTO.setFieldValve(12);
		// requestInformationDTO.setWaterCredit(300);
		// requestInformationDTO.setFlowRate(30);
		// requestInformationDTO.setTemperature(43);
		// requestInformationDTO.setHumidity(60);
		// requestInformationDTO.setDeviceStatus(1);
		// requestInformationDTO.setNodeID(2);
		// requestInformationDTO.setSeason(0);

		try {
			if (requestInformationService.saveRequestInformation(requestInformationDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				requestInformationDTO = null;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));

		}

	}

	public void updateRequestInformation() {

	}

	public void deleteReequestInformation() {

	}

	public RequestInformationDTO getRequestInformationDTO() {
		if (requestInformationDTO == null) {
			requestInformationDTO = new RequestInformationDTO();
		}
		return requestInformationDTO;
	}

	public void setRequestInformationDTO(RequestInformationDTO requestInformationDTO) {
		this.requestInformationDTO = requestInformationDTO;
	}

	public List<RequestInformationDTO> getRequestInformationListByOperator() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			requestInformationList = new ArrayList<>();
			requestInformationList = requestInformationService.findRequestInformationByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}

		return requestInformationList;
	}
	
	public List<RequestInformationDTO> getRequestInformationList() {
		
		try {
			requestInformationList = new ArrayList<>();
			requestInformationList = requestInformationService.findRequestInformationList();
		} catch (Exception e) {
			System.out.println(e);
		}

		return requestInformationList;
	}

	public void setRequestInformationList(List<RequestInformationDTO> requestInformationList) {
		this.requestInformationList = requestInformationList;
	}

	public RequestInformationService getRequestInformationService() {
		return requestInformationService;
	}

	public void setRequestInformationService(RequestInformationService requestInformationService) {
		this.requestInformationService = requestInformationService;
	}

	public CustomerDTO getCustomerDtoTarget() {
		if (customerDtoTarget == null) {
			this.customerDtoTarget = new CustomerDTO();
		}
		return customerDtoTarget;
	}

	public void setCustomerDtoTarget(CustomerDTO customerDtoTarget) {
		this.customerDtoTarget = customerDtoTarget;
	}

}
