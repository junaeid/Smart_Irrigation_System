package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.PaymentSummaryDTO;
import bd.ac.uiu.service.PaymentSummaryService;

@ManagedBean
@ViewScoped
public class PaymentSummaryController {

	private PaymentSummaryDTO paymentSummaryDTO;

	private List<PaymentSummaryDTO> paymentSummaryList;

	@ManagedProperty("#{paymentSummaryService}")
	private PaymentSummaryService paymentSummaryService;

	private long customerID = 1;

	
	public void savePaymentSummary() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			if (!paymentSummaryService.savePaymentSummary(paymentSummaryDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				paymentSummaryDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Contact Person Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void updatePaymentSummary() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			paymentSummaryService.updatePaymentSummary(paymentSummaryDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			paymentSummaryDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deletePaymentSummary() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			paymentSummaryService.deletePaymentSummary(paymentSummaryDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			paymentSummaryDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public PaymentSummaryDTO getPaymentSummaryDTO() {
		if (paymentSummaryDTO == null) {
			this.paymentSummaryDTO = new PaymentSummaryDTO();
		}
		return paymentSummaryDTO;
	}

	public void setPaymentSummaryDTO(PaymentSummaryDTO paymentSummaryDTO) {
		this.paymentSummaryDTO = paymentSummaryDTO;
	}

	public List<PaymentSummaryDTO> getPaymentSummaryList() {
		try {
			paymentSummaryList = new ArrayList<>();
			paymentSummaryList = paymentSummaryService.findList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return paymentSummaryList;
	}

	public void setPaymentSummaryList(List<PaymentSummaryDTO> paymentSummaryList) {
		this.paymentSummaryList = paymentSummaryList;
	}

	public PaymentSummaryService getPaymentSummaryService() {
		return paymentSummaryService;
	}

	public void setPaymentSummaryService(PaymentSummaryService paymentSummaryService) {
		this.paymentSummaryService = paymentSummaryService;
	}

}
