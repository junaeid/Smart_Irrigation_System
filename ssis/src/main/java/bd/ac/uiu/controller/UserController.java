package bd.ac.uiu.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.BeanUtils;

import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.dto.UserDTO;
import bd.ac.uiu.service.CerService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.OrganizationService;
import bd.ac.uiu.service.UserService;
import bd.ac.uiu.utility.ApplicationUtility;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

@ManagedBean
@ViewScoped
public class UserController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserDTO userDTO;
	private List<UserDTO> userList;
	private List<UserDTO> userListDTOByOperatorID;
	private List<UserDTO> userDTOsByDateRange;
	private String userNameForSearch;
	private boolean rendersearchResult = false;
	private CerDTO cerDTO;

	private OrganizationDTO organizationDTO;

	private OperatorDTO operatorDTO;

	private List<CerDTO> cerDTOs;

	private List<OrganizationDTO> organizationDTOs;

	private List<OperatorDTO> operatorDTOs;

	private long operatorID;

	@ManagedProperty("#{userService}")
	private UserService userService;

	@ManagedProperty("#{cerService}")
	private CerService cerService;

	@ManagedProperty("#{organizationService}")
	private OrganizationService organizationService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	private boolean renCerCommon = false;
	private boolean renCerAdmin = false;

	private boolean renOrgCommon = false;
	private boolean renOrgAdmin = false;

	private boolean renOpCommon = false;
	private boolean renOpAdmin = false;

	private boolean renForAllAdmin = false;
	private boolean renForAll = false;

	private boolean renCer = false;
	private boolean renOrg = false;
	private boolean renOp = false;

	private boolean renCerAndOrg = false;

	private String pageTitle;
	private String userName;

	@PostConstruct
	public void init() {
		try {

			uiRenderMenu();

			// uiRender();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

		
	public void searchUser() {
		try {
			userDTO = new UserDTO();
			UserDTO user = new UserDTO();
			userList = userService.findUserList();
			for (UserDTO dto : userList) {
				if (userNameForSearch != null && dto.getUsername().equals(userNameForSearch)) {
					user = userService.searchUser(userNameForSearch);
					System.out.println("user ......:  " + user.getName());
					if (user.getUsername() != null) {
						BeanUtils.copyProperties(dto, userDTO);
						rendersearchResult = true;
					}
				}
			}

			System.out.println("searchresultDTO: " + userDTO.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void checkUser() {

		if (userService.checkUser(userDTO)) {

		}

		else {

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "username is already exists", ""));
		}

	}

	public void saveUserOk() {
		String userType = ApplicationUtility.getUserType();
		long cerOrgOpID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (cerDTO == null) {
				CerDTO dto = new CerDTO();
				dto.setCerID(cerOrgOpID);
				userDTO.setCerDTO(dto);
				// userDTO.setUserType(ApplicationUtility.getUserType());
			}
			if (organizationDTO == null) {
				OrganizationDTO dto1 = new OrganizationDTO();
				dto1.setOrganizationID(cerOrgOpID);
				userDTO.setOrganizationDTO(dto1);
				// userDTO.setUserType(ApplicationUtility.getUserType());
			}
			if (operatorDTO == null) {
				OperatorDTO dto2 = new OperatorDTO();
				dto2.setOperatorID(cerOrgOpID);
				userDTO.setOperatorDTO(dto2);
				// userDTO.setUserType(ApplicationUtility.getUserType());

			}
			userDTO.setUserType(userType);

			if (userService.saveUser(userDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				userDTO = null;

			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "user Exist", ""));
			}

		}

		catch (Exception e) {
			System.out.println(e.getMessage());

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}

	}

	public void saveUserRegistration() {

		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (cerDTO != null) {
				CerDTO dto = new CerDTO();
				dto.setCerID(cerDTO.getCerID());
				userDTO.setCerDTO(dto);
				// userDTO.setUserType(ApplicationUtility.getUserType());
			}
			if (organizationDTO != null) {
				OrganizationDTO dto1 = new OrganizationDTO();
				dto1.setOrganizationID(organizationDTO.getOrganizationID());
				userDTO.setOrganizationDTO(dto1);
				// userDTO.setUserType(ApplicationUtility.getUserType());
			}
			if (operatorDTO != null) {
				OperatorDTO dto2 = new OperatorDTO();
				dto2.setOperatorID(operatorDTO.getOperatorID());
				userDTO.setOperatorDTO(dto2);
				// userDTO.setUserType(ApplicationUtility.getUserType());

			}

			if (userService.saveUser(userDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				userDTO = null;

			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "user Exist", ""));
			}

		}

		catch (Exception e) {
			System.out.println(e.getMessage());

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}

	}

	public void updateUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			userService.updateUser(userDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			userDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deleteUser() {

		FacesContext context = FacesContext.getCurrentInstance();
		try {
			userService.deleteCustomer(userDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			userDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void uiRenderMenu() throws Exception {
		String userType = null;
		String userRoleName = null;
		try {
			userType = ApplicationUtility.getUserType();
			userRoleName = ApplicationUtility.getUserRole();

			if (userType.equals("cer") && userRoleName.equals("ROLE_CER_ADMIN")) {
				renCerCommon = true;
				renCerAdmin = true;
				renOrgCommon = false;
				renOrgAdmin = false;
				renOpCommon = false;
				renOpAdmin = false;
				renForAll = true;
				renForAllAdmin = true;
				renCerAndOrg = true;
			} else if (userType.equals("cer") && userRoleName.equals("ROLE_CER_USER")) {
				renCerCommon = true;
				renCerAdmin = false;
				renOrgCommon = true;
				renOrgAdmin = true;
				renOpCommon = false;
				renOpAdmin = false;
				renForAll = true;
				renCerAndOrg = true;
			} else if (userType.equals("organization") && userRoleName.equals("ROLE_ORG_ADMIN")) {
				renCerCommon = false;
				renCerAdmin = false;
				renOrgCommon = true;
				renOrgAdmin = true;
				renOpCommon = false;
				renOpAdmin = false;
				renForAll = true;
				renForAllAdmin = true;
				renCerAndOrg = true;
			} else if (userType.equals("organization") && userRoleName.equals("ROLE_ORG_USER")) {
				renCerCommon = false;
				renCerAdmin = false;
				renOrgCommon = true;
				renOrgAdmin = false;
				renOpCommon = false;
				renOpAdmin = false;
				renForAll = true;
			} else if (userType.equals("operator") && userRoleName.equals("ROLE_OP_ADMIN")) {
				renCerCommon = false;
				renCerAdmin = false;
				renOrgCommon = false;
				renOrgAdmin = false;
				renOpCommon = true;
				renOpAdmin = true;
				renForAll = true;
				renForAllAdmin = true;
			} else if (userType.equals("operator") && userRoleName.equals("ROLE_OP_USER")) {
				renCerCommon = false;
				renCerAdmin = false;
				renOrgCommon = false;
				renOrgAdmin = false;
				renOpCommon = true;
				renOpAdmin = false;
				renForAll = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void uiRender() throws Exception {
		// String userType = ApplicationUtility.getUserType();
		switch (userDTO.getUserType()) {
		case "cer":
			renCer = true;
			renOrg = false;
			renOp = false;
			break;

		case "organization":
			renCer = false;
			renOrg = true;
			renOp = false;
			break;

		case "operator":
			renCer = false;
			renOrg = false;
			renOp = true;
			break;

		default:
			renCer = false;
			renOrg = false;
			renOp = false;
		}
	}

	public void renderPageTitletouserType() {
		String userType = ApplicationUtility.getUserType();
		System.out.println("user type: " + userType);
		switch (userType) {
		case "cer":
			pageTitle = ApplicationUtility.getPagetitle();

			break;
		case "organization":
			pageTitle = ApplicationUtility.getPagetitle();
			break;
		case "operator":
			pageTitle = ApplicationUtility.getPagetitle();
			break;
		default:
			pageTitle = "United International University";
		}
	}

	public void findUsersByOperatorID() {

		try {
			System.out.println("Operator ID:" + operatorID);

			userListDTOByOperatorID = new ArrayList<>();
			userListDTOByOperatorID = userService.findUsersbyOpID(operatorID, "operator");
			System.out.println("userListDTOByOperatorID size: " + userListDTOByOperatorID.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public UserDTO getUserDTO() {
		if (userDTO == null) {
			this.userDTO = new UserDTO();
		}
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public List<UserDTO> getUserList() {
		String userType = ApplicationUtility.getUserType();
		long cerOrgOpID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			userList = new ArrayList<>();
			userList = userService.findUsersbyCerOrgOpID(cerOrgOpID, userType);
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return userList;
	}

	public void setUserList(List<UserDTO> userList) {
		this.userList = userList;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public boolean isRenCer() {
		return renCer;
	}

	public void setRenCer(boolean renCer) {
		this.renCer = renCer;
	}

	public boolean isRenOrg() {
		return renOrg;
	}

	public void setRenOrg(boolean renOrg) {
		this.renOrg = renOrg;
	}

	public boolean isRenOp() {
		return renOp;
	}

	public void setRenOp(boolean renOp) {
		this.renOp = renOp;
	}

	public List<CerDTO> getCerDTOs() {
		try {
			cerDTOs = new ArrayList<>();
			cerDTOs = cerService.findAllCer();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return cerDTOs;
	}

	public void setCerDTOs(List<CerDTO> cerDTOs) {
		this.cerDTOs = cerDTOs;
	}

	public List<OperatorDTO> getOperatorDTOs() {
		try {
			operatorDTOs = new ArrayList<>();
			operatorDTOs = operatorService.findAllOperatorList();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return operatorDTOs;
	}

	public void setOperatorDTOs(List<OperatorDTO> operatorDTOs) {
		this.operatorDTOs = operatorDTOs;
	}

	public CerService getCerService() {
		return cerService;
	}

	public void setCerService(CerService cerService) {
		this.cerService = cerService;
	}

	public List<OrganizationDTO> getOrganizationDTOs() {
		try {
			organizationDTOs = new ArrayList<>();
			organizationDTOs = organizationService.findAllOrganization();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return organizationDTOs;
	}

	public void setOrganizationDTOs(List<OrganizationDTO> organizationDTOs) {
		this.organizationDTOs = organizationDTOs;
	}

	public OrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public CerDTO getCerDTO() {
		if (cerDTO == null) {

			cerDTO = new CerDTO();

		}
		return cerDTO;
	}

	public void setCerDTO(CerDTO cerDTO) {
		this.cerDTO = cerDTO;
	}

	public OrganizationDTO getOrganizationDTO() {
		if (organizationDTO == null) {
			organizationDTO = new OrganizationDTO();
		}
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}

	public OperatorDTO getOperatorDTO() {
		if (operatorDTO == null) {
			operatorDTO = new OperatorDTO();

		}
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public boolean isRenCerCommon() {
		return renCerCommon;
	}

	public void setRenCerCommon(boolean renCerCommon) {
		this.renCerCommon = renCerCommon;
	}

	public boolean isRenCerAdmin() {
		return renCerAdmin;
	}

	public void setRenCerAdmin(boolean renCerAdmin) {
		this.renCerAdmin = renCerAdmin;
	}

	public boolean isRenOrgCommon() {
		return renOrgCommon;
	}

	public void setRenOrgCommon(boolean renOrgCommon) {
		this.renOrgCommon = renOrgCommon;
	}

	public boolean isRenOrgAdmin() {
		return renOrgAdmin;
	}

	public void setRenOrgAdmin(boolean renOrgAdmin) {
		this.renOrgAdmin = renOrgAdmin;
	}

	public boolean isRenOpCommon() {
		return renOpCommon;
	}

	public void setRenOpCommon(boolean renOpCommon) {
		this.renOpCommon = renOpCommon;
	}

	public boolean isRenOpAdmin() {
		return renOpAdmin;
	}

	public void setRenOpAdmin(boolean renOpAdmin) {
		this.renOpAdmin = renOpAdmin;
	}

	public boolean isRenForAllAdmin() {
		return renForAllAdmin;
	}

	public void setRenForAllAdmin(boolean renForAllAdmin) {
		this.renForAllAdmin = renForAllAdmin;
	}

	public boolean isRenForAll() {
		return renForAll;
	}

	public void setRenForAll(boolean renForAll) {
		this.renForAll = renForAll;
	}

	public boolean isRenCerAndOrg() {
		return renCerAndOrg;
	}

	public void setRenCerAndOrg(boolean renCerAndOrg) {
		this.renCerAndOrg = renCerAndOrg;
	}

	public String getUserNameForSearch() {
		return userNameForSearch;
	}

	public void setUserNameForSearch(String userNameForSearch) {
		this.userNameForSearch = userNameForSearch;
	}

	public boolean isRendersearchResult() {
		return rendersearchResult;
	}

	public void setRendersearchResult(boolean rendersearchResult) {
		this.rendersearchResult = rendersearchResult;
	}

	public List<UserDTO> getUserDTOsByDateRange() {
		return userDTOsByDateRange;
	}

	public void setUserDTOsByDateRange(List<UserDTO> userDTOsByDateRange) {
		this.userDTOsByDateRange = userDTOsByDateRange;
	}

	public List<UserDTO> getUserListDTOByOperatorID() {

		return userListDTOByOperatorID;
	}

	public void setUserListDTOByOperatorID(List<UserDTO> userListDTOByOperatorID) {
		this.userListDTOByOperatorID = userListDTOByOperatorID;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*
	 * public void findUsersByUserName() throws Exception {
	 * System.out.println("Name :"+userName);
	 * userService.findUsersByUserName(userName); }
	 */

}
