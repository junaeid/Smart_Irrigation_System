package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.FieldTypeDTO;
import bd.ac.uiu.service.FieldTypeService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class FieldTypeController {

	private FieldTypeDTO fieldTypeDTO;

	private List<FieldTypeDTO> fieldTypeList;

	private List<FieldTypeDTO> fieldTypeListByOperator;

	@ManagedProperty("#{fieldTypeService}")
	private FieldTypeService fieldTypeService;

	/*
	 * get fieldtype information form fieldTypeDTO and save by
	 * fieldTypeService.saveFieldType(fieldTypeDTO)
	 */
	public void saveFieldType() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (fieldTypeService.saveFieldType(fieldTypeDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				fieldTypeDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "field Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	/*
	 * get fieldTypeInformation from fieldTypeDTO then update it using
	 * fieldTypeService.updateFieldType(fieldTypeDTO);
	 */

	public void updateFieldType() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			fieldTypeService.updateFieldType(fieldTypeDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			fieldTypeDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	/*
	 * get fieldTypeInformation from fieldTypeDTO then delete it using
	 * fieldTypeService.deleteFieldType(fieldTypeDTO);
	 */

	public void deleteFieldType() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			fieldTypeService.deleteFieldType(fieldTypeDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			fieldTypeDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public FieldTypeDTO getFieldTypeDTO() {
		if (fieldTypeDTO == null) {
			this.fieldTypeDTO = new FieldTypeDTO();
		}
		return fieldTypeDTO;
	}

	public void setFieldTypeDTO(FieldTypeDTO fieldTypeDTO) {
		this.fieldTypeDTO = fieldTypeDTO;
	}

	public List<FieldTypeDTO> getFieldTypeList() {
		try {
			fieldTypeList = new ArrayList<>();
			fieldTypeList = fieldTypeService.findFIeldTypeList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return fieldTypeList;
	}

	public void setFieldTypeList(List<FieldTypeDTO> fieldTypeList) {
		this.fieldTypeList = fieldTypeList;
	}

	public FieldTypeService getFieldTypeService() {
		return fieldTypeService;
	}

	public void setFieldTypeService(FieldTypeService fieldTypeService) {
		this.fieldTypeService = fieldTypeService;
	}

	private boolean renOpCommon = false;

	public void uiRenderView() throws Exception {
		String userType = ApplicationUtility.getUserType();

		if (userType.equals("operator")) {
			renOpCommon = true;
		}
	}

	public boolean isRenOpCommon() {
		return renOpCommon;
	}

	public void setRenOpCommon(boolean renOpCommon) {
		this.renOpCommon = renOpCommon;
	}

	public List<FieldTypeDTO> getFieldTypeListByOperator() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			fieldTypeListByOperator = new ArrayList<>();
			fieldTypeListByOperator = fieldTypeService.findFieldTypeByOperator(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}

		return fieldTypeListByOperator;
	}

	public void setFieldTypeListByOperator(List<FieldTypeDTO> fieldTypeListByOperator) {
		this.fieldTypeListByOperator = fieldTypeListByOperator;
	}

}
