package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import bd.ac.uiu.dto.ResponseInformationDTO;
import bd.ac.uiu.service.ResponseInformationService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class ResponseInformationController {
	
	private ResponseInformationDTO responseInformationDTO;
	
	private List<ResponseInformationDTO> responseInformationList;
	
	@ManagedProperty("#{responseInformationService}")
	private ResponseInformationService responseInformationService;

	public void saveResponseInformation() {
		
//		responseInformationDTO = new ResponseInformationDTO();
//		
//		responseInformationDTO.setFieldVulve(1);
//		responseInformationDTO.setSensorStatus(1);
//		responseInformationDTO.setNodeId(2);
//		responseInformationDTO.setBalance(5000);
//		responseInformationDTO.setPaymentSummaryId(1);
		
		try{
			if(responseInformationService.saveResponseInformation(responseInformationDTO)){
				responseInformationDTO=null;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void updateResponseInformation() {

	}

	public void deleteResponseInformation() {

	}
	
	public ResponseInformationDTO getResponseInformationDTO() {
		if (responseInformationDTO == null) {
			responseInformationDTO = new ResponseInformationDTO();
		}
		return responseInformationDTO;
	}

	public void setResponseInformationDTO(ResponseInformationDTO responseInformationDTO) {
		this.responseInformationDTO = responseInformationDTO;
	}

	public List<ResponseInformationDTO> getResponseInformationListByOperator() {
		long operatorID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			responseInformationList = new ArrayList<>();
			responseInformationList = responseInformationService.findResponseInformationByOperatorID(operatorID);
		} catch (Exception e) {
			System.out.println(e);
		}
		return responseInformationList;
	}
	
	public List<ResponseInformationDTO> getResponseInformationList() {

		try {
			responseInformationList = new ArrayList<>();
			responseInformationList = responseInformationService.findResponseInformationList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return responseInformationList;
	}

	public void setResponseInformationList(List<ResponseInformationDTO> responseInformationList) {
		this.responseInformationList = responseInformationList;
	}

	public ResponseInformationService getResponseInformationService() {
		return responseInformationService;
	}

	public void setResponseInformationService(ResponseInformationService responseInformationService) {
		this.responseInformationService = responseInformationService;
	}

}
