package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.SensorDTO;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.SensorService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class SensorController {

	private SensorDTO sensorDTO;
	private IpcuDTO ipcuDTO;

	private List<SensorDTO> sensorByOperatorID;

	private List<SensorDTO> sensorList;

	private List<IpcuDTO> ipcuList;

	@ManagedProperty("#{sensorService}")
	private SensorService sensorService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	/*
	 * get sensor information from sensorDTO and save it using
	 * sersorService.saveSensor(sensorDTO);
	 */

	public void saveSensor() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			sensorDTO.setIpcuDTO(ipcuDTO);
			if (sensorService.saveSensor(sensorDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				sensorDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Contact Person Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	/*
	 * get sensor information from sensorDTO then update it using
	 * sensorService.updateSersor(sensorDTO);
	 */

	public void updateSensor() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			sensorService.updateSensor(sensorDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			sensorDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	/*
	 * get sensorInformation from sensorDTO then delete it using
	 * sensorService.deleteSensor(sensorDTO);
	 */

	public void deleteSensor() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			sensorService.deleteSensor(sensorDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			sensorDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public SensorDTO getSensorDTO() {
		if (sensorDTO == null) {
			sensorDTO = new SensorDTO();
		}
		return sensorDTO;
	}

	public void setSensorDTO(SensorDTO sensorDTO) {
		this.sensorDTO = sensorDTO;
	}

	public List<SensorDTO> getSensorList() {
		try {
			sensorList = new ArrayList<>();
			sensorList = sensorService.findSensorList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sensorList;
	}

	public void setSensorList(List<SensorDTO> sensorList) {
		this.sensorList = sensorList;
	}

	public SensorService getSensorService() {
		return sensorService;
	}

	public void setSensorService(SensorService sensorService) {
		this.sensorService = sensorService;
	}

	public List<SensorDTO> getSensorByOperatorID() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {

			sensorByOperatorID = new ArrayList<>();
			sensorByOperatorID = sensorService.findSensorListByOperatorID(cerOrOrgOrOpIDLogin);
			System.out.println("sensorByOperatorID size: " + sensorByOperatorID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sensorByOperatorID;
	}

	public List<IpcuDTO> getIpcuList() {

		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {

			ipcuList = new ArrayList<>();
			ipcuList = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin);
			System.out.println("ipcuList size : " + ipcuList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ipcuList;
	}

	public void setIpcuList(List<IpcuDTO> ipcuList) {
		this.ipcuList = ipcuList;
	}

	public void setSensorByOperatorID(List<SensorDTO> sensorByOperatorID) {
		this.sensorByOperatorID = sensorByOperatorID;
	}

	public IpcuService getIpcuService() {
		return ipcuService;
	}

	public void setIpcuService(IpcuService ipcuService) {
		this.ipcuService = ipcuService;
	}

	public IpcuDTO getIpcuDTO() {
		if (ipcuDTO == null) {
			ipcuDTO = new IpcuDTO();
		}
		return ipcuDTO;
	}

	public void setIpcuDTO(IpcuDTO ipcuDTO) {
		this.ipcuDTO = ipcuDTO;
	}

}
