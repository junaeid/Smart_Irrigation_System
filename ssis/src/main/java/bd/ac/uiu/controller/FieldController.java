package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.exolab.castor.types.Date;
import org.exolab.castor.types.DateTime;
import org.springframework.test.context.transaction.TransactionConfiguration;

import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.FieldTypeDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.SoilDTO;
import bd.ac.uiu.dto.TransactionLogDTO;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.FieldService;
import bd.ac.uiu.service.FieldTypeService;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.SoilService;
import bd.ac.uiu.service.TransactionLogService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class FieldController {

	private FieldDTO fieldDTO;
	private TransactionLogDTO transactionLogDTO;

	private List<FieldDTO> fieldList;
	private List<FieldDTO> fieldListByOperatorID;
	private List<FieldTypeDTO> fieldTypeList;
	private List<CustomerDTO> customerList;
	private List<IpcuDTO> ipcuList;
	private List<SoilDTO> soilList;

	private long fieldTypeID;
	private long soilID;
	private long ipcuID;
	private long customerID;

	@ManagedProperty("#{fieldService}")
	private FieldService fieldService;

	@ManagedProperty("#{fieldTypeService}")
	private FieldTypeService fieldTypeService;

	@ManagedProperty("#{soilService}")
	private SoilService soilService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{transactionLogService}")
	private TransactionLogService transactionLogService;

	/*
	 * get fieldTypeID, ipcuID, soilID and customerID then save it using
	 * fieldService.saveField(fieldDTO);
	 */

	public void saveField() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {

			FieldTypeDTO fieldTypeDTO = new FieldTypeDTO();
			IpcuDTO ipcuDTO = new IpcuDTO();
			SoilDTO soilDTO = new SoilDTO();
			CustomerDTO customerDTO = new CustomerDTO();

			fieldTypeDTO.setFieldTypeID(fieldTypeID);
			ipcuDTO.setIpcuID(ipcuID);
			soilDTO.setSoilID(soilID);
			customerDTO.setCustomerID(customerID);

			fieldDTO.setFieldTypeDTO(fieldTypeDTO);
			fieldDTO.setIpcuDTO(ipcuDTO);
			fieldDTO.setSoilDTO(soilDTO);
			fieldDTO.setCustomerDTO(customerDTO);

			if (fieldService.saveField(fieldDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));

				try {
					
					TransactionLogDTO transactionLogDTO = new TransactionLogDTO();
					
					transactionLogDTO.setNoOfUnit(0);

					customerDTO.setCustomerID(customerID);

					transactionLogDTO.setCustomerDTO(customerDTO);

					transactionLogDTO.setCounter("N/A");
					transactionLogDTO.setTrxStatus("N/A");
					transactionLogDTO.setTrxId("N/A");
					transactionLogDTO.setReference("N/A");
					transactionLogDTO.setSender("N/A");
					transactionLogDTO.setService("N/A");
					transactionLogDTO.setCurrency("N/A");
					transactionLogDTO.setReceiver("N/A");
					transactionLogDTO.setTrxTimeStamp("N/A");
					transactionLogDTO.setDebitAmount(0);

					if (transactionLogService.saveTransactionFromField(transactionLogDTO)) {
						context.addMessage(null, new FacesMessage("Successfully Saved"));
						transactionLogDTO = null;
					} else {
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				fieldDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Field Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	/*
	 * update field information by fieldService.update(fieldDTO);
	 */

	public void updateField() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			fieldService.updateField(fieldDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			fieldDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	/*
	 * get field information from fieldDTO then delete it by
	 * fieldService.deleteField(fieldDTO)
	 **/

	public void deleteField() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			fieldService.deleteField(fieldDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			fieldDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public FieldDTO getFieldDTO() {
		if (fieldDTO == null) {
			this.fieldDTO = new FieldDTO();
		}
		return fieldDTO;
	}

	public void setFieldDTO(FieldDTO fieldDTO) {
		this.fieldDTO = fieldDTO;
	}

	public List<FieldDTO> getFieldList() {
		try {
			fieldList = new ArrayList<>();
			fieldList = fieldService.findFieldList();
			System.out.println("fieldList" + fieldList.size());
		} catch (Exception e) {
			System.out.println(e);
		}
		return fieldList;
	}

	public void setFieldList(List<FieldDTO> fieldList) {
		this.fieldList = fieldList;
	}

	public FieldService getFieldService() {
		return fieldService;
	}

	public void setFieldService(FieldService fieldService) {
		this.fieldService = fieldService;
	}

	public List<FieldTypeDTO> getFieldTypeList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			fieldTypeList = new ArrayList<>();
			fieldTypeList = fieldTypeService.findFieldTypeByOperator(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
			// TODO: handle exception
		}
		return fieldTypeList;
	}

	public void setFieldTypeList(List<FieldTypeDTO> fieldTypeList) {
		this.fieldTypeList = fieldTypeList;
	}

	public List<SoilDTO> getSoilList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			soilList = new ArrayList<>();
			soilList = soilService.findSoilByOperator(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
			// TODO: handle exception
		}
		return soilList;
	}

	public List<CustomerDTO> getCustomerList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			customerList = new ArrayList<>();
			customerList = customerService.findCustomersByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}
		return customerList;
	}

	public void setCustomerList(List<CustomerDTO> customerList) {
		this.customerList = customerList;
	}

	public List<IpcuDTO> getIpcuList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			ipcuList = new ArrayList<>();
			ipcuList = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}
		return ipcuList;
	}

	public void setIpcuList(List<IpcuDTO> ipcuList) {
		this.ipcuList = ipcuList;
	}

	public void setSoilList(List<SoilDTO> soilList) {
		this.soilList = soilList;
	}

	public FieldTypeService getFieldTypeService() {
		return fieldTypeService;
	}

	public void setFieldTypeService(FieldTypeService fieldTypeService) {
		this.fieldTypeService = fieldTypeService;
	}

	public SoilService getSoilService() {
		return soilService;
	}

	public void setSoilService(SoilService soilService) {
		this.soilService = soilService;
	}

	public IpcuService getIpcuService() {
		return ipcuService;
	}

	public void setIpcuService(IpcuService ipcuService) {
		this.ipcuService = ipcuService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public long getFieldTypeID() {
		return fieldTypeID;
	}

	public void setFieldTypeID(long fieldTypeID) {
		this.fieldTypeID = fieldTypeID;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}

	public long getSoilID() {
		return soilID;
	}

	public void setSoilID(long soilID) {
		this.soilID = soilID;
	}

	private boolean renOpCommon = false;

	public void uiRenderView() throws Exception {
		String userType = ApplicationUtility.getUserType();

		if (userType.equals("operator")) {
			renOpCommon = true;
		}
	}

	public boolean isRenOpCommon() {
		return renOpCommon;
	}

	public void setRenOpCommon(boolean renOpCommon) {
		this.renOpCommon = renOpCommon;
	}

	public List<FieldDTO> getFieldListByOperatorID() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {

			fieldListByOperatorID = new ArrayList<>();
			fieldListByOperatorID = fieldService.findFieldsByOperatorID(cerOrOrgOrOpIDLogin);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fieldListByOperatorID;
	}

	public void setFieldListByOperatorID(List<FieldDTO> fieldListByOperatorID) {
		this.fieldListByOperatorID = fieldListByOperatorID;
	}

	public TransactionLogService getTransactionLogService() {
		return transactionLogService;
	}

	public void setTransactionLogService(TransactionLogService transactionLogService) {
		this.transactionLogService = transactionLogService;
	}

	public TransactionLogDTO getTransactionLogDTO() {
		if (transactionLogDTO == null) {
			transactionLogDTO = new TransactionLogDTO();
		}
		return transactionLogDTO;
	}

	public void setTransactionLogDTO(TransactionLogDTO transactionDTO) {
		this.transactionLogDTO = transactionDTO;
	}

}
