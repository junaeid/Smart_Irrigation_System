package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.SoilDTO;
import bd.ac.uiu.service.FieldService;
import bd.ac.uiu.service.SoilService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class SoilController {
	private SoilDTO soilDTO;
	private FieldDTO fieldDTO;

	private List<SoilDTO> soilList;
	
	private List<SoilDTO> soilListForOperator;

	private List<FieldDTO> fieldList;

	@ManagedProperty("#{soilService}")
	private SoilService soilService;

	@ManagedProperty("#{fieldService}")
	private FieldService fieldService;

	public void checkSOil() {
		if (soilService.checkSoil(soilDTO)) {

		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "area name is already exists", ""));
		}
	}

	public void saveSoil() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			if (soilService.saveSoil(soilDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				soilDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "soil Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void updateSoil() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			soilService.updateSoil(soilDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			soilDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deleteSoil() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			soilService.deleteSoil(soilDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			soilDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public SoilDTO getSoilDTO() {
		if (soilDTO == null) {
			this.soilDTO = new SoilDTO();
		}
		return soilDTO;
	}

	public void setSoilDTO(SoilDTO soilDTO) {
		this.soilDTO = soilDTO;
	}

	public List<SoilDTO> getSoilList() {

		try {
			soilList = new ArrayList<>();
			soilList = soilService.findSoilList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return soilList;
	}

	public List<SoilDTO> getSoilListForOperator() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			soilListForOperator = new ArrayList<>();
			soilListForOperator = soilService.findSoilByOperator(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return soilListForOperator;
	}

	public void setSoilList(List<SoilDTO> soilList) {
		this.soilList = soilList;
	}

	public SoilService getSoilService() {
		return soilService;
	}

	public void setSoilService(SoilService soilService) {
		this.soilService = soilService;
	}

	private boolean renOpCommon = false;

	public void uiRenderView() throws Exception {
		String userType = ApplicationUtility.getUserType();

		if (userType.equals("operator")) {
			renOpCommon = true;
		}
	}

	public boolean isRenOpCommon() {
		return renOpCommon;
	}

	public void setRenOpCommon(boolean renOpCommon) {
		this.renOpCommon = renOpCommon;
	}

	public List<FieldDTO> getFieldList() {
		

		return fieldList;
	}

	public void setFieldList(List<FieldDTO> fieldList) {
		this.fieldList = fieldList;
	}

	public FieldDTO getFieldDTO() {
		return fieldDTO;
	}

	public void setFieldDTO(FieldDTO fieldDTO) {
		this.fieldDTO = fieldDTO;
	}

	public FieldService getFieldService() {
		return fieldService;
	}

	public void setFieldService(FieldService fieldService) {
		this.fieldService = fieldService;
	}

	public void setSoilListForOperator(List<SoilDTO> soilListForOperator) {
		this.soilListForOperator = soilListForOperator;
	}
	
	

}
