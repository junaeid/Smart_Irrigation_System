package bd.ac.uiu.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.service.HomeDashBoardService;

@ManagedBean
@ViewScoped
public class HomeDashBoardCon implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{homeDashBoardService}")
	private HomeDashBoardService homeDashBoardService;

	private CustomerDTO customerDTO;
	private OperatorDTO operatorDTO;
	int noOfcustomer;
	int noOfoperator;

	public void findNumberOfCustomer() {
		try {
			noOfcustomer = homeDashBoardService.findNumberOfCustomer();
			System.out.println("noOfcustomer: "+noOfcustomer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void findNumberOfOperators() {
		try {
			noOfoperator = homeDashBoardService.findNumberOfOperators();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public OperatorDTO getOperatorDTO() {
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public HomeDashBoardService getHomeDashBoardService() {
		return homeDashBoardService;
	}

	public void setHomeDashBoardService(HomeDashBoardService homeDashBoardService) {
		this.homeDashBoardService = homeDashBoardService;
	}

	public int getNoOfcustomer() {
		findNumberOfCustomer();
		return noOfcustomer;
	}

	public void setNoOfcustomer(int noOfcustomer) {
		findNumberOfOperators();
		this.noOfcustomer = noOfcustomer;
	}

	public int getNoOfoperator() {
		return noOfoperator;
	}

	public void setNoOfoperator(int noOfoperator) {
		this.noOfoperator = noOfoperator;
	}

}
