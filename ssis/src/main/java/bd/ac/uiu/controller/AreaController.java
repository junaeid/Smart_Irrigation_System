package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.service.AreaService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class AreaController {

	private AreaDTO areaDTO;

	private List<AreaDTO> areaList;

	private List<AreaDTO> areaListOrganization;

	private List<OperatorDTO> operatorList;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	private long operatorID;

	/*
	 * check area name is already in the server or not
	 */

	public void checkArea() {
		if (areaService.checkArea(areaDTO)) {

		} else {

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "area name is already exists", ""));
		}
	}

	/*
	 * save the inputed area information from front end If the input is not in
	 * the server then it will show a successful message
	 */

	public void saveArea() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(1);
			areaDTO.setOperatorDTO(operatorDTO);

			if (areaService.saveArea(areaDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				areaDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "area Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faild", ""));
		}
	}

	/*
	 * If user inputed wrong area name then can update by this method by
	 * areaService.updteArea(areaDTO)
	 */

	public void updateArea() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			areaService.updateArea(areaDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			areaDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	/*
	 * delete area by areaDTO using areaService()
	 */
	
	public void deleteArea() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			areaService.deleteArea(areaDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			areaDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public AreaDTO getAreaDTO() {
		if (areaDTO == null) {
			this.areaDTO = new AreaDTO();
		}
		return areaDTO;
	}

	public void setAreaDTO(AreaDTO areaDTO) {
		this.areaDTO = areaDTO;
	}

	public List<AreaDTO> getAreaListOrganization() {
		try {
			areaListOrganization = new ArrayList<>();
			areaListOrganization = areaService.findAreaList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return areaListOrganization;
	}

	public void setAreaListOrganization(List<AreaDTO> areaListOrganization) {
		this.areaListOrganization = areaListOrganization;
	}

	public List<AreaDTO> getAreaList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		System.out.println(cerOrOrgOrOpIDLogin);
		try {
			areaList = new ArrayList<>();
			areaList = areaService.findAreaByOperatorID(cerOrOrgOrOpIDLogin);

		} catch (Exception e) {
			System.out.println(e);
		}
		return areaList;
	}

	public void setAreaList(List<AreaDTO> areaList) {
		this.areaList = areaList;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public List<OperatorDTO> getOperatorList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {

			operatorList = new ArrayList<>();
			operatorList = operatorService.findAllOperatorsByOrganizationID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return operatorList;
	}

	public void setOperatorList(List<OperatorDTO> operatorList) {
		this.operatorList = operatorList;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

}
