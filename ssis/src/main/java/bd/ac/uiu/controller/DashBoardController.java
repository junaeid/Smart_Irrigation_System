package bd.ac.uiu.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.PieChartModel;

import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.DashBoardDTO;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.service.AreaService;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.FieldService;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.TransactionLogService;
import bd.ac.uiu.service.WaterCreditSummaryService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class DashBoardController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DashBoardDTO DashBoardDTO;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@ManagedProperty("#{fieldService}")
	private FieldService fieldService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@ManagedProperty("#{waterCreditSummaryService}")
	private WaterCreditSummaryService waterCreditSummaryService;

	@ManagedProperty("#{transactionLogService}")
	private TransactionLogService transactionLogService;

	private List<CustomerDTO> customerDTOs;
	private List<CustomerDTO> customerDtoOrg;
	private List<WaterCreditSummaryDTO> waterCreditSummarieDTOs;

	private List<AreaDTO> areaList;
	private List<AreaDTO> areaListForOrg;

	private DonutChartModel donutModelOrg;
	private DonutChartModel donutModelOp;

	private PieChartModel pieModelForCreditOrg;
	private PieChartModel pieModelForCreditOp;
	private PieChartModel pieChartForWaterCreditByDate;

	private PieChartModel pieModelOrg;
	private PieChartModel pieModelOp;

	private BarChartModel barModelOrg;
	private BarChartModel barModelOp;

	private long totalOp = 0;
	private long totalIpcu = 0;
	private long totalCust = 0;
	private long totalField = 0;

	@PostConstruct
	public void init() {
		createBarModels();
		createDonutModelsOrg();
		createDonutModelsOp();
		createPieModels();
	}

	private void createBarModels() {
		createBarModel();
		createBarModelOp();
		pieModelForWaterCreditOrg();
		pieModelForWaterCreditOp();

	}

	private void createPieModels() {
		createPieModelTransOrg();
		createPieModel2();
		createPieModelForWarterCreditOp();
		createPieModelForWaterCreditOpByDate();
	}

	public DashBoardDTO getDashBoardDTO() throws Exception {
		DashBoardDTO = new DashBoardDTO();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		long totalOperator = operatorService.findAllOperatorsByOrganizationID(cerOrOrgOrOpIDLogin).size();
		long totalIpcu = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin).size();
		long totalCustomer = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin).size();
		long totalField = fieldService.findFieldsByOperatorID(cerOrOrgOrOpIDLogin).size();

		DashBoardDTO.setTotalOperator(totalOperator);
		DashBoardDTO.setTotalIpcu(totalIpcu);
		DashBoardDTO.setTotalCustomer(totalCustomer);
		DashBoardDTO.setTotalField(totalField);

		return DashBoardDTO;
	}

	private BarChartModel initBarModel() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		try {
			totalOp = operatorService.findAllOperatorsByOrganizationID(cerOrOrgOrOpIDLogin).size();
			totalIpcu = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin).size();
			totalCust = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin).size();
			totalField = fieldService.findFieldsByOperatorID(cerOrOrgOrOpIDLogin).size();
		} catch (Exception e) {
			e.printStackTrace();
		}

		BarChartModel model = new BarChartModel();

		ChartSeries op = new ChartSeries();
		op.setLabel("Operator");
		op.set("", totalOp);

		ChartSeries ipcu = new ChartSeries();
		ipcu.setLabel("Pump");
		ipcu.set("", totalIpcu);

		ChartSeries field = new ChartSeries();
		field.setLabel("Field");
		field.set("", totalField);

		ChartSeries cust = new ChartSeries();
		cust.setLabel("Customer");
		cust.set("", totalCust);

		model.addSeries(op);
		model.addSeries(ipcu);
		model.addSeries(field);
		model.addSeries(cust);

		return model;
	}

	public PieChartModel getPieChartForWaterCreditByDate() {
		return pieChartForWaterCreditByDate;
	}

	private void createPieModelForWaterCreditOpByDate() {

		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Date date = new Date();
		String newstring = new SimpleDateFormat("yyyy-MM-dd").format(date);
		// System.out.println(newstring);
		// newstring = "2017-06-10";
		double totalWaterCredit = 0;
		double maxWaterCredit = 0;
		double minWaterCredit = 0;
		double avgWaterCredit = 0;
		// System.out.println("total Transaction " + totalWaterCredit);
		try {

			totalWaterCredit = waterCreditSummaryService.totalWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, newstring);
			// System.out.println("total WaterCredit " + totalWaterCredit);
			maxWaterCredit = waterCreditSummaryService.maxWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, newstring);
			minWaterCredit = waterCreditSummaryService.minWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, newstring);
			avgWaterCredit = waterCreditSummaryService.avgWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, newstring);

		} catch (Exception e) {
			e.printStackTrace();
		}

		pieChartForWaterCreditByDate = new PieChartModel();

		if (totalWaterCredit != 0.0 && maxWaterCredit != 0.0 && minWaterCredit != 0.0 && avgWaterCredit != 0.0) {

			// System.out.println("IF Water Credit- "+totalWaterCredit+" " +
			// totalWaterCredit);
			pieChartForWaterCreditByDate.set("Total- " + totalWaterCredit + "", totalWaterCredit);
			pieChartForWaterCreditByDate.set("Max- " + maxWaterCredit + "", maxWaterCredit);
			pieChartForWaterCreditByDate.set("Min- " + minWaterCredit + "", minWaterCredit);
			pieChartForWaterCreditByDate.set("Avg- " + String.format("%.2f", avgWaterCredit) + "", avgWaterCredit);

			pieChartForWaterCreditByDate.setTitle("Water Credit Pie By Date");

		} else {

			// System.out.println(" else Total WaterCredit " +
			// totalWaterCredit);
			pieChartForWaterCreditByDate.set("Total- " + 0 + "", 0);
			pieChartForWaterCreditByDate.set("Max- " + 0 + "", 0);
			pieChartForWaterCreditByDate.set("Min- " + 0 + "", 0);
			pieChartForWaterCreditByDate.set("Avg- " + 0 + " ", 0);

			pieChartForWaterCreditByDate.setTitle("....");

		}

		pieChartForWaterCreditByDate.setLegendPosition("w");
	}

	private PieChartModel pieChartForWaterCredit;

	public PieChartModel getPieChartForWaterCredit() {
		return pieChartForWaterCredit;
	}

	private void createPieModelForWarterCreditOp() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Date date = new Date();
		String newstring = new SimpleDateFormat("yyyy-MM-dd").format(date);
		// System.out.println(newstring);
		// newstring = "2017-04-24";
		double totalWaterCredit = 0;
		double maxWaterCredit = 0;
		double minWaterCredit = 0;
		double avgWaterCredit = 0;
		// System.out.println("total Transaction " + totalWaterCredit);
		try {

			totalWaterCredit = waterCreditSummaryService.totalWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, null);
			// System.out.println("total WaterCredit " + totalWaterCredit);
			maxWaterCredit = waterCreditSummaryService.maxWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, null);
			minWaterCredit = waterCreditSummaryService.minWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, null);
			avgWaterCredit = waterCreditSummaryService.avgWaterCreditSummaryByOpId(cerOrOrgOrOpIDLogin, null);

		} catch (Exception e) {
			e.printStackTrace();
		}

		pieChartForWaterCredit = new PieChartModel();

		pieChartForWaterCredit.set("total- " + totalWaterCredit + "", totalWaterCredit);
		pieChartForWaterCredit.set("max- " + maxWaterCredit + "", maxWaterCredit);
		pieChartForWaterCredit.set("min- " + minWaterCredit + "", minWaterCredit);
		pieChartForWaterCredit.set("avg- " + String.format("%.2f", avgWaterCredit) + "", avgWaterCredit);

		pieChartForWaterCredit.setTitle("Total Water Credit");
		pieChartForWaterCredit.setLegendPosition("w");
	}

	/* Create for Organization */

	private void createDonutModelsOrg() {

		donutModelOrg = initDonutModelOrg();
		donutModelOrg.setTitle("Transactions");
		donutModelOrg.setLegendPosition("e");
		donutModelOrg.setSliceMargin(5);
		donutModelOrg.setShowDataLabels(true);
		donutModelOrg.setDataFormat("value");
		donutModelOrg.setShadow(false);
	}

	private DonutChartModel initDonutModelOrg() {
		DonutChartModel model = new DonutChartModel();

		double totalTransaction = 0;
		double maxTrans = 0;
		double minTrans = 0;
		double avgTrans = 0;

		try {
			totalTransaction = transactionLogService.findTotalTransOrg(null);
			maxTrans = transactionLogService.findMaxTransOrg(null);
			minTrans = transactionLogService.findMinTransOrg(null);
			avgTrans = transactionLogService.findAvgTransOrg(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(" E+" + totalOp);

		Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
		circle1.put("Total- " + totalTransaction + " ", totalTransaction);
		circle1.put("Max- " + maxTrans + "", maxTrans);
		circle1.put("min- " + minTrans + "", minTrans);
		circle1.put("Avg- " + String.format("%.2f", avgTrans) + "", avgTrans);
		model.addCircle(circle1);

		return model;
	}

	private void createPieModelTransOrg() {

		Date date = new Date();
		String newstring = new SimpleDateFormat("yyyy-MM-dd").format(date);
		// System.out.println(newstring);
		// newstring = "2017-04-24";
		double totalTransaction = 0;
		double maxTrans = 0;
		double minTrans = 0;
		double avgTrans = 0;
		System.out.println("total Transaction " + totalTransaction);
		try {

			totalTransaction = transactionLogService.findTotalTransOrg(newstring);
			// System.out.println("total Transaction " + totalTransaction);
			maxTrans = transactionLogService.findMaxTransOrg(newstring);
			minTrans = transactionLogService.findMinTransOrg(newstring);
			avgTrans = transactionLogService.findAvgTransOrg(newstring);

		} catch (Exception e) {
			e.printStackTrace();
		}

		pieModelOrg = new PieChartModel();

		if (totalTransaction != 0 && maxTrans != 0 && minTrans != 0 && avgTrans != 0) {

			pieModelOrg.set("Total- " + totalTransaction + "", totalTransaction);
			pieModelOrg.set("Max- " + maxTrans + "", maxTrans);
			pieModelOrg.set("Min- " + minTrans + "", minTrans);
			pieModelOrg.set("Avg- " + String.format("%.2f", avgTrans) + "", avgTrans);
			pieModelOrg.setTitle("Todays Transactions");

		} else {
			pieModelOrg.set("Total- " + 0 + "", 0);
			pieModelOrg.set("Max- " + 0 + "", 0);
			pieModelOrg.set("Min- " + 0 + "", 0);
			pieModelOrg.set("Avg- " + 0 + "", 0);

			pieModelOrg.setTitle("No transaction has been made yet");

		}

		pieModelOrg.setLegendPosition("w");
	}

	private void createBarModel() {
		barModelOrg = initBarModel();

		barModelOrg.setTitle("Total Number of Pump Customer Field");
		barModelOrg.setLegendPosition("ne");

		Axis xAxis = barModelOrg.getAxis(AxisType.X);
		xAxis.setLabel("");

		Axis yAxis = barModelOrg.getAxis(AxisType.Y);
		yAxis.setLabel("");
		yAxis.setMin(0);
		long max = totalCust;

		if (totalOp > max)
			max = totalOp;
		if (totalField > max)
			max = totalField;
		if (totalIpcu > max)
			max = totalIpcu;
		yAxis.setMax(max + 1);

	}

	/* Create for Organization */

	/* Create Model for Operator */

	private BarChartModel initBarModelOp() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		try {
			totalIpcu = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin).size();
			totalCust = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin).size();
			totalField = fieldService.findFieldsByOperatorID(cerOrOrgOrOpIDLogin).size();
		} catch (Exception e) {
			e.printStackTrace();
		}

		BarChartModel model = new BarChartModel();

		ChartSeries ipcu = new ChartSeries();
		ipcu.setLabel("Pump");
		ipcu.set("", totalIpcu);

		ChartSeries field = new ChartSeries();
		field.setLabel("Field");
		field.set("", totalField);

		ChartSeries cust = new ChartSeries();
		cust.setLabel("Customer");
		cust.set("", totalCust);

		model.addSeries(ipcu);
		model.addSeries(field);
		model.addSeries(cust);

		return model;
	}

	private void createPieModel2() {

		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Date date = new Date();
		String newstring = new SimpleDateFormat("yyyy-MM-dd").format(date);
		// System.out.println(newstring);
		// newstring = "2017-04-24";
		double totalTransaction = 0;
		double maxTrans = 0;
		double minTrans = 0;
		double avgTrans = 0;
		// System.out.println("total Transaction " + totalTransaction);
		try {

			totalTransaction = transactionLogService.findTotalTransactionByOpID(cerOrOrgOrOpIDLogin, newstring);
			// System.out.println("total Transaction " + totalTransaction);
			maxTrans = transactionLogService.findMaxTransactionByOpID(cerOrOrgOrOpIDLogin, newstring);
			minTrans = transactionLogService.findMinTransactionByOpID(cerOrOrgOrOpIDLogin, newstring);
			avgTrans = transactionLogService.findAvgTransactionByOpID(cerOrOrgOrOpIDLogin, newstring);

		} catch (Exception e) {
			// e.printStackTrace();
		}

		pieModelOp = new PieChartModel();
		if (totalTransaction != 0 && maxTrans != 0 && minTrans != 0 && avgTrans != 0) {

			pieModelOp.set("Total- " + totalTransaction + "", totalTransaction);
			pieModelOp.set("Max- " + maxTrans + "", maxTrans);
			pieModelOp.set("Min- " + minTrans + "", minTrans);
			pieModelOp.set("Avg- " + String.format("%.2f", avgTrans) + "", avgTrans);
			pieModelOp.setTitle("Todays Transactions");

		} else {
			pieModelOp.set("Total- " + 0 + "", 0);
			pieModelOp.set("Max- " + 0 + "", 0);
			pieModelOp.set("Min- " + 0 + "", 0);
			pieModelOp.set("Avg- " + 0 + "", 0);

			pieModelOp.setTitle("No transaction has been made yet");

		}

		pieModelOp.setLegendPosition("e");
		pieModelOp.setFill(false);
		pieModelOp.setShowDataLabels(true);
		pieModelOp.setDiameter(150);
	}

	/* Create BarModel for Operator */
	private void createBarModelOp() {

		barModelOp = initBarModelOp();

		barModelOp.setTitle("Total Number of Pump Customer Field");
		barModelOp.setLegendPosition("ne");

		Axis xAxis = barModelOp.getAxis(AxisType.X);
		xAxis.setLabel("");

		Axis yAxis = barModelOp.getAxis(AxisType.Y);
		yAxis.setLabel("");
		yAxis.setMin(0);
		long max = totalCust;

		if (totalOp > max)
			max = totalOp;
		if (totalField > max)
			max = totalField;
		if (totalIpcu > max)
			max = totalIpcu;
		yAxis.setMax(max + 1);

	}

	public void pieModelForWaterCreditOrg() {
		pieModelForCreditOrg = new PieChartModel();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		try {
			long unpaid = waterCreditSummaryService
					.findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(cerOrOrgOrOpIDLogin).size();

			long waterCreditCustomerCount = waterCreditSummaryService
					.findWaterCreditSummaryByOperatorID(cerOrOrgOrOpIDLogin).size();
			long paid;

			if (unpaid < waterCreditCustomerCount) {
				paid = waterCreditCustomerCount - unpaid;
			} else {
				paid = unpaid - waterCreditCustomerCount;
			}

			pieModelForCreditOrg.set("Inactive- " + unpaid + "", unpaid);
			pieModelForCreditOrg.set("Active- " + paid + "", paid);
			pieModelForCreditOrg.setTitle("Customer active/inactive");
			pieModelForCreditOrg.setLegendPosition("w");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public void pieModelForWaterCreditOp() {
		pieModelForCreditOp = new PieChartModel();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		try {
			long unpaid = waterCreditSummaryService
					.findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(cerOrOrgOrOpIDLogin).size();

			long waterCreditCustomerCount = waterCreditSummaryService
					.findWaterCreditSummaryByOperatorID(cerOrOrgOrOpIDLogin).size();
			long paid;

			if (unpaid < waterCreditCustomerCount) {
				paid = waterCreditCustomerCount - unpaid;
			} else {
				paid = unpaid - waterCreditCustomerCount;
			}

			pieModelForCreditOp.set("Inactive- " + unpaid + "", unpaid);
			pieModelForCreditOp.set("Active- " + paid + "", paid);
			pieModelForCreditOp.setTitle("Customer active/inactive");
			pieModelForCreditOp.setLegendPosition("w");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private void createDonutModelsOp() {

		donutModelOp = initDonutModel();
		donutModelOp.setTitle("Total Transaction");
		donutModelOp.setLegendPosition("e");
		donutModelOp.setSliceMargin(5);
		donutModelOp.setShowDataLabels(true);
		donutModelOp.setDataFormat("value");
		donutModelOp.setShadow(false);
	}

	private DonutChartModel initDonutModel() {
		DonutChartModel model = new DonutChartModel();

		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		double totalTransaction = 0;
		double maxTrans = 0;
		double minTrans = 0;
		double avgTrans = 0;

		try {

			totalTransaction = transactionLogService.findTotalTransactionByOpID(cerOrOrgOrOpIDLogin, null);
			maxTrans = transactionLogService.findMaxTransactionByOpID(cerOrOrgOrOpIDLogin, null);
			minTrans = transactionLogService.findMinTransactionByOpID(cerOrOrgOrOpIDLogin, null);
			avgTrans = transactionLogService.findAvgTransactionByOpID(cerOrOrgOrOpIDLogin, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(" E+" + totalOp);

		Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
		circle1.put("Total- " + totalTransaction + " ", totalTransaction);
		circle1.put("Max- " + maxTrans + "", maxTrans);
		circle1.put("min- " + minTrans + "", minTrans);
		circle1.put("Avg- " + String.format("%.2f", avgTrans) + "", avgTrans);
		model.addCircle(circle1);

		return model;
	}

	/* Create Model for Operator */

	public List<CustomerDTO> getListOfLatestThreeCustomersByOperatorID() throws Exception {
		customerDTOs = new ArrayList<>();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		customerDTOs = customerService.findLatestThreeCustomersByOperatorID(cerOrOrgOrOpIDLogin);

		return customerDTOs;
	}

	public List<AreaDTO> getAreaList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			areaList = new ArrayList<>();
			areaList = areaService.findAreaByOperatorID(cerOrOrgOrOpIDLogin);
			System.out.println("area list size :" + areaList.size());
		} catch (Exception e) {
			System.out.println(e);
		}
		return areaList;
	}

	public List<AreaDTO> getAreaListForOrg() {
		try {
			areaListForOrg = new ArrayList<>();
			areaListForOrg = areaService.findAreaList();
			System.out.println("area list size :" + areaListForOrg.size());
		} catch (Exception e) {
			System.out.println(e);
		}
		return areaListForOrg;
	}

	public void setAreaListForOrg(List<AreaDTO> areaListForOrg) {
		this.areaListForOrg = areaListForOrg;
	}

	public void setDashBoardDTO(DashBoardDTO dashBoardDTO) {
		this.DashBoardDTO = dashBoardDTO;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public IpcuService getIpcuService() {
		return ipcuService;
	}

	public void setIpcuService(IpcuService ipcuService) {
		this.ipcuService = ipcuService;
	}

	public FieldService getFieldService() {
		return fieldService;
	}

	public void setFieldService(FieldService fieldService) {
		this.fieldService = fieldService;
	}

	public List<CustomerDTO> getCustomerDTOs() {
		return customerDTOs;
	}

	public void setCustomerDTOs(List<CustomerDTO> customerDTOs) {
		this.customerDTOs = customerDTOs;
	}

	public List<CustomerDTO> getCustomerDtoOrg() {
		customerDtoOrg = new ArrayList<>();
		try {
			customerDtoOrg = customerService.findCustomerList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerDtoOrg;
	}

	public void setCustomerDtoOrg(List<CustomerDTO> customerDtoOrg) {
		this.customerDtoOrg = customerDtoOrg;
	}

	public void setAreaList(List<AreaDTO> areaList) {
		this.areaList = areaList;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public TransactionLogService getTransactionLogService() {
		return transactionLogService;
	}

	public void setTransactionLogService(TransactionLogService transactionLogService) {
		this.transactionLogService = transactionLogService;
	}

	public List<WaterCreditSummaryDTO> getWaterCreditSummarieDTOs() {
		return waterCreditSummarieDTOs;
	}

	public void setWaterCreditSummarieDTOs(List<WaterCreditSummaryDTO> waterCreditSummarieDTOs) {
		this.waterCreditSummarieDTOs = waterCreditSummarieDTOs;
	}

	public DonutChartModel getDonutModelOrg() {
		return donutModelOrg;
	}

	public DonutChartModel getDonutModelOp() {
		return donutModelOp;
	}

	public WaterCreditSummaryService getWaterCreditSummaryService() {
		return waterCreditSummaryService;
	}

	public void setWaterCreditSummaryService(WaterCreditSummaryService waterCreditSummaryService) {
		this.waterCreditSummaryService = waterCreditSummaryService;
	}

	public PieChartModel getPieModelForCreditOrg() {
		return pieModelForCreditOrg;
	}

	public PieChartModel getPieModelForCreditOp() {
		return pieModelForCreditOp;
	}

	public PieChartModel getPieModelOrg() {
		return pieModelOrg;
	}

	public void setPieModelOrg(PieChartModel pieModelOrg) {
		this.pieModelOrg = pieModelOrg;
	}

	public PieChartModel getPieModelOp() {
		return pieModelOp;
	}

	public void setPieModelOp(PieChartModel pieModelOp) {
		this.pieModelOp = pieModelOp;
	}

	public BarChartModel getBarModelOrg() {
		return barModelOrg;
	}

	public BarChartModel getBarModelOp() {
		return barModelOp;
	}

}
