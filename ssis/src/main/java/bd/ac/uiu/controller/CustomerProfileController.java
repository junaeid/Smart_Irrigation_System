package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.RequestInformationLogDTO;
import bd.ac.uiu.dto.ResponseInformationLogDTO;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.RequestInformationLogService;
import bd.ac.uiu.service.ResponseInformationService;
import bd.ac.uiu.service.WaterCreditSummaryService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@SessionScoped
public class CustomerProfileController {

	private CustomerDTO customerDTO;
	private List<RequestInformationLogDTO> requestInformationLogDTOs;
	private List<ResponseInformationLogDTO> responseInformationLogDTOs;
	private List<WaterCreditSummaryDTO> waterCreditSummaryDTOs;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{responseInformationService}")
	private ResponseInformationService responseInformationService;

	@ManagedProperty("#{requestInformationLogService}")
	private RequestInformationLogService requestInformationLogService;

	@ManagedProperty("#{waterCreditSummaryService}")
	private WaterCreditSummaryService waterCreditSummaryService;
	
	private long ipcuID;
	
	private long operatorID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
	private long custID;
	

	public CustomerDTO getCustomerDTO() {
		if (customerDTO == null) {
			this.customerDTO = new CustomerDTO();
		}
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public List<RequestInformationLogDTO> getRequestInformationLogDTOs() {
		try {
			requestInformationLogDTOs = new ArrayList<>();
			requestInformationLogDTOs = requestInformationLogService
					.findRequestInformationLogDTOByOperatorIDAndCustomerID(operatorID, custID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestInformationLogDTOs;
	}

	public void setRequestInformationLogDTOs(List<RequestInformationLogDTO> requestInformationLogDTOs) {
		this.requestInformationLogDTOs = requestInformationLogDTOs;
	}

	public List<ResponseInformationLogDTO> getResponseInformationLogDTOs() {
		return responseInformationLogDTOs;
	}

	public void setResponseInformationLogDTOs(List<ResponseInformationLogDTO> responseInformationLogDTOs) {
		this.responseInformationLogDTOs = responseInformationLogDTOs;
	}

	public List<WaterCreditSummaryDTO> getWaterCreditSummaryDTOs() {
		return waterCreditSummaryDTOs;
	}

	public void setWaterCreditSummaryDTOs(List<WaterCreditSummaryDTO> waterCreditSummaryDTOs) {
		this.waterCreditSummaryDTOs = waterCreditSummaryDTOs;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public ResponseInformationService getResponseInformationService() {
		return responseInformationService;
	}

	public void setResponseInformationService(ResponseInformationService responseInformationService) {
		this.responseInformationService = responseInformationService;
	}

	public RequestInformationLogService getRequestInformationLogService() {
		return requestInformationLogService;
	}

	public void setRequestInformationLogService(RequestInformationLogService requestInformationLogService) {
		this.requestInformationLogService = requestInformationLogService;
	}

	public WaterCreditSummaryService getWaterCreditSummaryService() {
		return waterCreditSummaryService;
	}

	public void setWaterCreditSummaryService(WaterCreditSummaryService waterCreditSummaryService) {
		this.waterCreditSummaryService = waterCreditSummaryService;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public long getCustID() {
		return custID;
	}

	public void setCustID(long custID) {
		this.custID = custID;
	}
	
	
	

}
