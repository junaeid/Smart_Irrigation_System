package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import bd.ac.uiu.dto.ResponseInformationLogDTO;
import bd.ac.uiu.service.ResponseInformationLogService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class ResponseInformationLogController {
	private ResponseInformationLogDTO responseInformationLogDTO;
	private List<ResponseInformationLogDTO> responseInformationLogList;
	@ManagedProperty("#{responseInformationLogService}")
	private ResponseInformationLogService responseInformationLogService;

	public void saveResponseInformationLog() {

	}

	public void updateResponseInformationLog() {

	}

	public void deleteResponseInformationLog() {

	}

	public ResponseInformationLogDTO getResponseInformationLogDTO() {
		if (responseInformationLogDTO == null) {
			responseInformationLogDTO = new ResponseInformationLogDTO();
		}
		return responseInformationLogDTO;
	}

	public void setResponseInformationLogDTO(ResponseInformationLogDTO responseInformationLogDTO) {
		this.responseInformationLogDTO = responseInformationLogDTO;
	}

	public List<ResponseInformationLogDTO> getResponseInformationLogListByOperatorID() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			responseInformationLogList = new ArrayList<>();
			responseInformationLogList = responseInformationLogService
					.findResponseInformationLogListByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}
		return responseInformationLogList;
	}
	
	public List<ResponseInformationLogDTO> getResponseInformationLogList() {
		
		try {
			responseInformationLogList = new ArrayList<>();
			responseInformationLogList = responseInformationLogService
					.findResponseInformationLogList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return responseInformationLogList;
	}

	public void setResponseInformationLogList(List<ResponseInformationLogDTO> responseInformationLogList) {
		this.responseInformationLogList = responseInformationLogList;
	}

	public ResponseInformationLogService getResponseInformationLogService() {
		return responseInformationLogService;
	}

	public void setResponseInformationLogService(ResponseInformationLogService responseInformationLogService) {
		this.responseInformationLogService = responseInformationLogService;
	}
}
