package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.service.AreaService;
import bd.ac.uiu.service.IpcuService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class IpcuController {

	private IpcuDTO ipcuDTO;

	private List<IpcuDTO> ipcuList;
	private List<IpcuDTO> ipcuListOperator;
	private List<AreaDTO> areaList;
	private List<OperatorDTO> operatorList;
	private List<IpcuDTO> ipcuListByArea;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	private long areaID;
	private long operatorID;

	/*
	 * get areaID, and operatorID and set it to ipcuDTO then get the value from
	 * ipcuDTO then save it using ipcuService.saveIPCU(ipcuDTO);
	 */
	public void saveIpcu() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {

			// long operatorID =
			// Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

			AreaDTO areaDTO = new AreaDTO();
			areaDTO.setAreaID(areaID);
			ipcuDTO.setAreaDTO(areaDTO);

			OperatorDTO op = new OperatorDTO();
			op.setOperatorID(operatorID);
			ipcuDTO.setOperatorDTO(op);
			System.out.println("area id: " + areaDTO.getAreaID());

			System.out.println("Operator id: " + op.getOperatorID());
			if (ipcuService.saveIPCU(ipcuDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				ipcuDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ipcu Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	/*
	 * get ipcu information from ipcuDTO then update it using
	 * ipcuService.updateIPCU(ipcuDTO);
	 */

	public void updateIPCU() {
		FacesContext context = FacesContext.getCurrentInstance();
		OperatorDTO operatorDTO = new OperatorDTO();
		// long operatorID =
		// Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		operatorDTO.setOperatorID(ipcuDTO.getOperatorDTO().getOperatorID());
		ipcuDTO.setOperatorDTO(operatorDTO);

		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		ipcuDTO.setDateExecuted(date);
		ipcuDTO.setIpExecuted(ipAddress);
		ipcuDTO.setUserExecuted(userName);

		try {
			ipcuService.updateIPCU(ipcuDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			ipcuDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	/*
	 * get ipcuInformation from ipcuDTO then delete it using
	 * ipcuService.deleteIPCU(ipcuDTO);
	 */

	public void deleteIPCU() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			ipcuService.deleteIPCU(ipcuDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			ipcuDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public IpcuDTO getIpcuDTO() {
		if (ipcuDTO == null) {
			this.ipcuDTO = new IpcuDTO();
		}
		return ipcuDTO;
	}

	public void setIpcuDTO(IpcuDTO ipcuDTO) {
		this.ipcuDTO = ipcuDTO;
	}

	public List<IpcuDTO> getIpcuListOperator() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {
			ipcuListOperator = new ArrayList<>();
			ipcuListOperator = ipcuService.findIpcusByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			System.out.println(e);
		}
		return ipcuListOperator;
	}

	public void setIpcuListOperator(List<IpcuDTO> ipcuListOperator) {
		this.ipcuListOperator = ipcuListOperator;
	}

	public List<IpcuDTO> getIpcuList() {

		try {
			ipcuList = new ArrayList<>();
			ipcuList = ipcuService.findIPCUList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ipcuList;
	}

	public void setIpcuList(List<IpcuDTO> ipcuList) {
		this.ipcuList = ipcuList;
	}

	public IpcuService getIpcuService() {
		return ipcuService;
	}

	public void setIpcuService(IpcuService ipcuService) {
		this.ipcuService = ipcuService;
	}

	public List<AreaDTO> getAreaList() {
		try {
			areaList = new ArrayList<>();
			areaList = areaService.findAreaList();
		} catch (Exception e) {
			System.out.println(e);
		}
		return areaList;
	}

	public void setAreaList(List<AreaDTO> areaList) {
		this.areaList = areaList;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public long getAreaID() {
		return areaID;
	}

	public void setAreaID(long areaID) {
		this.areaID = areaID;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public List<OperatorDTO> getOperatorList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		try {

			operatorList = new ArrayList<>();
			operatorList = operatorService.findAllOperatorsByOrganizationID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return operatorList;
	}

	public void setOperatorList(List<OperatorDTO> operatorList) {
		this.operatorList = operatorList;
	}

	public void findIpcuListByArea() throws Exception {

		this.ipcuListByArea = new ArrayList<>();
		this.ipcuListByArea = ipcuService.findIPCUsByAreaID(areaID);
	}

	public List<IpcuDTO> getIpcuListByArea() {
		return ipcuListByArea;
	}

	public void setIpcuListByArea(List<IpcuDTO> ipcuListByArea) {
		this.ipcuListByArea = ipcuListByArea;
	}

}
