package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import bd.ac.uiu.customClass.TotalTrancastionByOperator;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.TransactionLogDTO;
import bd.ac.uiu.dto.WaterCreditLogDTO;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.service.CustomerService;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.TransactionLogService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@ViewScoped
public class TransactionLogController {

	private TransactionLogDTO transactionLogDTO;

	private List<TransactionLogDTO> transactionLogList;

	private List<TransactionLogDTO> transactionLogListOrganization;

	private List<CustomerDTO> customerDTOByOperatorID;

	private List<TransactionLogDTO> transactionLogFiltered;

	private CustomerDTO customerDTO;

	private WaterCreditSummaryDTO waterCreditSummaryDTO;

	private List<WaterCreditLogDTO> waterCreditSummaryLogDTO;

	private List<TransactionLogDTO> sumOfTransactionByDate;

	private List<TotalTrancastionByOperator> totalTrancastionByOperators;

	private List<OperatorDTO> opList;

	private List<TransactionLogDTO> transactionLogDTOs;

	private double unitPric = 10.0;
	private int noOfUnit;

	private Date stratDate;
	private Date endDate;

	private long operatorID;

	double totalTransactionAmount;

	private TotalTrancastionByOperator totalTrancastionByOperator;

	@ManagedProperty("#{transactionLogService}")
	private TransactionLogService transactionLogService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	public void changeOnDemand() {
		double amt = transactionLogDTO.getAmount();
		noOfUnit = (int) (amt / unitPric);

	}

	public void saveTransaction() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
			System.out.println(customerDTO.getCustomerID());
			System.out.println(transactionLogDTO.getPaymentTypeName());
			System.out.println(transactionLogDTO.getAmount());

			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(cerOrOrgOrOpIDLogin);
			transactionLogDTO.setOperatorDTO(operatorDTO);

			transactionLogDTO.setCustomerDTO(customerDTO);
			transactionLogDTO.setNoOfUnit(noOfUnit);

			transactionLogDTO.setDebitAmount(noOfUnit);

			transactionLogDTO.setCustomerDTO(customerDTO);
			System.out.println("cus ID" + transactionLogDTO.getCustomerDTO().getCustomerID());

			transactionLogDTO.setCounter("N/A");
			transactionLogDTO.setTrxStatus("N/A");
			transactionLogDTO.setTrxId("N/A");
			transactionLogDTO.setReference("N/A");
			transactionLogDTO.setSender("N/A");
			transactionLogDTO.setService("N/A");
			transactionLogDTO.setCurrency("N/A");
			transactionLogDTO.setReceiver("N/A");
			transactionLogDTO.setTrxTimeStamp("N/A");

			if (transactionLogService.saveTransaction(transactionLogDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				transactionLogDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "transaction Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void saveTransactionByPerameter(long customerID) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			
			transactionLogDTO.setNoOfUnit(0);

			customerDTO.setCustomerID(customerID);

			transactionLogDTO.setCustomerDTO(customerDTO);
			
			transactionLogDTO.setCounter("N/A");
			transactionLogDTO.setTrxStatus("N/A");
			transactionLogDTO.setTrxId("N/A");
			transactionLogDTO.setReference("N/A");
			transactionLogDTO.setSender("N/A");
			transactionLogDTO.setService("N/A");
			transactionLogDTO.setCurrency("N/A");
			transactionLogDTO.setReceiver("N/A");
			transactionLogDTO.setTrxTimeStamp("N/A");
			transactionLogDTO.setDebitAmount(0);

			if (transactionLogService.saveTransaction(transactionLogDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				transactionLogDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "transaction Exist", ""));
			}
		} catch (Exception e) {
			// TODO: handle exception

			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void updateTransaction() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			transactionLogService.updateTransaction(transactionLogDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			transactionLogDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public void deleteTransaction() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			transactionLogService.deleteTransaction(transactionLogDTO);
			context.addMessage(null, new FacesMessage("Successfully Deleted"));
			transactionLogDTO = null;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public TransactionLogDTO getTransactionLogDTO() {
		if (transactionLogDTO == null) {
			transactionLogDTO = new TransactionLogDTO();
		}
		return transactionLogDTO;
	}

	public void setTransactionLogDTO(TransactionLogDTO transactionDTO) {
		this.transactionLogDTO = transactionDTO;
	}

	// public List<TransactionDTO> getTransactionList() {
	// try {
	// transactionList = new ArrayList<>();
	// transactionList = transactionService.findTransactions();
	// System.out.println("size of transactionList" + transactionList.size());
	// } catch (Exception e) {
	// System.out.println(e);
	// }
	// return transactionList;
	// }

	public List<TransactionLogDTO> getTransactionLogList() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		try {
			transactionLogList = new ArrayList<>();
			transactionLogList = transactionLogService.findTransactionLogByOperatorID(cerOrOrgOrOpIDLogin);
			System.out.println("size of transactionList" + transactionLogList.size());
		} catch (Exception e) {
			System.out.println(e);
		}
		return transactionLogList;
	}

	public List<TransactionLogDTO> getSumOfTransactionByDate() {

		try {
			sumOfTransactionByDate = new ArrayList<>();
			sumOfTransactionByDate = transactionLogService.sumOfTransactionByDate("2017-07-17");

			System.out.println("sum of size " + sumOfTransactionByDate.size());
		} catch (Exception e) {
		}

		return sumOfTransactionByDate;

	}

	public void setSumOfTransactionByDate(List<TransactionLogDTO> sumOfTransactionByDate) {

		this.sumOfTransactionByDate = sumOfTransactionByDate;
	}

	public void setTransactionLogList(List<TransactionLogDTO> transactionLogList) {
		this.transactionLogList = transactionLogList;
	}

	public List<TransactionLogDTO> getTransactionLogListOrganization() {

		try {
			transactionLogListOrganization = new ArrayList<>();
			transactionLogListOrganization = transactionLogService.findTransactions();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactionLogListOrganization;
	}

	public void setTransactionLogListOrganization(List<TransactionLogDTO> transactionLogListOrganization) {
		this.transactionLogListOrganization = transactionLogListOrganization;
	}

	public TransactionLogService getTransactionService() {
		return transactionLogService;
	}

	public void setTransactionLogService(TransactionLogService transactionLogService) {
		this.transactionLogService = transactionLogService;
	}

	public List<CustomerDTO> getCustomerDTOByOperatorID() {
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		try {
			customerDTOByOperatorID = new ArrayList<>();
			customerDTOByOperatorID = customerService.findCustomersByOperatorID(cerOrOrgOrOpIDLogin);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerDTOByOperatorID;
	}

	public void setCustomerDTOByOperatorID(List<CustomerDTO> customerDTOByOperatorID) {
		this.customerDTOByOperatorID = customerDTOByOperatorID;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public CustomerDTO getCustomerDTO() {
		if (customerDTO == null) {
			customerDTO = new CustomerDTO();
		}
		return customerDTO;
	}

	public void setCustomerDTO(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public WaterCreditSummaryDTO getWaterCreditSummaryDTO() {
		return waterCreditSummaryDTO;
	}

	public void setWaterCreditSummaryDTO(WaterCreditSummaryDTO waterCreditSummaryDTO) {
		this.waterCreditSummaryDTO = waterCreditSummaryDTO;
	}

	public List<WaterCreditLogDTO> getWaterCreditSummaryLogDTO() {
		return waterCreditSummaryLogDTO;
	}

	public void setWaterCreditSummaryLogDTO(List<WaterCreditLogDTO> waterCreditSummaryLogDTO) {
		this.waterCreditSummaryLogDTO = waterCreditSummaryLogDTO;
	}

	public double getUnitPric() {
		return unitPric;
	}

	public void setUnitPric(double unitPric) {
		this.unitPric = unitPric;
	}

	public TransactionLogService getTransactionLogService() {
		return transactionLogService;
	}

	public int getNoOfUnit() {
		return noOfUnit;
	}

	public void setNoOfUnit(int noOfUnit) {
		this.noOfUnit = noOfUnit;
	}

	public List<TransactionLogDTO> getTransactionLogFiltered() {
		return transactionLogFiltered;
	}

	public void setTransactionLogFiltered(List<TransactionLogDTO> transactionLogFiltered) {
		this.transactionLogFiltered = transactionLogFiltered;
	}

	public List<TotalTrancastionByOperator> getTotalTrancastionByOperators() throws Exception {

		totalTrancastionByOperators = new ArrayList<>();

		this.totalTrancastionByOperators = transactionLogService.perOperatorTotalTransaction();
		System.out.println("Total transaction :" + totalTrancastionByOperators.size());

		return totalTrancastionByOperators;
	}

	public void setTotalTrancastionByOperators(List<TotalTrancastionByOperator> totalTrancastionByOperators) {
		this.totalTrancastionByOperators = totalTrancastionByOperators;
	}

	public Date getStratDate() {
		return stratDate;
	}

	public void setStratDate(Date stratDate) {
		this.stratDate = stratDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public void totalTransactionByOpAndDate() throws Exception {
		totalTrancastionByOperator = new TotalTrancastionByOperator();
		totalTrancastionByOperator = transactionLogService
				.perOperatorTotalTransactionByDateWiseAndOperatorWise(stratDate, endDate, operatorID);

	}

	public TotalTrancastionByOperator getTotalTrancastionByOperator() {
		return totalTrancastionByOperator;
	}

	public void setTotalTrancastionByOperator(TotalTrancastionByOperator totalTrancastionByOperator) {
		this.totalTrancastionByOperator = totalTrancastionByOperator;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public List<OperatorDTO> getOpList() throws Exception {

		opList = new ArrayList<>();
		opList = operatorService.findAllOperatorList();
		return opList;
	}

	public void setOpList(List<OperatorDTO> opList) {
		this.opList = opList;
	}

	public List<TransactionLogDTO> getTransactionLogDTOs() {
		return transactionLogDTOs;
	}

	public void setTransactionLogDTOs(List<TransactionLogDTO> transactionLogDTOs) {
		this.transactionLogDTOs = transactionLogDTOs;
	}

	public void finddateWiseTransactionLogList() {
		this.transactionLogDTOs = new ArrayList<>();
		System.out.println("startDate " + stratDate);
		System.out.println("end date " + endDate);
		try {

			this.transactionLogDTOs = this.transactionLogService.dateWiseTransactionLogList(operatorID, stratDate,
					endDate);
			System.out.println("list size::::: " + transactionLogDTOs.size());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public double getTotalTransactionAmount() {

		for (TotalTrancastionByOperator e : totalTrancastionByOperators) {
			this.totalTransactionAmount += e.getTotalAmountPerOperator();
		}
		System.out.println("totalTransaction Amount:" + totalTransactionAmount);

		return totalTransactionAmount;
	}

	public void setTotalTransactionAmount(double totalTransactionAmount) {
		this.totalTransactionAmount = totalTransactionAmount;
	}

}
