package bd.ac.uiu.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.formula.OperationEvaluationContext;

import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.dto.UserDTO;
import bd.ac.uiu.service.OperatorService;
import bd.ac.uiu.service.UserService;
import bd.ac.uiu.utility.ApplicationUtility;

@ManagedBean
@SessionScoped
public class OperatorController {

	private OperatorDTO operatorDTO;

	private OperatorDTO operatorDTOTarget;

	private List<OperatorDTO> operatorList;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{userService}")
	private UserService userService;

	private UserDTO userDTO;

	private OrganizationDTO organizationDTO;

	private long operatorID;

	public void findAllOperatorsByOrganizationID() {

		try {
			this.operatorList = operatorService.findAllOperatorsByOrganizationID(organizationDTO.getOrganizationID());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * get operator Information from operatorDTO then save it using
	 * operatorService.saveOperator(operatorDTO);
	 */

	public void saveOperator() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {

			if (operatorService.saveOperator(operatorDTO)) {
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				operatorDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "operator Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	public void saveOperatorUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {

			if (operatorService.saveOperator(operatorDTO)) {
				long operatorID = operatorService.operatorMaxID();
				String operatorName = operatorDTO.getOperatorName();
				// userDTO.setOperatorDTO();
				if (userService.saveUser(userDTO)) {
					userDTO = null;

				}
				context.addMessage(null, new FacesMessage("Successfully Saved"));
				operatorDTO = null;
			} else {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "operator Exist", ""));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Faield", ""));
		}
	}

	/*
	 * get organizationID from season then set it to opearatorDTO and get
	 * opearator information from opearatorDTO and update OperatorInforamtion by
	 * operatorService.updateOperator(operatorDTO);
	 */
	public void updateOperator() {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		FacesContext context = FacesContext.getCurrentInstance();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		OrganizationDTO organizationDTO = new OrganizationDTO();
		organizationDTO.setOrganizationID(cerOrOrgOrOpIDLogin);
		operatorDTO.setOrganizationDTO(organizationDTO);
		operatorDTO.setDateExecuted(new Date());
		operatorDTO.setIpExecuted(ipAddress);
		operatorDTO.setUserExecuted(userName);
		operatorDTO.setRecordNote("N/A");
		try {
			operatorService.updateOperator(operatorDTO);
			context.addMessage(null, new FacesMessage("Successfully Updated"));
			operatorDTO = null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", ""));
		}
	}

	public OperatorDTO getOperatorDTO() {
		if (operatorDTO == null) {
			operatorDTO = new OperatorDTO();
		}
		return operatorDTO;
	}

	public void setOperatorDTO(OperatorDTO operatorDTO) {
		this.operatorDTO = operatorDTO;
	}

	public List<OperatorDTO> getOperatorList() {
		try {
			operatorList = new ArrayList<>();
			operatorList = operatorService.findAllOperatorList();
		} catch (Exception e) {
			System.out.println(e);
		}

		return operatorList;
	}

	public void setOperatorList(List<OperatorDTO> operatorList) {
		this.operatorList = operatorList;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public OrganizationDTO getOrganizationDTO() {
		if (organizationDTO == null) {
			organizationDTO = new OrganizationDTO();

		}
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}

	public UserDTO getUserDTO() {
		if (userDTO == null) {
			this.userDTO = new UserDTO();
		}
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String jumpToOperatorProfile() {
		/*
		 * if (operatorDTOTarget != null) {
		 * setOperatorID(operatorDTOTarget.getOperatorID()); }
		 */
		System.out.println("Operator ID " + operatorDTOTarget.getOperatorID());
		System.out.println("OP Name:" + operatorDTOTarget.getOperatorName());
		System.out.println("Op Mobile  " + operatorDTOTarget.getMobile());
		return "operatorProfile.xhtml?faces-redirect=true";
	}

	public OperatorDTO getOperatorDTOTarget() {
		if (operatorDTOTarget == null) {
			operatorDTOTarget = new OperatorDTO();
		}
		return operatorDTOTarget;
	}

	public void setOperatorDTOTarget(OperatorDTO operatorDTOTarget) {
		this.operatorDTOTarget = operatorDTOTarget;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

}
