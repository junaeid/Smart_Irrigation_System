package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.CerDAO;
import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class CerService {

	public CerService() {
	}

	@Autowired
	private CerDAO cerDAO;

	/*
	 * TO save cer information get data from dto then save it using
	 * cerDAO.persist(cer);
	 */
	@Transactional
	public boolean saveCER(CerDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		Cer cer = new Cer(dto.getCerName(), dto.getMobile(), dto.getEmail(), dto.getPresentAddress(),
				dto.getPermanentAddress(), dto.getTinCerNumber(), dto.getEstablishDate(), "", "", "", "",
				dto.getRegistrationNumber(), "N/A", userName, new Date(), ipAddress, true);

		this.cerDAO.persist(cer);
		return true;
	}

	/*
	 * Find all cer from database using list of array then add new CerDTO to the
	 * list and return it
	 */
	public List<CerDTO> findAllCer() throws Exception {
		List<CerDTO> list = new ArrayList<>();

		List<Cer> entityList = cerDAO.findAll();
		for (Cer cer : entityList) {

			list.add(new CerDTO(cer.getCerID(), cer.getCerName(), cer.getMobile(), cer.getEmail(),
					cer.getPresentAddress(), cer.getPermanentAddress(), cer.getTinCerNumber(), cer.getEstablishDate(),
					cer.getRegistrationNumber(), cer.isEnabled()));
		}
		entityList = null;
		return list;
	}

	/*
	 * long cerID, String cerName, String mobile, String email, String
	 * presentAddress, String permanentAddress, String tinCerNumber, Date
	 * establishDate, String imageName, String sizeOfImage, String imagePath, String
	 * imageTitle, String registrationNumber, String recordNote, String
	 * userExecuted, Date dateExecuted, String ipExecuted, boolean enabled
	 */

	/*
	 * To update cer information get data from dto then update it using
	 * cerDAO.merge(cer);
	 */
	@Transactional
	public void updateCer(CerDTO dto) throws Exception {
		Cer cer = new Cer();

		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		cer = new Cer(dto.getCerID(), dto.getCerName(), dto.getMobile(), dto.getEmail(), dto.getPresentAddress(),
				dto.getPermanentAddress(), dto.getTinCerNumber(), dto.getEstablishDate(), dto.getImageName(),
				dto.getSizeOfImage(), dto.getImageTitle(), dto.getImageTitle(), dto.getRegistrationNumber(),
				dto.getRecordNote(), userName, date, ipAddress, true);
		cerDAO.merge(cer);
		cer = null;

	}

	/*
	 * get data from dto then delete it using cerDAO.remove(dto.getCerID())
	 */
	@Transactional
	public void deleteCER(CerDTO dto) throws Exception {
		cerDAO.remove(dto.getCerID());
	}

	/*
	 * copy all entity to dto
	 */
	public CerDTO copyCerFrmEntity(Cer cer) throws Exception {
		if (cer.getCerID() != 0) {
			CerDTO cerDTO = new CerDTO();
			BeanUtils.copyProperties(cer, cerDTO);
			return cerDTO;

		}
		return null;
	}

	/*
	 * Copy dto to entity
	 */
	public Cer copyCerFrmDTO(CerDTO cerDTO) throws Exception {
		if (cerDTO.getCerID() != 0 || cerDTO.getCerName() == null) {
			Cer cer = new Cer();
			BeanUtils.copyProperties(cerDTO, cer);
			return cer;
		}
		return null;
	}

}
