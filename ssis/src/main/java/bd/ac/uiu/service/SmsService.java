package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.SMSDAO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.SMSDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.SMS;

@Service
public class SmsService {

	public SmsService() {
	}

	@Autowired
	private SMSDAO smsdao;

	@ManagedProperty("#operatorService")
	private OperatorService operatorService;

	@ManagedProperty("#customerService")
	private CustomerService customerService;

	@Transactional
	public void saveSms(SMSDTO dto) throws Exception {
		Date date = new Date();

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		Customer customer = new Customer();
		customer.setCustomerID(dto.getCustomerDTO().getCustomerID());
		System.out.println("smsTO " + dto.getSmsTo());
		/*
		 * String smsTo, String smsFrom, int availableWaterCredit, String
		 * smsBody, Operator operator, Customer customer, Date dateExecuted
		 */
		SMS sms = new SMS(dto.getSmsTo(), dto.getSmsFrom(), dto.getAvailableWaterCredit(), dto.getSmsBody(), operator,
				customer, date);
		this.smsdao.persist(sms);
		sms = null;
	}

	public List<SMSDTO> findAllSMSList() throws Exception {
		List<SMSDTO> smsList = new ArrayList<>();
		List<SMS> sms = smsdao.findAll();

		/*
		 * long smsID, String smsTo, String smsFrom, OperatorDTO operatorDTO,
		 * CustomerDTO customerDTO, String recordNote, String userExecuted, Date
		 * dateExecuted, String ipExecuted, boolean enabled
		 */

		for (SMS s : sms) {

			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(s.getOperator().getOperatorID());
			operatorDTO.setOperatorName(s.getOperator().getOperatorName());

			CustomerDTO customerDTO = new CustomerDTO();
			customerDTO.setCustomerID(s.getCustomer().getCustomerID());
			customerDTO.setCustomerName(s.getCustomer().getCustomerName());

			smsList.add(new SMSDTO(s.getSmsID(), s.getSmsTo(), s.getSmsFrom(), s.getAvailableWaterCredit(),
					s.getSmsBody(), operatorDTO, customerDTO, s.getDateExecuted()));

		}
		sms = null;

		return smsList;
	}
}
