package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.ResponseInformationDAO;
import bd.ac.uiu.dto.ResponseInformationDTO;
import bd.ac.uiu.entity.ResponseInformation;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class ResponseInformationService {

	@Autowired
	private ResponseInformationDAO responseInformationDAO;

	public ResponseInformationService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Transactional
	public boolean saveResponseInformation(ResponseInformationDTO dto) throws Exception {

		String ipAddress = ApplicationUtility.getIpAddress();
		//String userName = ApplicationUtility.getUserID();

		ResponseInformation responseInformation = new ResponseInformation(dto.getFieldVulve(), dto.getSensorStatus(),
				dto.getNodeID(), dto.getBalance(), dto.getPaymentSummaryID(), dto.getRecordNote(), "N/A", new Date(),
				ipAddress, true);

		responseInformation.setOperatorID(dto.getOperatorID());
		responseInformation.setIpcuID(dto.getIpcuID());
		responseInformation.setCustomerID(dto.getCustomerID());
		responseInformation.setFieldID(dto.getFieldID());
		responseInformation.setMainVulve(dto.getMainVulve());

		this.responseInformationDAO.persist(responseInformation);

		return true;
	}

	public List<ResponseInformationDTO> findResponseInformationList() throws Exception {
		List<ResponseInformationDTO> responseInformationList = new ArrayList<>();

		List<ResponseInformation> entityList = responseInformationDAO.findAllForCERAndOrg();

		for (ResponseInformation e : entityList) {
			responseInformationList.add(new ResponseInformationDTO(e.getResponseID(), e.getOperatorID(), e.getIpcuID(),
					e.getCustomerID(), e.getFieldID(), e.getFieldVulve(), e.getSensorStatus(), e.getNodeID(),
					e.getBalance(), e.getPaymentSummaryID(), e.getRecordNote(), e.getUserExecuted(),
					e.getDateExecuted(), e.getIpExecuted(), e.isEnabled()));
		}

		entityList = null;
		return responseInformationList;
	}

	public List<ResponseInformationDTO> findResponseInformationByOperatorID(long operatorID) throws Exception {
		List<ResponseInformationDTO> responseInformationDTOs = new ArrayList<>();

		List<ResponseInformation> responseInformations = responseInformationDAO
				.findResponseInformationByOperatorID(operatorID);

		responseInformations.stream().forEach((respo) -> {
			responseInformationDTOs.add(copyResponseInformationFrmEntity(respo));
		});
		return responseInformationDTOs;
	}

	public List<ResponseInformationDTO> findResListByOpAndCustID(long operatorID, long customerID) throws Exception {
		List<ResponseInformationDTO> responseInformationDTOs = new ArrayList<>();
		List<ResponseInformation> responseInformations = responseInformationDAO.findResListByOpAndCustID(operatorID,
				customerID);
		responseInformations.stream().forEach((res) -> {
			responseInformationDTOs.add(copyResponseInformationFrmEntity(res));
		});
		return responseInformationDTOs;
	}

	@Transactional
	public void updateResponseInformation(ResponseInformationDTO dto) throws Exception {
		ResponseInformation responseInformation = new ResponseInformation();
		responseInformation = copyResponseInformationFrmDTO(dto);
		responseInformationDAO.merge(responseInformation);
		responseInformation = null;
	}

	@Transactional
	public void delete(ResponseInformationDTO dto) throws Exception {
		responseInformationDAO.remove(dto.getResponseID());
	}

	public ResponseInformationDTO copyResponseInformationFrmEntity(ResponseInformation responseInformation) {
		if (responseInformation.getResponseID() != 0) {
			ResponseInformationDTO responseInformationDTO = new ResponseInformationDTO();
			BeanUtils.copyProperties(responseInformation, responseInformationDTO);
			return responseInformationDTO;
		}
		return null;
	}

	public ResponseInformation copyResponseInformationFrmDTO(ResponseInformationDTO responseInformationDTO) {
		if (responseInformationDTO.getResponseID() != 0) {
			ResponseInformation responseInformation = new ResponseInformation();
			BeanUtils.copyProperties(responseInformationDTO, responseInformation);
			return responseInformation;
		}
		return null;
	}
}
