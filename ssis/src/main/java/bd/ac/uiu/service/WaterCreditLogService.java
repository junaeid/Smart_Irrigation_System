package bd.ac.uiu.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.WaterCreditLogDAO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.WaterCreditLogDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.WaterCreditLog;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class WaterCreditLogService {

	public WaterCreditLogService() {
	}

	@Autowired
	private WaterCreditLogDAO waterCreditLogDAO;

	@Transactional
	public boolean saveWaterCreditLogWhenPayment(WaterCreditLogDTO dto) throws Exception {
		/*
		 * WaterCreditLog(int usedWaterCredit, int availableWaterCredit, Date
		 * usedDate, String year, String month, Field field, Ipcu ipcu, Customer
		 * customer, Operator operator, String recordNote, String userExecuted,
		 * Date dateExecuted, String ipExecuted, boolean enabled)
		 *
		 * String ipAddress = ApplicationUtility.getIpAddress(); String username
		 * = ApplicationUtility.getUserID(); Date date = new Date();
		 * 
		 * String month = new SimpleDateFormat("MMMM").format(date); String year
		 * = new SimpleDateFormat("YYYY").format(date);
		 */
		return true;
	}

	public void saveWaterCreditLogWhenWaterCreditIsUsedInField(long customerID, long operatorID, int fieldID,
			int ipcuID, int usedWaterCredit, int availableWaterCredit) {
		String ipAddress = ApplicationUtility.getIpAddress();
		Customer customer = new Customer();
		customer.setCustomerID(customerID);

		Operator operator = new Operator();
		operator.setOperatorID(operatorID);
		Date date = new Date();
		String month = new SimpleDateFormat("MMMM").format(date);
		String year = new SimpleDateFormat("YYYY").format(date);

		WaterCreditLog waternew = new WaterCreditLog(usedWaterCredit, availableWaterCredit, date, year, month, fieldID, ipcuID,
				customer, operator, "N/A", "N/A", date, ipAddress, true);

		try {
			waterCreditLogDAO.persist(waternew);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<WaterCreditLogDTO> findWaterCreditLogList() throws Exception {
		List<WaterCreditLogDTO> waterCreditLogList = new ArrayList<>();

		List<WaterCreditLog> entityList = waterCreditLogDAO.findAllForCERAndOrg();
		for (WaterCreditLog e : entityList) {
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(e.getOperator().getOperatorID());
			operatorDTO.setOperatorName(e.getOperator().getOperatorName());

			CustomerDTO customerDTO = new CustomerDTO();
			customerDTO.setCustomerID(e.getCustomer().getCustomerID());
			customerDTO.setCustomerName(e.getCustomer().getCustomerName());

			WaterCreditLogDTO creditLogDTO = new WaterCreditLogDTO();
			creditLogDTO.setWaterCreditID(e.getWaterCreditID());
			creditLogDTO.setUsedWaterCredit(e.getUsedWaterCredit());
			creditLogDTO.setAvailableWaterCredit(e.getAvailableWaterCredit());
			creditLogDTO.setUsedDate(e.getUsedDate());
			creditLogDTO.setYear(e.getYear());
			creditLogDTO.setMonth(e.getMonth());
			creditLogDTO.setIpcuID(e.getIpcuID());
			creditLogDTO.setFieldID(e.getFieldID());
			creditLogDTO.setOperatorDTO(operatorDTO);
			creditLogDTO.setCustomerDTO(customerDTO);

			waterCreditLogList.add(creditLogDTO);

		}

		entityList = null;
		return waterCreditLogList;
	}

	public List<WaterCreditLogDTO> findWaterCreditLogByOperatorID(long operatorID) throws Exception {
		List<WaterCreditLogDTO> waterCreditLogDTOs = new ArrayList<>();
		List<WaterCreditLog> waterCreditLogs = waterCreditLogDAO.findWaterCreditLogByOperatorID(operatorID);
		waterCreditLogs.stream().forEach((wcs) -> {
			waterCreditLogDTOs.add(copyWaterCreditLogFrmEntity(wcs));
		});
		return waterCreditLogDTOs;
	}

	@Transactional
	public void updateWaterCreditLog(WaterCreditLogDTO dto) throws Exception {
		WaterCreditLog waterCreditSummaryLog = new WaterCreditLog();
		waterCreditSummaryLog = copyWaterCreditLogFrmDTO(dto);
		waterCreditLogDAO.merge(waterCreditSummaryLog);
		waterCreditSummaryLog = null;
	}

	@Transactional
	public void deleteWaterCreditLog(WaterCreditLogDTO dto) throws Exception {
		waterCreditLogDAO.remove(dto.getWaterCreditID());
	}

	public WaterCreditLogDTO copyWaterCreditLogFrmEntity(WaterCreditLog waterCreditLog) {
		if (waterCreditLog.getWaterCreditID() != 0) {
			WaterCreditLogDTO dto = new WaterCreditLogDTO();
			BeanUtils.copyProperties(waterCreditLog, dto);

			CustomerDTO customerDTO = new CustomerDTO();
			OperatorDTO operatorDTO = new OperatorDTO();

			customerDTO.setCustomerID(waterCreditLog.getCustomer().getCustomerID());
			customerDTO.setCustomerName(waterCreditLog.getCustomer().getCustomerName());

			operatorDTO.setOperatorID(waterCreditLog.getOperator().getOperatorID());
			operatorDTO.setOperatorName(waterCreditLog.getOperator().getOperatorName());

			dto.setCustomerDTO(customerDTO);
			dto.setOperatorDTO(operatorDTO);

			return dto;
		}
		return null;
	}

	public WaterCreditLog copyWaterCreditLogFrmDTO(WaterCreditLogDTO waterCreditLogDTO) {
		if (waterCreditLogDTO.getWaterCreditID() != 0) {
			WaterCreditLog waterCreditLog = new WaterCreditLog();
			BeanUtils.copyProperties(waterCreditLogDTO, waterCreditLog);
			return waterCreditLog;
		}
		return null;
	}

}
