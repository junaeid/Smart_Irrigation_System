package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.customClass.TotalWaterCreditConsumeByOperator;
import bd.ac.uiu.dao.WaterCreditSummaryDAO;
import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.WaterCreditSummaryDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.WaterCreditSummary;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class WaterCreditSummaryService {

	public WaterCreditSummaryService() {
	}

	@Autowired
	private WaterCreditSummaryDAO waterCreditSummaryDAO;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@Transactional
	public boolean saveWaterCreditSummary(WaterCreditSummaryDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		Operator operator = new Operator();
		operator.setOperatorID(cerOrOrgOrOpIDLogin);

		Customer customer = new Customer();
		customer.setCustomerID(2);

		WaterCreditSummary waterCreditSummary = new WaterCreditSummary(0, 100, customer, operator, "N/A", userName,
				new Date(), ipAddress, true);

		try {
			WaterCreditSummary waSummary = new WaterCreditSummary();
			waSummary = waterCreditSummaryDAO.checkCustomerIDExistInWaterCreditSummary(2);
			if (waSummary == null) {
				waterCreditSummaryDAO.persist(waterCreditSummary);
				return true;
			} else {
				waterCreditSummaryDAO.updateAvailableWaterCreditAdd(2, 20);
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<WaterCreditSummaryDTO> findWaterCreditSummaryList() throws Exception {
		List<WaterCreditSummaryDTO> waterCreditSummaryList = new ArrayList<>();

		List<WaterCreditSummary> entityList = waterCreditSummaryDAO.findAll();
		for (WaterCreditSummary e : entityList) {
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(e.getOperator().getOperatorID());
			operatorDTO.setOperatorName(e.getOperator().getOperatorName());

			CustomerDTO customerDTO = new CustomerDTO();
			customerDTO.setCustomerID(e.getCustomer().getCustomerID());
			customerDTO.setCustomerName(e.getCustomer().getCustomerName());

			WaterCreditSummaryDTO wSummaryDTO = new WaterCreditSummaryDTO();

			wSummaryDTO.setWaterCreditSummaryID(e.getWaterCreditSummaryID());
			wSummaryDTO.setUsedWaterCredit(e.getUsedWaterCredit());
			wSummaryDTO.setAvailableWaterCredit(e.getAvailableWaterCredit());
			wSummaryDTO.setCustomerDTO(customerDTO);
			wSummaryDTO.setOperatorDTO(operatorDTO);

			waterCreditSummaryList.add(wSummaryDTO);
		}

		entityList = null;
		return waterCreditSummaryList;
	}

	public WaterCreditSummaryDTO findWaterCreditSummaryByOperatorIDAndCustomerID(long operatorID, long customerID) {
		WaterCreditSummary waterCreditSummary = new WaterCreditSummary();

		try {
			waterCreditSummary = waterCreditSummaryDAO.findWaterCreditSummaryByOperatorIDAndCustomerID(operatorID,
					customerID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		WaterCreditSummaryDTO dto = new WaterCreditSummaryDTO();
		BeanUtils.copyProperties(waterCreditSummary, dto);
		return dto;
	}

	public List<WaterCreditSummaryDTO> findWaterCreditSummaryByOpAndCustID(long operatorID, long customerID)
			throws Exception {
		List<WaterCreditSummaryDTO> creditSummaryDTOs = new ArrayList<>();
		List<WaterCreditSummary> creditSummaries = waterCreditSummaryDAO.findWacreSumByOpAndCustIDList(operatorID,
				customerID);
		creditSummaries.stream().forEach((cs) -> {
			creditSummaryDTOs.add(copyWaterCreditSummaryFrmEntity(cs));
		});
		return creditSummaryDTOs;
	}

	public List<WaterCreditSummaryDTO> findWaterCreditSummaryByOperatorID(long operatorID) throws Exception {
		List<WaterCreditSummaryDTO> creditSummaryDTOs = new ArrayList<>();

		List<WaterCreditSummary> waterCreditSummaries = waterCreditSummaryDAO
				.findWaterCreditSummaryByOperatorID(operatorID);

		waterCreditSummaries.stream().forEach((wcs) -> {

			creditSummaryDTOs.add(copyWaterCreditSummaryFrmEntity(wcs));
		});

		return creditSummaryDTOs;
	}

	public List<WaterCreditSummaryDTO> findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(long operatorID)
			throws Exception {

		List<WaterCreditSummary> waterCreditSummaries = waterCreditSummaryDAO
				.findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(operatorID);
		List<WaterCreditSummaryDTO> waterCreditSummaryDTOs = new ArrayList<>();

		waterCreditSummaries.stream().forEach((wcs) -> {

			waterCreditSummaryDTOs.add(copyWaterCreditSummaryFrmEntity(wcs));
		});

		return waterCreditSummaryDTOs;
	}

	@Transactional
	public void updateWaterCreditSummary(WaterCreditSummaryDTO dto) throws Exception {
		WaterCreditSummary waterCreditSummary = new WaterCreditSummary();
		waterCreditSummary = copyWaterCreditSummaryFrmDTO(dto);
		waterCreditSummaryDAO.merge(waterCreditSummary);
		waterCreditSummary = null;
	}

	@Transactional
	public void deleteWaterCreditSummary(WaterCreditSummaryDTO dto) throws Exception {
		waterCreditSummaryDAO.remove(dto.getWaterCreditSummaryID());
	}

	public void updateWaterCreditEnableStatus(long operatorID, long customerID, boolean isEnable) throws Exception {
		waterCreditSummaryDAO.updateWaterCreditEnableStatus(operatorID, customerID, isEnable);
	}

	public WaterCreditSummaryDTO copyWaterCreditSummaryFrmEntity(WaterCreditSummary waterCreditSummary) {
		if (waterCreditSummary.getWaterCreditSummaryID() != 0) {
			WaterCreditSummaryDTO waterCreditSummaryDTO = new WaterCreditSummaryDTO();
			BeanUtils.copyProperties(waterCreditSummary, waterCreditSummaryDTO);
			CustomerDTO customerDTO = new CustomerDTO();
			OperatorDTO operatorDTO = new OperatorDTO();

			customerDTO.setCustomerID(waterCreditSummary.getCustomer().getCustomerID());
			customerDTO.setCustomerName(waterCreditSummary.getCustomer().getCustomerName());

			operatorDTO.setOperatorID(waterCreditSummary.getOperator().getOperatorID());
			operatorDTO.setOperatorName(waterCreditSummary.getOperator().getOperatorName());

			waterCreditSummaryDTO.setCustomerDTO(customerDTO);
			waterCreditSummaryDTO.setOperatorDTO(operatorDTO);

			return waterCreditSummaryDTO;
		}
		return null;
	}

	public void waterCreditSummaryUpdateByUsedWaterCredit(long customerID, double numberOfWaterCredit)
			throws Exception {

		waterCreditSummaryDAO.updateUsedWaterCreditMinus(customerID, numberOfWaterCredit);

	}

	public WaterCreditSummary copyWaterCreditSummaryFrmDTO(WaterCreditSummaryDTO waterCreditSummaryDTO) {
		if (waterCreditSummaryDTO.getWaterCreditSummaryID() != 0) {
			WaterCreditSummary waterCreditSummary = new WaterCreditSummary();
			BeanUtils.copyProperties(waterCreditSummaryDTO, waterCreditSummary);
			return waterCreditSummary;
		}
		return null;
	}

	public TotalWaterCreditConsumeByOperator totalWaterCreditOperatorWise(long operatorID) throws Exception {
		List<WaterCreditSummaryDTO> waterCreditSummaryDTOs = new ArrayList<>();

		TotalWaterCreditConsumeByOperator totalWaterCreditConsumeByOperator = new TotalWaterCreditConsumeByOperator();

		waterCreditSummaryDTOs = findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(operatorID);

		double totalWaterCredit = 0.0;

		for (WaterCreditSummaryDTO e : waterCreditSummaryDTOs) {
			if (e.getOperatorDTO().getOperatorID() == operatorID) {
				totalWaterCredit += e.getUsedWaterCredit();
			}
		}

		totalWaterCreditConsumeByOperator.setOperatorID(operatorID);
		totalWaterCreditConsumeByOperator.setTotalWaterCredit(totalWaterCredit);
		totalWaterCreditConsumeByOperator.setAreaName("N/A");

		return totalWaterCreditConsumeByOperator;

	}

	public long findTotalWaterCreditByOperatorID(long operatorID){
		long usedWaterCredit=0;
		
		
		try {
			usedWaterCredit = waterCreditSummaryDAO.findTotalWaterCreditByOperatorID(operatorID);
		} catch (Exception e) {
			return 0;
		}
		return usedWaterCredit;
		
	}

	public double totalWaterCreditSummaryByOpId(long operatorID, String date) {
		System.out.println("From Service " + waterCreditSummaryDAO.totalWaterCreditSummaryByOpId(operatorID, date));
		return waterCreditSummaryDAO.totalWaterCreditSummaryByOpId(operatorID, date);
	}

	public double avgWaterCreditSummaryByOpId(long operatorID, String date) {
		return waterCreditSummaryDAO.avgWaterCreditSummaryByOpId(operatorID, date);
	}

	public double maxWaterCreditSummaryByOpId(long operatorID, String date) {
		return waterCreditSummaryDAO.maxWaterCreditSummaryByOpId(operatorID, date);
	}

	public double minWaterCreditSummaryByOpId(long operatorID, String date) {
		return waterCreditSummaryDAO.minWaterCreditSummaryByOpId(operatorID, date);
	}

	public double totalWaterCreditSummary(String date) {
		return waterCreditSummaryDAO.avgWaterCreditSummary(date);
	}

	public double avgWaterCreditSummary(String date) {
		return waterCreditSummaryDAO.avgWaterCreditSummary(date);
	}

	public double maxWaterCreditSummary(String date) {
		return waterCreditSummaryDAO.maxWaterCreditSummary(date);
	}

	public double minWaterCreditSummary(String date) {
		return waterCreditSummaryDAO.minWaterCreditSummary(date);
	}

	public WaterCreditSummaryDAO getWaterCreditSummaryDAO() {
		return waterCreditSummaryDAO;
	}

	public void setWaterCreditSummaryDAO(WaterCreditSummaryDAO waterCreditSummaryDAO) {
		this.waterCreditSummaryDAO = waterCreditSummaryDAO;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

}
