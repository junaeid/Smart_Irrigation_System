package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.AreaDAO;
import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.entity.Area;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class AreaService {

	public AreaService() {
	}

	@Autowired
	private AreaDAO areaDAO;

	/*
	 * check the area exists or not using areaDAO.checkArea(areaDTO.getAreaName())
	 */
	public boolean checkArea(AreaDTO areaDTO) {
		if (areaDAO.checkArea(areaDTO.getAreaName()) == null) {
			System.out.println("Area not exist");
			return true;
		}
		return false;
	}

	/*
	 * get area inputed data from dto and add date ipAddress username organizationID
	 * operatorID then save it using persist(area)
	 */
	@Transactional
	public boolean saveArea(AreaDTO dto) throws Exception {
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long orgID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		Organization organization = new Organization();
		organization.setOrganizationID(orgID);
		Operator op = new Operator();
		op.setOperatorID(dto.getOperatorDTO().getOperatorID());
		Area area = new Area();

		area = new Area(dto.getAreaName(), dto.getAreaCode(), dto.getRecordNote(), userName, date, ipAddress, true, op);
		area.setOrganization(organization);
		this.areaDAO.persist(area);
		return true;
	}

	/*
	 * get the value of area by operatorID,operatorName, organizationID,
	 * organizationName from areaDTO then add to the areaList and return the value
	 */
	public List<AreaDTO> findAreaList() throws Exception {
		List<AreaDTO> areaList = new ArrayList<>();

		List<Area> entityList = areaDAO.findAll();

		for (Area e : entityList) {
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(e.getOperator().getOperatorID());
			operatorDTO.setOperatorName(e.getOperator().getOperatorName());

			OrganizationDTO organizationDTO = new OrganizationDTO();
			organizationDTO.setOrganizationID(e.getOrganization().getOrganizationID());
			organizationDTO.setOrganizationName(e.getOrganization().getOrganizationName());

			AreaDTO areaDTO = new AreaDTO(e.getAreaID(), e.getAreaName(), e.getAreaCode(), e.getRecordNote(),
					e.getUserExecuted(), e.getDateExecuted(), e.getIpExecuted(), e.isEnabled(), operatorDTO);

			areaDTO.setOrganizationDTO(organizationDTO);

			areaList.add(areaDTO);

		}

		entityList = null;
		return areaList;
	}

	/*
	 * get the value if area by operatorID then add the value to areaDTOs and return
	 * the value
	 */

	public List<AreaDTO> findAreaByOperatorID(long operatorID) throws Exception {
		List<AreaDTO> areaDTOs = new ArrayList<>();

		List<Area> areas = areaDAO.findAreaByOperatorID(operatorID);

		System.out.println("Size :" + areas.size());

		areas.stream().forEach((ar) -> {
			areaDTOs.add(copyAreaFrmEntity(ar));

		});

		System.out.println("areaDTOs Size :" + areas.size());
		return areaDTOs;
	}

	/*
	 * update the area get the updated value from the area then update it using
	 * areaDAO.merge(area);
	 */

	@Transactional
	public void updateArea(AreaDTO dto) throws Exception {
		Area area = new Area();
		/*
		 * long areaID, String areaName, String areaCode,Operator operator, String
		 * recordNote, String userExecuted, Date dateExecuted, String ipExecuted,
		 * boolean enabled
		 */
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long orgID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Organization organization = new Organization();
		organization.setOrganizationID(orgID);

		Operator op = new Operator();
		op.setOperatorID(dto.getOperatorDTO().getOperatorID());

		area = new Area(dto.getAreaID(), dto.getAreaName(), dto.getAreaCode(), op, "N/A", userName, date, ipAddress,
				true);
		area.setOrganization(organization);

		areaDAO.merge(area);
		area = null;
	}

	/*
	 * delete area get the areadata from areaDTO then delete the area from the
	 * database
	 */
	@Transactional
	public void deleteArea(AreaDTO dto) throws Exception {
		areaDAO.remove(dto.getAreaID());
	}

	/*
	 * Copy area values from the entity to dto
	 */

	public AreaDTO copyAreaFrmEntity(Area area) {
		if (area.getAreaID() != 0) {
			AreaDTO areaDTO = new AreaDTO();
			OperatorDTO operatorDTO = new OperatorDTO();
			OrganizationDTO organizationDTO = new OrganizationDTO();

			BeanUtils.copyProperties(area, areaDTO);
			BeanUtils.copyProperties(area.getOperator(), operatorDTO);
			BeanUtils.copyProperties(area.getOrganization(), organizationDTO);

			areaDTO.setOperatorDTO(operatorDTO);
			areaDTO.setOrganizationDTO(organizationDTO);

			return areaDTO;
		}
		return null;
	}

	/*
	 * Copy area values from dto to entities
	 */
	public Area copyAreaFrmDTO(AreaDTO areaDTO) {
		if (areaDTO.getAreaID() != 0) {
			Area area = new Area();
			BeanUtils.copyProperties(areaDTO, area);
			return area;
		}
		return null;
	}

}
