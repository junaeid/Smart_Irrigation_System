package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import bd.ac.uiu.api.sms.ShortMessageSystem;
import bd.ac.uiu.dao.CustomerDAO;
import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.entity.Area;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class CustomerService {

	public CustomerService() {
	}

	@Autowired
	private CustomerDAO customerDAO;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	private String reference;

	@Transactional
	public boolean saveCustomer(CustomerDTO dto) throws DataIntegrityViolationException, Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Area area = new Area();
		area.setAreaID(dto.getAreaDTO().getAreaID());

		Operator operator = new Operator();
		operator.setOperatorID(cerOrOrgOrOpIDLogin);

		Customer customer = new Customer(dto.getCustomerName(), dto.getNationalID(), dto.getDateOfBirth(),
				dto.getMobile(), dto.getVulveNumber(), dto.getBirthCertificate(), dto.getReligion(), dto.getGender(),
				dto.getMaritalStatus(), dto.getNationality(), dto.getBloodGroup(), operator, area, "N/A", userName,
				new Date(), ipAddress, true);
		// customer.setCustomerID(customerID);
		this.customerDAO.persist(customer);
		
		ShortMessageSystem shortMessageSystem = new ShortMessageSystem();
		shortMessageSystem.customerRegistrationSuccessfully(dto.getMobile());
		
		return true;
	}

	public CustomerDTO findCustomerDetail(long operatorID, long customerID) throws Exception {
		CustomerDTO customerDTO = new CustomerDTO();
		Customer customers = customerDAO.findCustomerDetailByOperatorIDAndCustomerID(operatorID, customerID);
		BeanUtils.copyProperties(customers, customerDTO);
		return customerDTO;
	}

	public CustomerDTO findCustomerByOperatorIDAndCustomerID(long operatorID, long customerID) throws Exception {
		Customer customer = customerDAO.findCustomerDetailByOperatorIDAndCustomerID(operatorID, customerID);
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO = copyCustomerFrmEntity(customer);
		return customerDTO;
	}

	public List<CustomerDTO> findCustomerList() throws Exception {
		List<CustomerDTO> customerList = new ArrayList<>();

		/*
		 * long customerID, String customerName, String nationalID, Date
		 * dateOfBirth, String mobile, String vulveNumber, String
		 * birthCertificate, String religion, String gender, String
		 * maritalStatus, String nationality, String bloodGroup, AreaDTO
		 * areaDTO, OperatorDTO operatorDTO, String recordNote, String
		 * userExecuted, Date dateExecuted, String ipExecuted, boolean enabled
		 */

		List<Customer> entityList = customerDAO.findAll();
		for (Customer cus : entityList) {

			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(cus.getOperator().getOperatorID());
			operatorDTO.setOperatorName(cus.getOperator().getOperatorName());

			AreaDTO areaDTO = new AreaDTO();
			areaDTO.setAreaID(cus.getArea().getAreaID());
			areaDTO.setAreaName(cus.getArea().getAreaName());

			customerList.add(new CustomerDTO(cus.getCustomerID(), cus.getCustomerName(), cus.getNationalID(),
					cus.getDateOfBirth(), cus.getMobile(), cus.getVulveNumber(), cus.getBirthCertificate(),
					cus.getReligion(), cus.getGender(), cus.getMaritalStatus(), cus.getNationality(),
					cus.getBloodGroup(), areaDTO, operatorDTO, cus.isEnabled()));
		}
		entityList = null;
		return customerList;
	}

	public List<CustomerDTO> findCustomersByOperatorID(long operatorID) throws Exception {
		/*
		 * List<CustomerDTO> customerDTOs = new ArrayList<>(); List<Customer>
		 * customers = customerDAO.findCustomerByOperatorID(operatorID);
		 * customers.stream().forEach((cus) -> {
		 * customerDTOs.add(copyCustomerFrmEntity(cus)); }); return
		 * customerDTOs;
		 */

		List<CustomerDTO> customerList = new ArrayList<>();

		/*
		 * long customerID, String customerName, String nationalID, Date
		 * dateOfBirth, String mobile, String vulveNumber, String
		 * birthCertificate, String religion, String gender, String
		 * maritalStatus, String nationality, String bloodGroup, AreaDTO
		 * areaDTO, OperatorDTO operatorDTO, String recordNote, String
		 * userExecuted, Date dateExecuted, String ipExecuted, boolean enabled
		 */

		List<Customer> entityList = customerDAO.findCustomerByOperatorID(operatorID);
		for (Customer cus : entityList) {

			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(cus.getOperator().getOperatorID());
			operatorDTO.setOperatorName(cus.getOperator().getOperatorName());

			AreaDTO areaDTO = new AreaDTO();
			areaDTO.setAreaID(cus.getArea().getAreaID());
			areaDTO.setAreaName(cus.getArea().getAreaName());

			customerList.add(new CustomerDTO(cus.getCustomerID(), cus.getCustomerName(), cus.getNationalID(),
					cus.getDateOfBirth(), cus.getMobile(), cus.getVulveNumber(), cus.getBirthCertificate(),
					cus.getReligion(), cus.getGender(), cus.getMaritalStatus(), cus.getNationality(),
					cus.getBloodGroup(), areaDTO, operatorDTO, cus.isEnabled()));
		}
		entityList = null;
		return customerList;
	}

	public List<CustomerDTO> findLatestThreeCustomersByOperatorID(long cerOrOrgOrOpIDLogin) throws Exception {
		List<CustomerDTO> customerDTOs = new ArrayList<>();

		List<Customer> customers = customerDAO.findLatestThreeCustomersByOperatorID(cerOrOrgOrOpIDLogin);

		customers.stream().forEach((cus) -> {
			customerDTOs.add(copyCustomerFrmEntity(cus));
		});

		return customerDTOs;
	}

	@Transactional
	public void updateCustomer(CustomerDTO dto) throws Exception {
		/*
		 * Customer customer = new Customer(); customer =
		 * copyCustomerFrmDTO(dto); customerDAO.merge(customer); customer =
		 * null;
		 */

		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long operatorID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		Customer customer = new Customer();

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		Area area = new Area();
		area.setAreaID(dto.getAreaDTO().getAreaID());

		/*
		 * long customerID, String customerName, String nationalID, Date
		 * dateOfBirth, String mobile, String vulveNumber, String
		 * birthCertificate, String religion, String gender, String
		 * maritalStatus, String nationality, String bloodGroup, Operator
		 * operator, Area area, String recordNote, String userExecuted, Date
		 * dateExecuted, String ipExecuted, boolean enabled
		 */

		customer = new Customer(dto.getCustomerID(), dto.getCustomerName(), dto.getNationalID(), dto.getDateOfBirth(),
				dto.getMobile(), dto.getVulveNumber(), dto.getBirthCertificate(), dto.getReligion(), dto.getGender(),
				dto.getMaritalStatus(), dto.getNationality(), dto.getBloodGroup(), operator, area, "N/A", userName,
				date, ipAddress, dto.isEnabled());
		customerDAO.merge(customer);
		customer = null;
	}

	@Transactional
	public void deleteCustomer(CustomerDTO dto) throws Exception {
		customerDAO.remove(dto.getCustomerID());
	}

	// search
	public List<CustomerDTO> searchCustomerByName(String customerName) throws Exception {

		List<CustomerDTO> customerDTOs = new ArrayList<CustomerDTO>();
		List<Customer> customers = customerDAO.searchCustomerByName(customerName);

		customers.stream().forEach((cus) -> {
			customerDTOs.add(copyCustomerFrmEntity(cus));
		});
		
		return customerDTOs;

	}

	public List<CustomerDTO> seachCustomersByArea(long areaID) throws Exception {

		List<CustomerDTO> customerDTOs = new ArrayList<CustomerDTO>();
		List<Customer> customers = customerDAO.seachCustomersByArea(areaID);

		customers.stream().forEach((cus) -> {
			customerDTOs.add(copyCustomerFrmEntity(cus));
		});
		System.out.println("Customer DTO's Customer Size "+customers.size());
		
		return customerDTOs;
	}

	public CustomerDTO copyCustomerFrmEntity(Customer customer) {
		if (customer.getCustomerID() != 0) {
			CustomerDTO dto = new CustomerDTO();
			AreaDTO areaDTO = new AreaDTO();
			OperatorDTO operatorDTO = new OperatorDTO();
			BeanUtils.copyProperties(customer, dto);
			BeanUtils.copyProperties(customer.getArea(), areaDTO);
			BeanUtils.copyProperties(customer.getOperator(), operatorDTO);
			dto.setAreaDTO(areaDTO);
			dto.setOperatorDTO(operatorDTO);
			
			
			return dto;
		}
		return null;
	}

	public Customer copyCustomerFrmDTO(CustomerDTO customerDTO) {
		if (customerDTO.getCustomerID() != 0 || customerDTO.getCustomerName() == null) {
			Customer customer = new Customer();
			BeanUtils.copyProperties(customerDTO, customer);
			return customer;
		}
		return null;
	}

}
