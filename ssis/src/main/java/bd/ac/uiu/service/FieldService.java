package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.api.sms.ShortMessageSystem;
import bd.ac.uiu.dao.CustomerDAO;
import bd.ac.uiu.dao.FieldDAO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.FieldTypeDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.SoilDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.FieldType;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Soil;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class FieldService {

	@ManagedProperty("#{fieldTypeService}")
	private FieldTypeService fieldTypeService;

	@ManagedProperty("#{SoilService}")
	private SoilService soilService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@Autowired
	private FieldDAO fieldDAO;

	@Autowired
	private CustomerDAO customerDAO;

	@Transactional
	public boolean saveField(FieldDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		Soil soil = new Soil();
		soil.setSoilID(dto.getSoilDTO().getSoilID());

		long operatorID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Operator op = new Operator();
		op.setOperatorID(operatorID);

		Customer customer = new Customer();
		customer = customerDAO.findCustomerDetailByOperatorIDAndCustomerID(operatorID,
				dto.getCustomerDTO().getCustomerID());

		Ipcu ipcu = new Ipcu();
		ipcu.setIpcuID(dto.getIpcuDTO().getIpcuID());

		FieldType fieldType = new FieldType();
		fieldType.setFieldTypeID(dto.getFieldTypeDTO().getFieldTypeID());

		String reference = getReference(operatorID, dto.getCustomerDTO().getCustomerID());

		Field field = new Field(dto.getFieldMeasurement(), fieldType, ipcu, op, customer, soil, "N/A", userName,
				new Date(), ipAddress, true);

		field.setReference(reference);

		this.fieldDAO.persist(field);

		ShortMessageSystem sms = new ShortMessageSystem();
		System.out.println("Get Customer Number " + dto.getCustomerDTO().getMobile());
		sms.customerFieldNumberViaSMS(customer.getMobile(), reference);

		return true;
	}

	public List<FieldDTO> findFieldList() throws Exception {
		List<FieldDTO> fieldList = new ArrayList<>();

		List<Field> entityList = fieldDAO.findAll();

		for (Field e : entityList) {

			FieldDTO dto = new FieldDTO();
			dto.setFieldMeasurement(e.getFieldMeasurement());
			dto.setFieldID(e.getFieldID());
			dto.setReference(e.getReference());
			CustomerDTO customerDTO = new CustomerDTO();
			customerDTO.setCustomerName(e.getCustomer().getCustomerName());
			customerDTO.setCustomerID(e.getCustomer().getCustomerID());
			dto.setCustomerDTO(customerDTO);
			IpcuDTO ipcuDTO = new IpcuDTO();
			ipcuDTO.setIpcuID(e.getIpcu().getIpcuID());
			dto.setIpcuDTO(ipcuDTO);
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(e.getOperator().getOperatorID());
			operatorDTO.setOperatorName(e.getOperator().getOperatorName());
			dto.setOperatorDTO(operatorDTO);
			FieldTypeDTO fieldTypeDTO = new FieldTypeDTO();
			fieldTypeDTO.setFieldTypeID(e.getFieldType().getFieldTypeID());
			fieldTypeDTO.setFieldTypeName(e.getFieldType().getFieldTypeName());
			dto.setFieldTypeDTO(fieldTypeDTO);
			SoilDTO soilDTO = new SoilDTO();
			soilDTO.setSoilID(e.getSoil().getSoilID());
			soilDTO.setSoilType(e.getSoil().getSoilType());
			dto.setSoilDTO(soilDTO);

			fieldList.add(dto);
		}

		entityList = null;

		return fieldList;
	}

	@Transactional
	public void updateField(FieldDTO dto) throws Exception {
		Field field = new Field();
		field = copyFieldFrmDTO(dto);

		System.out.println("field update :" + field.getOperator().getOperatorID());

		fieldDAO.merge(field);
		field = null;

	}

	@Transactional
	public void deleteField(FieldDTO dto) throws Exception {
		fieldDAO.remove(dto.getFieldID());
	}

	public List<FieldDTO> findFieldsByOperatorID(long operatorID) throws Exception {

		List<FieldDTO> fieldDTOs = new ArrayList<>();
		List<Field> fields = fieldDAO.findFieldsByOperator(operatorID);
		fields.stream().forEach((fi) -> {
			fieldDTOs.add(copyFieldFrmEntity(fi));
		});

		return fieldDTOs;
	}

	public FieldDTO copyFieldFrmEntity(Field field) {
		if (field.getFieldID() != 0) {
			FieldDTO fieldDTO = new FieldDTO();
			BeanUtils.copyProperties(field, fieldDTO);

			CustomerDTO customerDTO = new CustomerDTO();
			OperatorDTO operatorDTO = new OperatorDTO();
			IpcuDTO ipcuDTO = new IpcuDTO();
			SoilDTO soilDTO = new SoilDTO();
			FieldTypeDTO fieldTypeDTO = new FieldTypeDTO();

			customerDTO.setCustomerID(field.getCustomer().getCustomerID());
			customerDTO.setCustomerName(field.getCustomer().getCustomerName());

			BeanUtils.copyProperties(field.getIpcu(), ipcuDTO);

			operatorDTO.setOperatorID(field.getOperator().getOperatorID());
			operatorDTO.setOperatorName(field.getOperator().getOperatorName());

			BeanUtils.copyProperties(field.getSoil(), soilDTO);
			BeanUtils.copyProperties(field.getFieldType(), fieldTypeDTO);

			fieldDTO.setCustomerDTO(customerDTO);
			fieldDTO.setOperatorDTO(operatorDTO);
			fieldDTO.setIpcuDTO(ipcuDTO);
			fieldDTO.setSoilDTO(soilDTO);
			fieldDTO.setFieldTypeDTO(fieldTypeDTO);

			return fieldDTO;
		}
		return null;
	}

	public Field copyFieldFrmDTO(FieldDTO fieldDTO) {
		if (fieldDTO.getFieldID() != 0) {
			Field field = new Field();
			Customer customer = new Customer();
			Operator operator = new Operator();
			Ipcu ipcu = new Ipcu();
			FieldType fieldType = new FieldType();
			Soil soil = new Soil();

			BeanUtils.copyProperties(fieldDTO, field);
			System.out.println("Field ID " + field.getFieldID());
			BeanUtils.copyProperties(fieldDTO.getCustomerDTO(), customer);
			System.out.println("CustomerID " + customer.getCustomerID());
			BeanUtils.copyProperties(fieldDTO.getOperatorDTO(), operator);
			System.out.println("OperatorID " + operator.getOperatorID());

			BeanUtils.copyProperties(fieldDTO.getIpcuDTO(), ipcu);

			ipcu.setIpcuID(fieldDTO.getIpcuDTO().getIpcuID());
			System.out.println("ipcu ID " + ipcu.getIpcuID());

			// BeanUtils.copyProperties(fieldDTO.getFieldTypeDTO(), fieldType);
			fieldType.setFieldTypeID(fieldDTO.getFieldTypeDTO().getFieldTypeID());
			System.out.println("FieldTypeID" + fieldType.getFieldTypeID());
			BeanUtils.copyProperties(fieldDTO.getSoilDTO(), soil);
			System.out.println("SoilID" + soil.getSoilID());

			field.setCustomer(customer);
			field.setOperator(operator);
			field.setIpcu(ipcu);
			field.setFieldType(fieldType);
			field.setSoil(soil);
			System.out.println("field update :" + field.getOperator().getOperatorID());
			return field;
		}
		return null;
	}

	public String getReference(long operatorID, long customerID) {

		String reference = null;

		String str = Integer.toString((int) customerID);
		if (str.length() == 1) {
			str = "00" + str;
		}
		if (str.length() == 2) {
			str = "0" + str;
		}
		String str1 = Integer.toString((int) operatorID);

		if (str1.length() == 1) {
			str1 = "00" + str1;
		}
		if (str1.length() == 2) {
			str1 = "0" + str1;
		}

		reference = str + str1;
		return reference;
	}

}
