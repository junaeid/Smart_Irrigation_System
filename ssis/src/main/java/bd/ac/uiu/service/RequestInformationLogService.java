package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.RequestInformationLogDAO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.RequestInformationLogDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.RequestInformationLog;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class RequestInformationLogService {

	public RequestInformationLogService() {
	}

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@ManagedProperty("#{fieldService}")
	private FieldService fieldService;

	@Autowired
	private RequestInformationLogDAO requestInformationLogDAO;

	@Transactional
	public boolean saveRequestInformationLog(RequestInformationLogDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		//String userName = ApplicationUtility.getUserID();

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		Customer customer = new Customer();
		customer.setCustomerID(dto.getCustomerDTO().getCustomerID());

		Field field = new Field();
		field.setFieldID(dto.getFieldDTO().getFieldID());

		Ipcu ipcu = new Ipcu();
		ipcu.setIpcuID(dto.getIpcuDTO().getIpcuID());

		RequestInformationLog requestInformationLog = new RequestInformationLog(dto.getSoilMoisture(),
				dto.getMotoIPCU(), dto.getWaterLevelSensor(), dto.getFieldValve(), dto.getWaterCredit(),
				dto.getFlowRate(), dto.getTemperature(), dto.getHumidity(), dto.getDeviceStatus(), dto.getNodeID(),
				dto.getSeason(), field, operator, ipcu, customer, dto.getMainValve(), "N/A", "N/A", new Date(),
				ipAddress, true);
		try {
			RequestInformationLog reInformationLog = new RequestInformationLog();
			reInformationLog = requestInformationLogDAO
					.checkOperatorIDPumpIDCustomerIDAndFieldIDExistInRequestInformation(
							dto.getOperatorDTO().getOperatorID(), dto.getIpcuDTO().getIpcuID(),
							dto.getCustomerDTO().getCustomerID(), dto.getFieldDTO().getFieldID());

			if (reInformationLog == null) {
				requestInformationLogDAO.persist(requestInformationLog);
				return true;
			} else {
				requestInformationLogDAO.updateRequestInformation(dto.getSoilMoisture(), dto.getMotoIPCU(),
						dto.getWaterLevelSensor(), dto.getFieldValve(), dto.getWaterCredit(), dto.getFlowRate(),
						dto.getTemperature(), dto.getHumidity(), dto.getDeviceStatus(), dto.getNodeID(),
						dto.getSeason(), dto.getFieldDTO().getFieldID(), dto.getOperatorDTO().getOperatorID(),
						dto.getIpcuDTO().getIpcuID(), dto.getCustomerDTO().getCustomerID(), dto.getMainValve());
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<RequestInformationLogDTO> findRequestInformationLogList() throws Exception {
		List<RequestInformationLogDTO> reqInfoLogList = new ArrayList<>();

		List<RequestInformationLog> informationLogs = requestInformationLogDAO.findAll();

		informationLogs.stream().forEach((ril) -> {

			RequestInformationLogDTO dto = copyRequestInformationLogFrmEntity(ril);
			reqInfoLogList.add(dto);
		});
		return reqInfoLogList;
	}

	public List<RequestInformationLogDTO> findRequestInformationLogDTOByOperatorID(long operatorID) throws Exception {
		List<RequestInformationLogDTO> dtos = new ArrayList<>();
		List<RequestInformationLog> informationLogs = requestInformationLogDAO
				.findRequestInformationLogByOperatorID(operatorID);
		informationLogs.stream().forEach((ril) -> {

			RequestInformationLogDTO dto = copyRequestInformationLogFrmEntity(ril);
			// dto.setCustomerDTO(customerService.copyCustomerFrmEntity(ril.getCustomer()));
			// dto.setFieldDTO(fieldService.copyFieldFrmEntity(ril.getField()));
			// dto.setIpcuDTO(ipcuService.copyIpcuFrmEntity(ril.getIpcu()));
			// dto.setOperatorDTO(operatorService.copyOperatorFrmEntity(ril.getOperator()));
			dtos.add(dto);
		});
		return dtos;
	}

	public List<RequestInformationLogDTO> findRequestInformationLogDTOByOperatorIDAndCustomerID(long operatorID,
			long customerID) throws Exception {
		List<RequestInformationLogDTO> dtos = new ArrayList<>();
		List<RequestInformationLog> informationLogs = requestInformationLogDAO
				.findRequestInformationLogByOperatorIDAndCustomerID(operatorID, customerID);
		informationLogs.stream().forEach((req) -> {
			dtos.add(copyRequestInformationLogFrmEntity(req));
		});

		return dtos;
	}

	@Transactional
	public void update(RequestInformationLogDTO dto) throws Exception {
		RequestInformationLog requestInformationLog = new RequestInformationLog();
		requestInformationLog = copyRequestInformationLogFrmDTO(dto);
		requestInformationLogDAO.merge(requestInformationLog);
		requestInformationLog = null;
	}

	@Transactional
	public void delete(RequestInformationLogDTO dto) throws Exception {
		requestInformationLogDAO.remove(dto.getRequestInformationLogID());
	}

	public RequestInformationLogDTO copyRequestInformationLogFrmEntity(RequestInformationLog rInformationLog) {
		if (rInformationLog.getRequestInformationLogID() != 0) {
			RequestInformationLogDTO reDto = new RequestInformationLogDTO();
			BeanUtils.copyProperties(rInformationLog, reDto);

			CustomerDTO customerDTO = new CustomerDTO();
			OperatorDTO operatorDTO = new OperatorDTO();
			IpcuDTO ipcuDTO = new IpcuDTO();
			FieldDTO fieldDTO = new FieldDTO();

			customerDTO.setCustomerID(rInformationLog.getCustomer().getCustomerID());
			customerDTO.setCustomerName(rInformationLog.getCustomer().getCustomerName());

			operatorDTO.setOperatorID(rInformationLog.getOperator().getOperatorID());
			operatorDTO.setOperatorName(rInformationLog.getOperator().getOperatorName());

			ipcuDTO.setIpcuID(rInformationLog.getIpcu().getIpcuID());

			fieldDTO.setFieldID(rInformationLog.getField().getFieldID());

			reDto.setCustomerDTO(customerDTO);
			reDto.setOperatorDTO(operatorDTO);
			reDto.setIpcuDTO(ipcuDTO);
			reDto.setFieldDTO(fieldDTO);

			return reDto;
		}
		return null;
	}

	public RequestInformationLog copyRequestInformationLogFrmDTO(RequestInformationLogDTO dto) {
		if (dto.getRequestInformationLogID() != 0) {
			RequestInformationLog log = new RequestInformationLog();
			BeanUtils.copyProperties(dto, log);
			return log;
		}
		return null;
	}

	public RequestInformationLogDAO getRequestInformationLogDAO() {
		return requestInformationLogDAO;
	}

	public void setRequestInformationLogDAO(RequestInformationLogDAO requestInformationLogDAO) {
		this.requestInformationLogDAO = requestInformationLogDAO;
	}

}
