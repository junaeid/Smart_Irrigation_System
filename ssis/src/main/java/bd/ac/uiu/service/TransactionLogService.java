package bd.ac.uiu.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

import org.exolab.castor.xml.handlers.ValueOfFieldHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.api.sms.ShortMessageSystem;
import bd.ac.uiu.customClass.TotalTrancastionByOperator;
import bd.ac.uiu.dao.CustomerDAO;
import bd.ac.uiu.dao.OperatorDAO;
import bd.ac.uiu.dao.PaymentSummaryDAO;
import bd.ac.uiu.dao.TransactionLogDAO;
import bd.ac.uiu.dao.TransactionManualDAO;
import bd.ac.uiu.dao.WaterCreditLogDAO;
import bd.ac.uiu.dao.WaterCreditSummaryDAO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.TransactionLogDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.PaymentSummary;
import bd.ac.uiu.entity.TransactionLog;
import bd.ac.uiu.entity.TransactionManual;
import bd.ac.uiu.entity.WaterCreditLog;
import bd.ac.uiu.entity.WaterCreditSummary;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class TransactionLogService {

	@Autowired
	private TransactionLogDAO transactionDAO;

	@Autowired
	private PaymentSummaryDAO paymentSummaryDAO;

	@Autowired
	private WaterCreditSummaryDAO waterCreditSummaryDAO;

	@Autowired
	private WaterCreditLogDAO waterCreditLogDAO;

	@Autowired
	private CustomerDAO customerDAO;

	@Autowired
	private OperatorDAO operatorDAO;

	@Autowired
	private TransactionManualDAO transactionManualDAO;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	public TransactionLogService() {
	}

	@Transactional
	public boolean saveTransaction(TransactionLogDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String username = ApplicationUtility.getUserID();
		Date date = new Date();
		String month = new SimpleDateFormat("MMMM").format(date);
		String year = new SimpleDateFormat("YYYY").format(date);

		DateFormat paymentDate = new SimpleDateFormat("yyyy-DD-mm");
		String paymentDateStr = new SimpleDateFormat("yyyy-DD-mm").format(date);
		Date pDate = paymentDate.parse(paymentDateStr);
		dto.setPaymentDate(pDate);

		// TransactionManual transactionManual = new TransactionManual();
		// transactionManual =
		// transactionManualDAO.findLatestTransactionManual();

		/*
		 * double perUnitCal = transactionManual.getUnitPrice() /
		 * transactionManual.getAmountOfWater();
		 * 
		 * double res = ((double) dto.getAmount() * perUnitCal) *
		 * transactionManual.getUnitPrice();
		 */

		// double res = (double)
		// dto.getAmount()/transactionManual.getUnitPrice();

		// System.out.println("res : " + res);

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		String reference = getReference(operator.getOperatorID(), dto.getCustomerDTO().getCustomerID());

		Customer customer = new Customer();
		customer = customerDAO.findCustomerDetailByOperatorIDAndCustomerID(operator.getOperatorID(),
				dto.getCustomerDTO().getCustomerID());
		System.out.println("Cus in Service" + dto.getCustomerDTO().getCustomerID());
		System.out.println("Cus in Service" + operator.getOperatorID());

		// For first TransactionLog table
		try {
			TransactionLog transactionLog = new TransactionLog(dto.getPaymentTypeName(), dto.getPaymentDate(),
					dto.getDebitAmount(), 0.0, year, month, customer, operator, "N/A", "N/A", date, ipAddress, true);

			transactionLog.setCounter(dto.getCounter());
			transactionLog.setTrxStatus(dto.getTrxStatus());
			transactionLog.setTrxId(dto.getTrxId());
			transactionLog.setReference(dto.getReference());
			transactionLog.setSender(dto.getSender());
			transactionLog.setService(dto.getService());
			transactionLog.setCurrency(dto.getCurrency());
			transactionLog.setReceiver(dto.getReceiver());
			transactionLog.setTrxTimeStamp(dto.getTrxTimeStamp());
			dto.setNoOfUnit((int) dto.getDebitAmount());

			System.out.println(dto.getNoOfUnit());

			this.transactionDAO.persist(transactionLog);

			// For second TransactionLog table
			PaymentSummary paymentSummary = new PaymentSummary(dto.getAmount(), 0.0, dto.getAmount(), operator,
					customer, "N/A", username, date, ipAddress, true);

			PaymentSummary summ = new PaymentSummary();
			summ = paymentSummaryDAO.checkCustomerIDExistInPaymentSummary(dto.getCustomerDTO().getCustomerID());
			if (summ == null) {
				paymentSummaryDAO.persist(paymentSummary);
			} else {
				paymentSummaryDAO.updateDepositPaymentSummary(dto.getCustomerDTO().getCustomerID(), dto.getAmount());
			}
			// For third WaterCreditSummary table

			WaterCreditSummary waterCreditSummaryNew = new WaterCreditSummary(0, dto.getNoOfUnit(), customer, operator,
					"N/A", username, new Date(), ipAddress, true);

			WaterCreditSummary waSummary = new WaterCreditSummary();
			waSummary = waterCreditSummaryDAO
					.checkCustomerIDExistInWaterCreditSummary(dto.getCustomerDTO().getCustomerID());
			if (waSummary == null) {
				waterCreditSummaryDAO.persist(waterCreditSummaryNew);
				paymentSummaryDAO.updateExpensePaymentSummary(dto.getCustomerDTO().getCustomerID(), dto.getAmount());
				this.transactionDAO.persist(transactionLog);
			} else {
				waterCreditSummaryDAO.updateAvailableWaterCreditAdd(dto.getCustomerDTO().getCustomerID(),
						dto.getNoOfUnit());
				waterCreditSummaryDAO.updateWaterCreditEnableStatus(dto.getOperatorDTO().getOperatorID(),
						dto.getCustomerDTO().getCustomerID(), true);
				paymentSummaryDAO.updateExpensePaymentSummary(dto.getCustomerDTO().getCustomerID(), dto.getAmount());
			}

			// For fourth WaterCreditSummary table
			WaterCreditLog waternew = new WaterCreditLog(0, dto.getNoOfUnit(), date, year, month, 0, 0, customer,
					operator, "N/A", username, date, ipAddress, true);

			waterCreditLogDAO.persist(waternew);

			ShortMessageSystem shortMessageSystem = new ShortMessageSystem();
			shortMessageSystem.successfullyPayment(customer.getMobile(), dto.getDebitAmount(), reference);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean saveTransactionFromField(TransactionLogDTO dto) throws Exception {
		/*
		 * this.paymentTypeName = paymentTypeName; this.paymentDate =
		 * paymentDate; this.debitAmount = debitAmount; this.creditAmount =
		 * creditAmount; this.year = year; this.month = month; this.customer =
		 * customer; this.operator = operator; this.recordNote = recordNote;
		 * this.userExecuted = userExecuted; this.dateExecuted = dateExecuted;
		 * this.ipExecuted = ipExecuted; this.enabled = enabled;
		 */

		
		String ipAddress = ApplicationUtility.getIpAddress();
		String username = ApplicationUtility.getUserID();
		Date date = new Date();
		String month = new SimpleDateFormat("MMMM").format(date);
		String year = new SimpleDateFormat("YYYY").format(date);

		DateFormat paymentDate = new SimpleDateFormat("yyyy-DD-mm");
		String paymentDateStr = new SimpleDateFormat("yyyy-DD-mm").format(date);
		Date pDate = paymentDate.parse(paymentDateStr);
		dto.setPaymentDate(pDate);

		long opID = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		Operator operator = new Operator();
		operator.setOperatorID(opID);
		Customer customer = new Customer();
		customer.setCustomerID(dto.getCustomerDTO().getCustomerID());
		
		String reference = getReference(operator.getOperatorID(), dto.getCustomerDTO().getCustomerID());

				
		try {
			TransactionLog transactionLog = new TransactionLog(dto.getPaymentTypeName(), dto.getPaymentDate(),
					dto.getDebitAmount(), 0.0, year, month, customer, operator, "N/A", "N/A", date, ipAddress, true);

			transactionLog.setCounter(dto.getCounter());
			transactionLog.setTrxStatus(dto.getTrxStatus());
			transactionLog.setTrxId(dto.getTrxId());
			transactionLog.setReference(dto.getReference());
			transactionLog.setSender(dto.getSender());
			transactionLog.setService(dto.getService());
			transactionLog.setCurrency(dto.getCurrency());
			transactionLog.setReceiver(dto.getReceiver());
			transactionLog.setTrxTimeStamp(dto.getTrxTimeStamp());
			dto.setNoOfUnit((int) dto.getDebitAmount());

			//this.transactionDAO.persist(transactionLog);

			// For second TransactionLog table
			PaymentSummary paymentSummary = new PaymentSummary(dto.getAmount(), 0.0, dto.getAmount(), operator,
					customer, "N/A", username, date, ipAddress, true);

			PaymentSummary summ = new PaymentSummary();
			summ = paymentSummaryDAO.checkCustomerIDExistInPaymentSummary(dto.getCustomerDTO().getCustomerID());
			if (summ == null) {
				paymentSummaryDAO.persist(paymentSummary);
			} else {
				paymentSummaryDAO.updateDepositPaymentSummary(dto.getCustomerDTO().getCustomerID(), dto.getAmount());
			}
			// For third WaterCreditSummary table

			WaterCreditSummary waterCreditSummaryNew = new WaterCreditSummary(0, dto.getNoOfUnit(), customer, operator,
					"N/A", username, new Date(), ipAddress, true);

			WaterCreditSummary waSummary = new WaterCreditSummary();
			waSummary = waterCreditSummaryDAO
					.checkCustomerIDExistInWaterCreditSummary(dto.getCustomerDTO().getCustomerID());
			if (waSummary == null) {
				waterCreditSummaryDAO.persist(waterCreditSummaryNew);
				paymentSummaryDAO.updateExpensePaymentSummary(dto.getCustomerDTO().getCustomerID(), dto.getAmount());
				this.transactionDAO.persist(transactionLog);
			} else {
				waterCreditSummaryDAO.updateAvailableWaterCreditAdd(dto.getCustomerDTO().getCustomerID(),
						dto.getNoOfUnit());
				waterCreditSummaryDAO.updateWaterCreditEnableStatus(dto.getOperatorDTO().getOperatorID(),
						dto.getCustomerDTO().getCustomerID(), true);
				paymentSummaryDAO.updateExpensePaymentSummary(dto.getCustomerDTO().getCustomerID(), dto.getAmount());
			}

			// For fourth WaterCreditSummary table
			WaterCreditLog waternew = new WaterCreditLog(0, dto.getNoOfUnit(), date, year, month, 0, 0, customer,
					operator, "N/A", username, date, ipAddress, true);

			waterCreditLogDAO.persist(waternew);

			//ShortMessageSystem shortMessageSystem = new ShortMessageSystem();
			//shortMessageSystem.successfullyPayment(customer.getMobile(), dto.getDebitAmount(), reference);

		} catch (Exception e) {
			e.printStackTrace();
		}

		

		return true;
	}

	public boolean paymentStatusExistOrNot(long operatorID, long customerID, String trxTimeStamp) {
		TransactionLog transactionLog = new TransactionLog();
		try {
			transactionLog = transactionDAO.duplicateOrNot(operatorID, customerID);
			if (transactionLog.getTrxTimeStamp().equals(trxTimeStamp)) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return true;
		}

	}

	@Transactional
	public void updateTransaction(TransactionLogDTO dto) throws Exception {

	}

	@Transactional
	public void deleteTransaction(TransactionLogDTO dto) throws Exception {

	}

	public List<TransactionLogDTO> findTransactions() throws Exception {
		List<TransactionLogDTO> dtos = new ArrayList<>();
		List<TransactionLog> transactions = transactionDAO.findAllForCerAndOrg();

		/*
		 * long transationLogID, String paymentTypeName, Date paymentDate,
		 * double amount, double debitAmount, double creditAmount, CustomerDTO
		 * customerDTO, OperatorDTO operatorDTO, String recordNote, String
		 * userExecuted, Date dateExecuted, String ipExecuted, boolean enabled
		 */

		for (TransactionLog e : transactions) {

			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(e.getOperator().getOperatorID());
			operatorDTO.setOperatorName(e.getOperator().getOperatorName());

			CustomerDTO customerDTO = new CustomerDTO();
			customerDTO.setCustomerID(e.getCustomer().getCustomerID());
			customerDTO.setCustomerName(e.getCustomer().getCustomerName());

			dtos.add(new TransactionLogDTO(e.getTransationLogID(), e.getPaymentTypeName(), e.getPaymentDate(),
					e.getDebitAmount(), e.getCreditAmount(), customerDTO, operatorDTO, e.getRecordNote(),
					e.getUserExecuted(), e.getDateExecuted(), e.getIpExecuted(), e.isEnabled()));

		}

		return dtos;
	}

	public List<TotalTrancastionByOperator> perOperatorTotalTransaction() throws Exception {
		List<TransactionLogDTO> dtos = new ArrayList<>();
		List<TotalTrancastionByOperator> totalTrancastionByOperators = new ArrayList<>();
		dtos = findTransactions();

		List<Operator> operators = new ArrayList<>();

		operators = operatorDAO.findAll();

		System.out.println("Operator size :" + operators.size());

		for (Operator e : operators) {
			TotalTrancastionByOperator totalTrancastionByOperator = new TotalTrancastionByOperator();
			double totalAmount = 0.0;
			long counter = 0;

			totalTrancastionByOperator.setOperatorID(e.getOperatorID());
			totalTrancastionByOperator.setOperatorName(e.getOperatorName());
			for (TransactionLogDTO t : dtos) {
				if (t.getOperatorDTO().getOperatorID() == totalTrancastionByOperator.getOperatorID()) {
					counter++;
					totalAmount += t.getDebitAmount();

				}
			}

			totalTrancastionByOperator.setNoOfOperator(counter);
			totalTrancastionByOperator.setTotalAmountPerOperator(totalAmount);
			totalTrancastionByOperators.add(totalTrancastionByOperator);

			System.out.println("Counter :" + counter);

		}
		System.out.println("Total transaction :" + totalTrancastionByOperators.size());

		return totalTrancastionByOperators;

	}

	public TotalTrancastionByOperator perOperatorTotalTransactionByDateWiseAndOperatorWise(Date startDate, Date endDate,
			long operatorID) throws Exception {
		List<TransactionLogDTO> dtos = new ArrayList<>();
		dtos = findTransactions();

		TotalTrancastionByOperator totalTrancastionByOperator = new TotalTrancastionByOperator();
		double totalAmount = 0.0;
		long counter = 0;

		totalTrancastionByOperator.setOperatorID(operatorID);
		totalTrancastionByOperator.setOperatorName("N/A");
		for (TransactionLogDTO t : dtos) {
			if (t.getOperatorDTO().getOperatorID() == totalTrancastionByOperator.getOperatorID()
					&& (((startDate.compareTo(t.getDateExecuted()) <= 0)
							|| (endDate.compareTo(t.getDateExecuted()) >= 0)))) {
				counter++;
				totalAmount += t.getDebitAmount();

			}
		}

		totalTrancastionByOperator.setNoOfOperator(counter);
		totalTrancastionByOperator.setTotalAmountPerOperator(totalAmount);

		System.out.println("Counter :" + counter);

		return totalTrancastionByOperator;

	}

	public TransactionLogDTO copyTransactionFrmEntity(TransactionLog transaction) {
		if (transaction.getTransationLogID() != 0) {
			TransactionLogDTO dto = new TransactionLogDTO();
			BeanUtils.copyProperties(transaction, dto);

			CustomerDTO customerDTO = new CustomerDTO();
			OperatorDTO operatorDTO = new OperatorDTO();

			customerDTO.setCustomerID(transaction.getCustomer().getCustomerID());
			customerDTO.setCustomerName(transaction.getCustomer().getCustomerName());

			operatorDTO.setOperatorID(transaction.getOperator().getOperatorID());
			operatorDTO.setOperatorName(transaction.getOperator().getOperatorName());

			dto.setCustomerDTO(customerDTO);
			dto.setOperatorDTO(operatorDTO);

			return dto;
		}
		return null;
	}

	public List<TransactionLogDTO> findTransactionLogByOperatorID(long operatorID) throws Exception {
		List<TransactionLogDTO> transactionDTOs = new ArrayList<>();
		List<TransactionLog> transactions = transactionDAO.findAllTransactionLogByOperatorID(operatorID);
		transactions.stream().forEach((trans) -> {
			transactionDTOs.add(copyTransactionFrmEntity(trans));
		});

		return transactionDTOs;
	}

	public List<TransactionLogDTO> sumOfTransactionByDate(String date) throws Exception {

		List<TransactionLogDTO> dtos = new ArrayList<>();
		List<TransactionLog> logs = transactionDAO.sumOfTransactionByDate("");

		for (TransactionLog e : logs) {
			TransactionLogDTO transactionLogDTO = new TransactionLogDTO();

			transactionLogDTO.setDebitAmount(e.getDebitAmount());
			dtos.add(transactionLogDTO);

		}

		return dtos;
		/*
		 * List<TransactionLogDTO> transactionDTOs = new ArrayList<>();
		 * List<TransactionLog> transactions =
		 * transactionDAO.sumOfTransactionByDate(date);
		 * transactions.stream().forEach((trans) -> {
		 * transactionDTOs.add(copyTransactionFrmEntity(trans)); });
		 * System.out.println("Transaction Log " + transactions.size()); return
		 * transactionDTOs;
		 */
	}

	public TransactionLog copyTransactionFrmDTO(TransactionLogDTO transactionDTO) {
		if (transactionDTO.getTransationLogID() != 0 || transactionDTO.getPaymentTypeName() == null) {
			TransactionLog transaction = new TransactionLog();
			BeanUtils.copyProperties(transactionDTO, transaction);
			return transaction;
		}
		return null;
	}

	public double findTotalTransactionByOpID(long operatorID, String date) throws Exception {

		double totalTransaction = transactionDAO.findTotalTransactionByOpID(operatorID, date);
		System.out.println("transaction Total " + totalTransaction);
		return totalTransaction;
	}

	public double findMaxTransactionByOpID(long operatorID, String date) throws Exception {
		double maxTransaction = transactionDAO.findMaxTransactionByOpID(operatorID, date);
		System.out.println("transaction Max " + maxTransaction);
		return maxTransaction;
	}

	public double findMinTransactionByOpID(long operatorID, String date) throws Exception {
		double minTransaction = transactionDAO.findMinTransactionByOpID(operatorID, date);
		System.out.println("transaction min " + minTransaction);
		return minTransaction;
	}

	public double findAvgTransactionByOpID(long operatorID, String date) throws Exception {
		double avgTransaction = transactionDAO.findAvgTransactionByOpID(operatorID, date);
		System.out.println("transaction min " + avgTransaction);
		return avgTransaction;
	}

	public double findTotalTransOrg(String date) throws Exception {

		double totalTransaction = transactionDAO.findTotalTransOrg(date);
		System.out.println("transaction Total " + totalTransaction);
		return totalTransaction;
	}

	public double findMaxTransOrg(String date) throws Exception {
		double maxTransaction = transactionDAO.findMaxTransOrg(date);
		System.out.println("transaction Max " + maxTransaction);
		return maxTransaction;
	}

	public double findMinTransOrg(String date) throws Exception {
		double minTransaction = transactionDAO.findMinTransOrg(date);
		System.out.println("transaction min " + minTransaction);
		return minTransaction;
	}

	public double findAvgTransOrg(String date) throws Exception {
		double avgTransaction = transactionDAO.findAvgTransOrg(date);
		System.out.println("transaction min " + avgTransaction);
		return avgTransaction;
	}

	public String getReference(long operatorID, long customerID) {

		char[] customCustomerID = { '0', '0', '0' };
		char[] customOperatorID = { '0', '0', '0' };

		String finalCustID = new String();
		String finalOperID = new String();

		String reference;

		String str = Integer.toString((int) customerID);
		for (int i = str.length() - 1; i >= 0; i--) {
			System.out.println(str.charAt(i));
			customCustomerID[i] = str.charAt(i);
		}

		String str1 = Integer.toString((int) operatorID);
		for (int i = str1.length() - 1; i >= 0; i--) {
			System.out.println(str1.charAt(i));
			customOperatorID[i] = str1.charAt(i);
		}

		for (int i = customCustomerID.length - 1; i >= 0; i--) {
			finalCustID += customCustomerID[i];
		}

		for (int i = customOperatorID.length - 1; i >= 0; i--) {
			finalOperID += customOperatorID[i];
		}

		reference = finalOperID + finalCustID;

		System.out.println(reference);

		return reference;
	}

	public List<TransactionLogDTO> findTransactionLogListFromDateBetween(Date startDate, Date endDate, long operatorID)
			throws Exception {
		List<TransactionLog> transactionLogs = transactionDAO.findTransactionDateBetween(startDate, endDate,
				operatorID);
		List<TransactionLogDTO> transactionLogDTOs = new ArrayList<>();
		transactionLogs.forEach((log) -> {
			TransactionLogDTO transactionLogDTO = copyTransactionFrmEntity(log);
			transactionLogDTOs.add(transactionLogDTO);
		});
		return transactionLogDTOs;
	}

	public List<TransactionLogDTO> dateWiseTransactionLogList(long operatorID, Date fromDate, Date toDate)
			throws Exception {
		List<TransactionLog> list = transactionDAO.dateWiseTransactionLogList(operatorID, fromDate, toDate);
		List<TransactionLogDTO> transactionLogDTOs = new ArrayList<>();
		list.forEach((log) -> {
			TransactionLogDTO transactionLogDTO = copyTransactionFrmEntity(log);
			transactionLogDTOs.add(transactionLogDTO);
		});
		return transactionLogDTOs;

	}

}
