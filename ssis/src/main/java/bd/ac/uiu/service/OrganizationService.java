package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.dao.OrganizationDAO;
import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class OrganizationService {

	@ManagedProperty("#{cerService}")
	private CerService cerService;

	@Autowired
	private OrganizationDAO organizationDAO;

	@Transactional
	public void saveOrganization(OrganizationDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		Cer cer = new Cer();
		cer.setCerID(dto.getCerDTO().getCerID());
		System.out.println("cer id in service: " + cer.getCerID());

		Organization organization = new Organization(dto.getOrganizationName(), dto.getMobile(), dto.getEmail(),
				dto.getPresentAddress(), dto.getPermanentAddress(), dto.getTinCerNumber(), dto.getEstablishDate(), "",
				"", "", "", dto.getRegistrationNumber(), cer, "N/A", userName, new Date(), ipAddress, true);

		this.organizationDAO.persist(organization);
		organization = null;

	}

	public List<OrganizationDTO> findAllOrganization() throws Exception {
		List<OrganizationDTO> list = new ArrayList<>();
		List<Organization> entityList = organizationDAO.findAll();
		for (Organization orga : entityList) {
			CerDTO cerDTO = new CerDTO();
			cerDTO.setCerID(orga.getCer().getCerID());
			cerDTO.setCerName(orga.getCer().getCerName());
			list.add(new OrganizationDTO(orga.getOrganizationID(), orga.getOrganizationName(), orga.getMobile(),
					orga.getEmail(), cerDTO));
		}
		entityList = null;
		return list;
	}

	// long organizationID,String organizationName, String mobile, String email,
	// String presentAddress,
	// String permanentAddress, String tinCerNumber, Date establishDate, String
	// imageName, String sizeOfImage,
	// String imagePath, String imageTitle, String registrationNumber, String
	// recordNote,
	// String userExecuted, Date dateExecuted, String ipExecuted, boolean
	// enabled

	@Transactional
	public void updateOrganization(OrganizationDTO dto) throws Exception {
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		
		Cer cer = new Cer();
		cer.setCerID(cerOrOrgOrOpIDLogin);
		
		
		Organization organization = new Organization(dto.getOrganizationID(), dto.getOrganizationName(),
				dto.getMobile(), dto.getEmail(), dto.getPresentAddress(), dto.getPermanentAddress(),
				dto.getTinCerNumber(), dto.getEstablishDate(), dto.getImageName(), dto.getSizeOfImage(),
				dto.getImagePath(), dto.getImageTitle(), dto.getRegistrationNumber(), dto.getRecordNote(),
				userName, date, ipAddress, true,cer);

		organizationDAO.merge(organization);
		organization = null;
	}

	public OrganizationDTO copyOrganizationFrmEntity(Organization organization) {
		if (organization.getOrganizationID() != 0) {
			OrganizationDTO dto = new OrganizationDTO();
			BeanUtils.copyProperties(organization, dto);
			return dto;
		}
		return null;

	}

	public Organization copyOrganizationFrmDTO(OrganizationDTO organizationDTO) {
		if (organizationDTO.getOrganizationID() != 0 || organizationDTO.getOrganizationName() != null) {
			Organization organization = new Organization();
			BeanUtils.copyProperties(organizationDTO, organization);
			return organization;
		}
		return null;

	}

	@Transactional
	public void delete(OrganizationDTO organizationDTO) throws Exception {

		organizationDAO.remove(organizationDTO);
	}

}
