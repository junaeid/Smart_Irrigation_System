package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.SensorDAO;
import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.SensorDTO;
import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Sensor;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class SensorService {

	@Autowired
	private SensorDAO sensorDAO;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{ipcuService}")
	private IpcuService ipcuService;

	@ManagedProperty("#{organizationService}")
	private OrganizationService organizationService;

	@Transactional
	public boolean saveSensor(SensorDTO dto) throws Exception {

		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		Ipcu ipcu = new Ipcu();
		ipcu.setIpcuID(dto.getIpcuDTO().getIpcuID());

		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Operator operator = new Operator();
		operator.setOperatorID(cerOrOrgOrOpIDLogin);

		// String sensorName, String sensorCode, Ipcu ipcu, Operator operator,
		// String recordNote,
		// String userExecuted, Date dateExecuted, String ipExecuted, boolean
		// enabled

		Sensor sensor = new Sensor(dto.getSensorName(), dto.getSensorCode(), ipcu, operator, dto.getRecordNote(),
				userName, new Date(), ipAddress, true);

		this.sensorDAO.persist(sensor);

		return true;
	}

	public List<SensorDTO> findSensorList() throws Exception {
		List<SensorDTO> sensorList = new ArrayList<>();

		List<Sensor> sensors = sensorDAO.findAll();

		sensors.stream().forEach((se) -> {
			SensorDTO dto = copySensorFrmEntity(se);

			sensorList.add(dto);
		});

		return sensorList;
	}

	@Transactional
	public void updateSensor(SensorDTO dto) throws Exception {

		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		Ipcu ipcu = new Ipcu();
		ipcu.setIpcuID(dto.getIpcuDTO().getIpcuID());

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		Sensor sensor = new Sensor(dto.getSensorID(), dto.getSensorName(), dto.getSensorCode(), operator, "N/A",
				userName, new Date(), ipAddress, true, ipcu);

		sensorDAO.merge(sensor);
		sensor = null;
	}

	@Transactional
	public void deleteSensor(SensorDTO dto) throws Exception {
		sensorDAO.remove(dto.getSensorID());
	}

	public List<SensorDTO> findSensorListByOperatorID(long operatorID) throws Exception {
		List<SensorDTO> sensorList = new ArrayList<>();

		List<Sensor> sensors = sensorDAO.findSensorsByOperator(operatorID);
		System.out.println(" sensors size at service: " + sensors.size());

		sensors.stream().forEach((se) -> {
			SensorDTO dto = copySensorFrmEntity(se);
			sensorList.add(dto);
		});

		return sensorList;
	}

	public Sensor copySensorFrmSensorDTO(SensorDTO sensorDTO) {
		if (sensorDTO.getSensorID() != 0 || sensorDTO.getSensorName() != null) {
			Sensor sensor = new Sensor();

			Operator operator = new Operator();
			Ipcu ipcu = new Ipcu();

			BeanUtils.copyProperties(sensorDTO, sensor);

			operator.setOperatorID(sensor.getOperator().getOperatorID());
			operator.setOperatorName(sensor.getOperator().getOperatorName());

			ipcu.setIpcuID(sensor.getIpcu().getIpcuID());

			sensor.setOperator(operator);
			sensor.setIpcu(ipcu);

			return sensor;
		}
		return null;
	}

	public SensorDTO copySensorFrmEntity(Sensor sensor) {
		if (sensor.getSensorID() != 0) {
			SensorDTO sensorDTO = new SensorDTO();
			BeanUtils.copyProperties(sensor, sensorDTO);

			OperatorDTO operatorDTO = new OperatorDTO();
			IpcuDTO ipcuDTO = new IpcuDTO();

			operatorDTO.setOperatorID(sensor.getOperator().getOperatorID());
			operatorDTO.setOperatorName(sensor.getOperator().getOperatorName());

			ipcuDTO.setIpcuID(sensor.getIpcu().getIpcuID());

			sensorDTO.setOperatorDTO(operatorDTO);
			sensorDTO.setIpcuDTO(ipcuDTO);

			return sensorDTO;
		}
		return null;
	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public IpcuService getIpcuService() {
		return ipcuService;
	}

	public void setIpcuService(IpcuService ipcuService) {
		this.ipcuService = ipcuService;
	}

	public OrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

}
