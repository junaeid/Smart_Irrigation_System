package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.OperatorDAO;
import bd.ac.uiu.dto.CerDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class OperatorService {

	@Autowired
	private OperatorDAO operatorDAO;

	@ManagedProperty("#{organizationService}")
	private OrganizationService organizationService;

	public OperatorService() {

	}

	@Transactional
	public boolean saveOperator(OperatorDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Organization organization = new Organization();
		organization.setOrganizationID(cerOrOrgOrOpIDLogin);

		Operator operator = new Operator(dto.getOperatorName(), dto.getMobile(), dto.getEmail(),
				dto.getPresentAddress(), dto.getPermanentAddress(), dto.getVoterID(), dto.getPassport(),
				dto.getBirthCerNumber(), dto.getTinCerNumber(), dto.getEstablishDate(), null, null, null, null,
				dto.getRegistrationNumber(), organization, "N/A", userName, new Date(), ipAddress, true);
		if (operator != null) {
			this.operatorDAO.persist(operator);
			return true;
		}

		return false;
	}

	/*
	 * 
	 * long operatorID, String operatorName, String mobile, String email, String
	 * presentAddress, String permanentAddress, String voterID, String passport,
	 * String birthCerNumber, String tinCerNumber, Date establishDate, String
	 * registrationNumber, OrganizationDTO organizationDTO, boolean enabled
	 */
	public List<OperatorDTO> findAllOperatorList() throws Exception {
		List<OperatorDTO> operatorDTOs = new ArrayList<>();
		List<Operator> operators = operatorDAO.findAll();

		for (Operator op : operators) {
			OrganizationDTO organizationDTO = new OrganizationDTO();
			organizationDTO.setOrganizationID(op.getOrganization().getOrganizationID());
			organizationDTO.setOrganizationName(op.getOrganization().getOrganizationName());

			CerDTO cerDTO = new CerDTO();
			cerDTO.setCerName(op.getOrganization().getCer().getCerName());

			organizationDTO.setCerDTO(cerDTO);

			OperatorDTO dto = new OperatorDTO(op.getOperatorID(), op.getOperatorName(), op.getMobile(), op.getEmail(),
					op.getPresentAddress(), op.getPermanentAddress(), op.getVoterID(), op.getPassport(),
					op.getBirthCerNumber(), op.getTinCerNumber(), op.getEstablishDate(), op.getRegistrationNumber(),
					organizationDTO, op.isEnabled());

			operatorDTOs.add(dto);
		}

		return operatorDTOs;
	}
	
	public long operatorMaxID() throws Exception{
		return operatorDAO.operatorMaxID();
	}

	public List<OperatorDTO> findAllOperatorsByOrganizationID(long organizationID) throws Exception {
		List<OperatorDTO> operatorDTOs = new ArrayList<>();
		List<Operator> operators = operatorDAO.findAllOperatorsByOragnization(organizationID);
		operators.stream().forEach((op) -> {
			operatorDTOs.add(copyOperatorFrmEntity(op));
		});
		return operatorDTOs;
	}
	
	public OperatorDTO findOperatorByOperatorID(long operatorID) throws Exception{
		Operator operator = operatorDAO.findOperatorByOperatorID(operatorID);
		OperatorDTO operatorDTO = new OperatorDTO();
		operatorDTO = copyOperatorFrmEntity(operator);
		return operatorDTO;
	}

	public OperatorDTO copyOperatorFrmEntity(Operator operator) {
		if (operator.getOperatorID() != 0) {
			OperatorDTO dto = new OperatorDTO();
			OrganizationDTO organizationDTO = new OrganizationDTO();
			BeanUtils.copyProperties(operator, dto);
			BeanUtils.copyProperties(operator.getOrganization(), organizationDTO);
			dto.setOrganizationDTO(organizationDTO);
			return dto;
		}
		return null;

	}

	public Operator copyOperatorFrmDTO(OperatorDTO operatorDTO) {
		if (operatorDTO.getOperatorID() != 0 || operatorDTO.getOperatorName() != null) {
			Operator operator = new Operator();
			Organization organization = new Organization();
			BeanUtils.copyProperties(operatorDTO, operator);
			BeanUtils.copyProperties(operatorDTO.getOrganizationDTO(), organization);
			operator.setOrganization(organization);
			return operator;
		}
		return null;

	}

	@Transactional
	public void updateOperator(OperatorDTO dto) throws Exception {
		Operator operator = new Operator();
		operator = copyOperatorFrmDTO(dto);
		operatorDAO.merge(operator);
		operator=null;
	}

	@Transactional
	public void deleteOperator(OperatorDTO dto) throws Exception {
		operatorDAO.remove(dto.getOperatorID());
	}
}
