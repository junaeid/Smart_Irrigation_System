package bd.ac.uiu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.CustomerDAO;
import bd.ac.uiu.dao.OperatorDAO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;

@Service
public class HomeDashBoardService {
	@Autowired
	private CustomerDAO customerDAO;
	@Autowired
	private OperatorDAO operatorDAO;

	public int findNumberOfCustomer() throws Exception {
		List<Customer> customers = customerDAO.findAll();
		int noOfcustomer = customers.size();
		return noOfcustomer;
	}
	public int findNumberOfOperators() throws Exception {
		List<Operator> operators = operatorDAO.findAll();
		int noOfoperator = operators.size();
		return noOfoperator;
	}
}
