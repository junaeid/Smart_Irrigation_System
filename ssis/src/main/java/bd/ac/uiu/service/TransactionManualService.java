package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.TransactionManualDAO;
import bd.ac.uiu.dto.TransactionManualDTO;
import bd.ac.uiu.entity.TransactionManual;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class TransactionManualService {

	@Autowired
	private TransactionManualDAO transactionManualDAO;

	public TransactionManualService() {
	}

	@Transactional
	public boolean saveTransactionManual(TransactionManualDTO dto) throws Exception {
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		TransactionManual transactionManual = new TransactionManual();
		transactionManual.setUnitPrice(dto.getUnitPrice());
		transactionManual.setAmountOfWater(dto.getAmountOfWater());
		transactionManual.setWaterCredit(dto.getWaterCredit());
		transactionManual.setDateExecuted(new Date());
		transactionManual.setRecordNote("N/A");
		transactionManual.setUserExecuted(userName);
		transactionManual.setIpExecuted(ipAddress);
		transactionManual.setEnabled(true);
		this.transactionManualDAO.persist(transactionManual);
		return true;
	}

	public List<TransactionManualDTO> findTransactionManualList() throws Exception {
		List<TransactionManualDTO> transactionManualDTOs = new ArrayList<>();
		List<TransactionManual> transactionManuals = transactionManualDAO.findAll();

		for (TransactionManual dtos : transactionManuals) {
			TransactionManualDTO transactionManualDTO = new TransactionManualDTO();

			transactionManualDTO.setUnitPrice(dtos.getUnitPrice());
			transactionManualDTO.setAmountOfWater(dtos.getAmountOfWater());
			transactionManualDTO.setWaterCredit(dtos.getWaterCredit());
			transactionManualDTO.setDateExecuted(dtos.getDateExecuted());
			transactionManualDTO.setRecordNote(dtos.getRecordNote());
			transactionManualDTO.setUserExecuted(dtos.getUserExecuted());
			transactionManualDTO.setIpExecuted(dtos.getIpExecuted());
			transactionManualDTO.setEnabled(dtos.isEnabled());

			transactionManualDTOs.add(transactionManualDTO);
		}

		return transactionManualDTOs;
	}

	public void updateTransactionManual(TransactionManualDTO dto) throws Exception {
		TransactionManual transactionManual = new TransactionManual();

		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		transactionManual.setTransactionManualID(dto.getTransactionManualID());
		transactionManual.setUnitPrice(dto.getUnitPrice());
		transactionManual.setAmountOfWater(dto.getAmountOfWater());
		transactionManual.setWaterCredit(dto.getWaterCredit());
		transactionManual.setDateExecuted(date);
		transactionManual.setRecordNote(dto.getRecordNote());
		transactionManual.setUserExecuted(userName);
		transactionManual.setIpExecuted(ipAddress);
		transactionManual.setEnabled(true);

		transactionManualDAO.merge(transactionManual);
		transactionManual = null;

	}

}
