package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.ContactPersonDAO;
import bd.ac.uiu.dto.ContactPersonDTO;
import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.entity.ContactPerson;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class ContactPersonService {

	@Autowired
	private ContactPersonDAO contactPersonDAO;

	private ContactPersonDTO contactPersonDTO;

	// @ManagedProperty("#{cerService}")
	// private CerService cerService;
	// @ManagedProperty("#{organizationService}")
	// private OrganizationService organizationService;
	// @ManagedProperty("#{operatorService}")
	// private OperatorService operatorService;

	public ContactPersonService() {

	}

	@Transactional
	public boolean saveContactPerson(ContactPersonDTO dto) throws Exception {
		// Incomplete
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		Cer cer = new Cer();
		Organization organization = new Organization();
		Operator operator = new Operator();

		cer.setCerID(dto.getCerDTO().getCerID());
		organization.setOrganizationID(dto.getOrganizationDTO().getOrganizationID());
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		ContactPerson contactPerson = new ContactPerson(dto.getContactPersonName(), "N/A", userName, new Date(),
				ipAddress, true, dto.getUserType());

		ContactPerson contactPersonCer = new ContactPerson(dto.getContactPersonName(), cer, "N/A", userName, new Date(),
				ipAddress, true, dto.getUserType());

		ContactPerson contactPersonOrg = new ContactPerson(dto.getContactPersonName(), organization, "N/A", userName,
				new Date(), ipAddress, true, dto.getUserType());

		ContactPerson contactPersonOp = new ContactPerson(dto.getContactPersonName(), operator, "N/A", userName,
				new Date(), ipAddress, true, dto.getUserType());

		switch (dto.getUserType()) {
		case "cer":
			this.contactPersonDAO.persist(contactPersonCer);
			break;
		case "organization":
			this.contactPersonDAO.persist(contactPersonOrg);
			break;
		case "operator":
			this.contactPersonDAO.persist(contactPersonOp);
			break;
		default:
			this.contactPersonDAO.persist(contactPerson);
		}

		return true;
	}

	public List<ContactPersonDTO> findContactPersonList() throws Exception {
		List<ContactPersonDTO> contactPersonList = new ArrayList<>();

		List<ContactPerson> entityList = contactPersonDAO.findAll();

		for (ContactPerson e : entityList) {
			contactPersonList.add(new ContactPersonDTO(e.getContactPersonID(), e.getContactPersonName(),
					e.getRecordNote(), e.getUserExecuted(), e.getDateExecuted(), e.getIpExecuted(), e.isEnabled()));
		}
		entityList = null;
		return contactPersonList;
	}

	@Transactional
	public void updateContactPerson(ContactPersonDTO dto) throws Exception {
		ContactPerson contactPerson = new ContactPerson();
		
	}

	@Transactional
	public void deleteContactPerson(ContactPersonDTO dto) throws Exception {
		contactPersonDAO.remove(dto.getContactPersonID());
	}

}
