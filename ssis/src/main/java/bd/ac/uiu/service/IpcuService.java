package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.IpcuDAO;
import bd.ac.uiu.dto.AreaDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.entity.Area;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class IpcuService {

	@Autowired
	private IpcuDAO ipcuDAO;

	@ManagedProperty("#{areaService}")
	private AreaService areaService;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	public IpcuService() {

	}
	
	public int findMaxCustomID(long operatorID) throws Exception{
		List<Ipcu> ipcus = ipcuDAO.findIpcusByOperator(operatorID);
		
		int maxID =0;
		for(Ipcu ipcu : ipcus){
			if(ipcu.getIpcuCustomID()==null){
				maxID =1;
			}
			else if(ipcu.getIpcuCustomID()>maxID){
				maxID=ipcu.getIpcuCustomID();
			}
		}
		
		return maxID;
		
	}
	
	@Transactional
	public boolean saveIPCU(IpcuDTO dto) throws DataIntegrityViolationException, Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		Area area = new Area();
		area.setAreaID(dto.getAreaDTO().getAreaID());

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		
		int ipcuCustomID = findMaxCustomID(dto.getOperatorDTO().getOperatorID());
		
		
		if(ipcuCustomID!=1){
			ipcuCustomID+=1;
		}
		
		
		Ipcu ipcu = new Ipcu(dto.getIpcuWattCapacity(), dto.getIpcuDepth(), dto.getWaterDynamicHead(),
				dto.getDistanceFromCanel(), area, operator, "N/A", userName, new Date(), ipAddress, true);
		
		ipcu.setIpcuCustomID(ipcuCustomID);

		this.ipcuDAO.persist(ipcu);
		return true;

	}

	public List<IpcuDTO> findIPCUList() throws Exception {
		List<IpcuDTO> ipcuDTOs = new ArrayList<>();
		List<Ipcu> ipcus = ipcuDAO.findAll();

		for (Ipcu ipcu : ipcus) {
			AreaDTO areaDTO = new AreaDTO();
			areaDTO.setAreaName(ipcu.getArea().getAreaName());

			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(ipcu.getOperator().getOperatorID());
			operatorDTO.setOperatorName(ipcu.getOperator().getOperatorName());

			IpcuDTO dto = new IpcuDTO(ipcu.getIpcuID(), ipcu.getIpcuWattCapacity(), ipcu.getIpcuDepth(),
					ipcu.getWaterDynamicHead(), ipcu.getDistanceFromCanel(), areaDTO, operatorDTO, true);

			ipcuDTOs.add(dto);
		}

		return ipcuDTOs;
	}
	
	public List<IpcuDTO> findIPCUsByAreaID(long areaID) throws Exception{
		List<IpcuDTO> ipcuDTOs = new ArrayList<>();
		List<Ipcu> ipcus = ipcuDAO.findIpcusByAreaIDAndOperatorID(areaID);
		
		ipcus.stream().forEach((ip) -> {
			ipcuDTOs.add(copyIpcuFrmEntity(ip));
		});
		
		return ipcuDTOs;
	}

	@Transactional
	public void updateIPCU(IpcuDTO dto) throws Exception {
		Ipcu ipcu = new Ipcu();
		ipcu = copyIpcuFrmDTO(dto);
		ipcuDAO.merge(ipcu);
		ipcu = null;
	}

	@Transactional
	public void deleteIPCU(IpcuDTO dto) throws Exception {
		ipcuDAO.remove(dto.getIpcuID());
	}

	public List<IpcuDTO> findIpcusByOperatorID(long operatorID) throws Exception {
		List<IpcuDTO> ipcuDTOs = new ArrayList<>();
		List<Ipcu> ipcus = ipcuDAO.findIpcusByOperator(operatorID);
		ipcus.stream().forEach((ip) -> {
			ipcuDTOs.add(copyIpcuFrmEntity(ip));
		});

		return ipcuDTOs;
	}

	public IpcuDTO findIpcuByOperatorAndIpcu(long operatorID, long ipcuID) throws Exception {
		Ipcu ipcu = ipcuDAO.findIpcuByOperatorAndIpcu(operatorID, ipcuID);
		IpcuDTO ipcuDTO = copyIpcuFrmEntity(ipcu);

		return ipcuDTO;
	}

	public IpcuDTO copyIpcuFrmEntity(Ipcu ipcu) {
		if (ipcu.getIpcuID() != 0) {
			IpcuDTO ipcuDTO = new IpcuDTO();
			BeanUtils.copyProperties(ipcu, ipcuDTO);

			AreaDTO areaDTO = new AreaDTO();
			areaDTO.setAreaID(ipcu.getArea().getAreaID());
			areaDTO.setAreaName(ipcu.getArea().getAreaName());
			
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(ipcu.getOperator().getOperatorID());
			operatorDTO.setOperatorName(ipcu.getOperator().getOperatorName());

			ipcuDTO.setAreaDTO(areaDTO);
			ipcuDTO.setOperatorDTO(operatorDTO);

			return ipcuDTO;
		}
		return null;
	}

	public Ipcu copyIpcuFrmDTO(IpcuDTO ipcuDTO) {
		if (ipcuDTO.getIpcuID() != 0) {
			Ipcu ipcu = new Ipcu();
			Area area = new Area();
			Operator operator = new Operator();
			BeanUtils.copyProperties(ipcuDTO, ipcu);
			BeanUtils.copyProperties(ipcuDTO.getAreaDTO(), area);
			BeanUtils.copyProperties(ipcuDTO.getOperatorDTO(), operator);
			/*area.setAreaID(ipcuDTO.getAreaDTO().getAreaID());
			area.setAreaName(ipcuDTO.getAreaDTO().getAreaName());
	
			
			operator.setOperatorID(ipcuDTO.getOperatorDTO().getOperatorID());
			operator.setOperatorName(ipcuDTO.getOperatorDTO().getOperatorName());*/
			ipcu.setArea(area);
			ipcu.setOperator(operator);
			return ipcu;
		}
		return null;
	}
}
