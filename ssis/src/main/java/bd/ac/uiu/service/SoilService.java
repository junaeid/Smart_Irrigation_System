package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.SoilDAO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.SoilDTO;
import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Soil;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class SoilService {

	public SoilService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private SoilDAO soilDAO;

	public boolean checkSoil(SoilDTO soilDTO) {

		if (soilDAO.checkSoil(soilDTO.getSoilType()) == null) {
			System.out.println("Soil not exist.");
			return true;
		}

		return false;
	}

	@Transactional
	public boolean saveSoil(SoilDTO dto) throws Exception {

		Date date = new Date();

		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());
		Operator op = new Operator();
		op.setOperatorID(cerOrOrgOrOpIDLogin);

		Soil soil;

		// String soilType, String geographicalInformation, String
		// fieldDescription, Field field,
		// Operator operator, String recordNote, String userExecuted, Date
		// dateExecuted, String ipExecuted,
		// boolean enabled

		soil = new Soil(dto.getSoilType(), dto.getGeographicalInformation(), dto.getFieldDescription(), op,
				dto.getRecordNote(), userName, date, ipAddress, true);

		this.soilDAO.persist(soil);

		return true;
	}

	public List<SoilDTO> findSoilList() throws Exception {
		List<SoilDTO> soilList = new ArrayList<>();

		List<Soil> entityList = soilDAO.findAll();
		
		
		for (Soil s : entityList) {
			
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(s.getOperator().getOperatorID());
			operatorDTO.setOperatorName(s.getOperator().getOperatorName());
			
			soilList.add(new SoilDTO(s.getSoilID(), s.getSoilType(), s.getGeographicalInformation(),
					s.getFieldDescription(), s.getRecordNote(), s.getUserExecuted(), s.getDateExecuted(),
					s.getIpExecuted(), s.isEnabled(),operatorDTO));
			
		}
		entityList = null;
		return soilList;
	}

	public List<SoilDTO> findSoilByOperator(long operatorID) throws Exception {
		List<SoilDTO> soilDTOs = new ArrayList<>();
		List<Soil> soils = soilDAO.findSoilByOperator(operatorID);

		for (Soil s : soils) {

			SoilDTO soilDTO = new SoilDTO();
			OperatorDTO operatorDTO = new OperatorDTO();
			operatorDTO.setOperatorID(s.getOperator().getOperatorID());
			operatorDTO.setOperatorName(s.getOperator().getOperatorName());
			BeanUtils.copyProperties(s, soilDTO);
			//soilDTO.setSoilID(s.getSoilId());
			soilDTO.setOperatorDTO(operatorDTO);
			soilDTOs.add(soilDTO);
		}
		soils=null;
		
		return soilDTOs;
	}

	@Transactional
	public void updateSoil(SoilDTO dto) throws Exception {
		Soil soil = new Soil();
		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());
		soil = new Soil(dto.getSoilID(), dto.getSoilType(), dto.getGeographicalInformation(), dto.getFieldDescription(),
				dto.getRecordNote(), dto.getUserExecuted(), dto.getDateExecuted(), dto.getIpExecuted(), dto.isEnabled(),
				operator);

		soilDAO.merge(soil);
		soil = null;
	}

	@Transactional
	public void deleteSoil(SoilDTO dto) throws Exception {
		soilDAO.remove(dto.getSoilID());
	}
}
