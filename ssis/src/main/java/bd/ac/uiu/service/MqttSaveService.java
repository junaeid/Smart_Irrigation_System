package bd.ac.uiu.service;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.MqttSaveDAO;
import bd.ac.uiu.decesion.FieldStatusObj;
import bd.ac.uiu.entity.MqttSave;

@Service
public class MqttSaveService {

	@Autowired
	private MqttSaveDAO mqttSaveDAO;
	
	public boolean saveMqttData(FieldStatusObj fieldStatusObj) throws MqttException{
		
		MqttSave mqttSave=new MqttSave(fieldStatusObj.getTemperature());
		if(mqttSave != null ){
			
			try {
				mqttSaveDAO.persist(mqttSave);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
}
