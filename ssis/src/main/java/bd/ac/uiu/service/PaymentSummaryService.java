package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.PaymentSummaryDAO;
import bd.ac.uiu.dto.OrganizationDTO;
import bd.ac.uiu.dto.PaymentSummaryDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.entity.PaymentSummary;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class PaymentSummaryService {

	public PaymentSummaryService() {
	}

	@Autowired
	private PaymentSummaryDAO paymentSummaryDAO;

	@ManagedProperty("#{operatorService}")
	private OperatorService operatorService;

	@ManagedProperty("#{customerService}")
	private CustomerService customerService;

	@Transactional
	public boolean savePaymentSummary(PaymentSummaryDTO dto) {
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		Operator operator = new Operator();
		operator.setOperatorID(cerOrOrgOrOpIDLogin);

		Customer customer = new Customer();
		customer.setCustomerID(2);

		PaymentSummary paymentSummary = new PaymentSummary(100.0, 0.0, 100.0, operator, customer, "N/A", userName,
				new Date(), ipAddress, true);
		try {
			PaymentSummary summ = new PaymentSummary();
			summ = paymentSummaryDAO.checkCustomerIDExistInPaymentSummary(2);
			if (summ == null) {
				paymentSummaryDAO.persist(paymentSummary);
				return true;
			} else {

				paymentSummaryDAO.updateDepositPaymentSummary(2, 50.0);
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	public java.util.List<PaymentSummaryDTO> findList() throws Exception {
		List<PaymentSummaryDTO> PaymentSummaryList = new ArrayList<>();

		List<PaymentSummary> entityList = paymentSummaryDAO.findAll();
		for (PaymentSummary e : entityList) {

		}

		entityList = null;
		return PaymentSummaryList;
	}

	@Transactional
	public void updatePaymentSummary(PaymentSummaryDTO dto) throws Exception {
		PaymentSummary paymentSummary = new PaymentSummary();
		paymentSummary = copyPaymentSummaryFrmDTO(dto);
		paymentSummaryDAO.merge(paymentSummary);
		paymentSummary = null;
	}

	@Transactional
	public void deletePaymentSummary(PaymentSummaryDTO dto) throws Exception {
		paymentSummaryDAO.remove(dto.getPaymentSummaryId());
	}

	public PaymentSummaryDTO copyPaymentSummaryFrmEntity(PaymentSummary paymentSummary) {
		if (paymentSummary.getPaymentSummaryId() != 0) {
			PaymentSummaryDTO dto = new PaymentSummaryDTO();
			BeanUtils.copyProperties(paymentSummary, dto);
			return dto;
		}
		return null;

	}

	public PaymentSummary copyPaymentSummaryFrmDTO(PaymentSummaryDTO paymentSummaryDTO) {
		if (paymentSummaryDTO.getPaymentSummaryId() != 0) {
			PaymentSummary entity = new PaymentSummary();
			BeanUtils.copyProperties(paymentSummaryDTO, entity);
			return entity;
		}
		return null;

	}

	public OperatorService getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(OperatorService operatorService) {
		this.operatorService = operatorService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}
