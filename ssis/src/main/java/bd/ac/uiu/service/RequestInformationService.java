package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.RequestInformationDAO;
import bd.ac.uiu.dto.RequestInformationDTO;
import bd.ac.uiu.entity.RequestInformation;
import bd.ac.uiu.entity.Users;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class RequestInformationService {

	public RequestInformationService() {
	}

	@Autowired
	private RequestInformationDAO requestInformationDAO;

	@Transactional
	public boolean saveRequestInformation(RequestInformationDTO dto) throws Exception {

		String ipAddress = ApplicationUtility.getIpAddress();
		//String userName = ApplicationUtility.getUserID();

		RequestInformation requestInformation;
		requestInformation = new RequestInformation(dto.getOperatorID(), dto.getIpcuID(), dto.getCustomerID(),
				dto.getFieldID(), dto.getSoilMoisture(), dto.getMotorIpcu(), dto.getWaterLevelSensor(),
				dto.getFieldValve(), dto.getMainValve(), dto.getWaterCredit(), dto.getFlowRate(), dto.getTemperature(),
				dto.getHumidity(), dto.getDeviceStatus(), dto.getNodeID(), dto.getSeason(), "N/A", "N/A", new Date(),
				ipAddress, true);

		this.requestInformationDAO.persist(requestInformation);

		return true;
	}

	public List<RequestInformationDTO> findRequestInformationList() throws Exception {
		List<RequestInformationDTO> requestInformationList = new ArrayList<>();

		List<RequestInformation> entityList = requestInformationDAO.findAllForCERAndOrg();

		entityList.stream().forEach((ri) -> {
			requestInformationList.add(copyRequestInformationFrmEntity(ri));
		});

		entityList = null;
		return requestInformationList;
	}

	public List<RequestInformationDTO> findRequestInformationByOperatorID(long operatorID) throws Exception {
		List<RequestInformationDTO> dtos = new ArrayList<>();
		List<RequestInformation> requestInformations = requestInformationDAO
				.findRequestInformationByOperatorID(operatorID);
		requestInformations.stream().forEach((ri) -> {
			dtos.add(copyRequestInformationFrmEntity(ri));
		});

		return dtos;
	}

	public List<RequestInformationDTO> findRequestInfoByOpAndCustID(long operatorID, long customerID) throws Exception {
		List<RequestInformationDTO> dtos = new ArrayList<>();
		List<RequestInformation> informations = requestInformationDAO
				.findRequestInformationByOperatorIDAndCustomerID(operatorID, customerID);
		informations.stream().forEach((ri) -> {
			dtos.add(copyRequestInformationFrmEntity(ri));
		});

		return dtos;
	}

	@Transactional
	public void update(RequestInformationDTO dto) throws Exception {
		RequestInformation requestInformation = new RequestInformation();
		requestInformation = copyRequestInformationFrmDTO(dto);
		requestInformationDAO.merge(requestInformation);
		requestInformation = null;
	}

	@Transactional
	public void delete(RequestInformationDTO dto) throws Exception {
		requestInformationDAO.remove(dto.getRequestInformationID());
	}

	public RequestInformationDTO copyRequestInformationFrmEntity(RequestInformation information) {
		if (information.getRequestInformationID() != 0) {
			RequestInformationDTO requestInformationDTO = new RequestInformationDTO();
			BeanUtils.copyProperties(information, requestInformationDTO);
			return requestInformationDTO;
		}
		return null;
	}

	public RequestInformation copyRequestInformationFrmDTO(RequestInformationDTO requestInformationDTO) {
		if (requestInformationDTO.getRequestInformationID() != 0) {
			RequestInformation requestInformation = new RequestInformation();
			BeanUtils.copyProperties(requestInformationDTO, requestInformation);
			return requestInformation;
		}
		return null;
	}

}
