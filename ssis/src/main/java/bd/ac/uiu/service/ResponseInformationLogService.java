package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.ResponseInformationLogDAO;
import bd.ac.uiu.dto.CustomerDTO;
import bd.ac.uiu.dto.FieldDTO;
import bd.ac.uiu.dto.IpcuDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.dto.ResponseInformationLogDTO;
import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.entity.Operator;

import bd.ac.uiu.entity.ResponseInformationLog;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class ResponseInformationLogService {

	public ResponseInformationLogService() {
	}

	@Autowired
	private ResponseInformationLogDAO responseInformationLogDAO;

	@Transactional
	public boolean saveResponseInformationLog(ResponseInformationLogDTO dto) throws Exception {
		String ipAddress = ApplicationUtility.getIpAddress();
		//String userName = ApplicationUtility.getUserID();

		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());

		Customer customer = new Customer();
		customer.setCustomerID(dto.getCustomerDTO().getCustomerID());

		Field field = new Field();
		field.setFieldID(dto.getFieldDTO().getFieldID());

		Ipcu ipcu = new Ipcu();
		ipcu.setIpcuID(dto.getIpcuDTO().getIpcuID());

		ResponseInformationLog responseInformationLog = new ResponseInformationLog(dto.getFieldValve(),
				dto.getSensorStatus(), dto.getNodeID(), dto.getBalance(), dto.getMainValve(), field, operator, ipcu,
				customer, "N/A", "N/A", new Date(), ipAddress, true);

		try {
			ResponseInformationLog reInformationLog = new ResponseInformationLog();
			reInformationLog = responseInformationLogDAO
					.checkOperatorIDPumpIDCustomerIDAndFieldIDExistInResponseInformation(
							dto.getOperatorDTO().getOperatorID(), dto.getIpcuDTO().getIpcuID(),
							dto.getCustomerDTO().getCustomerID(), dto.getFieldDTO().getFieldID());
			if (reInformationLog == null) {
				responseInformationLogDAO.persist(responseInformationLog);
				return true;
			} else {
				responseInformationLogDAO.updateResponseInformation(dto.getFieldValve(), dto.getSensorStatus(),
						dto.getNodeID(), dto.getBalance(), dto.getMainValve(), dto.getFieldDTO().getFieldID(),
						dto.getOperatorDTO().getOperatorID(), dto.getIpcuDTO().getIpcuID(),
						dto.getCustomerDTO().getCustomerID());
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<ResponseInformationLogDTO> findResponseInformationLogList() throws Exception {
		List<ResponseInformationLogDTO> responseInformationLogDTOs = new ArrayList<>();

		List<ResponseInformationLog> responseInformationLogs = responseInformationLogDAO.findAll();
		responseInformationLogs.stream().forEach((resInfLog) -> {
			responseInformationLogDTOs.add(copyResponseInformationLogFrmEntity(resInfLog));
		});


		return responseInformationLogDTOs;
	}

	public List<ResponseInformationLogDTO> findResponseInformationListByOperatorIDAndIpcuID(long operatorID,
			long ipcuID) throws Exception {

		List<ResponseInformationLogDTO> responseInformationLogDTOs = new ArrayList<>();
		List<ResponseInformationLog> responseInformationLogs = responseInformationLogDAO
				.findResponseInformationListByOperatorIDAndIpcuID(operatorID, ipcuID);
		responseInformationLogs.stream().forEach((resInfLog) -> {
			responseInformationLogDTOs.add(copyResponseInformationLogFrmEntity(resInfLog));
		});

		return responseInformationLogDTOs;

	}

	@Transactional
	public void updateResponseInformationLog(ResponseInformationLogDTO dto) throws Exception {
		ResponseInformationLog responseInformationLog = new ResponseInformationLog();
		responseInformationLog = copyResponseInformationLogFrmDTO(dto);
		responseInformationLogDAO.merge(responseInformationLog);
		responseInformationLog = null;
	}

	@Transactional
	public void deleteResponseInformationLog(ResponseInformationLogDTO dto) throws Exception {
		responseInformationLogDAO.remove(dto.getResponseID());
	}

	public ResponseInformationLogDTO copyResponseInformationLogFrmEntity(
			ResponseInformationLog responseInformationLog) {
		if (responseInformationLog.getResponseID() != 0) {
			ResponseInformationLogDTO dto = new ResponseInformationLogDTO();
			BeanUtils.copyProperties(responseInformationLog, dto);

			CustomerDTO customerDTO = new CustomerDTO();
			OperatorDTO operatorDTO = new OperatorDTO();
			IpcuDTO ipcuDTO = new IpcuDTO();
			FieldDTO fieldDTO = new FieldDTO();

			customerDTO.setCustomerID(responseInformationLog.getCustomer().getCustomerID());
			customerDTO.setCustomerName(responseInformationLog.getCustomer().getCustomerName());

			operatorDTO.setOperatorID(responseInformationLog.getOperator().getOperatorID());
			operatorDTO.setOperatorName(responseInformationLog.getOperator().getOperatorName());

			ipcuDTO.setIpcuID(responseInformationLog.getIpcu().getIpcuID());

			fieldDTO.setFieldID(responseInformationLog.getField().getFieldID());

			dto.setCustomerDTO(customerDTO);
			dto.setOperatorDTO(operatorDTO);
			dto.setIpcuDTO(ipcuDTO);
			dto.setFieldDTO(fieldDTO);

			return dto;
		}
		return null;
	}

	public List<ResponseInformationLogDTO> findResponseInformationLogListByOperatorID(long operatorID)
			throws Exception {
		List<ResponseInformationLogDTO> responseInformationLogDTOs = new ArrayList<>();
		List<ResponseInformationLog> responseInformationLogs = responseInformationLogDAO
				.findRequestInformationLogByOperatorID(operatorID);
		responseInformationLogs.stream().forEach((resInfLog) -> {
			responseInformationLogDTOs.add(copyResponseInformationLogFrmEntity(resInfLog));
		});
		return responseInformationLogDTOs;
	}

	public List<ResponseInformationLogDTO> findResponseInformationLogListByOperatorIDAndCustomerID(long operatorID,
			long customerID) throws Exception {
		List<ResponseInformationLogDTO> dtos = new ArrayList<>();
		List<ResponseInformationLog> informationLogs = responseInformationLogDAO
				.findResquestInformationListByOperatorIDAndCustomerID(operatorID, customerID);
		informationLogs.stream().forEach((res) -> {
			dtos.add(copyResponseInformationLogFrmEntity(res));
		});
		return dtos;
	}

	public ResponseInformationLog copyResponseInformationLogFrmDTO(
			ResponseInformationLogDTO responseInformationLogDTO) {
		if (responseInformationLogDTO.getResponseID() != 0) {
			ResponseInformationLog entity = new ResponseInformationLog();
			BeanUtils.copyProperties(responseInformationLogDTO, entity);
			return entity;
		}
		return null;
	}

	public ResponseInformationLogDAO getResponseInformationLogDAO() {
		return responseInformationLogDAO;
	}

	public void setResponseInformationLogDAO(ResponseInformationLogDAO responseInformationLogDAO) {
		this.responseInformationLogDAO = responseInformationLogDAO;
	}

}
