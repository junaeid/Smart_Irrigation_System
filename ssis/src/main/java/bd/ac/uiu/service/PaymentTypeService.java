package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.PaymentTypeDAO;
import bd.ac.uiu.dto.PaymentTypeDTO;
import bd.ac.uiu.entity.PaymentType;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class PaymentTypeService {

	public PaymentTypeService() {
	}

	@Autowired
	private PaymentTypeDAO paymentTypeDAO;

	@Transactional
	public boolean savePaymentType(PaymentTypeDTO dto) throws Exception {

		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();

		PaymentType paymentType = new PaymentType(dto.getPaymentTypeName(), "N/A", userName, new Date(), ipAddress,
				true);
		
		this.paymentTypeDAO.persist(paymentType);

		return true;
	}

	public List<PaymentTypeDTO> findPaymentTypeList() throws Exception {
		List<PaymentTypeDTO> List = new ArrayList<>();

		List<PaymentType> entityList = paymentTypeDAO.findAll();
		for (PaymentType e : entityList) {

		}

		entityList = null;
		return List;
	}

	@Transactional
	public void updatePaymentType(PaymentTypeDTO dto) throws Exception {
		PaymentType paymentType = new PaymentType();
		paymentType = new PaymentType();
		paymentTypeDAO.merge(paymentType);
		paymentType = null;
	}

	@Transactional
	public void deletePaymentType(PaymentTypeDTO dto) throws Exception {
		paymentTypeDAO.remove(dto.getPaymentTypeId());
	}

}
