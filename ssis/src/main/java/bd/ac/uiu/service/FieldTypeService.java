package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bd.ac.uiu.dao.FieldTypeDAO;
import bd.ac.uiu.dto.FieldTypeDTO;
import bd.ac.uiu.dto.OperatorDTO;
import bd.ac.uiu.entity.FieldType;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.utility.ApplicationUtility;

@Service
public class FieldTypeService {

	@Autowired
	private FieldTypeDAO fieldTypeDAO;

	@Transactional
	public boolean saveFieldType(FieldTypeDTO dto) throws Exception {
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		
		long cerOrOrgOrOpIDLogin = Long.parseLong(ApplicationUtility.getCerOrOrgOrOpID());

		FieldType fieldType = new FieldType();
		Operator operator = new Operator();
		operator.setOperatorID(cerOrOrgOrOpIDLogin);

		fieldType = new FieldType(dto.getFieldTypeName(), dto.getRecordNote(), userName, date, ipAddress, true,operator);

		this.fieldTypeDAO.persist(fieldType);
		return true;
	}

	public List<FieldTypeDTO> findFIeldTypeList() throws Exception {
		List<FieldTypeDTO> fieldTypeList = new ArrayList<>();
		List<FieldType> entityList = fieldTypeDAO.findAll();
		for (FieldType e : entityList) {
			fieldTypeList.add(new FieldTypeDTO(e.getFieldTypeID(), e.getFieldTypeName(), e.getRecordNote(),
					e.getUserExecuted(), e.getDateExecuted(), e.getIpExecuted(), e.isEnabled()));
		}
		entityList = null;
		return fieldTypeList;
	}

	public List<FieldTypeDTO> findFieldTypeByOperator(long operatorID) throws Exception {
		List<FieldTypeDTO> fieldTypeDTOs = new ArrayList<>();
		List<FieldType> fieldTypes = fieldTypeDAO.findFieldTypeByOperatorID(operatorID);

		for (FieldType f : fieldTypes) {
			OperatorDTO operatorDTO = new OperatorDTO();
			FieldTypeDTO fieldTypeDTO = new FieldTypeDTO();
			BeanUtils.copyProperties(f, fieldTypeDTO);
			BeanUtils.copyProperties(f.getOperator(), operatorDTO);

			fieldTypeDTO.setOperatorDTO(operatorDTO);
			fieldTypeDTOs.add(fieldTypeDTO);
		}

		return fieldTypeDTOs;
	}

	@Transactional
	public void updateFieldType(FieldTypeDTO dto) throws Exception {
		FieldType fieldType;
		Operator operator = new Operator();
		operator.setOperatorID(dto.getOperatorDTO().getOperatorID());
		
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		
		fieldType = new FieldType(dto.getFieldTypeID(), dto.getFieldTypeName(), dto.getRecordNote(),
				userName, date, ipAddress, true,operator);
		fieldTypeDAO.merge(fieldType);
		fieldType = null;
	}

	@Transactional
	public void deleteFieldType(FieldTypeDTO dto) throws Exception {
		fieldTypeDAO.remove(dto.getFieldTypeID());
	}
}
