package bd.ac.uiu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bd.ac.uiu.api.sms.ShortMessageSystem;
import bd.ac.uiu.dao.UsersDAO;
import bd.ac.uiu.dao.UsersRoleDAO;
import bd.ac.uiu.dto.UserDTO;
import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.entity.UserRole;
import bd.ac.uiu.entity.Users;
import bd.ac.uiu.utility.ApplicationUtility;
import bd.ac.uiu.utility.PasswordEncryption;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

@Service
public class UserService {

	public UserService() {

	}

	@Autowired
	private UsersDAO usersDAO;

	@Autowired
	private UsersRoleDAO userRoleDAO;
	

	PasswordEncryption passwordEncryption=new PasswordEncryption();

	public boolean checkUser(UserDTO userDTO) {

		if (usersDAO.checkUser(userDTO.getUsername()) == null) {
			System.out.println("User Not exist");

			return true;
		}

		return false;

	}

	@Transactional
	public boolean saveUser(UserDTO dto) throws Exception {
		Date date = new Date();
		String ipAddress = ApplicationUtility.getIpAddress();
		String userName = ApplicationUtility.getUserID();
		Cer cer = new Cer();
		if (dto.getCerDTO() != null) {
			cer.setCerID(dto.getCerDTO().getCerID());
		}
		Organization organization = new Organization();
		if (dto.getOrganizationDTO() != null) {

			organization.setOrganizationID(dto.getOrganizationDTO().getOrganizationID());
		}
		Operator operator = new Operator();
		if (dto.getOperatorDTO() != null) {

			operator.setOperatorID(dto.getOperatorDTO().getOperatorID());
		}
		Users users = new Users(dto.getName(), dto.getUsername(), passwordEncryption.getHashPassword(dto.getPassword()) , true, "n/a", userName, new Date(),
				ipAddress, 1, dto.getUserType());
		
		users.setMobile(dto.getMobile());

		Users usersCer = new Users(dto.getName(), dto.getUsername(), passwordEncryption.getHashPassword(dto.getPassword()), true, "n/a", userName,
				new Date(), ipAddress, 1, dto.getUserType(), cer);
		
		usersCer.setMobile(dto.getMobile());

		Users usersOrg = new Users(dto.getName(), dto.getUsername(), passwordEncryption.getHashPassword(dto.getPassword()), true, "n/a", userName,
				new Date(), ipAddress, 1, dto.getUserType(), organization);
		
		usersOrg.setMobile(dto.getMobile());

		Users usersOp = new Users(dto.getName(), dto.getUsername(), passwordEncryption.getHashPassword(dto.getPassword()), true, "n/a", userName,
				new Date(), ipAddress, 1, dto.getUserType(), operator);
		
		usersOp.setMobile(dto.getMobile());

		UserRole uRoleCer = new UserRole(usersCer, dto.getRolename(), dto.getUsername(), "n/a", userName, date,
				ipAddress, 1);

		UserRole uRoleOrg = new UserRole(usersOrg, dto.getRolename(), dto.getUsername(), "n/a", userName, date,
				ipAddress, 1);

		UserRole uRoleOp = new UserRole(usersOp, dto.getRolename(), dto.getUsername(), "n/a", userName, date, ipAddress,
				1);

		UserRole uRole = new UserRole(users, dto.getRolename(), dto.getUsername(), "n/a", userName, date, ipAddress, 1);

		switch (dto.getUserType()) {

		case "cer":
			this.usersDAO.persist(usersCer);
			this.userRoleDAO.persist(uRoleCer);
			break;

		case "organization":
			this.usersDAO.persist(usersOrg);
			this.userRoleDAO.persist(uRoleOrg);
			break;

		case "operator":
			this.usersDAO.persist(usersOp);
			this.userRoleDAO.persist(uRoleOp);
			break;

		default:
			this.usersDAO.persist(users);
			this.userRoleDAO.persist(uRole);

		}

		return true;

	}

	public List<UserDTO> findUserList() throws Exception {
		List<UserDTO> list = new ArrayList<>();

		List<Users> entityList = usersDAO.findAll();
		for (Users e : entityList) {

			// list.add(new UserDTO(e.getName(), e.getUsername(),
			// e.getUserRole().getRolename()));
			list.add(new UserDTO(e.getUserID(), e.getName(), e.getUsername(), e.getPassword(), e.isEnabled(),
					e.getRecordNote(), e.getUserExecuted(), e.getDateExecuted(), e.getIpExecuted(), e.getRecordStatus(),
					e.getUserRole().getRolename()));
		}
		entityList = null;
		return list;
	}

	public List<UserDTO> findUsersbyCerOrgOpID(long cerOrgOpID, String userType) throws Exception {
		List<UserDTO> userDTOs = new ArrayList<>();
		List<Users> usersOfCer = usersDAO.findAllUsersByCerIDs(cerOrgOpID, userType);
		List<Users> usersOfOrganization = usersDAO.findAllUsersByOrgIDs(cerOrgOpID, userType);
		List<Users> usersOfOperator = usersDAO.findAllUsersByOpIDs(cerOrgOpID, userType);
		if (userType.equals("cer")) {
			usersOfCer.stream().forEach((cers) -> {
				userDTOs.add(copyUserFrmEntity(cers));
			});
		} else if (userType.equals("organization")) {
			usersOfOrganization.stream().forEach((org) -> {
				userDTOs.add(copyUserFrmEntity(org));
			});
		} else if (userType.equals("operator")) {
			usersOfOperator.stream().forEach((op) -> {
				userDTOs.add(copyUserFrmEntity(op));
			});
		}

		return userDTOs;
	}

	public List<UserDTO> findUsersbyOpID(long operatorID, String userType) throws Exception {
		List<UserDTO> userDTOs = new ArrayList<>();
		List<Users> usersOfOperator = usersDAO.findAllUsersByOpIDs(operatorID, userType);
		System.out.println("usersOfOperator size:" + usersOfOperator.size());
		usersOfOperator.stream().forEach((cers) -> {
			userDTOs.add(copyUserFrmEntity(cers));
		});
		return userDTOs;
	}

	public UserDTO findUsersByUserName(String username) throws Exception {
		Users users = usersDAO.findPasswordByUserName(username);
		UserDTO dto = copyUserFrmEntity(users);
		return dto;
	}

	@Transactional
	public void updateUser(UserDTO dto) throws Exception {

		Users users = new Users(dto.getId(), dto.getName(), dto.getUsername(), dto.getPassword(), dto.isEnabled(),
				dto.getRecordNote(), dto.getUserExecuted(), dto.getDateExecuted(), dto.getIpExecuted(),
				dto.getRecordStatus(), dto.getUserRole());
		users.setMobile(dto.getMobile());
		usersDAO.merge(users);
		users = null;
	}

	@Transactional
	public void deleteCustomer(UserDTO dto) throws Exception {
		usersDAO.remove(dto.getId());
	}

	public UserDTO copyUserFrmEntity(Users users) {
		if (users.getUserID() != 0) {
			UserDTO dto = new UserDTO();
			BeanUtils.copyProperties(users, dto);
			return dto;
		}
		return null;
	}

	public Users copyUserFrmDTO(UserDTO userDTO) {
		if (userDTO.getId() != 0 || userDTO.getUsername() == null) {
			Users users = new Users();
			BeanUtils.copyProperties(userDTO, users);
			return users;
		}
		return null;
	}
	
	public UserDTO findUserByUserNameAndPassword(String userName,String password){
		UserDTO userDTO = new UserDTO();
		
		
		Users users = usersDAO.userByUserNameAndPassword(userName, passwordEncryption.getHashPassword(password));
		
		userDTO = copyUserFrmEntity(users);
		
		return userDTO;
	}
	
	public void changePassword(UserDTO userDTO) throws Exception{
		System.out.println("Start");
		usersDAO.updatePassword(userDTO.getUsername(), passwordEncryption.getHashPassword(userDTO.getPassword()) );
		System.out.println("End");
	}

	// Search
	public UserDTO searchUser(String username) throws Exception {
		UserDTO userDTO = new UserDTO();
		Users user = usersDAO.findPasswordByUserName(username);
		BeanUtils.copyProperties(user, userDTO);
		return userDTO;
	}

}
