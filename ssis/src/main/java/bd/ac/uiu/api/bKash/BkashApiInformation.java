package bd.ac.uiu.api.bKash;

/**
 * Created by junaeid on 7/11/2017.
 */
public class BkashApiInformation {

    private String userName = "RENEWABLEENERGYSERVICES";
    private String password = "R3n3W@b16247";
    private String msisdn = "01828755401";
    private String trxid = "4G92AHRO4K";
    private String reference;
    private String start_datetime = "2017-07-08 12:58:20";
    private String end_datetime = "2017-07-11 12:58:20";


    private String method = "GET";
    private String propertyAccept = "Accept";
    private String propertyType = "Content-Type";
    private String applicationJson = "application/json";


    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public String getReference() {
        return reference;
    }

    public String getStart_datetime() {
        return start_datetime;
    }

    public String getEnd_datetime() {
        return end_datetime;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setStart_datetime(String start_datetime) {
        this.start_datetime = start_datetime;
    }

    public void setEnd_datetime(String end_datetime) {
        this.end_datetime = end_datetime;
    }

    public String trxUrl(String trxId) {
        return "https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/sendmsg?user=" + userName + "&pass=" + password + "&msisdn=" + msisdn + "&trxid=" + trxId + "";
    }

    public String referenceUrl(String reference) {
        return "https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/refmsg?user=" + userName + "&pass=" + password + "&msisdn=" + msisdn + "&reference=" + reference + "";
    }

    private String periodicUrl(String start_datetime, String end_datetime) {
        return "https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/periodicpullmsg?user=" + userName + "&pass=" + password + "&msisdn=" + msisdn + "&start_datetime=" + getStart_datetime() + "&end_datetime=" + getEnd_datetime() + "";
    }


    public String getMethod() {
        return method;
    }

    public String getPropertyAccept() {
        return propertyAccept;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public String getApplicationJson() {
        return applicationJson;
    }
}
