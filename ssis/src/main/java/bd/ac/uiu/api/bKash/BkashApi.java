package bd.ac.uiu.api.bKash;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BkashApi {

	private String dateTime;
	private String reference;
	private BkashTransactionData data;

	public BkashTransactionData bKashService() {
		BkashApiTransactionCheck transactionCheck = new BkashApiTransactionCheck();
		JSONObject jObject = null;
		JSONObject transaction = null;
		try {
			jObject = new JSONObject(transactionCheck.getTransactionInfo(reference));

			JSONArray values = null;
			values = jObject.getJSONArray("transaction");

			data = new BkashTransactionData();

			transaction = values.getJSONObject(values.length() - 2);
			data.setReference(transaction.getString("reference"));
			data.setAmount(transaction.getString("amount"));
			data.setTrxId(transaction.getString("trxId"));
			data.setTrxTimeStamp(transaction.getString("trxTimestamp"));
			data.setCounter(transaction.getString("counter"));
			data.setTrxStatus(transaction.getString("trxStatus"));
			data.setService(transaction.getString("service"));
			data.setSender(transaction.getString("sender"));
			data.setReceiver(transaction.getString("receiver"));
			data.setCurrency(transaction.getString("currency"));
			data.setCounter(transaction.getString("counter"));
			data.setReversed(transaction.getString("reversed"));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return data;

	}

	public BkashTransactionData bKashServiceWithDate() {
		BkashApiTransactionCheck transactionCheck = new BkashApiTransactionCheck();
		JSONObject jObject = null; // json
		JSONObject transaction = null;
		try {
			jObject = new JSONObject(transactionCheck.getTransactionInfo(reference));

			String strTime = null;
			// System.out.println(jObject);
			JSONArray values = null;

			values = jObject.getJSONArray("transaction");

			List<BkashTransactionData> dataList = new ArrayList<BkashTransactionData>();
			List<TimeData> timeDatas = new ArrayList<TimeData>();
			String test = null;
			for (int i = 0; i < values.length() - 1; i++) {

				// System.out.println("length " + values.length());
				transaction = values.getJSONObject(i);
				//System.out.println("trxStatus " + transaction.getString("trxStatus"));

				if (transaction.getString("trxStatus").equals("0000")) {

					try {
						test = transaction.getString("trxTimestamp");
					} catch (JSONException je) {
						je.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

					String[] output = null;
					output = test.split(Pattern.quote("T"));

					// System.out.println("Date " + i + " " + output[0]);

					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					String dateFormat = formatter.format(new Date());
					// System.out.println("com date " + dateFormat);

					if (output[0].equals(dateFormat) == true) {

						data = new BkashTransactionData();

						data.setReference(transaction.getString("reference"));
						data.setAmount(transaction.getString("amount"));
						data.setTrxId(transaction.getString("trxId"));
						// data.setTrxTimeStamp(transaction.getString("trxTimestamp"));
						data.setCounter(transaction.getString("counter"));
						data.setTrxStatus(transaction.getString("trxStatus"));
						data.setService(transaction.getString("service"));
						data.setSender(transaction.getString("sender"));
						data.setReceiver(transaction.getString("receiver"));
						data.setReversed(transaction.getString("reversed"));
						data.setCurrency(transaction.getString("currency"));

						dateTime = (output[1]);

						data.setTrxTimeStamp(output[1]);

						dataList.add(data);

						/*
						 * System.out.println("trxStatus " + transaction.getString("amount"));
						 * System.out.println( "trxTimestamp " + transaction.getString("trxTimestamp"));
						 * System.out.println("service " + transaction.getString("service"));
						 * System.out.println( "sender " + transaction.getString("sender"));
						 * System.out.println("receiver " + transaction.getString("receiver"));
						 * System.out.println( "reference " + transaction.getString("reference"));
						 * System.out.println("trxId " + transaction.getString("trxId"));
						 * System.out.println( "sender " + transaction.getString("sender"));
						 * System.out.println("counter " + transaction.getString("counter"));
						 */
					}
				} else {
					dateTime = null;
					data = null;
				}
			}
			if (dateTime != null) {
				for (BkashTransactionData d : dataList) {
					// System.out.println("Time " + d.getTrxTimeStamp());
					// System.out.println("trxStatus " + d.getAmount());
					// System.out.println("TrxId " + d.getTrxId());

					String[] time = d.getTrxTimeStamp().split(Pattern.quote("+"));
					String[] hour = time[0].split(Pattern.quote(":"));
					String[] second = hour[2].split((Pattern.quote(".")));

					TimeData timeData = new TimeData();

					timeData.setHour(Integer.parseInt(hour[0]));
					timeData.setMinute(Integer.parseInt(hour[1]));
					timeData.setSecond(Integer.parseInt(second[0]));

					timeData.setHourStr(hour[0]);
					timeData.setMinuteStr(hour[1]);
					timeData.setSecondStr(second[0]);

					timeDatas.add(timeData);
					// System.out.println("Hour " + hour[0]);
					// System.out.println("Minute " + hour[1]);
					// System.out.println("Second " + second[0]);
					// System.out.println("Time "+time[0]);
				}
				int maxHour = timeDatas.get(0).getHour();
				int maxMinute = timeDatas.get(0).getMinute();
				int maxSecond = timeDatas.get(0).getSecond();

				String maxHourStr = timeDatas.get(0).getHourStr();
				String maxMinuteStr = timeDatas.get(0).getMinuteStr();
				String maxSecondStr = timeDatas.get(0).getSecondStr();

				for (TimeData d : timeDatas) {
					if (maxHour < d.getHour()) {
						maxHour = d.getHour();
						maxMinute = d.getMinute();
						maxSecond = d.getSecond();

						maxHourStr = d.getHourStr();
						maxMinuteStr = d.getMinuteStr();
						maxSecondStr = d.getSecondStr();
					} else if (maxHour == d.getHour()) {
						if (maxMinute < d.getMinute()) {
							maxHour = d.getHour();
							maxMinute = d.getMinute();
							maxSecond = d.getSecond();

							maxHourStr = d.getHourStr();
							maxMinuteStr = d.getMinuteStr();
							maxSecondStr = d.getSecondStr();
						} else if (maxMinute == d.getMinute()) {
							if (maxSecond < d.getSecond()) {
								maxHour = d.getHour();
								maxMinute = d.getMinute();
								maxSecond = d.getSecond();

								maxHourStr = d.getHourStr();
								maxMinuteStr = d.getMinuteStr();
								maxSecondStr = d.getSecondStr();
							}
						}
					}
				}

				strTime = maxHourStr + ":" + maxMinuteStr + ":" + maxSecondStr;
				System.out.println("Time " + strTime);

				for (BkashTransactionData bd : dataList) {

					String[] str = bd.getTrxTimeStamp().split(Pattern.quote("T"));
					String[] str1 = str[0].split(Pattern.quote("+"));
					String[] str2 = str1[0].split(Pattern.quote("."));

					if (str2[0].equals(strTime)) {
						System.out.println("Amount " + bd.getAmount());
						data.setReference(bd.getReference());
						System.out.println("Reference " + data.getReference());

						data.setAmount(bd.getAmount());
						data.setTrxId(bd.getTrxId());
						data.setTrxTimeStamp(bd.getTrxTimeStamp());
						data.setCounter(bd.getCounter());
						data.setTrxStatus(bd.getTrxStatus());
						data.setService(bd.getService());
						data.setSender(bd.getSender());
						System.out.println("Sender " + data.getSender());
						data.setReceiver(bd.getReceiver());
						System.out.println(" " + data.getReceiver());
						data.setCurrency(bd.getCurrency());
						data.setReversed(bd.getReversed());
					}
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		/*
		 * try { data.setReference(transaction.getString("reference"));
		 * data.setAmount(transaction.getString("amount"));
		 * data.setTrxId(transaction.getString("trxId"));
		 * data.setTrxTimeStamp(transaction.getString("trxTimestamp"));
		 * data.setCounter(transaction.getString("counter"));
		 * data.setTrxStatus(transaction.getString("trxStatus"));
		 * data.setService(transaction.getString("service"));
		 * data.setSender(transaction.getString("sender"));
		 * data.setReceiver(transaction.getString("receiver"));
		 * data.setCurrency(transaction.getString("currency"));
		 * data.setCounter(transaction.getString("counter"));
		 * data.setReversed(transaction.getString("reversed")); } catch (Exception e) {
		 * 
		 * }
		 */
		return data;

	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public BkashTransactionData getBkashTransactionData() {
		return data;
	}

}