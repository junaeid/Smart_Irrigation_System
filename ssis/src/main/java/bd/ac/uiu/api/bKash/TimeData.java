package bd.ac.uiu.api.bKash;

/**
 * Created by junaeid on 7/13/2017.
 */
public class TimeData {

    private int hour;
    private int minute;
    private int second;

    private String hourStr;
    private String minuteStr;
    private String secondStr;




    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getHourStr() {
        return hourStr;
    }

    public void setHourStr(String hourStr) {
        this.hourStr = hourStr;
    }

    public String getMinuteStr() {
        return minuteStr;
    }

    public void setMinuteStr(String minuteStr) {
        this.minuteStr = minuteStr;
    }

    public String getSecondStr() {
        return secondStr;
    }

    public void setSecondStr(String secondStr) {
        this.secondStr = secondStr;
    }
}
