package bd.ac.uiu.api.bKash;

/**
 * Created by junaeid on 7/11/2017.
 */
public class BkashTransactionData {

	private String trxTimeStamp = "N/A";
	private String trxStatus = "N/A";
	private String amount = "N/A";
	private String trxId = "N/A";
	private String counter = "N/A";
	private String reference = "N/A";
	private String reversed = "N/A";
	private String sender = "N/A";
	private String service = "N/A";
	private String currency = "N/A";
	private String receiver = "N/A";

	public String getTrxTimeStamp() {
		return trxTimeStamp;
	}

	public void setTrxTimeStamp(String trxTimeStamp) {
		this.trxTimeStamp = trxTimeStamp;
	}

	public String getTrxStatus() {
		return trxStatus;
	}

	public void setTrxStatus(String trxStatus) {
		this.trxStatus = trxStatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReversed() {
		return reversed;
	}

	public void setReversed(String reversed) {
		this.reversed = reversed;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
}
