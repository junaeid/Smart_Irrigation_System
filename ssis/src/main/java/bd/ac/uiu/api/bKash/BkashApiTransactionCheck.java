package bd.ac.uiu.api.bKash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by junaeid on 7/11/2017.
 */
public class BkashApiTransactionCheck {


    private String bKashApiData(String reference) {
        BkashApiInformation bkashApiInfo = new BkashApiInformation();


        String output = null;
        try {
            URL url = new URL(bkashApiInfo.referenceUrl(reference));

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(bkashApiInfo.getMethod());
            conn.setRequestProperty(bkashApiInfo.getPropertyAccept(), bkashApiInfo.getApplicationJson());
            conn.setRequestProperty(bkashApiInfo.getPropertyType(), bkashApiInfo.getApplicationJson());

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            StringBuilder buffer = new StringBuilder();


            while ((output = br.readLine()) != null) {
                System.out.println(output);
                buffer.append(output);
            }
            output = buffer.toString();
            conn.disconnect();


        } catch (MalformedURLException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }

    public String getTransactionInfo(String reference) {
        return bKashApiData(reference);
    }

}
