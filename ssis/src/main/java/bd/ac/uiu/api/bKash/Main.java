package bd.ac.uiu.api.bKash;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by junaeid on 7/11/2017.
 */
public class Main {
	public static void main(String[] args) {

		getReference(25, 013);

		System.out.println("test " + getReferenceID("225", "13"));

		/*
		 * BkashApi api = new BkashApi(); api.setReference("001002");
		 * //System.out.println(api.bKashService().getCounter());
		 * 
		 * 
		 * try { Date date = new Date(); SimpleDateFormat df = new
		 * SimpleDateFormat("yyyy-DD-mm"); String paymentDateStr =
		 * df.format(date); Date pDate = df.parse(paymentDateStr);
		 * System.out.println("aaa"+pDate); SimpleDateFormat df = new
		 * SimpleDateFormat("dd/M/yyyy"); String date = df.format(new Date());
		 * System.out.println(date);
		 * 
		 * 
		 * 
		 * SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy"); String
		 * dateInString = "31-08-1982"; Date kdate = sdf.parse(dateInString);
		 * System.out.println(kdate);
		 * 
		 * 
		 * 
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		/*
		 * String dateJSON = "2017-07-13T08:47+00:00"; Date date = new Date();
		 */

		/*
		 * System.out.println(date); try { date =
		 * parse("1997-07-16T19:20:30+01:00"); } catch (ParseException e) {
		 * e.printStackTrace(); }
		 */

		/*
		 * String str = toString(date); System.out.println(str);
		 * System.err.println(date); System.out.println(date.getTime());
		 */
		/*
		 * Date fullDate = new Date(); Date dateTime = new Date();
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 * SimpleDateFormat time = new SimpleDateFormat("hh:mm:ss");
		 * 
		 * try { fullDate = sdf.parse("2013-12-24 18:31:20"); } catch
		 * (ParseException e) { e.printStackTrace(); }
		 * 
		 * String strTime =
		 * fullDate.getHours()+":"+fullDate.getMinutes()+":"+fullDate.getSeconds
		 * ();
		 * 
		 * try { dateTime = time.parse(strTime); } catch (ParseException e) {
		 * e.printStackTrace(); }
		 * 
		 * System.out.println(fullDate.getHours());
		 * System.out.println(fullDate.getDate()); System.out.println(fullDate);
		 * 
		 * System.out.println(dateTime);
		 */

	}

	public static String getReferenceID(String operatorID, String customerID) {

		if (operatorID.length() < 3) {
			if (operatorID.length() == 2) {
				operatorID = "0" + operatorID;
			} else {
				operatorID = "00" + operatorID;
			}
		}
		if (customerID.length() < 3) {
			if (customerID.length() == 2) {
				customerID = "0" + customerID;
			} else {
				customerID = "00" + customerID;
			}
		}

		return operatorID + customerID;
	}

	public static String getReference(long operatorID, long customerID) {

		char[] customCustomerID = { '0', '0', '0' };
		char[] customOperatorID = { '0', '0', '0' };

		String finalCustID = new String();
		String finalOperID = new String();

		String reference;

		String str = Integer.toString((int) customerID);
		for (int i = 0; i <= str.length() - 1; i++) {
			System.out.println(str.charAt(i));
			customCustomerID[i] = str.charAt(i);
		}

		String str1 = Integer.toString((int) operatorID);
		for (int i = 0; i <= str1.length() - 1; i++) {
			System.out.println(str1.charAt(i));
			customOperatorID[i] = str1.charAt(i);
		}

		for (int i = str.length(); i >= 0; i--) {
			for (int j = 0; j <= i; j++)
				finalCustID += customCustomerID[j];
		}

		for (int i = 0; i <= customOperatorID.length - 1; i++) {
			finalOperID += customOperatorID[i];
		}

		reference = finalOperID + finalCustID;

		System.out.println(reference);

		return reference;
	}

	public static Date parse(String input) throws java.text.ParseException {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
		if (input.endsWith("Z")) {
			input = input.substring(0, input.length() - 1) + "GMT-00:00";
		} else {
			int inset = 6;

			String s0 = input.substring(0, input.length() - inset);
			String s1 = input.substring(input.length() - inset, input.length());

			input = s0 + "GMT" + s1;
		}

		return df.parse(input);

	}

	public static String toString(Date date) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		TimeZone tz = TimeZone.getTimeZone("UTC");

		df.setTimeZone(tz);

		String output = df.format(date);

		int inset0 = 9;
		int inset1 = 6;

		String s0 = output.substring(0, output.length() - inset0);
		String s1 = output.substring(output.length() - inset1, output.length());

		String result = s0 + s1;

		// result = result.replaceAll("UTC", "+00:00");

		return result;

	}

}
