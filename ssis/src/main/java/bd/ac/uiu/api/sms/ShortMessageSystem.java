package bd.ac.uiu.api.sms;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
public class ShortMessageSystem {
	private String userName = "uiubd";
	private String password = "UIUbd123";
	private ObjectMapper objectMapper = new ObjectMapper();

	public void lowCreditWarning(long operatorID, String customerName, String phoneNumber, int waterCredit) {
		phoneNumber = "+88" + phoneNumber;
		try {
			HttpResponse<String> response = Unirest.post("https://api.infobip.com/sms/1/text/single")
					.basicAuth(userName, password).header("authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==")
					.header("content-type", "application/json").header("accept", "application/json")
					.body("{\"from\":\"InfoSMS\",\"to\":\"" + phoneNumber.trim() + "\",\"text\":\"Dear Sir ," + "\\n"
							+ "your water credit is running low. your remaining water is '" + waterCredit
							+ "KL'.Please recharge... P.O.- " + operatorID + "  \"}")
					.asString();

			System.out.println(response.getBody());
		} catch (UnirestException e) {
			e.printStackTrace();
		}

	}

	public void sendSMS(String phoneNumber, int secretCode) {
		phoneNumber = "+88" + phoneNumber;
		try {
			HttpResponse<String> response = Unirest.post("https://api.infobip.com/sms/1/text/single")
					.basicAuth(userName, password).header("authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==")
					.header("content-type", "application/json")
					.header("accept", "application/json").body("{\"from\":\"InfoSMS\",\"to\":\"" + phoneNumber.trim()
							+ "\",\"text\":\"Dear Sir ," + "\\n" + "your Secret Code is " + secretCode + "  \"}")
					.asString();

			System.out.println(response.getBody());
		} catch (UnirestException e) {
			e.printStackTrace();
		}

	}

	public void customerFieldNumberViaSMS(String phoneNumber, String referenceNumber) {
		phoneNumber = "+88" + phoneNumber;
		try {
			HttpResponse<String> response = Unirest.post("https://api.infobip.com/sms/1/text/single")
					.basicAuth(userName, password).header("authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==")
					.header("content-type", "application/json").header("accept", "application/json")
					.body("{\"from\":\"InfoSMS\",\"to\":\"" + phoneNumber.trim() + "\",\"text\":\"Dear Sir ," + "\\n"
							+ "Your field has successfully created.Your reference number is " + referenceNumber
							+ "  \"}")
					.asString();

			System.out.println(response.getBody());
		} catch (UnirestException e) {
			e.printStackTrace();
		}
	}

	public void successfullyPayment(String phoneNumber, double debitAmount, String reference) {
		phoneNumber = "+88" + phoneNumber;
		try {
			HttpResponse<String> response = Unirest.post("https://api.infobip.com/sms/1/text/single")
					.basicAuth(userName, password).header("authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==")
					.header("content-type", "application/json").header("accept", "application/json")
					.body("{\"from\":\"InfoSMS\",\"to\":\"" + phoneNumber.trim() + "\",\"text\":\"Dear Sir ," + "\\n"
							+ "Your account has successfully recharged by " + debitAmount
							+ " tk. Your reference number is " + reference + "  \"}")
					.asString();

			System.out.println(response.getBody());
		} catch (UnirestException e) {
			e.printStackTrace();
		}
	}

	public void customerRegistrationSuccessfully(String phoneNumber) {
		phoneNumber = "+88" + phoneNumber;
		try {
			HttpResponse<String> response = Unirest.post("https://api.infobip.com/sms/1/text/single")
					.basicAuth(userName, password).header("authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==")
					.header("content-type", "application/json").header("accept", "application/json")
					.body("{\"from\":\"InfoSMS\",\"to\":\"" + phoneNumber.trim() + "\",\"text\":\"Dear Sir ," + "\\n"
							+ "Your registration is successfully completed. Your phone number is " + phoneNumber
							+ "  \"}")
					.asString();

			System.out.println(response.getBody());
		} catch (UnirestException e) {
			e.printStackTrace();
		}
	}
}
