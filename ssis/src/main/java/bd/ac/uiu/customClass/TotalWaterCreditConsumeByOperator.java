package bd.ac.uiu.customClass;

public class TotalWaterCreditConsumeByOperator {

	long operatorID;
	double totalWaterCredit;
	String areaName;

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public double getTotalWaterCredit() {
		return totalWaterCredit;
	}

	public void setTotalWaterCredit(double totalWaterCredit) {
		this.totalWaterCredit = totalWaterCredit;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

}
