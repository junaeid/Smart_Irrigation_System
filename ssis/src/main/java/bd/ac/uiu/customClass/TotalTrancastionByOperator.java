package bd.ac.uiu.customClass;

public class TotalTrancastionByOperator {

	private long operatorID;
	private String operatorName;
	private long noOfOperator;
	private double totalAmountPerOperator;

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public long getNoOfOperator() {
		return noOfOperator;
	}

	public void setNoOfOperator(long noOfOperator) {
		this.noOfOperator = noOfOperator;
	}

	public double getTotalAmountPerOperator() {
		return totalAmountPerOperator;
	}

	public void setTotalAmountPerOperator(double totalAmountPerOperator) {
		this.totalAmountPerOperator = totalAmountPerOperator;
	}

}
