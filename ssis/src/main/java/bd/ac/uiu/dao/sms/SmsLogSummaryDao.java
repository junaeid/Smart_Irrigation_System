package bd.ac.uiu.dao.sms;


import java.util.Date;
import java.util.List;

import bd.ac.uiu.entity.sms.SmsLogSummary;
import bd.ac.uiu.generic.EntityDao;

public interface SmsLogSummaryDao extends EntityDao<SmsLogSummary> {
	List<SmsLogSummary> findAllLogSummryByMonth(
			Object year, Object operatorID) throws Exception;
	public List<SmsLogSummary> findAllSmsLogSumryByDatRng(
			Date fromDat, Date toDat, Object operatorID)throws Exception;
	public List<SmsLogSummary> findAllLogSummryByMonthYearWise(Object year)
			throws Exception;
}
