package bd.ac.uiu.dao.sms;


import java.util.Date;

import bd.ac.uiu.entity.sms.OperatorSmsBalance;
import bd.ac.uiu.generic.EntityDao;


public interface OperatorSmsBalanceDao extends EntityDao<OperatorSmsBalance>{
	public OperatorSmsBalance findOperatorSmsBalanceObject(Object operatorID) throws Exception;

	public int updateOperatorSMSBalance(Object operatorID,int givenBalance,double paybleAmount) throws Exception;

	public int updateSMSBalanceByCollectAmount(Object operatorID,double paidAmount) throws Exception;
	
	public int updateOperatorSMSBalanceBySend(Object operatorID,int givenBalance) throws Exception;

	public long smsBalanceCheck(Object operatorID,Date date) throws Exception;
}
