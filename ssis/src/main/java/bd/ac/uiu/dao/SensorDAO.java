package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.Sensor;
import bd.ac.uiu.generic.EntityDao;

public interface SensorDAO extends EntityDao<Sensor> {
	/*
	 * Find list of sensor by operatorID
	 */
	public List<Sensor> findSensorsByOperator(long operatorID) throws Exception;
}
