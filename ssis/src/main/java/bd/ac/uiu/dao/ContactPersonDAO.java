package bd.ac.uiu.dao;

import bd.ac.uiu.entity.ContactPerson;
import bd.ac.uiu.generic.EntityDao;

public interface ContactPersonDAO extends EntityDao<ContactPerson> {

}
