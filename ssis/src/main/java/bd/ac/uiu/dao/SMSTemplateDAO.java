package bd.ac.uiu.dao;

import bd.ac.uiu.entity.SMSTemplate;
import bd.ac.uiu.generic.EntityDao;

public interface SMSTemplateDAO  extends EntityDao<SMSTemplate>{

}
