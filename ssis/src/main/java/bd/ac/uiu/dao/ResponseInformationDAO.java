package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.ResponseInformation;
import bd.ac.uiu.generic.EntityDao;

public interface ResponseInformationDAO extends EntityDao<ResponseInformation> {

	/*
	 * Find List of Response Inforamtion by OperatorID
	 */
	public List<ResponseInformation> findResponseInformationByOperatorID(long operatorID) throws Exception;

	/*
	 * Find List of Reponse Information by operatorID and customerID
	 */
	public List<ResponseInformation> findResListByOpAndCustID(long operatorID, long customerID) throws Exception;

	/*
	 * Find all list of information
	 */
	public List<ResponseInformation> findAllForCERAndOrg() throws Exception;
}
