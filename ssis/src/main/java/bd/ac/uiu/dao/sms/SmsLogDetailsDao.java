package bd.ac.uiu.dao.sms;

import java.util.Date;
import java.util.List;

import bd.ac.uiu.entity.sms.SmsLogDetails;
import bd.ac.uiu.generic.EntityDao;

public interface SmsLogDetailsDao extends EntityDao<SmsLogDetails> {
	public List<SmsLogDetails> findSmsLogUnsuccessStatusList(Object operatorID);

	public int updateSmsLogDetails(Object smsLogID, Date date, Object monthName, Object year);

	public int updateSmsLogDetailsBySmsFail(Object smsLogID, Date date, Object monthName, Object year);

	public List<SmsLogDetails> findAllSmsLogByDateRang(Date fromDat, Date toDat, Object operatorID) throws Exception;
}
