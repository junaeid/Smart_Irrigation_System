package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.ResponseInformationLog;
import bd.ac.uiu.generic.EntityDao;

public interface ResponseInformationLogDAO extends EntityDao<ResponseInformationLog> {

	/*
	 * Find list of response information log by operatorID
	 */
	public List<ResponseInformationLog> findRequestInformationLogByOperatorID(long operatorID) throws Exception;

	/*
	 * Find Response Information Log by operatorID customerID and field ID
	 */
	public ResponseInformationLog checkOperatorIDPumpIDCustomerIDAndFieldIDExistInResponseInformation(long operatorID,
			long ipcuID, long customerID, long fieldID);

	/*
	 * update response information by fieldValve,sensorStatus,
	 * nodeID,balance,mainValve,fieldID, operatorID,ipcuID,customerID
	 */
	public void updateResponseInformation(int fieldValve, int sensorStatus, int nodeID, double balance, int mainValve,
			long fieldID, long operatorID, long ipcuID, long customerID) throws Exception;

	/*
	 * Find list of response Information Log by operatorID,customerID
	 */
	public List<ResponseInformationLog> findResquestInformationListByOperatorIDAndCustomerID(long operatorID,
			long customerID) throws Exception;

	/*
	 * Find list of Response information Log by operatorID and ipcuID
	 */
	public List<ResponseInformationLog> findResponseInformationListByOperatorIDAndIpcuID(long operatorID, long ipcuID)
			throws Exception;
}
