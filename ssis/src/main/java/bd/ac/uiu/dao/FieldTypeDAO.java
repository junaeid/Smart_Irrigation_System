package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Area;
import bd.ac.uiu.entity.FieldType;
import bd.ac.uiu.generic.EntityDao;

public interface FieldTypeDAO extends EntityDao<FieldType> {

	/*
	 * Find List of fieldType by OperatorID
	 */
	public List<FieldType> findFieldTypeByOperatorID(long operatorID) throws Exception;
}
