package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Area;
import bd.ac.uiu.generic.EntityDao;

public interface AreaDAO extends EntityDao<Area> {

	/*
	 * Check the area name already exists or not.
	 */
	public String checkArea(String areaName);

	/*
	 * Find the list of area's by operatorID
	 */
	public List<Area> findAreaByOperatorID(long operatorID) throws Exception;
}
