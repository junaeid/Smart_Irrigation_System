package bd.ac.uiu.dao;

import bd.ac.uiu.entity.PaymentSummary;
import bd.ac.uiu.generic.EntityDao;

public interface PaymentSummaryDAO extends EntityDao<PaymentSummary> {

	/*
	 * Check the customer exists or not in the payment summary in customerID
	 */
	public PaymentSummary checkCustomerIDExistInPaymentSummary(long customerID);

	/*
	 * update deposit payment summary by customerID and amount
	 */

	public void updateDepositPaymentSummary(long customerID, double amount) throws Exception;

	/*
	 * update expense payment summary by customerID and amount
	 */

	public void updateExpensePaymentSummary(long customerID, double amount) throws Exception;

}
