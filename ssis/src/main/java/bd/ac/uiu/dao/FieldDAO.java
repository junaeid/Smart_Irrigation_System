package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Field;
import bd.ac.uiu.generic.EntityDao;

public interface FieldDAO extends EntityDao<Field> {

	/*
	 * Find List of Field's By OperatorID
	 */
	public List<Field> findFieldsByOperator(long operatorID) throws Exception;
}
