package bd.ac.uiu.dao;

import bd.ac.uiu.entity.TransactionManual;
import bd.ac.uiu.generic.EntityDao;

public interface TransactionManualDAO extends EntityDao<TransactionManual> {

	/*
	 * Find the Latest transaction information from the transaction manual
	 */
	TransactionManual findLatestTransactionManual() throws Exception;

}
