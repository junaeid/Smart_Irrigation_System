package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Field;
import bd.ac.uiu.entity.Ipcu;
import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.generic.EntityDao;

public interface IpcuDAO extends EntityDao<Ipcu> {

	/*
	 * Find list of IPCU"S by operatorID
	 */
	public List<Ipcu> findIpcusByOperator(long operatorID) throws Exception;

	/*
	 * Find IPCU's information by operatorID and ipcuID
	 */
	public Ipcu findIpcuByOperatorAndIpcu(long operatorID, long ipcuID) throws Exception;

	/*
	 * Find IPCU by operatorID
	 */
	public Ipcu findIpcuByOperator(long operatorID) throws Exception;

	/*
	 * Find List of IPCU by areaID and OperatorID
	 */
	public List<Ipcu> findIpcusByAreaIDAndOperatorID(long areaID) throws Exception;
}
