package bd.ac.uiu.dao;

import bd.ac.uiu.entity.Cer;
import bd.ac.uiu.generic.EntityDao;

public interface CerDAO extends EntityDao<Cer> {

	/*
	 * Find user information by their username
	 */
	public Cer findCerByuserName(Object userName) throws Exception;
}
