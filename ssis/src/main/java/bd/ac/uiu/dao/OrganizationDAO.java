package bd.ac.uiu.dao;

import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.generic.EntityDao;

public interface OrganizationDAO extends EntityDao<Organization> {

	/*
	 * Find organization by Information by username
	 */
	public Organization findOrganizationByuserName(Object userName) throws Exception;
}
