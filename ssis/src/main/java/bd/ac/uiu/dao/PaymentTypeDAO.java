package bd.ac.uiu.dao;

import bd.ac.uiu.entity.PaymentType;
import bd.ac.uiu.generic.EntityDao;

public interface PaymentTypeDAO extends EntityDao<PaymentType> {

}
