package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Customer;
import bd.ac.uiu.generic.EntityDao;

public interface CustomerDAO extends EntityDao<Customer> {

	/*
	 * Find all customer's by operatorID
	 */
	public List<Customer> findCustomerByOperatorID(long operatorID) throws Exception;

	/*
	 * Find customer Detail by operator and Customer ID
	 */
	public Customer findCustomerDetailByOperatorIDAndCustomerID(long operatorID, long customerID) throws Exception;

	/*
	 * Search Customer Information's by their name
	 */
	public List<Customer> searchCustomerByName(String customerName) throws Exception;

	public List<Customer> findLatestThreeCustomersByOperatorID(long operatorID) throws Exception;

	/*
	 * Find customerMax ID from the database
	 */
	public long customerMaxID() throws Exception;

	/*
	 * Find List of Customers By Area name
	 */

	public List<Customer> seachCustomersByArea(long areaID) throws Exception;

}
