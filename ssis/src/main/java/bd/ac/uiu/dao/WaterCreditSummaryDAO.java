package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.WaterCreditLog;
import bd.ac.uiu.entity.WaterCreditSummary;
import bd.ac.uiu.generic.EntityDao;

public interface WaterCreditSummaryDAO extends EntityDao<WaterCreditSummary> {

	/*
	 * Find customer is exists or not by customerID
	 */
	public WaterCreditSummary checkCustomerIDExistInWaterCreditSummary(long customerID);

	/*
	 * update available water credit by customerID and nuumber if water credit
	 */
	public void updateAvailableWaterCreditAdd(long customerID, double numberOfWaterCredit) throws Exception;

	/*
	 * update used water credit by customerID and number of water credit
	 */
	public void updateUsedWaterCreditMinus(long customerID, double numberOfWaterCredit) throws Exception;

	/*
	 * Find list of water credit summary by operatorID from waterCredit summary
	 * table
	 */
	public List<WaterCreditSummary> findWaterCreditSummaryByOperatorID(long operatorID) throws Exception;

	/*
	 * Find water credit summary for an customer by customerID and operatorID
	 */
	public WaterCreditSummary findWaterCreditSummaryByOperatorIDAndCustomerID(long operatorID, long customerID)
			throws Exception;

	/*
	 * Find list of water credit summary by operatorID and customerID
	 */
	public List<WaterCreditSummary> findWacreSumByOpAndCustIDList(long operatorID, long customerID) throws Exception;

	/*
	 * Find list of water credit summary and check it is less or not from amount
	 * by operatorID
	 */
	public List<WaterCreditSummary> findWaterCreditSummaryIsLessThenCertainAmountByOperatorID(long operatorID)
			throws Exception;

	/*
	 * update water credit enable status for transaction purpose by operatorID
	 * and customerID
	 */
	public void updateWaterCreditEnableStatus(long operatorID, long customerID, boolean isEnable) throws Exception;

	/*
	 * Find max watercredit summary by operatorID and date wise
	 */
	public double maxWaterCreditSummaryByOpId(long operatorID, String date);

	/*
	 * Find min water credit summary by operatorID and date wise
	 */
	public double minWaterCreditSummaryByOpId(long operatorID, String date);

	/*
	 * Find total water credit summary by operatorID and date wise
	 */
	public double totalWaterCreditSummaryByOpId(long operatorID, String date);

	/*
	 * Find average water credit summary by operatorID and date wise
	 */
	public double avgWaterCreditSummaryByOpId(long operatorID, String date);

	/*
	 * Find max water credit summary by date wise for organization
	 */
	public double maxWaterCreditSummary(String date);

	/*
	 * Find min water credit summray by date wise for organization
	 */
	public double minWaterCreditSummary(String date);

	/*
	 * Find total water credit summary by date wise for organization
	 */
	public double totalWaterCreditSummary(String date);

	/*
	 * Find average water credit summary by date wise for organization
	 */
	public double avgWaterCreditSummary(String date);

	/*
	 * Find total water credit by operatorID for organization
	 */

	public long findTotalWaterCreditByOperatorID(long operatorID) throws Exception;
}
