package bd.ac.uiu.dao;

import bd.ac.uiu.entity.SMSTo;
import bd.ac.uiu.generic.EntityDao;

public interface SMSToDAO extends EntityDao<SMSTo>{

}
