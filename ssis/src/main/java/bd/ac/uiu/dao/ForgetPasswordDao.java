package bd.ac.uiu.dao;

import bd.ac.uiu.entity.ForgetPassword;
import bd.ac.uiu.generic.EntityDao;

public interface ForgetPasswordDao extends EntityDao<ForgetPassword> {

	/*
	 * update password by userName with resetCode
	 */
	public ForgetPasswordDao findForgetPassword(Object userName, Object resectCode);

	public int forgetPasswordAfterReset(Object userName);

}
