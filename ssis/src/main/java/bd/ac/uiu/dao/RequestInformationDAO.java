package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.RequestInformation;
import bd.ac.uiu.entity.Users;
import bd.ac.uiu.generic.EntityDao;

public interface RequestInformationDAO extends EntityDao<RequestInformation> {

	/*
	 * Find List of Request Inforamtion by OperatorID
	 */
	public List<RequestInformation> findRequestInformationByOperatorID(long operatorID) throws Exception;

	/*
	 * Find List of Request Inforamtion by operatorID and CustomerID
	 */
	public List<RequestInformation> findRequestInformationByOperatorIDAndCustomerID(long operatorID, long customerID)
			throws Exception;

	/*
	 * Find all RequestInformation
	 */
	public List<RequestInformation> findAllForCERAndOrg() throws Exception;

}
