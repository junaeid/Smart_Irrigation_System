package bd.ac.uiu.dao;

import java.util.Date;
import java.util.List;

import bd.ac.uiu.entity.TransactionLog;
import bd.ac.uiu.generic.EntityDao;

public interface TransactionLogDAO extends EntityDao<TransactionLog> {

	/*
	 * Find all list of transaction for cer and organization
	 */
	List<TransactionLog> findAllForCerAndOrg() throws Exception;

	/*
	 * Find list of transaction by operatorID
	 */
	List<TransactionLog> findAllTransactionLogByOperatorID(long operatorID) throws Exception;

	/*
	 * Find max transaction by operatorID and date wise for dashboard
	 */

	double findMaxTransactionByOpID(long operatorID, String date) throws Exception;

	/*
	 * Find min transaction by operatorID and date wise for dashboard
	 */
	double findMinTransactionByOpID(long operatorID, String date) throws Exception;

	/*
	 * Find total transaction by operatorID and date wise for dashboard
	 */
	double findTotalTransactionByOpID(long operatorID, String date) throws Exception;

	/*
	 * Find avarage transation by operatorID and date wise for dashboard
	 */

	double findAvgTransactionByOpID(long operatorID, String date) throws Exception;

	/*
	 * Find max transaction for organization by date wise for dashboard
	 */
	double findMaxTransOrg(String date) throws Exception;

	/*
	 * Find min transaction for organization by date wise for dashboard
	 */
	double findMinTransOrg(String date) throws Exception;

	/*
	 * Find total transaction for organization by date wise for dashboard
	 */
	double findTotalTransOrg(String date) throws Exception;

	/*
	 * Find avarage transaction for organization by date wise for dashboard
	 */
	double findAvgTransOrg(String date) throws Exception;

	/*
	 * Find the transaction is duplicateOrNot by operatorID and customerID
	 */
	TransactionLog duplicateOrNot(long operatorID, long customerID) throws Exception;

	/*
	 * Find list of transaction by date wise for organization
	 */
	List<TransactionLog> sumOfTransactionByDate(String date) throws Exception;

	/*
	 * Find list of Transaction between dates by operatorID
	 */
	List<TransactionLog> findTransactionDateBetween(Date startDate, Date endDate, long operatorID) throws Exception;

	/*
	 * Find list of transaction by date wise by operatorID
	 */

	public List<TransactionLog> dateWiseTransactionLogList(long operatorID, Date fromDate, Date toDate)
			throws Exception;

}
