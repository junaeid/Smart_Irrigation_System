package bd.ac.uiu.dao;


import bd.ac.uiu.entity.UserRole;
import bd.ac.uiu.generic.EntityDao;
/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

public interface UsersRoleDAO extends EntityDao<UserRole>{

}
