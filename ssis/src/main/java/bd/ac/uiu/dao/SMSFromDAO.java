package bd.ac.uiu.dao;

import bd.ac.uiu.entity.SMSFrom;
import bd.ac.uiu.generic.EntityDao;

public interface SMSFromDAO extends EntityDao<SMSFrom> {

}
