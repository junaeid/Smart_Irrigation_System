package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Soil;
import bd.ac.uiu.generic.EntityDao;

public interface SoilDAO extends EntityDao<Soil> {

	/*
	 * check soil type exists or not
	 */
	public String checkSoil(String soilType);

	public List<Soil> findSoilByOperator(long operatorID) throws Exception;
}
