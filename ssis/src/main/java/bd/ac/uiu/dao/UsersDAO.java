package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Users;
import bd.ac.uiu.generic.EntityDao;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */
public interface UsersDAO extends EntityDao<Users> {
	public String checkUser(String username);

	public Users userByUserNameAndPassword(String username, String password);

	public Users findPasswordByUserName(String username);

	public List<Users> findAllUsersByCerIDs(long cerID, String userType)throws Exception;

	public List<Users> findAllUsersByOrgIDs(long organizationID,  String userType) throws Exception;

	public List<Users> findAllUsersByOpIDs(long operatorID,  String userType) throws Exception;
	
	public void updatePassword(String username, String password) throws Exception;


}
