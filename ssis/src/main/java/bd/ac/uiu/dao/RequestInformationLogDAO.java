package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.RequestInformationLog;

import bd.ac.uiu.generic.EntityDao;

public interface RequestInformationLogDAO extends EntityDao<RequestInformationLog> {

	/*
	 * Find list of request informationLog by operatorID
	 */
	public List<RequestInformationLog> findRequestInformationLogByOperatorID(long operatorID) throws Exception;

	/*
	 * Find Request Information Log by OperatorID ipcuID fieldID and customerID
	 * exist information
	 */
	public RequestInformationLog checkOperatorIDPumpIDCustomerIDAndFieldIDExistInRequestInformation(long operatorID,
			long ipcuID, long customerID, long fieldID);

	/*
	 * Update request Information by soilMoisture, motoIPCU, waterLevelSensor,
	 * fieldValve, waterCredit, flowRate,temperature, humidity, deviceStatus,
	 * nodeID, season, fieldID, operatorID, ipcuID, customerID, mainValve
	 */
	public void updateRequestInformation(int soilMoisture, int motoIPCU, int waterLevelSensor, int fieldValve,
			int waterCredit, int flowRate, int temperature, int humidity, int deviceStatus, int nodeID, int season,
			long fieldID, long operatorID, long ipcuID, long customerID, int mainValve) throws Exception;

	/*
	 * Find List of Request Information by operatorID and customerID
	 */
	public List<RequestInformationLog> findRequestInformationLogByOperatorIDAndCustomerID(long operatorID,
			long customerID) throws Exception;

}
