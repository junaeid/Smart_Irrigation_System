package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.Operator;
import bd.ac.uiu.entity.Organization;
import bd.ac.uiu.generic.EntityDao;

public interface OperatorDAO extends EntityDao<Operator> {

	/*
	 * Find Operator Information by operator Username
	 */
	public Operator findOperatorByuserName(Object userName) throws Exception;

	/*
	 * Find List of operator by organizationID
	 */
	List<Operator> findAllOperatorsByOragnization(long organizationID) throws Exception;

	/*
	 * Find OperatorMaxID from the database
	 */
	long operatorMaxID() throws Exception;

	/*
	 * Find operator Information by OperatorID
	 */

	public Operator findOperatorByOperatorID(long operatorID) throws Exception;

}
