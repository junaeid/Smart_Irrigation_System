package bd.ac.uiu.dao;

import java.util.List;

import bd.ac.uiu.entity.WaterCreditLog;
import bd.ac.uiu.generic.EntityDao;

public interface WaterCreditLogDAO extends EntityDao<WaterCreditLog> {

	/*
	 * Find list of water credit log information by operator ID
	 */
	public List<WaterCreditLog> findWaterCreditLogByOperatorID(long operatorID) throws Exception;

	/*
	 * Find list of water credit log the last three data using operatorID
	 */
	public List<WaterCreditLog> findWaterCreditIsLessThenHunByOpID(long operatorID) throws Exception;

	/*
	 * Find list of water credit log for cer and organization
	 */

	public List<WaterCreditLog> findAllForCERAndOrg() throws Exception;
}
