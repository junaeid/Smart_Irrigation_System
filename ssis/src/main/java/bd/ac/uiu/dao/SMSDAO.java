package bd.ac.uiu.dao;

import bd.ac.uiu.entity.SMS;
import bd.ac.uiu.generic.EntityDao;

public interface SMSDAO extends EntityDao<SMS> {

}
