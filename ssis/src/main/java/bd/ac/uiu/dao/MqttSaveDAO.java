package bd.ac.uiu.dao;

import bd.ac.uiu.entity.MqttSave;
import bd.ac.uiu.generic.EntityDao;

public interface MqttSaveDAO extends EntityDao<MqttSave> {

}
