package bd.ac.uiu.dao.sms;

import bd.ac.uiu.entity.sms.SmsPhonBkCategory;
import bd.ac.uiu.generic.EntityDao;

public interface SmsPhonBkCategoryDao extends EntityDao<SmsPhonBkCategory> {

}
