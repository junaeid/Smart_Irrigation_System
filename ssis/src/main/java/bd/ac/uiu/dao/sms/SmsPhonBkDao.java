package bd.ac.uiu.dao.sms;

import bd.ac.uiu.entity.sms.SmsPhonBk;
import bd.ac.uiu.generic.EntityDao;

public interface SmsPhonBkDao extends EntityDao<SmsPhonBk>{

}
