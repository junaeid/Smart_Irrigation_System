package bd.ac.uiu.entity.sms;

public class MobileInfo {
	
	
	
	
	public static String operatorName(String mobileNo){
		
		String operatorName="Unknown";
		
		if(mobileNo==null){
			mobileNo="";
		}
		
		int length=mobileNo.length();
		
		// ............................//
		
		if(mobileNo.startsWith("016") && length==11){
			operatorName="airtel";	
		}
		
		else if(mobileNo.startsWith("017") && length==11){
			operatorName="GP";	
		}
		
		else if(mobileNo.startsWith("019") && length==11){
			operatorName="BL";	
		}
		
		else if(mobileNo.startsWith("018") && length==11){
			operatorName="Robi";	
		}
		
		else if(mobileNo.startsWith("015") && length==11){
			operatorName="Teletalk";	
		}
		
		else if(mobileNo.startsWith("011") && length==11){
			operatorName="Citycell";	
		}
		
		return operatorName;
	}
	
public static String operatorName2(String mobileNo){
		
		String operatorName="Unknown";
		
		if(mobileNo==null){
			mobileNo="";
		}

		// ............................//
		
		if(mobileNo.startsWith("016")){
			operatorName="airtel";	
		}
		
		else if(mobileNo.startsWith("017")){
			operatorName="GP";	
		}
		
		else if(mobileNo.startsWith("019")){
			operatorName="BL";	
		}
		
		else if(mobileNo.startsWith("018")){
			operatorName="Robi";	
		}
		
		else if(mobileNo.startsWith("015")){
			operatorName="Teletalk";	
		}
		
		else if(mobileNo.startsWith("011")){
			operatorName="Citycell";	
		}
		
		return operatorName;
	}

}
