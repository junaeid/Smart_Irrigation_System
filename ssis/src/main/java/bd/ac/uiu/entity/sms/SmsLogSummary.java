package bd.ac.uiu.entity.sms;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cer_sms_logsummary")
public class SmsLogSummary {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="logSummaryID")
	private long logSummaryID;
	
	@Column(name="sendDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;
	
	@Column(name="sendMonth")
	private String sendMonth;
	
	@Column(name="sendYear")
	private String sendYear;
	
	@Column(name="smsCategory")
	private String smsCategory;
	
	@Column(name="numberOfSMS")
	private int numberOfSMS;
	
	@Column(name="smsBodySize")
	private int smsBodySize;
	
	@Column(name="operatorID")
	private long operatorID;
	
	@Column(name="recordNote")
	private String recordNote;
	
	@Column(name="userExecuted")
	private String userExecuted;
	
	@Column(name="dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	
	@Column(name="ipExecuted")
	private String ipExecuted;
	
	@Column(name="recordStatus")
	private int recordStatus;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="logSummary", fetch=FetchType.LAZY)
	private List<SmsLogDetails> logDetails;

	public long getLogSummaryID() {
		return logSummaryID;
	}

	public void setLogSummaryID(long logSummaryID) {
		this.logSummaryID = logSummaryID;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getSendMonth() {
		return sendMonth;
	}

	public void setSendMonth(String sendMonth) {
		this.sendMonth = sendMonth;
	}

	public String getSendYear() {
		return sendYear;
	}

	public void setSendYear(String sendYear) {
		this.sendYear = sendYear;
	}

	public String getSmsCategory() {
		return smsCategory;
	}

	public void setSmsCategory(String smsCategory) {
		this.smsCategory = smsCategory;
	}

	public int getNumberOfSMS() {
		return numberOfSMS;
	}

	public void setNumberOfSMS(int numberOfSMS) {
		this.numberOfSMS = numberOfSMS;
	}

	public int getSmsBodySize() {
		return smsBodySize;
	}

	public void setSmsBodySize(int smsBodySize) {
		this.smsBodySize = smsBodySize;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public List<SmsLogDetails> getLogDetails() {
		return logDetails;
	}

	public void setLogDetails(List<SmsLogDetails> logDetails) {
		this.logDetails = logDetails;
	}
	
	
}
