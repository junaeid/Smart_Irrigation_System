package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_responseInformationLog")
public class ResponseInformationLog {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "responseID")
	private long responseID;
	@Column(name = "fieldValve")
	private int fieldValve;
	@Column(name = "sensorStatus")
	private int sensorStatus;
	@Column(name = "nodeID")
	private int nodeID;
	@Column(name = "balance")
	private double balance;
	@Column(name = "mainValve")
	private int mainValve;
	// @Column(name = "paymentSummaryID")
	// private long paymentSummaryID;

	@ManyToOne
	@JoinColumn(name = "fieldID")
	private Field field;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@ManyToOne
	@JoinColumn(name = "ipcuID")
	private Ipcu ipcu;

	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public ResponseInformationLog() {
	}

	public ResponseInformationLog(int fieldValve, int sensorStatus, int nodeID, double balance, int mainValve,
			Field field, Operator operator, Ipcu ipcu, Customer customer, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.fieldValve = fieldValve;
		this.sensorStatus = sensorStatus;
		this.nodeID = nodeID;
		this.balance = balance;
		this.mainValve = mainValve;
		// this.paymentSummaryId = paymentSummaryId;
		this.field = field;
		this.operator = operator;
		this.ipcu = ipcu;
		this.customer = customer;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getResponseID() {
		return responseID;
	}

	public void setResponseID(long responseID) {
		this.responseID = responseID;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(int sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getMainValve() {
		return mainValve;
	}

	public void setMainValve(int mainValve) {
		this.mainValve = mainValve;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Ipcu getIpcu() {
		return ipcu;
	}

	public void setIpcu(Ipcu ipcu) {
		this.ipcu = ipcu;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
