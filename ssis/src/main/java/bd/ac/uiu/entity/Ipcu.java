package bd.ac.uiu.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_ipcu")
public class Ipcu {

	@Id
	@Column(name = "ipcuID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long ipcuID;

	@Column(name = "ipcuCustomID")
	private Integer ipcuCustomID;

	@Column(name = "ipcuWattCapacity")
	private double ipcuWattCapacity;

	@Column(name = "ipcuDepth")
	private double ipcuDepth;

	@Column(name = "waterDynamicHead")
	private String waterDynamicHead;

	@Column(name = "distanceFromCanel")
	private String distanceFromCanel;

	@ManyToOne
	@JoinColumn(name = "areaID")
	private Area area;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@OneToMany(mappedBy = "ipcu", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Sensor> sensor;

	@OneToMany(mappedBy = "ipcu", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Field> field;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public Ipcu() {
	}

	public Ipcu(double ipcuWattCapacity, double ipcuDepth, String waterDynamicHead, String distanceFromCanel, Area area,
			Operator operator, String recordNote, String userExecuted, Date dateExecuted, String ipExecuted,
			boolean enabled) {
		super();
		this.ipcuWattCapacity = ipcuWattCapacity;
		this.ipcuDepth = ipcuDepth;
		this.waterDynamicHead = waterDynamicHead;
		this.distanceFromCanel = distanceFromCanel;
		this.area = area;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public double getIpcuWattCapacity() {
		return ipcuWattCapacity;
	}

	public void setIpcuWattCapacity(double ipcuWattCapacity) {
		this.ipcuWattCapacity = ipcuWattCapacity;
	}

	public double getIpcuDepth() {
		return ipcuDepth;
	}

	public void setIpcuDepth(double ipcuDepth) {
		this.ipcuDepth = ipcuDepth;
	}

	public String getWaterDynamicHead() {
		return waterDynamicHead;
	}

	public void setWaterDynamicHead(String waterDynamicHead) {
		this.waterDynamicHead = waterDynamicHead;
	}

	public String getDistanceFromCanel() {
		return distanceFromCanel;
	}

	public void setDistanceFromCanel(String distanceFromCanel) {
		this.distanceFromCanel = distanceFromCanel;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Set<Sensor> getSensor() {
		return sensor;
	}

	public void setSensor(Set<Sensor> sensor) {
		this.sensor = sensor;
	}

	public List<Field> getField() {
		return field;
	}

	public void setField(List<Field> field) {
		this.field = field;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getIpcuCustomID() {
		return ipcuCustomID;
	}

	public void setIpcuCustomID(Integer ipcuCustomID) {
		this.ipcuCustomID = ipcuCustomID;
	}

	

}
