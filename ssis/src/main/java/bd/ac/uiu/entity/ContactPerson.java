package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_contactPerson")
public class ContactPerson {
	@Id
	@Column(name = "contactPersonID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long contactPersonID;
	@Column(name = "contactPersonName")
	private String contactPersonName;

	private String userType;

	@ManyToOne
	@JoinColumn(name = "cerID")
	private Cer cer;

	@ManyToOne
	@JoinColumn(name = "organizationID")
	private Organization organization;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public ContactPerson() {
	}

	public ContactPerson(String contactPersonName, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled,String userType) {
		super();
		this.contactPersonName = contactPersonName;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.userType = userType;
	}

	// for cer

	public ContactPerson(String contactPersonName, Cer cer, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled,String userType) {
		super();
		this.contactPersonName = contactPersonName;
		this.cer = cer;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.userType = userType;
	}

	// for organization

	public ContactPerson(String contactPersonName, Organization organization, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled,String userType) {
		super();
		this.contactPersonName = contactPersonName;
		this.organization = organization;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.userType = userType;
	}

	// for operator

	public ContactPerson(String contactPersonName, Operator operator, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled,String userType) {
		super();
		this.contactPersonName = contactPersonName;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.userType = userType;
	}

	public long getContactPersonID() {
		return contactPersonID;
	}

	public void setContactPersonID(long contactPersonID) {
		this.contactPersonID = contactPersonID;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public Cer getCer() {
		return cer;
	}

	public void setCer(Cer cer) {
		this.cer = cer;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
