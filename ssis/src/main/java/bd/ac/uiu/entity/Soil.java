package bd.ac.uiu.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_soil")
public class Soil {

	@Id
	@Column(name = "soilID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long soilID;

	@Column(name = "soilType")
	private String soilType;

	@Column(name = "geographicalInformation")
	private String geographicalInformation;

	@Column(name = "fieldDescription")
	private String fieldDescription;

	/*@OneToOne(mappedBy = "soil", cascade = CascadeType.ALL)
	private Field field;*/

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public long getSoilID() {
		return soilID;
	}

	public void setSoilID(long soilID) {
		this.soilID = soilID;
	}

	public String getSoilType() {
		return soilType;
	}

	public void setSoilType(String soilType) {
		this.soilType = soilType;
	}

	public String getGeographicalInformation() {
		return geographicalInformation;
	}

	public void setGeographicalInformation(String geographicalInformation) {
		this.geographicalInformation = geographicalInformation;
	}

	public String getFieldDescription() {
		return fieldDescription;
	}

	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}


	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Soil() {

	}

	public Soil(String soilType, String geographicalInformation, String fieldDescription, Operator operator,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.soilType = soilType;
		this.geographicalInformation = geographicalInformation;
		this.fieldDescription = fieldDescription;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public Soil(long soilID, String soilType, String geographicalInformation, String fieldDescription,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled,
			Operator operator) {
		super();
		this.soilID = soilID;
		this.soilType = soilType;
		this.geographicalInformation = geographicalInformation;
		this.fieldDescription = fieldDescription;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

}
