package bd.ac.uiu.entity.sms;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="cer_sms_logdetails")
public class SmsLogDetails {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="smsLogID")
	private long smsLogID;
	
	@Column(name="sendDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;
	
	@Column(name="sendMonth")
	private String sendMonth;
	
	@Column(name="sendYear")
	private String sendYear;
	
	@Column(name="smsType")
	private String smsType;
	
	@Column(name="fromNumber")
	private String fromNumber;
	
	@Column(name="toNumber")
	private String toNumber;
	
	@Column(name="operatorName")
	private String operatorName;
	
	@Column(name="smsBody")
	private String smsBody;
	
	@Column(name="smsBodySize")
	private int smsBodySize;
	
	@Column(name="smsCount")
	private int smsCount;
	
	@Column(name="successfulStatus")
	private int successfulStatus;
	
	@Column(name="successfulDate")
	@Temporal(TemporalType.DATE)
	private Date successfulDate;
	
	@Column(name="successfulMonth")
	private String successfulMonth;
	
	@Column(name="sucessfulYear")
	private String sucessfulYear;
	
	@Column(name="sendToUser")
	private String sendToUser;
	
	@Column(name="customerID")
	private long customerID;
	
	@Column(name="operatorID")
	private long operatorID;
	
	@Column(name="fileName")
	private String fileName;
	
	@Column(name="phnBkID")
	private long phnBkID;
	
	
	@Column(name="recordNote")
	private String recordNote;
	
	@Column(name="userExecuted")
	private String userExecuted;
	
	@Column(name="dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	
	@Column(name="ipExecuted")
	private String ipExecuted;
	
	@Column(name="recordStatus")
	private int recordStatus;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="logSummaryID")
	private SmsLogSummary logSummary;
	
	
	@Transient
	private String name;
	
	@Transient
	private String smsStatus;
	
	
	

	public SmsLogDetails() {
		super();
	}




	public SmsLogDetails(Date sendDate, String sendMonth, String sendYear, String smsType, String fromNumber,
			String toNumber, String operatorName, String smsBody, int smsBodySize, int smsCount, int successfulStatus,
			Date successfulDate, String successfulMonth, String sucessfulYear, String sendToUser, long customerID,
			long operatorID, String fileName, long phnBkID, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, int recordStatus, SmsLogSummary logSummary, String name, String smsStatus) {
		super();
		this.sendDate = sendDate;
		this.sendMonth = sendMonth;
		this.sendYear = sendYear;
		this.smsType = smsType;
		this.fromNumber = fromNumber;
		this.toNumber = toNumber;
		this.operatorName = operatorName;
		this.smsBody = smsBody;
		this.smsBodySize = smsBodySize;
		this.smsCount = smsCount;
		this.successfulStatus = successfulStatus;
		this.successfulDate = successfulDate;
		this.successfulMonth = successfulMonth;
		this.sucessfulYear = sucessfulYear;
		this.sendToUser = sendToUser;
		this.customerID = customerID;
		this.operatorID = operatorID;
		this.fileName = fileName;
		this.phnBkID = phnBkID;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.logSummary = logSummary;
		this.name = name;
		this.smsStatus = smsStatus;
	}




	public long getSmsLogID() {
		return smsLogID;
	}




	public void setSmsLogID(long smsLogID) {
		this.smsLogID = smsLogID;
	}




	public Date getSendDate() {
		return sendDate;
	}




	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}




	public String getSendMonth() {
		return sendMonth;
	}




	public void setSendMonth(String sendMonth) {
		this.sendMonth = sendMonth;
	}




	public String getSendYear() {
		return sendYear;
	}




	public void setSendYear(String sendYear) {
		this.sendYear = sendYear;
	}




	public String getSmsType() {
		return smsType;
	}




	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}




	public String getFromNumber() {
		return fromNumber;
	}




	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}




	public String getToNumber() {
		return toNumber;
	}




	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}




	public String getOperatorName() {
		return operatorName;
	}




	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}




	public String getSmsBody() {
		return smsBody;
	}




	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}




	public int getSmsBodySize() {
		return smsBodySize;
	}




	public void setSmsBodySize(int smsBodySize) {
		this.smsBodySize = smsBodySize;
	}




	public int getSmsCount() {
		return smsCount;
	}




	public void setSmsCount(int smsCount) {
		this.smsCount = smsCount;
	}




	public int getSuccessfulStatus() {
		return successfulStatus;
	}




	public void setSuccessfulStatus(int successfulStatus) {
		this.successfulStatus = successfulStatus;
	}




	public Date getSuccessfulDate() {
		return successfulDate;
	}




	public void setSuccessfulDate(Date successfulDate) {
		this.successfulDate = successfulDate;
	}




	public String getSuccessfulMonth() {
		return successfulMonth;
	}




	public void setSuccessfulMonth(String successfulMonth) {
		this.successfulMonth = successfulMonth;
	}




	public String getSucessfulYear() {
		return sucessfulYear;
	}




	public void setSucessfulYear(String sucessfulYear) {
		this.sucessfulYear = sucessfulYear;
	}




	public String getSendToUser() {
		return sendToUser;
	}




	public void setSendToUser(String sendToUser) {
		this.sendToUser = sendToUser;
	}




	public long getCustomerID() {
		return customerID;
	}




	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}




	public long getOperatorID() {
		return operatorID;
	}




	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}




	public String getFileName() {
		return fileName;
	}




	public void setFileName(String fileName) {
		this.fileName = fileName;
	}




	public long getPhnBkID() {
		return phnBkID;
	}




	public void setPhnBkID(long phnBkID) {
		this.phnBkID = phnBkID;
	}




	public String getRecordNote() {
		return recordNote;
	}




	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}




	public String getUserExecuted() {
		return userExecuted;
	}




	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}




	public Date getDateExecuted() {
		return dateExecuted;
	}




	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}




	public String getIpExecuted() {
		return ipExecuted;
	}




	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}




	public int getRecordStatus() {
		return recordStatus;
	}




	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}




	public SmsLogSummary getLogSummary() {
		return logSummary;
	}




	public void setLogSummary(SmsLogSummary logSummary) {
		this.logSummary = logSummary;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getSmsStatus() {
		return smsStatus;
	}




	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}


}
