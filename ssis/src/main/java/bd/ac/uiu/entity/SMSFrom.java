package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_smsFrom")
public class SMSFrom {
	@Id
	@Column(name = "smsFromID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long smsFromID;
	@Column(name = "smsFromName")
	private String smsFromName;
	@Column(name = "smsFromMobile")
	private String smsFromMobile;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public SMSFrom() {
	}

	public SMSFrom(long smsFromID, String smsFromName, String smsFromMobile, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.smsFromID = smsFromID;
		this.smsFromName = smsFromName;
		this.smsFromMobile = smsFromMobile;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public SMSFrom(String smsFromName, String smsFromMobile, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.smsFromName = smsFromName;
		this.smsFromMobile = smsFromMobile;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSmsFromID() {
		return smsFromID;
	}

	public void setSmsFromID(long smsFromID) {
		this.smsFromID = smsFromID;
	}

	public String getSmsFromName() {
		return smsFromName;
	}

	public void setSmsFromName(String smsFromName) {
		this.smsFromName = smsFromName;
	}

	public String getSmsFromMobile() {
		return smsFromMobile;
	}

	public void setSmsFromMobile(String smsFromMobile) {
		this.smsFromMobile = smsFromMobile;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
