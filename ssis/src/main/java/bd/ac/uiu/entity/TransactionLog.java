package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import bd.ac.uiu.dto.CustomerDTO;


@Entity
@Table(name = "cer_transaction_log")
public class TransactionLog {

	@Id
	@Column(name = "transationLogID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long transationLogID;

	@Column(name = "paymentTypeName")
	private String paymentTypeName;

	@Column(name = "paymentDate")
	@Temporal(TemporalType.DATE)
	private Date paymentDate;

	@Column(name = "debitAmount")
	private double debitAmount;

	@Column(name = "creditAmount")
	private double creditAmount;

	@Column(name = "year")
	private String year;

	@Column(name = "month")
	private String month;

	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "trxStatus")
	private String trxStatus;

	@Column(name = "trxId")
	private String trxId;

	@Column(name = "counter")
	private String counter;

	@Column(name = "reference")
	private String reference;

	@Column(name = "sender")
	private String sender;

	@Column(name = "service")
	private String service;

	@Column(name = "currency")
	private String currency;

	@Column(name = "receiver")
	private String receiver;

	@Column(name = "trxTimeStamp")
	private String trxTimeStamp;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public TransactionLog() {
	}

	public TransactionLog(String paymentTypeName, Date paymentDate, double debitAmount, double creditAmount,
			String year, String month, Customer customer, Operator operator, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.paymentTypeName = paymentTypeName;
		this.paymentDate = paymentDate;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.year = year;
		this.month = month;
		this.customer = customer;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getTransationLogID() {
		return transationLogID;
	}

	public void setTransationLogID(long transationLogID) {
		this.transationLogID = transationLogID;
	}

	public String getPaymentTypeName() {
		return paymentTypeName;
	}

	public void setPaymentTypeName(String paymentTypeName) {
		this.paymentTypeName = paymentTypeName;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getTrxStatus() {
		return trxStatus;
	}

	public void setTrxStatus(String trxStatus) {
		this.trxStatus = trxStatus;
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getTrxTimeStamp() {
		return trxTimeStamp;
	}

	public void setTrxTimeStamp(String trxTimeStamp) {
		this.trxTimeStamp = trxTimeStamp;
	}

}
