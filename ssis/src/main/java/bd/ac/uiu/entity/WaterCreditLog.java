package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_waterCreditLog")
public class WaterCreditLog {

	@Id
	@Column(name = "waterCreditID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long waterCreditID;

	@Column(name = "usedWaterCredit")
	private int usedWaterCredit;

	@Column(name = "availableWaterCredit")
	private int availableWaterCredit;

	@Column(name = "usedDate")
	private Date usedDate;

	@Column(name = "year")
	private String year;

	@Column(name = "month")
	private String month;

	private long fieldID;

	private long ipcuID;
	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public WaterCreditLog() {
	}

	public long getWaterCreditID() {
		return waterCreditID;
	}

	public void setWaterCreditID(long waterCreditID) {
		this.waterCreditID = waterCreditID;
	}

	public int getUsedWaterCredit() {
		return usedWaterCredit;
	}

	public void setUsedWaterCredit(int usedWaterCredit) {
		this.usedWaterCredit = usedWaterCredit;
	}

	public int getAvailableWaterCredit() {
		return availableWaterCredit;
	}

	public void setAvailableWaterCredit(int availableWaterCredit) {
		this.availableWaterCredit = availableWaterCredit;
	}

	public Date getUsedDate() {
		return usedDate;
	}

	public void setUsedDate(Date usedDate) {
		this.usedDate = usedDate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public long getFieldID() {
		return fieldID;
	}

	public void setFieldID(long fieldID) {
		this.fieldID = fieldID;
	}

	public long getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(long ipcuID) {
		this.ipcuID = ipcuID;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public WaterCreditLog(int usedWaterCredit, int availableWaterCredit, Date usedDate, String year, String month,
			long fieldID, long ipcuID, Customer customer, Operator operator, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.usedWaterCredit = usedWaterCredit;
		this.availableWaterCredit = availableWaterCredit;
		this.usedDate = usedDate;
		this.year = year;
		this.month = month;
		this.fieldID = fieldID;
		this.ipcuID = ipcuID;
		this.customer = customer;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

}
