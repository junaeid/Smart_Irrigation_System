package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cer_sensor", uniqueConstraints = { @UniqueConstraint(columnNames = { "sensorCode", "ipcuID" }) })
public class Sensor {

	@Id
	@Column(name = "sensorID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long sensorID;

	@Column(name = "sensorName")
	private String sensorName;

	@Column(name = "sensorCode")
	private String sensorCode;

	@ManyToOne
	@JoinColumn(name = "ipcuID")
	private Ipcu ipcu;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public Sensor() {
	}

	
	
	public Sensor(long sensorID, String sensorName, String sensorCode, Operator operator, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled, Ipcu ipcu) {
		super();
		this.sensorID = sensorID;
		this.sensorName = sensorName;
		this.sensorCode = sensorCode;
		this.ipcu = ipcu;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}



	

	public Sensor(String sensorName, String sensorCode, Ipcu ipcu, Operator operator, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.sensorName = sensorName;
		this.sensorCode = sensorCode;
		this.ipcu = ipcu;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	

	public long getSensorID() {
		return sensorID;
	}

	public void setSensorID(long sensorID) {
		this.sensorID = sensorID;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public String getSensorCode() {
		return sensorCode;
	}

	public void setSensorCode(String sensorCode) {
		this.sensorCode = sensorCode;
	}

	public Ipcu getIpcu() {
		return ipcu;
	}

	public void setIpcu(Ipcu ipcu) {
		this.ipcu = ipcu;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

}
