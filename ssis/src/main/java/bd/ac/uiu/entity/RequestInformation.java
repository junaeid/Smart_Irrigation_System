package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_requestInformation")
public class RequestInformation {

	@Id
	@Column(name = "requestInformationID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long requestInformationID;

	@Column(name = "operatorID") // S000
	private int operatorID;
	@Column(name = "ipcuID") // P000 pump means ipcu
	private int ipcuID;
	@Column(name = "customerID") // U000
	private int customerID;
	@Column(name = "fieldID") // F000
	private int fieldID;
	@Column(name = "soilMoisture") // M000
	private int soilMoisture;
	@Column(name = "motorIpcu") // m1
	private int motorIpcu;
	@Column(name = "waterLevelSensor") // L000
	private int waterLevelSensor;
	@Column(name = "fieldValve") // v1
	private int fieldValve;
	@Column(name = "mainValve") // V1
	private int mainValve;
	@Column(name = "waterCredit") // l000
	private int waterCredit;
	@Column(name = "flowRate") // f000
	private int flowRate;
	@Column(name = "temperature") // T000
	private int temperature;
	@Column(name = "humidity") // H000
	private int humidity;
	@Column(name = "deviceStatus") // B000 device status means bulb
	private int deviceStatus;
	@Column(name = "nodeID") // N1
	private int nodeID;
	@Column(name = "season") // s1
	private int season;

	// @OneToOne
	// @JoinColumn(name = "fieldID")
	// private Field field;

	// @ManyToOne
	// @JoinColumn(name = "operatorID")
	// private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public RequestInformation() {
	}

	public RequestInformation(int operatorID, int ipcuID, int customerID, int fieldID, int soilMoisture, int motorIpcu,
			int waterLevelSensor, int fieldValve, int mainValve, int waterCredit, int flowRate, int temperature,
			int humidity, int deviceStatus, int nodeID, int season, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.operatorID = operatorID;
		this.ipcuID = ipcuID;
		this.customerID = customerID;
		this.fieldID = fieldID;
		this.soilMoisture = soilMoisture;
		this.motorIpcu = motorIpcu;
		this.waterLevelSensor = waterLevelSensor;
		this.fieldValve = fieldValve;
		this.mainValve = mainValve;
		this.waterCredit = waterCredit;
		this.flowRate = flowRate;
		this.temperature = temperature;
		this.humidity = humidity;
		this.deviceStatus = deviceStatus;
		this.nodeID = nodeID;
		this.season = season;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getRequestInformationID() {
		return requestInformationID;
	}

	public void setRequestInformationID(long requestInformationID) {
		this.requestInformationID = requestInformationID;
	}

	public int getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(int operatorID) {
		this.operatorID = operatorID;
	}

	public int getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(int ipcuID) {
		this.ipcuID = ipcuID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getFieldID() {
		return fieldID;
	}

	public void setFieldID(int fieldID) {
		this.fieldID = fieldID;
	}

	public int getSoilMoisture() {
		return soilMoisture;
	}

	public void setSoilMoisture(int soilMoisture) {
		this.soilMoisture = soilMoisture;
	}

	public int getMotorIpcu() {
		return motorIpcu;
	}

	public void setMotorIpcu(int motoIpcu) {
		this.motorIpcu = motoIpcu;
	}

	public int getWaterLevelSensor() {
		return waterLevelSensor;
	}

	public void setWaterLevelSensor(int waterLevelSensor) {
		this.waterLevelSensor = waterLevelSensor;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getMainValve() {
		return mainValve;
	}

	public void setMainValve(int mainValve) {
		this.mainValve = mainValve;
	}

	public int getWaterCredit() {
		return waterCredit;
	}

	public void setWaterCredit(int waterCredit) {
		this.waterCredit = waterCredit;
	}

	public int getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(int flowRate) {
		this.flowRate = flowRate;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(int deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
