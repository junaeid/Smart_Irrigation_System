package bd.ac.uiu.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

@Entity
@Table(name = "users")
public class Users implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "userID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long userID;

	@Column(name = "name")
	private String name;

	@Column(name = "username", unique = true)
	private String username;

	@Column(name = "password")
	private String password;
	
	@Column(name = "mobile")
	private String mobile;

	@Column(name = "enabled")
	private boolean enabled;

	@Column(name = "recordNote")
	private String recordNote;

	@Column(name = "userExecuted")
	private String userExecuted;

	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	@Column(name = "ipExecuted")
	private String ipExecuted;

	@Column(name = "recordStatus")
	private int recordStatus;

	@OneToOne(mappedBy = "users", cascade = CascadeType.ALL)
	private UserRole userRole;
	
	@Column(name = "userType")
	private String userType;

	@ManyToOne
	@JoinColumn(name = "cerID", nullable = true)
	private Cer cer;

	@ManyToOne
	@JoinColumn(name = "organizationID", nullable = true)
	private Organization organization;

	@ManyToOne
	@JoinColumn(name = "operatorID", nullable = true)
	private Operator operator;

	public Users() {
		super();
	}

	public Users(String name, String username, String password, boolean enabled, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, int recordStatus, String userType) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.userType = userType;
	}

	// for cer
	public Users(String name, String username, String password, boolean enabled, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, int recordStatus, String userType, Cer cer) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.userType = userType;
		this.cer = cer;
	}

	// for organization

	public Users(String name, String username, String password, boolean enabled, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, int recordStatus, String userType, Organization organization) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.userType = userType;
		this.organization = organization;
	}

	// for operator

	public Users(String name, String username, String password, boolean enabled, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, int recordStatus, String userType, Operator operator) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.userType = userType;
		this.operator = operator;
	}

	public Users(long userID, String name, String username, String password, boolean enabled, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, int recordStatus, UserRole userRole) {
		super();
		this.userID = userID;
		this.name = name;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
		this.userRole = userRole;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Cer getCer() {
		return cer;
	}

	public void setCer(Cer cer) {
		this.cer = cer;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	

}
