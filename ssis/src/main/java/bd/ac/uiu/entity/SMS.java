package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_sms")
public class SMS {
	@Id
	@Column(name = "smsID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long smsID;

	@Column(name = "smsTo")
	private String smsTo;

	@Column(name = "smsFrom")
	private String smsFrom;

	@Column(name = "availableWaterCredit")
	private int availableWaterCredit;

	@Column(name = "smsBody")
	private String smsBody;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	/*
	 * @Column(name="smsBody") private String smsBody;
	 * 
	 * @Column(name="smsLength") private double smsLength;
	 */
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	public SMS() {
	}

	public SMS(String smsTo, String smsFrom, int availableWaterCredit, String smsBody, Operator operator,
			Customer customer, Date dateExecuted) {
		super();
		this.smsTo = smsTo;
		this.smsFrom = smsFrom;
		this.availableWaterCredit = availableWaterCredit;
		this.smsBody = smsBody;
		this.operator = operator;
		this.customer = customer;
		this.dateExecuted = dateExecuted;
	}

	public long getSmsID() {
		return smsID;
	}

	public void setSmsID(long smsID) {
		this.smsID = smsID;
	}

	public String getSmsTo() {
		return smsTo;
	}

	public void setSmsTo(String smsTo) {
		this.smsTo = smsTo;
	}

	public String getSmsFrom() {
		return smsFrom;
	}

	public void setSmsFrom(String smsFrom) {
		this.smsFrom = smsFrom;
	}

	public int getAvailableWaterCredit() {
		return availableWaterCredit;
	}

	public void setAvailableWaterCredit(int availableWaterCredit) {
		this.availableWaterCredit = availableWaterCredit;
	}

	public String getSmsBody() {
		return smsBody;
	}

	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

}
