package bd.ac.uiu.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@SuppressWarnings("unused")
@Entity
@Table(name = "cer_field")
public class Field {
	@Id
	@Column(name = "fieldID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long fieldID;

	@Column(name = "fieldMeasurement")
	private double fieldMeasurement;

	@ManyToOne
	@JoinColumn(name = "fieldTypeID")
	private FieldType fieldType;

	@Column(name = "reference")
	private String reference;

	@ManyToOne
	@JoinColumn(name = "ipcuID")
	private Ipcu ipcu;

	// @OneToOne(mappedBy = "field")
	// private RequestInformation requestInformation;

	// @OneToOne(mappedBy = "field")
	// private ResponseInformation responseInformation;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "soilId")
	private Soil soil;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public Field() {
	}

	public Field(double fieldMeasurement, FieldType fieldType, Ipcu ipcu, Operator operator, Customer customer,
			Soil soil, String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.fieldMeasurement = fieldMeasurement;
		this.fieldType = fieldType;
		this.ipcu = ipcu;
		this.operator = operator;
		this.customer = customer;
		this.soil = soil;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public Field(double fieldMeasurement, FieldType fieldType, Ipcu ipcu, Customer customer, Soil soil,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();

		this.fieldMeasurement = fieldMeasurement;
		this.fieldType = fieldType;
		this.ipcu = ipcu;

		this.customer = customer;
		this.soil = soil;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public Field(long fieldID, double fieldMeasurement, FieldType fieldType, Ipcu ipcu, Operator operator,
			Customer customer, Soil soil, String recordNote, String userExecuted, Date dateExecuted, String ipExecuted,
			boolean enabled) {
		super();
		this.fieldID = fieldID;
		this.fieldMeasurement = fieldMeasurement;
		this.fieldType = fieldType;
		this.ipcu = ipcu;
		this.operator = operator;
		this.customer = customer;
		this.soil = soil;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getFieldID() {
		return fieldID;
	}

	public void setFieldID(long fieldID) {
		this.fieldID = fieldID;
	}

	public double getFieldMeasurement() {
		return fieldMeasurement;
	}

	public void setFieldMeasurement(double fieldMeasurement) {
		this.fieldMeasurement = fieldMeasurement;
	}

	public FieldType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public Ipcu getIpcu() {
		return ipcu;
	}

	public void setIpcu(Ipcu ipcu) {
		this.ipcu = ipcu;
	}

	// public RequestInformation getRequestInformation() {
	// return requestInformation;
	// }
	//
	// public void setRequestInformation(RequestInformation requestInformation)
	// {
	// this.requestInformation = requestInformation;
	// }

	// public ResponseInformation getResponseInformation() {
	// return responseInformation;
	// }
	//
	// public void setResponseInformation(ResponseInformation
	// responseInformation) {
	// this.responseInformation = responseInformation;
	// }

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Soil getSoil() {
		return soil;
	}

	public void setSoil(Soil soil) {
		this.soil = soil;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}
