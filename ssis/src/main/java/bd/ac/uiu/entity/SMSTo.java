package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cer_smsTo")
public class SMSTo {
	@Id
	@Column(name="smsToID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long smsToID;
	@Column(name="smsToName")
	private String smsToName;
	@Column(name="smsToMobile")
	private String smsToMobile;
	
	
	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public SMSTo(){}

	public SMSTo(long smsToID, String smsToName, String smsToMobile, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.smsToID = smsToID;
		this.smsToName = smsToName;
		this.smsToMobile = smsToMobile;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public SMSTo(String smsToName, String smsToMobile, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.smsToName = smsToName;
		this.smsToMobile = smsToMobile;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSmsToID() {
		return smsToID;
	}

	public void setSmsToID(long smsToID) {
		this.smsToID = smsToID;
	}

	public String getSmsToName() {
		return smsToName;
	}

	public void setSmsToName(String smsToName) {
		this.smsToName = smsToName;
	}

	public String getSmsToMobile() {
		return smsToMobile;
	}

	public void setSmsToMobile(String smsToMobile) {
		this.smsToMobile = smsToMobile;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
