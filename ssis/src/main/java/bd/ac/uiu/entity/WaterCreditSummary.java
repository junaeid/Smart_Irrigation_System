package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "cer_waterCreditSummary")
public class WaterCreditSummary {

	@Id
	@Column(name = "waterCreditSummaryID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long waterCreditSummaryID;

	@Column(name = "usedWaterCredit")
	private int usedWaterCredit;

	@Column(name = "availableWaterCredit")
	private int availableWaterCredit;

	@OneToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	@Transient
	private int noOfUnit;

	public WaterCreditSummary() {
	}

	public long getWaterCreditSummaryID() {
		return waterCreditSummaryID;
	}

	public void setWaterCreditSummaryID(long waterCreditSummaryID) {
		this.waterCreditSummaryID = waterCreditSummaryID;
	}

	public int getUsedWaterCredit() {
		return usedWaterCredit;
	}

	public void setUsedWaterCredit(int usedWaterCredit) {
		this.usedWaterCredit = usedWaterCredit;
	}

	public int getAvailableWaterCredit() {
		return availableWaterCredit;
	}

	public void setAvailableWaterCredit(int availableWaterCredit) {
		this.availableWaterCredit = availableWaterCredit;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getNoOfUnit() {
		return noOfUnit;
	}

	public void setNoOfUnit(int noOfUnit) {
		this.noOfUnit = noOfUnit;
	}

	public WaterCreditSummary(int usedWaterCredit, int availableWaterCredit, Customer customer, Operator operator,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.usedWaterCredit = usedWaterCredit;
		this.availableWaterCredit = availableWaterCredit;
		this.customer = customer;
		this.operator = operator;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	
}
