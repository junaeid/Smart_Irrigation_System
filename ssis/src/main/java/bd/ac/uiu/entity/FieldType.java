package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_fieldType")
public class FieldType {

	@Id
	@Column(name = "fieldTypeID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long fieldTypeID;
	@Column(name = "fieldTypeName", unique = true)
	private String fieldTypeName;

	/*@OneToOne(mappedBy = "fieldType", cascade = CascadeType.ALL)
	private Field field;*/

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public FieldType() {
	}

	public FieldType(long fieldTypeID, String fieldTypeName, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled, Operator operator) {
		super();
		this.fieldTypeID = fieldTypeID;
		this.fieldTypeName = fieldTypeName;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.operator=operator;
	}

	public FieldType(String fieldTypeName, String recordNote, String userExecuted, Date dateExecuted, String ipExecuted,
			boolean enabled, Operator operator) {
		super();
		this.fieldTypeName = fieldTypeName;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.operator = operator;
	}

	public long getFieldTypeID() {
		return fieldTypeID;
	}

	public void setFieldTypeID(long fieldTypeID) {
		this.fieldTypeID = fieldTypeID;
	}

	public String getFieldTypeName() {
		return fieldTypeName;
	}

	public void setFieldTypeName(String fieldTypeName) {
		this.fieldTypeName = fieldTypeName;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

}
