package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_requestInformationLog")
public class RequestInformationLog {

	@Id
	@Column(name = "requestInformationLogID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long requestInformationLogID;
	@Column(name = "soilMoisture")
	private int soilMoisture;
	@Column(name = "motoIPCU")
	private int motoIPCU;
	@Column(name = "waterLevelSensor")
	private int waterLevelSensor;
	@Column(name = "fieldValve")
	private int fieldValve;
	@Column(name = "waterCredit")
	private int waterCredit;
	@Column(name = "flowRate")
	private int flowRate;
	@Column(name = "temperature")
	private int temperature;
	@Column(name = "humidity")
	private int humidity;
	@Column(name = "deviceStatus")
	private int deviceStatus;
	@Column(name = "nodeID")
	private int nodeID;
	@Column(name = "season")
	private int season;
	@Column(name = "mainValve")
	private int mainValve;

	@ManyToOne
	@JoinColumn(name = "fieldID")
	private Field field;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@ManyToOne
	@JoinColumn(name = "ipcuID")
	private Ipcu ipcu;

	@ManyToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public RequestInformationLog() {
	}

	public RequestInformationLog(int soilMoisture, int motoIPCU, int waterLevelSensor, int fieldValve, int waterCredit,
			int flowRate, int temperature, int humidity, int deviceStatus, int nodeID, int season, Field field,
			Operator operator, Ipcu ipcu, Customer customer, int mainValve, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.soilMoisture = soilMoisture;
		this.motoIPCU = motoIPCU;
		this.waterLevelSensor = waterLevelSensor;
		this.fieldValve = fieldValve;
		this.waterCredit = waterCredit;
		this.flowRate = flowRate;
		this.temperature = temperature;
		this.humidity = humidity;
		this.deviceStatus = deviceStatus;
		this.nodeID = nodeID;
		this.season = season;
		this.field = field;
		this.operator = operator;
		this.ipcu = ipcu;
		this.customer = customer;
		this.mainValve = mainValve;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getRequestInformationLogID() {
		return requestInformationLogID;
	}

	public void setRequestInformationLogID(long requestInformationLogID) {
		this.requestInformationLogID = requestInformationLogID;
	}

	public int getSoilMoisture() {
		return soilMoisture;
	}

	public void setSoilMoisture(int soilMoisture) {
		this.soilMoisture = soilMoisture;
	}

	public int getMotoIPCU() {
		return motoIPCU;
	}

	public void setMotoIPCU(int motoIPCU) {
		this.motoIPCU = motoIPCU;
	}

	public int getWaterLevelSensor() {
		return waterLevelSensor;
	}

	public void setWaterLevelSensor(int waterLevelSensor) {
		this.waterLevelSensor = waterLevelSensor;
	}

	public int getFieldValve() {
		return fieldValve;
	}

	public void setFieldValve(int fieldValve) {
		this.fieldValve = fieldValve;
	}

	public int getWaterCredit() {
		return waterCredit;
	}

	public void setWaterCredit(int waterCredit) {
		this.waterCredit = waterCredit;
	}

	public int getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(int flowRate) {
		this.flowRate = flowRate;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(int deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Ipcu getIpcu() {
		return ipcu;
	}

	public void setIpcu(Ipcu ipcu) {
		this.ipcu = ipcu;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
