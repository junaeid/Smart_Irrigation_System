package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */
@Entity
@Table(name="users_role")
public class UserRole {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userRoleId")
	private long userRoleId;

	@OneToOne
	@JoinColumn(name="userID")
	private Users users;

	@Column(name = "rolename")
	private String rolename;
	
	@Column(name = "username", unique=true)
	private String username;
	
	@Column(name = "recordNote")
	private String recordNote;

	@Column(name = "userExecuted")
	private String userExecuted;

	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	@Column(name = "ipExecuted")
	private String ipExecuted;

	@Column(name = "recordStatus")
	private int recordStatus;

	
	public UserRole() {
		super();
	}




	public UserRole(long userRoleId, Users users, String rolename,String username, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, int recordStatus) {
		super();
		this.userRoleId = userRoleId;
		this.users = users;
		this.rolename = rolename;
		this.username=username;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
	}





	public UserRole(Users users, String rolename, String username, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, int recordStatus) {
		super();
		this.users = users;
		this.rolename = rolename;
		this.username=username;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
	}







	public long getUserRoleId() {
		return userRoleId;
	}


	public void setUserRoleId(long userRoleId) {
		this.userRoleId = userRoleId;
	}


	public Users getUsers() {
		return users;
	}


	public void setUsers(Users users) {
		this.users = users;
	}


	public String getRolename() {
		return rolename;
	}


	public void setRolename(String rolename) {
		this.rolename = rolename;
	}


	public String getRecordNote() {
		return recordNote;
	}


	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}


	public String getUserExecuted() {
		return userExecuted;
	}


	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}


	public Date getDateExecuted() {
		return dateExecuted;
	}


	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}


	public String getIpExecuted() {
		return ipExecuted;
	}


	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}


	public int getRecordStatus() {
		return recordStatus;
	}


	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}




	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}


	

}
