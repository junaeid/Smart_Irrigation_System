package bd.ac.uiu.entity.sms;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="cer_sms_phonbk", 
uniqueConstraints={@UniqueConstraint(columnNames={"contactName", "phnBkCategoryID", "operatorID"})})
public class SmsPhonBk {

	@Id
	@Column(name="phnBkID")
	private long phnBkID;
	
	@Column(name="contactName")
	private String contactName;
	
	@Column(name="contactNumber")
	private String contactNumber;
	
	@Column(name="contactNote")
	private String contactNote;
	
	@Column(name="phnBkCategoryID")
	private String phnBkCategoryID;
	
	@Column(name="operatorID")
	private long operatorID;
	
	@Column(name="recordNote")
	private String recordNote;
	
	@Column(name="userExecuted")
	private String userExecuted;
	
	@Column(name="dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	
	@Column(name="ipExecuted")
	private String ipExecuted;
	
	@Column(name="recordStatus")
	private int recordStatus;

	
	@Transient
	private String phnBkCategoryName;


	public long getPhnBkID() {
		return phnBkID;
	}


	public void setPhnBkID(long phnBkID) {
		this.phnBkID = phnBkID;
	}


	public String getContactName() {
		return contactName;
	}


	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public String getContactNumber() {
		return contactNumber;
	}


	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}


	public String getContactNote() {
		return contactNote;
	}


	public void setContactNote(String contactNote) {
		this.contactNote = contactNote;
	}


	public String getPhnBkCategoryID() {
		return phnBkCategoryID;
	}


	public void setPhnBkCategoryID(String phnBkCategoryID) {
		this.phnBkCategoryID = phnBkCategoryID;
	}


	public long getOperatorID() {
		return operatorID;
	}


	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}


	public String getRecordNote() {
		return recordNote;
	}


	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}


	public String getUserExecuted() {
		return userExecuted;
	}


	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}


	public Date getDateExecuted() {
		return dateExecuted;
	}


	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}


	public String getIpExecuted() {
		return ipExecuted;
	}


	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}


	public int getRecordStatus() {
		return recordStatus;
	}


	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}


	public String getPhnBkCategoryName() {
		return phnBkCategoryName;
	}


	public void setPhnBkCategoryName(String phnBkCategoryName) {
		this.phnBkCategoryName = phnBkCategoryName;
	}
	
	
	
}
