package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cer_smsTemplate")
public class SMSTemplate {
	
	@Id
	@Column(name="smsTemplateID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long smsTemplateID;
	@Column(name="smsTemplateType")
	private String smsTemplateType;
	
	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;
	
	public SMSTemplate(){}

	public SMSTemplate(long smsTemplateID, String smsTemplateType, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.smsTemplateID = smsTemplateID;
		this.smsTemplateType = smsTemplateType;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public SMSTemplate(String smsTemplateType, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.smsTemplateType = smsTemplateType;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getSmsTemplateID() {
		return smsTemplateID;
	}

	public void setSmsTemplateID(long smsTemplateID) {
		this.smsTemplateID = smsTemplateID;
	}

	public String getSmsTemplateType() {
		return smsTemplateType;
	}

	public void setSmsTemplateType(String smsTemplateType) {
		this.smsTemplateType = smsTemplateType;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	

}
