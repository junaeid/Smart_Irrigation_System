package bd.ac.uiu.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cer_organization", uniqueConstraints =@UniqueConstraint(columnNames = { "mobile", "email" }))
public class Organization {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "organizationID")
	private long organizationID;

	@Column(name = "organizationName")
	private String organizationName;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "email")
	private String email;

	@Column(name = "presentAddress")
	private String presentAddress;

	@Column(name = "permanentAddress")
	private String permanentAddress;

	@Column(name = "tinCerNumber")
	private String tinCerNumber;

	@Column(name = "establishDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date establishDate;

	@Column(name = "imageName")
	private String imageName;

	@Column(name = "sizeOfImage")
	private String sizeOfImage;

	@Column(name = "imagePath")
	private String imagePath;

	@Column(name = "imageTitle")
	private String imageTitle;

	@Column(name = "registrationNumber")
	private String registrationNumber;

	@ManyToOne
	@JoinColumn(name ="cerID")
	private Cer cer;

	@Column(name = "recordNote")
	private String recordNote;

	@Column(name = "userExecuted")
	private String userExecuted;

	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	@Column(name = "ipExecuted")
	private String ipExecuted;

	@Column(name = "enabled")
	private boolean enabled;

	public Organization() {

	}

	public Organization(String organizationName, String mobile, String email, String presentAddress,
			String permanentAddress, String tinCerNumber, Date establishDate, String imageName, String sizeOfImage,
			String imagePath, String imageTitle, String registrationNumber, Cer cer, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.organizationName = organizationName;
		this.mobile = mobile;
		this.email = email;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.tinCerNumber = tinCerNumber;
		this.establishDate = establishDate;
		this.imageName = imageName;
		this.sizeOfImage = sizeOfImage;
		this.imagePath = imagePath;
		this.imageTitle = imageTitle;
		this.registrationNumber = registrationNumber;
		this.cer = cer;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}
	
	public Organization(long organizationID,String organizationName, String mobile, String email, String presentAddress,
			String permanentAddress, String tinCerNumber, Date establishDate, String imageName, String sizeOfImage,
			String imagePath, String imageTitle, String registrationNumber, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled, Cer cer) {
		super();
		this.organizationID=organizationID;
		this.organizationName = organizationName;
		this.mobile = mobile;
		this.email = email;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.tinCerNumber = tinCerNumber;
		this.establishDate = establishDate;
		this.imageName = imageName;
		this.sizeOfImage = sizeOfImage;
		this.imagePath = imagePath;
		this.imageTitle = imageTitle;
		this.registrationNumber = registrationNumber;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
		this.cer=cer;
	}

	public long getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(long organizationID) {
		this.organizationID = organizationID;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getTinCerNumber() {
		return tinCerNumber;
	}

	public void setTinCerNumber(String tinCerNumber) {
		this.tinCerNumber = tinCerNumber;
	}

	public Date getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Date establishDate) {
		this.establishDate = establishDate;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getSizeOfImage() {
		return sizeOfImage;
	}

	public void setSizeOfImage(String sizeOfImage) {
		this.sizeOfImage = sizeOfImage;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Cer getCer() {
		return cer;
	}

	public void setCer(Cer cer) {
		this.cer = cer;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
