package bd.ac.uiu.entity.sms;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import bd.ac.uiu.entity.Operator;



@Entity
@Table(name="cer_sms_balance")
public class OperatorSmsBalance {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="smsBalanceID")
	private long smsBalanceID;
	
	@Column(name="smsBalance")
	private long smsBalance;
	
	@Column(name="paybleAmount")
	private double paybleAmount;
	
	@Column(name="paidAmount")
	private double paidAmount;
	
	@Column(name="dueAmount")
	private double dueAmount;
	
	@Column(name="expireDate")
	@Temporal(TemporalType.DATE)
	private Date expireDate;
	
	@Column(name="expireMonth")
	private String expireMonth;
	
	@Column(name="expireYear")
	private String expireYear;
	
	@Column(name="activeStatus")
	private int activeStatus;
	
	@OneToOne
	@JoinColumn(name="operatorID")
	private Operator operator;
	
	@Column(name = "recordNote")
	private String recordNote;

	@Column(name = "userExecuted")
	private String userExecuted;

	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	@Column(name = "ipExecuted")
	private String ipExecuted;

	@Column(name = "recordStatus")
	private int recordStatus;

	public long getSmsBalanceID() {
		return smsBalanceID;
	}

	public void setSmsBalanceID(long smsBalanceID) {
		this.smsBalanceID = smsBalanceID;
	}

	public long getSmsBalance() {
		return smsBalance;
	}

	public void setSmsBalance(long smsBalance) {
		this.smsBalance = smsBalance;
	}

	public double getPaybleAmount() {
		return paybleAmount;
	}

	public void setPaybleAmount(double paybleAmount) {
		this.paybleAmount = paybleAmount;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getExpireMonth() {
		return expireMonth;
	}

	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}

	public String getExpireYear() {
		return expireYear;
	}

	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	
	
}
