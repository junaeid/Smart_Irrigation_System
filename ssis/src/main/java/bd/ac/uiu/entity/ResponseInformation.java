package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "cer_responseInformation")
public class ResponseInformation {

	@Id
	@Column(name = "responseID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long responseID;

	@Column(name = "operatorID")
	private int operatorID;
	@Column(name = "ipcuID")
	private int ipcuID;
	@Column(name = "customerID")
	private int customerID;
	@Column(name = "fieldID")
	private int fieldID;

	@Column(name = "fieldVulve")
	private int fieldVulve;

	@Column(name = "mainVulve")
	private int mainVulve;

	@Column(name = "sensorStatus")
	private int sensorStatus;
	@Column(name = "nodeID")
	private int nodeID;

	@Column(name = "balance")
	private double balance;

	@Column(name = "paymentSummaryID")
	private long paymentSummaryID;

	// @OneToOne
	// @JoinColumn(name = "fieldID")
	// private Field field;

	// @ManyToOne
	// @JoinColumn(name = "operatorID")
	// private Operator operator;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public ResponseInformation() {
	}

	public ResponseInformation(int fieldVulve, int sensorStatus, int nodeID, double balance, long paymentSummaryID, // Field
																													// field,
			String recordNote, String userExecuted, Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.fieldVulve = fieldVulve;
		this.sensorStatus = sensorStatus;
		this.nodeID = nodeID;
		this.balance = balance;
		this.paymentSummaryID = paymentSummaryID;
		// this.field = field;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getResponseID() {
		return responseID;
	}

	public void setResponseID(long responseID) {
		this.responseID = responseID;
	}

	public int getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(int operatorID) {
		this.operatorID = operatorID;
	}

	public int getIpcuID() {
		return ipcuID;
	}

	public void setIpcuID(int ipcuID) {
		this.ipcuID = ipcuID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getFieldID() {
		return fieldID;
	}

	public void setFieldID(int fieldID) {
		this.fieldID = fieldID;
	}

	public int getFieldVulve() {
		return fieldVulve;
	}

	public void setFieldVulve(int fieldVulve) {
		this.fieldVulve = fieldVulve;
	}

	public int getMainVulve() {
		return mainVulve;
	}

	public void setMainVulve(int mainVulve) {
		this.mainVulve = mainVulve;
	}

	public int getSensorStatus() {
		return sensorStatus;
	}

	public void setSensorStatus(int sensorStatus) {
		this.sensorStatus = sensorStatus;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public long getPaymentSummaryID() {
		return paymentSummaryID;
	}

	public void setPaymentSummaryID(long paymentSummaryID) {
		this.paymentSummaryID = paymentSummaryID;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
