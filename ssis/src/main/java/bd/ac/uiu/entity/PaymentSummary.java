package bd.ac.uiu.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "cer_paymentSummary")
public class PaymentSummary {

	@Id
	@Column(name = "paymentSummaryId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long paymentSummaryId;

	@Column(name = "totalDepositeAmount")
	private double totalDepositeAmount;

	@Column(name = "totalExpenseAmount")
	private double totalExpenseAmount;

	@Column(name = "balanceAmount")
	private double balanceAmount;

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@OneToOne
	@JoinColumn(name = "customerID")
	private Customer customer;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;
	
	@Transient
	private double amount;

	public PaymentSummary() {
	}

	public PaymentSummary(double totalDepositeAmount, double totalExpenseAmount, double balanceAmount,
			Operator operator, Customer customer, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.totalDepositeAmount = totalDepositeAmount;
		this.totalExpenseAmount = totalExpenseAmount;
		this.balanceAmount = balanceAmount;
		this.operator = operator;
		this.customer = customer;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getPaymentSummaryId() {
		return paymentSummaryId;
	}

	public void setPaymentSummaryId(long paymentSummaryId) {
		this.paymentSummaryId = paymentSummaryId;
	}

	public double getTotalDepositeAmount() {
		return totalDepositeAmount;
	}

	public void setTotalDepositeAmount(double totalDepositeAmount) {
		this.totalDepositeAmount = totalDepositeAmount;
	}

	public double getTotalExpenseAmount() {
		return totalExpenseAmount;
	}

	public void setTotalExpenseAmount(double totalExpenseAmount) {
		this.totalExpenseAmount = totalExpenseAmount;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
