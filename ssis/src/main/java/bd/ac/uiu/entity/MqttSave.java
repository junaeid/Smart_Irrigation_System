package bd.ac.uiu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mqtt_test_save")
public class MqttSave {
	@Id
	@Column(name = "test_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long testID;

	@Column(name = "tempareture")
	private int tempareture;

	
	
	public MqttSave() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MqttSave(int tempareture) {
		super();
		this.tempareture = tempareture;
	}

	public long getTestID() {
		return testID;
	}

	public void setTestID(long testID) {
		this.testID = testID;
	}

	public int getTempareture() {
		return tempareture;
	}

	public void setTempareture(int tempareture) {
		this.tempareture = tempareture;
	}
	
	
	
}
