/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bd.ac.uiu.entity.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

public class SMS_ServiceImpl implements Serializable,SMS_Service
{

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    
    public int sendBulkSms(StringBuilder contactNo, String smsbody)
    {
        StringBuilder rqString; 
         
        List<NameValuePair> urlParameters; 
          
        CloseableHttpClient client; 
          
        HttpGet request; 
          
        HttpResponse r = null; 
        
        BufferedReader rd;
        
        StringBuilder result;
        
        int rspnCode;
        
        String[] rspn_arry;
          
       
        urlParameters = new ArrayList<NameValuePair>(); 
          
        urlParameters.add(new BasicNameValuePair("REQUESTTYPE","SMSSubmitReq")); 
  
        urlParameters.add(new BasicNameValuePair("USERNAME","01678568240")); 
          
        urlParameters.add(new BasicNameValuePair("PASSWORD","Allahu")); 
  
        urlParameters.add(new BasicNameValuePair("MOBILENO",contactNo.toString())); 
          
        urlParameters.add(new BasicNameValuePair("MESSAGE",smsbody)); 
                
        urlParameters.add(new BasicNameValuePair("TYPE","0"));
        
        urlParameters.add(new BasicNameValuePair("ORIGIN_ADDR","01678568240"));
  
        rqString = new StringBuilder(); 
   
        rqString.append("http://portals.bd.airtel.com/msdpapi".replaceAll("\\s","")); 
  
        rqString.append("?"); 
  
        rqString.append(URLEncodedUtils.format(urlParameters,"UTF-8")); 
        
        client  = HttpClients.createDefault(); 
  
        request = new HttpGet(rqString.toString()); 
          
        //System.out.println(""+rqString.toString());
        
        try 
        
        { 
            
            r = client.execute(request); 
            
            rd = new BufferedReader(new InputStreamReader(r.getEntity().getContent()));

            result = new StringBuilder();

            String line;

            while ((line = rd.readLine()) != null)
            {
                result.append(line);
            }
            
            rspn_arry=result.toString().split("\\s+");

            if (r.getStatusLine().getStatusCode() == 200)
            {
                if (rspn_arry[0].equals("+OK"))
                {
                    rspnCode=200;
                } 
                else 
                {
                    rspnCode=500;//sms not sent
                }
            } 
            else
            {
                rspnCode=450;//internet not available
            }

            return rspnCode; 
        }  
        catch (IOException e) 
        { 
            System.out.println("SMS_ServiceImpl : sendBulkSms : "+e.getMessage()); 
        }  
        finally 
        { 
            try
            { 
                if(r!=null) 
                { 
                    r.getEntity().getContent().close(); 
                } 
                  
                request.abort(); 
  
                client.close(); 
                
                contactNo.setLength(0);
                
                rqString.setLength(0);
            }  
            catch (IOException e) 
            { 
                 System.out.println("SMS_ServiceImpl : sendBulkSms : "+e.getMessage()); 
            } 
        } 
        
        return 0;
    }

    /**
     *
     * @param contactNo
     * @param smsbody
     * @return
     */
    @Override
    public int sendIndividual_Sms(String contactNo, String smsbody)
    {
        StringBuilder rqString; 
         
        List<NameValuePair> urlParameters; 
          
        CloseableHttpClient client; 
          
        HttpGet request; 
          
        HttpResponse r = null;
        
        BufferedReader rd;
        
        StringBuilder result;
        
        int rspnCode;
        
        String[] rspn_arry;
          
       
        urlParameters = new ArrayList<>(); 
          
        urlParameters.add(new BasicNameValuePair("REQUESTTYPE","SMSSubmitReq")); 
  
        urlParameters.add(new BasicNameValuePair("USERNAME","01678568240")); 
          
        urlParameters.add(new BasicNameValuePair("PASSWORD","Allahu")); 
  
        urlParameters.add(new BasicNameValuePair("MOBILENO",contactNo)); 
          
        urlParameters.add(new BasicNameValuePair("MESSAGE",smsbody)); 
                
        urlParameters.add(new BasicNameValuePair("TYPE","0"));
        
        urlParameters.add(new BasicNameValuePair("ORIGIN_ADDR","01678568240"));
  
        rqString = new StringBuilder(); 
   
        rqString.append("http://portals.bd.airtel.com/msdpapi".replaceAll("\\s","")); 
  
        rqString.append("?"); 
  
        rqString.append(URLEncodedUtils.format(urlParameters,"UTF-8")); 
        
        client  = HttpClients.createDefault(); 
  
        request = new HttpGet(rqString.toString()); 
          
        try 
        { 
            r = client.execute(request); 
            
            rd = new BufferedReader(new InputStreamReader(r.getEntity().getContent()));

            result = new StringBuilder();

            String line;

            while ((line = rd.readLine()) != null)
            {
                result.append(line);
            }
            
            rspn_arry=result.toString().split("\\s+");

            if (r.getStatusLine().getStatusCode() == 200)
            {
                if (rspn_arry[0].equals("+OK"))
                {
                    rspnCode=200;
                } 
                else 
                {
                    rspnCode=500;//sms not sent
                }
            } 
            else
            {
                rspnCode=450;//internet not available
            }

            return rspnCode; 

        }  
        catch (IOException e) 
        { 
             System.out.println("SMS_ServiceImpl : sendIndividual_Sms : "+e.getMessage()); 
        }  
        finally 
        { 
            try
            { 
                if(r!=null) 
                { 
                    r.getEntity().getContent().close(); 
                } 
                  
                request.abort(); 
  
                client.close(); 
            }  
            catch (IOException e) 
            { 
               System.out.println("SMS_ServiceImpl : sendIndividual_Sms : "+e.getMessage());
            } 
        } 
        
        return 0;
    }

  
   
   
}
