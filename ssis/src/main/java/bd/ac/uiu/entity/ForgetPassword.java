package bd.ac.uiu.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="forget_password")
public class ForgetPassword {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private long id;
	
	@Column(name="username")
	private String username;
	
	@Column(name="resetCode")
	private String resetCode;
	
	@Column(name="resetDate")
	@Temporal(TemporalType.DATE)
	private Date resetDate;
	
	@Column(name="resetMonth")
	private String resetMonth;
	
	@Column(name="resetYear")
	private String resetYear;
	
	@Column(name="active")
	private boolean active;
	
	@Column(name="cerID")
	private String cerID;
	
	@Column(name="organizationID")
	private String organizationID;
	
	@Column(name="operatorID")
	private String operatorID;
	
	@Column(name = "recordNote")
	private String recordNote;

	@Column(name = "userExecuted")
	private String userExecuted;

	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	@Column(name = "ipExecuted")
	private String ipExecuted;

	@Column(name = "recordStatus")
	private int recordStatus;


	
	public ForgetPassword(String username, String resetCode, Date resetDate, String resetMonth, String resetYear,
			boolean active, String cerID, String organizationID, String operatorID, String recordNote,
			String userExecuted, Date dateExecuted, String ipExecuted, int recordStatus) {
		super();
		this.username = username;
		this.resetCode = resetCode;
		this.resetDate = resetDate;
		this.resetMonth = resetMonth;
		this.resetYear = resetYear;
		this.active = active;
		this.cerID = cerID;
		this.organizationID = organizationID;
		this.operatorID = operatorID;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.recordStatus = recordStatus;
	}



	public ForgetPassword() {
		super();
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getResetCode() {
		return resetCode;
	}



	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}



	public Date getResetDate() {
		return resetDate;
	}



	public void setResetDate(Date resetDate) {
		this.resetDate = resetDate;
	}



	public String getResetMonth() {
		return resetMonth;
	}



	public void setResetMonth(String resetMonth) {
		this.resetMonth = resetMonth;
	}



	public String getResetYear() {
		return resetYear;
	}



	public void setResetYear(String resetYear) {
		this.resetYear = resetYear;
	}



	public boolean isActive() {
		return active;
	}



	public void setActive(boolean active) {
		this.active = active;
	}



	public String getCerID() {
		return cerID;
	}



	public void setCerID(String cerID) {
		this.cerID = cerID;
	}



	public String getOrganizationID() {
		return organizationID;
	}



	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}



	public String getOperatorID() {
		return operatorID;
	}



	public void setOperatorID(String operatorID) {
		this.operatorID = operatorID;
	}



	public String getRecordNote() {
		return recordNote;
	}



	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}



	public String getUserExecuted() {
		return userExecuted;
	}



	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}



	public Date getDateExecuted() {
		return dateExecuted;
	}



	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}



	public String getIpExecuted() {
		return ipExecuted;
	}



	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}



	public int getRecordStatus() {
		return recordStatus;
	}



	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	
	
	
	
}
