package bd.ac.uiu.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cer_customer", uniqueConstraints = { @UniqueConstraint(columnNames = { "nationalID"}) })
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customerID")
	private long customerID;
	@Column(name = "customerName")
	private String customerName;
	@Column(name = "nationalID")
	private String nationalID;
	@Column(name = "dateOfBirth")
	private Date dateOfBirth;
	@Column(name = "mobile")
	private String mobile;
	@Column(name = "vulveNumber")
	private String vulveNumber;
	@Column(name = "birthCertificate")
	private String birthCertificate;
	@Column(name = "religion")
	private String religion;
	@Column(name = "gender")
	private String gender;
	@Column(name = "maritalStatus")
	private String maritalStatus;
	@Column(name = "nationality")
	private String nationality;
	@Column(name = "bloodGroup")
	private String bloodGroup;

	

	@ManyToOne
	@JoinColumn(name = "operatorID")
	private Operator operator;

	@ManyToOne
	@JoinColumn(name = "areaID")
	private Area area;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private List<Field> field;

	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL)
	private PaymentSummary paymentSummary;

	@OneToOne(mappedBy = "customer", cascade = CascadeType.ALL)
	private WaterCreditSummary waterCreditSummary;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<TransactionLog> transaction;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<PaymentType> paymentType;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public Customer() {
	}

	public Customer(String customerName, String nationalID, Date dateOfBirth, String mobile, String vulveNumber,
			String birthCertificate, String religion, String gender, String maritalStatus, String nationality,
			String bloodGroup, Operator operator, Area area, String recordNote, String userExecuted, Date dateExecuted,
			String ipExecuted, boolean enabled) {
		super();
		this.customerName = customerName;
		this.nationalID = nationalID;
		this.dateOfBirth = dateOfBirth;
		this.mobile = mobile;
		this.vulveNumber = vulveNumber;
		this.birthCertificate = birthCertificate;
		this.religion = religion;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.nationality = nationality;
		this.bloodGroup = bloodGroup;
		this.operator = operator;
		this.area = area;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public Customer(long customerID, String customerName, String nationalID, Date dateOfBirth, String mobile,
			String vulveNumber, String birthCertificate, String religion, String gender, String maritalStatus,
			String nationality, String bloodGroup, Operator operator, Area area, String recordNote, String userExecuted,
			Date dateExecuted, String ipExecuted, boolean enabled) {
		super();
		this.customerID = customerID;
		this.customerName = customerName;
		this.nationalID = nationalID;
		this.dateOfBirth = dateOfBirth;
		this.mobile = mobile;
		this.vulveNumber = vulveNumber;
		this.birthCertificate = birthCertificate;
		this.religion = religion;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.nationality = nationality;
		this.bloodGroup = bloodGroup;
		this.operator = operator;
		this.area = area;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getNationalID() {
		return nationalID;
	}

	public void setNationalID(String nationalID) {
		this.nationalID = nationalID;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getVulveNumber() {
		return vulveNumber;
	}

	public void setVulveNumber(String vulveNumber) {
		this.vulveNumber = vulveNumber;
	}

	public String getBirthCertificate() {
		return birthCertificate;
	}

	public void setBirthCertificate(String birthCertificate) {
		this.birthCertificate = birthCertificate;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public List<Field> getField() {
		return field;
	}

	public void setField(List<Field> field) {
		this.field = field;
	}

	public PaymentSummary getPaymentSummary() {
		return paymentSummary;
	}

	public void setPaymentSummary(PaymentSummary paymentSummary) {
		this.paymentSummary = paymentSummary;
	}

	public WaterCreditSummary getWaterCreditSummary() {
		return waterCreditSummary;
	}

	public void setWaterCreditSummary(WaterCreditSummary waterCreditSummary) {
		this.waterCreditSummary = waterCreditSummary;
	}

	public Set<TransactionLog> getTransaction() {
		return transaction;
	}

	public void setTransaction(Set<TransactionLog> transaction) {
		this.transaction = transaction;
	}

	public Set<PaymentType> getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Set<PaymentType> paymentType) {
		this.paymentType = paymentType;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
