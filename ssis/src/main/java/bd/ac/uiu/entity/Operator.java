package bd.ac.uiu.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cer_operator", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "mobile", "email", "tinCerNumber" }) })
public class Operator {

	@Id
	@Column(name = "operatorID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long operatorID;

	@Column(name = "operatorName")
	private String operatorName;

	@Column(name = "mobile", unique = true)
	private String mobile;

	@Column(name = "email", unique = true)
	private String email;

	@Column(name = "presentAddress")
	private String presentAddress;

	@Column(name = "permanentAddress")
	private String permanentAddress;

	@Column(name = "voterID")
	private String voterID;

	@Column(name = "passport")
	private String passport;

	@Column(name = "birthCerNumber")
	private String birthCerNumber;

	@Column(name = "tinCerNumber", unique = true)
	private String tinCerNumber;

	@Column(name = "establishDate")
	private Date establishDate;

	@Column(name = "imageName")
	private String imageName;

	@Column(name = "sizeOfImage")
	private String sizeOfImage;

	@Column(name = "imagePath")
	private String imagePath;

	@Column(name = "imageTitle")
	private String imageTitle;

	@Column(name = "registrationNumber")
	private String registrationNumber;

	@ManyToOne
	@JoinColumn(name = "organizationID")
	private Organization organization;

	@OneToMany(mappedBy = "operator", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Ipcu> ipcu;

	@OneToMany(mappedBy = "operator", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<ContactPerson> contactPerson;

	@OneToMany(mappedBy = "operator", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Customer> customer;

	@Column(name = "recordNote")
	private String recordNote;
	@Column(name = "userExecuted")
	private String userExecuted;
	@Column(name = "dateExecuted")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;
	@Column(name = "ipExecuted")
	private String ipExecuted;
	@Column(name = "enabled")
	private boolean enabled;

	public Operator() {
	}

	public Operator(String operatorName, String mobile, String email, String presentAddress, String permanentAddress,
			String voterID, String passport, String birthCerNumber, String tinCerNumber, Date establishDate,
			String imageName, String sizeOfImage, String imagePath, String imageTitle, String registrationNumber,
			Organization organization, String recordNote, String userExecuted, Date dateExecuted, String ipExecuted,
			boolean enabled) {
		super();
		this.operatorName = operatorName;
		this.mobile = mobile;
		this.email = email;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.voterID = voterID;
		this.passport = passport;
		this.birthCerNumber = birthCerNumber;
		this.tinCerNumber = tinCerNumber;
		this.establishDate = establishDate;
		this.imageName = imageName;
		this.sizeOfImage = sizeOfImage;
		this.imagePath = imagePath;
		this.imageTitle = imageTitle;
		this.registrationNumber = registrationNumber;
		this.organization = organization;
		this.recordNote = recordNote;
		this.userExecuted = userExecuted;
		this.dateExecuted = dateExecuted;
		this.ipExecuted = ipExecuted;
		this.enabled = enabled;
	}

	public long getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(long operatorID) {
		this.operatorID = operatorID;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getVoterID() {
		return voterID;
	}

	public void setVoterID(String voterID) {
		this.voterID = voterID;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getBirthCerNumber() {
		return birthCerNumber;
	}

	public void setBirthCerNumber(String birthCerNumber) {
		this.birthCerNumber = birthCerNumber;
	}

	public String getTinCerNumber() {
		return tinCerNumber;
	}

	public void setTinCerNumber(String tinCerNumber) {
		this.tinCerNumber = tinCerNumber;
	}

	public Date getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Date establishDate) {
		this.establishDate = establishDate;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getSizeOfImage() {
		return sizeOfImage;
	}

	public void setSizeOfImage(String sizeOfImage) {
		this.sizeOfImage = sizeOfImage;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Set<Ipcu> getIpcu() {
		return ipcu;
	}

	public void setIpcu(Set<Ipcu> ipcu) {
		this.ipcu = ipcu;
	}

	public Set<ContactPerson> getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(Set<ContactPerson> contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Set<Customer> getCustomer() {
		return customer;
	}

	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}

	public String getRecordNote() {
		return recordNote;
	}

	public void setRecordNote(String recordNote) {
		this.recordNote = recordNote;
	}

	public String getUserExecuted() {
		return userExecuted;
	}

	public void setUserExecuted(String userExecuted) {
		this.userExecuted = userExecuted;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getIpExecuted() {
		return ipExecuted;
	}

	public void setIpExecuted(String ipExecuted) {
		this.ipExecuted = ipExecuted;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
