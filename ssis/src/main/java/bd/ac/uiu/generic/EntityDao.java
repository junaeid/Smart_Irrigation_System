package bd.ac.uiu.generic;

import java.util.List;

/**
 * 
 * @author Mohammad Rajaul Islam
 *
 */

public interface EntityDao<E> {

	void persist(E e) throws Exception;

	void merge(E e) throws Exception;

	void remove(Object id) throws Exception;

	E findById(Object id) throws Exception;

	List<E> findAll() throws Exception;

	public E findSingleObjectByProperty(String prop, Object val) throws Exception;

	List<E> findByProperty(String prop, Object val) throws Exception;

	List<E> findInRange(int firstResult, int maxResults) throws Exception;

	long count() throws Exception;

	String findMaxID(String colmnWithRange, String prop, String id) throws Exception;

	E findByThreeProperty(String prop1, Object id1, String prop2, Object id2, String prop3, Object id3)
			throws Exception;

	public void dynamicListPersists(List<E> entities) throws Exception;

	public void dynamicListMerge(List<E> listForMerge) throws Exception;

	public E findByCompositePK(String prop1, Object id1, String prop2, Object id2) throws Exception;
}
