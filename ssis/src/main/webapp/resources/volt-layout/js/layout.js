$.fn.clickOff=function(e,n){var t=!1,i=this
i.click(function(){t=!0}),$(document).click(function(n){t||e(i,n),t=!1})}
var Volt={init:function(){this.menuWrapper=$("#layout-menu-cover"),this.menu=this.menuWrapper.find("ul.layout-menu"),this.menulinks=this.menu.find("a.menulink"),this.menuButton=$("#menu-button"),this.expandedMenuitems=this.expandedMenuitems||[],this.bindEvents()},bindEvents:function(){var e=this
this.menuButton.on("click",function(){e.menuButtonClick=!0,e.menuWrapper.hasClass("active-menu")?(e.menuButton.removeClass("active-menu"),e.menuWrapper.removeClass("active-menu")):(e.menuButton.addClass("active-menu"),e.menuWrapper.addClass("active-menu"))}),this.menulinks.on("click",function(n){var t=$(this),i=t.parent()
if(i.hasClass("active-menu-parent"))i.removeClass("active-menu-parent"),t.removeClass("active-menu").next("ul").removeClass("active-menu"),e.removeMenuitem(i.attr("id"))
else{var a=i.siblings(".active-menu-parent")
a.length&&(a.removeClass("active-menu-parent"),e.removeMenuitem(a.attr("id")),a.find("ul.active-menu,a.active-menu").removeClass("active-menu"),a.find("li.active-menu-parent").each(function(){var n=$(this)
n.removeClass("active-menu-parent"),e.removeMenuitem(n.attr("id"))})),i.addClass("active-menu-parent"),t.addClass("active-menu").next("ul").addClass("active-menu"),e.addMenuitem(i.attr("id"))}t.next().is("ul")?n.preventDefault():(e.menuButton.removeClass("active-menu"),e.menuWrapper.removeClass("active-menu")),e.saveMenuState()}).clickOff(function(n){e.menuButtonClick?e.menuButtonClick=!1:(e.menuButton.removeClass("active-menu"),e.menuWrapper.removeClass("active-menu"))})},removeMenuitem:function(e){this.expandedMenuitems=$.grep(this.expandedMenuitems,function(n){return n!==e})},addMenuitem:function(e){-1===$.inArray(e,this.expandedMenuitems)&&this.expandedMenuitems.push(e)},saveMenuState:function(){$.cookie("volt_expandeditems",this.expandedMenuitems.join(","),{path:"/"})},clearMenuState:function(){$.removeCookie("volt_expandeditems",{path:"/"})},restoreMenuState:function(){var e=$.cookie("volt_expandeditems")
if(e){this.expandedMenuitems=e.split(",")
for(var n=0;n<this.expandedMenuitems.length;n++){var t=this.expandedMenuitems[n]
if(t){var i=$("#"+this.expandedMenuitems[n].replace(/:/g,"\\:"))
i.addClass("active-menu-parent"),i.children("a,ul").addClass("active-menu")}}}},isMobile:function(){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(window.navigator.userAgent)}}
$(function(){Volt.init()})
