

INSERT INTO `sis`.`cer_cer`(
	`cerName`,`enabled`,`mobile`,`email`,`permanentAddress`,`presentAddress`,`recordNote`,`registrationNumber`,`tinCerNumber`,`userExecuted`
)
VALUES(
	'CER',true,'01686239146','cer@gmail.com','dhanmondi','Dhaka','N/A','0001','258','admin'
) ON DUPLICATE KEY UPDATE    
cerName="CER";


INSERT INTO `sis`.`users`(
	`enabled`,`name`,`password`,`recordStatus`,`recordNote`,`userExecuted`,`userType`,`username`,`cerID`
)
VALUES(
	true,'centre for energy research','e10adc3949ba59abbe56e057f20f883e',1,'N/A','admin','cer','cer',1
)ON DUPLICATE KEY UPDATE    
name="centre for energy research";

INSERT INTO `sis`.`users_role`(
	`recordNote`,`recordStatus`,`rolename`,`userExecuted`,`username`,`userID`
)
VALUES(
	'N/A',1,'ROLE_CER_ADMIN','admin','cer',1)ON DUPLICATE KEY UPDATE    
username='cer';


